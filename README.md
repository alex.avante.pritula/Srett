# Orbitracker React FrontEnd
 
### Prerequisites

- [NodeJS](http://nodejs.org) version 6.3.1 or newer.
- [NVM](https://github.com/creationix/nvm) can be used for a better experience managing NodeJS releases.

### Get Started

Start playing with it using:
- `npm install` to install the dependencies.
- `npm run clean` to clean dist directory. 
- `npm run build` to build dist with staging env.
- `npm run build:production` to build dist with production env.
- `npm run start` to start the webpack development server.
- `npm run test` to run all test.
Put __USER__ and __PASSWORD__ to 'globals' in jest.config.json to before running tests.