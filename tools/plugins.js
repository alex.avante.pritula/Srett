'use strict';
/* GENERAL */
const path = require('path');
const webpack = require('webpack');

/* WEBPACK PLUGINS */
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');

/* CONSTANTS */
const {
    ENV, 
    PATH, 
    NODE_ENV,
} = require('./basic.config');

const {PROD, DEV} = NODE_ENV;
const {src, root, build} = PATH;

const basicPlugins = [
    new HtmlWebpackPlugin({
        minify: {
            collapseWhitespace: true,
        },
        hash: true,
        template: path.join(src, 'index.html'),
        inject: 'body',
    }),
    new webpack.LoaderOptionsPlugin({
        minimize: true,
        debug: false,
    }),
    new ExtractTextPlugin({
        filename: 'app.css',
        disable: false,
        allChunks: true,
    }),
    new CopyWebpackPlugin([{ 
            from: path.join(root, 'assets'), 
            to: path.join(build, 'assets'), 
            ignore: [path.join(build, 'assets/styles/*')],
        }, {
            from: path.join(root, 'locales'), 
            to: path.join(build, 'locales'), 
    }]),
    new webpack.NormalModuleReplacementPlugin(
        /\/iconv-loader$/, 
        'node-noop',
    ),
    new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(ENV || DEV)
    }),
];

if(ENV === PROD){
    basicPlugins.push(new UglifyJSPlugin());
}

module.exports = basicPlugins;
