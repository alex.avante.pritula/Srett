'use strict';
/* GENERAL */
const path = require('path');

/* WEBPACK PLUGINS */
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = [
    {
        test: /\.(scss|css|sass)$/, 
        use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: ['css-loader','sass-loader'],
            publicPath: '/dist'
        }),
    },{
        test: /\.json($|\?)/,
        loader: 'json-loader',
    },{
        test: /.jsx?$/,
        use: ['babel-loader', 'eslint-loader'],
        exclude: /node_modules/
    },{
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
        loader: 'url-loader',
    },
];
