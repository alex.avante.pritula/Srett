'use strict';
/* GENERAL */
const path = require('path');


/* CONSTANTS */
const NODE_ENV = {
    TEST: 'test',
    DEV: 'development',
    PROD: 'production',
};

const root = path.join(__dirname, '..');

module.exports = {
    NODE_ENV,
    ENV: (process.env.NODE_ENV || NODE_ENV.DEV),
    PATH: {
        root,
        nodeModules: path.join(root, '/node_modules'),
        src: path.join(root, '/src'),
        build: path.join(root, '/build'),
    },
};