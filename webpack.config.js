/* GENERAL */
const path = require('path');
const rules = require('./tools/loaders');
const plugins = require('./tools/plugins');

/* CONSTANTS */
const {
    ENV, 
    PATH,
    NODE_ENV,
} = require('./tools/basic.config');

const {PROD} = NODE_ENV;
const {src, build} = PATH;

module.exports = {
    entry: path.join(src, 'app.jsx'),
    output: {
        path: build,
        filename: '[name].[hash].js',
        publicPath: '/',
        sourceMapFilename: '[name].[hash].js.map',
        chunkFilename: '[id].chunk.js',
    },  
    resolve: { 
        extensions: ['.js', '.jsx'],
    },
    plugins,
    devtool: (ENV === PROD ? 'source-map' : 'inline-source-map'),
    module: {rules},
    devServer: {
        open: true,
        compress: true,
        contentBase: build,
        stats: 'errors-only',
        historyApiFallback: true,
    },
};