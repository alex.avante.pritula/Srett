/* GENERAL */
import React from 'react';
import {Route, Redirect, IndexRedirect} from 'react-router';

/* GENERAL COMPONENTS */
import AppComponent from './components/App/App';
import SrettComponent from './components/Srett/Srett';
import NotFoundPageComponent from './components/General/NotFound/NotFound';

/* SPECIFIC PAGES */
import LogInComponent from './components/LogIn/LogIn';
import SettingsComponent from './components/Settings/Settings';
import EquipmentComponent from './components/Equipment/Equipment';
import GlobalMapComponent from './components/GlobalMap/GlobalMap';
import OrbitrackerComponent from './components/Orbitracker/Orbitracker';

/* CONSTANTS */
import {
    PAGES,
    AUTH_TOKEN,
} from './constants/General';

const {
    SUB,
    MAP,
    LOGIN,
    START, 
    ANOTHER,
    SETTINGS,
    EQUIPMENT,
    ORBITRACKER,
} = PAGES;

/* AUTH MIDDLEWARE */
const onChange = (prevProps, nextProps, replace) => { 
    let path = nextProps.location.pathname;
    if(path[0] === START) path = path.substring(1);
    const isLoginPage = (path === LOGIN);
    const isAuthenticated = (!!localStorage.getItem(AUTH_TOKEN));
    
    if(isLoginPage && isAuthenticated){ replace(START); } 
    if(!isLoginPage && !isAuthenticated){ replace(LOGIN); }
};

export default (
    <Route path={START} component={AppComponent} onChange={onChange} >
        <IndexRedirect to={`${SUB}/${EQUIPMENT}`} />
        <Route path={LOGIN} component={LogInComponent} />

        <Route path={SUB} component={SrettComponent} >
            <IndexRedirect to={EQUIPMENT} />
            <Route path={MAP} component={GlobalMapComponent} name="MAP"/>
            <Route path={SETTINGS} component={SettingsComponent} name="SETTINGS" />
            <Route path={EQUIPMENT} component={EquipmentComponent} name="EQUIPMENT"/>
            <Route path={ORBITRACKER} component={OrbitrackerComponent} name="ORBITRACKER"/>
        </Route>

        <Redirect from={MAP} to={`${SUB}/${MAP}`} />
        <Redirect from={SETTINGS} to={`${SUB}/${SETTINGS}`} />
        <Redirect from={EQUIPMENT} to={`${SUB}/${EQUIPMENT}`} />
        <Redirect from={ORBITRACKER} to={`${SUB}/${ORBITRACKER}`} />

        <Route path={ANOTHER} component={NotFoundPageComponent} />
    </Route>
);
