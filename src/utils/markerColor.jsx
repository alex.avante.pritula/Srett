import {EQUIPMENT_DETAILS_ICON_META} from '../constants/General';

const {
    LIGHTNESS,
    HUE_START,
    HUE_VALUE,
    SATURATION,
} = EQUIPMENT_DETAILS_ICON_META;

export default (n, i) => `hsl(${(HUE_VALUE * (1 - (n - i) / n) + HUE_START)}, ${SATURATION}%, ${LIGHTNESS}%)`;