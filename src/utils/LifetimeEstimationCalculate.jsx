/* UTILS */
import {LIFE_TIME_ESTIMATED} from '../constants/General';

export default (nb_wu) => {
    // RTC + timestamp + sleep + autodecharge + ble
    // 0.013+0.045+27.1+5.936+2.063
    const CONSO_GLOBAL = 35.157;

    // GPS + tx + rx
    // 32421.5 * 60 + 37441.3 * 2 + 12941.3 * 2
    const CONSO_PERIODIC = 2046055.2;

    // Capacity after one year storage and autodecharge
    const BATTERY_CAP = 4108;

    const period = 86400/nb_wu;
    
    const conso = (CONSO_GLOBAL + CONSO_PERIODIC/period) + 1;

    // lifetime in days
    const lifetime = (BATTERY_CAP * 1000) / (24 * conso);

    // round off to prevent update the state each 1 millisecond
    const d = new Date(+(Math.round(new Date() / 1000000) + '000000'));

    return  {
        [LIFE_TIME_ESTIMATED.YEAR]: Math.floor(lifetime / 365) || '0',
        [LIFE_TIME_ESTIMATED.MONTH]: Math.floor((lifetime % 365) / 30) || '0',
        [LIFE_TIME_ESTIMATED.DAY]: Math.round((lifetime % 365) % 30) || '0',
        [LIFE_TIME_ESTIMATED.MEASURE_DATE]:  d.setDate(d.getDate() + lifetime), 
    };
};
