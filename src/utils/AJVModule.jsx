import ajv from 'ajv';

export const Ajv = new ajv({
    format: 'full',
    allErrors: true,
    sourceCode: false,
    useDefaults: true,
    coerceTypes: 'array',
    errorDataPath: 'property',
});

export default (params, schema) => Ajv.compile(schema)(params);
