/* CONSTANTS */
import {
    SEARCH_META,
    NULL_ENTITY,
} from '../constants/General';

const {EQUIPMENT} = SEARCH_META;
const {ENTITY_TYPE_IDENTIFIER} = EQUIPMENT;
const INDEX_OF_VISIBLE_PART = ENTITY_TYPE_IDENTIFIER.length + 1;

export default (title) => ((title && title !== NULL_ENTITY) ? title.substring(INDEX_OF_VISIBLE_PART): '-');