/* UTILS */
import lifetimeCalculate from './LifetimeEstimationCalculate';
import positionsInPeriodicModeCalculate from './PositionsInPeriodicModeCalculate';

/* CONSTANTS */
import {
    DATA_RATE,
    WAKE_UP_TYPE,
    MAX_ACCELO_MEASURE,
    ORBITRACKER_PARAMS,
    MOV_DETECTION_FLAG,
    LIFE_TIME_ESTIMATED,
    DEFAULT_PARAMETERS_SETTINGS,
} from '../constants/General';

const O_P = ORBITRACKER_PARAMS;

export default (parameters) => {
    const {
        movement_detection_flag,
        wake_up_type,
        wake_up_period,

        calendar_start_hour,
        calendar_start_minute,
        calendar_stop_hour,
        calendar_stop_minute,
        calendar_nb_positions,

        mov_immobile_to_start,
        mov_immobile_to_fix,
        mov_time_between_fix,

        mov_data_rate,
        mov_max_measure,
        mov_threshold,

        gps_timeout,
        nb_miss,
    } = parameters || {};

    const transformValues = {};
    /* GENERAL */
    if (movement_detection_flag) {
        transformValues[MOV_DETECTION_FLAG] = (movement_detection_flag === 'true' ? true : false);
    };
    if (wake_up_type) {
        transformValues[WAKE_UP_TYPE.NAME] = wake_up_type;
    };
    /* PERIODIC */
    if (wake_up_period) {
        transformValues[O_P.PERIODIC_HOURS.NAME] = ('00' + Math.floor(wake_up_period / 3600)).slice(-2);
        transformValues[O_P.PERIODIC_MINUTES.NAME] = ('00' + Math.floor((wake_up_period % 3600) / 60)).slice(-2);
    };

    /* CALENDAR */
    if (calendar_start_hour) {
        transformValues[O_P.CALENDAR_START_HOURS.NAME] = ('00' + (+calendar_start_hour)).slice(-2);
    };
    if (calendar_start_minute) {
        transformValues[O_P.CALENDAR_START_MINUTES.NAME] = ('00' + (+calendar_start_minute)).slice(-2);
    };
    if (calendar_stop_hour) {
        transformValues[O_P.CALENDAR_STOP_HOURS.NAME] = ('00' + (+calendar_stop_hour)).slice(-2);
    };
    if (calendar_stop_minute) {
        transformValues[O_P.CALENDAR_STOP_MINUTES.NAME] = ('00' + (+calendar_stop_minute)).slice(-2);
    };

    if (calendar_nb_positions) {
        transformValues[O_P.CALENDAR_NUM_POSITION.NAME] = +calendar_nb_positions;
    };
    /* MOVEMENT DETECTION */

    if (mov_immobile_to_start) {
        transformValues[O_P.DETECTION_START_MINUTES.NAME] = Math.floor(mov_immobile_to_start / 60) || '0';
        transformValues[O_P.DETECTION_START_SECONDS.NAME] = Math.round(mov_immobile_to_start % 60) || '0';
    };
    if (mov_immobile_to_fix) {
        transformValues[O_P.DETECTION_TRIGGER_MINUTES.NAME] = Math.floor(mov_immobile_to_fix / 60) || '0';
        transformValues[O_P.DETECTION_TRIGGER_SECONDS.NAME] = Math.round(mov_immobile_to_fix % 60) || '0';
    };

    if (mov_time_between_fix) {
        transformValues[O_P.DETECTION_BETWEEN_HOURS.NAME] = Math.floor(mov_time_between_fix / 3600) || '0';
        transformValues[O_P.DETECTION_BETWEEN_MINUTES.NAME] = Math.floor((mov_time_between_fix % 3600) / 60) || '0';
    };

    /* ACCELEROMETER */
    if (mov_data_rate) {
        transformValues[DATA_RATE.NAME] = +mov_data_rate || '0';
    };

    if (mov_max_measure) {
        transformValues[MAX_ACCELO_MEASURE.NAME] = +mov_max_measure || '0';
    };
    if (mov_threshold) {
        transformValues[O_P.ACCELERO_THERESHOLD.NAME] = +mov_threshold || '0';
    };
    /* GPS */
    if (gps_timeout) {
        transformValues[O_P.GPS_TIMEOUT.NAME] = +gps_timeout || '0';
    };

    if (nb_miss) {
        transformValues[O_P.GPS_MISSED.NAME] = +nb_miss || '0';
    };

    /* ESTIMATED LIFETIME */
    let lifetime = {};
    /* CALENDAR */
    if (wake_up_type === WAKE_UP_TYPE.OPTIONS[1].value) {
        lifetime = lifetimeCalculate(
            +calendar_nb_positions
        );
    /* PERIODIC */
    } else if (wake_up_type === WAKE_UP_TYPE.OPTIONS[2].value) {
        lifetime = lifetimeCalculate(
            positionsInPeriodicModeCalculate(
                +transformValues[O_P.PERIODIC_HOURS.NAME],
                +transformValues[O_P.PERIODIC_MINUTES.NAME]
            )
        );
    }
    transformValues[LIFE_TIME_ESTIMATED.YEAR] = lifetime[LIFE_TIME_ESTIMATED.YEAR];
    transformValues[LIFE_TIME_ESTIMATED.DAY] = lifetime[LIFE_TIME_ESTIMATED.DAY];
    transformValues[LIFE_TIME_ESTIMATED.MONTH] = lifetime[LIFE_TIME_ESTIMATED.MONTH];
    transformValues[LIFE_TIME_ESTIMATED.MEASURE_DATE] = lifetime[LIFE_TIME_ESTIMATED.MEASURE_DATE];

    return transformValues;
};
