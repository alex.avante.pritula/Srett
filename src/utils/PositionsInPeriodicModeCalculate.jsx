export default (period_hour, period_min) => {
    const total_min = period_hour * 60 + period_min;
    return Math.round(1440 / total_min);
};
