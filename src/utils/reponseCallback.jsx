/* CONSTANTS */
import {API_ERROR_STATUS} from '../constants/General';

const {BAD_REQUEST} = API_ERROR_STATUS;

export default (response) => {
    const {
        status, 
        statusText,
    } = response;

    return response.json()
        .then((data) => {
            if (status < BAD_REQUEST) return data;
            const error = Object.assign({
                status,
                statusText,
            }, data);
            return Promise.reject(error);
        });
};
