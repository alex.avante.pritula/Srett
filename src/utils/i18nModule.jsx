
/* GENERAL */
import i18n from 'i18next';
import moment from 'moment';
import Backend from 'i18next-xhr-backend';
import {reactI18nextModule} from 'react-i18next';

/* CONSTANTS */
import {
    NODE_ENV,
    LOCALIZATION,
} from '../constants/General';

const {PROD} = NODE_ENV;
const {EN, FR} = LOCALIZATION;

i18n
    .use(Backend)
    .use(reactI18nextModule)
    .init({
        ns: ['translations'],
        fallbackLng: [EN, FR],
        defaultNS: 'translations',
        debug: (process.env.NODE_ENV !== PROD),
        react: {wait: true}
    })
    .on('languageChanged', (lng) => moment.locale(lng));

export {moment};
export default i18n;
