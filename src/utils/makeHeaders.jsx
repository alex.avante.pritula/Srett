/* CONSTANTS */
import {
    AUTH_TOKEN, 
    CONTENT_TYPE,
} from '../constants/General';

const {JSON_BODY} = CONTENT_TYPE;

export default (options = {}) => {
    const Authorization = (
        options.Authorization ? options.Authorization : (
            window.localStorage ? window.localStorage.getItem(AUTH_TOKEN): ''
        )
    );

    return {
        Authorization,
        'Content-Type': options['Content-Type'] || JSON_BODY,
    };
};
