export default (values) => {

    const valueKeys = Object.keys(values);

    const 
        start_hour = +values[valueKeys[0]],
        start_min = +values[valueKeys[1]],
        stop_hour = +values[valueKeys[2]],
        stop_min = +values[valueKeys[3]],
        nb_wu = +values[valueKeys[4]];

    const result = [];

    const pseudo_start = (start_hour * 60 + start_min) * 60;
    const pseudo_stop = (stop_hour * 60 + stop_min) * 60;
    
    let period,
        counter = 0,
        next_pseudo = pseudo_start;
    
    if(pseudo_start < pseudo_stop) {
        // Standard day
        if(nb_wu == 1) {
            period = (pseudo_stop - pseudo_start) / 2;
        } else {
            period = (pseudo_stop - pseudo_start) / (nb_wu - 1);

            result.push({
                output_date_hours: start_hour,
                output_date_minutes: start_min	
            });

            counter += 1;
        }

    } else {
        //Overlap a night
        if(nb_wu == 1) {
            period = ((86400-pseudo_start) + pseudo_stop) / 2;
        } else {
            period = ((86400-pseudo_start) + pseudo_stop) / (nb_wu - 1);

            result.push({
                output_date_hours: start_hour,
                output_date_minutes: start_min	
            });

            counter += 1;
        }
    }

    for (let i = counter; i < nb_wu; i++) {

        next_pseudo = next_pseudo + period;

        result.push({
            output_date_hours: (((next_pseudo/60) - ((next_pseudo/60) % 60) ) / 60 ) % 24,
            output_date_minutes: Math.round((next_pseudo/60) % 60)
        });
    }

    return result;
};
