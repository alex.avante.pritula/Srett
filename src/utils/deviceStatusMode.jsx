
export default (value) => {
    const status = parseInt(value);
    return {
        VALID: (status >> 0) & 1,
        MOVEMENT_ENABLED: (status >> 3) & 1,
        BATTERY_ERROR: (status >> 6) & 1,
        CONFIGURATION_ERROR: (status >> 7) & 1,
        DATA_NOT_SENT: (status >> 15) & 1,
        WAKE_UP_MODE: (status >> 1) & 3,
        LAST_WAKE_UP_MODE: (status >> 4) & 3,
    };
};