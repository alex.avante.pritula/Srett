/* LOCATION */
import i18nModule from './i18nModule';

export default (nb_missed_positions, nb_sent_positions) => {
    let period = `<code style='color: #d50000'>${i18nModule.t('REDUX_FORM_ERROR.DATE.RANGE')}!</code>`;

    if (!nb_sent_positions) return period;
    
    const daysToString = (days) => {
        let result;
        if (days === 1) {
            result = `1 ${i18nModule.t('TIME.ONE_DAY')}`;
        } else if (days > 1) {
            result = `${days} ${i18nModule.t('TIME.DAY')}`;
        }
        return result;
    };

    const weeksToString = (weeks) => {
        let result = '';
        if (weeks === 1) {
            result = `1 ${i18nModule.t('TIME.WEEK')}`;
        } else if (weeks > 1) {
            result = `${weeks} ${i18nModule.t('TIME.WEEKS')}`;
        }
        return result;
    };

    const periodOfDataResentInDays = Math.floor(nb_missed_positions / nb_sent_positions);

    const periodWeeks = Math.floor(periodOfDataResentInDays / 7);
    const periodDays = Math.floor(periodOfDataResentInDays % 7);

    period = periodDays
    ? (periodWeeks ? `${weeksToString(periodWeeks)} ${i18nModule.t('TIME.AND')} ${daysToString(periodDays)}` : daysToString(periodDays))
    : (periodWeeks ? weeksToString(periodWeeks) : `0 ${i18nModule.t('TIME.DAY')}`);

    return period;
};
