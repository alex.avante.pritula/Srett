import {
    AUTH_TOKEN,
    AUTH_PREFIX,
    LOCALIZATION,
} from '../constants/General';

import {START_CONFIG} from '../constants/Map';

const {EN} = LOCALIZATION;
const {LOCATION} = START_CONFIG;

const DEFAULT_META = {
    language: EN,
    mapCenter: LOCATION,
    minimizedMenu: false,
};

class UserMetaService  {

    get(identifier){
        const meta = localStorage.getItem(identifier);
        return (meta ? JSON.parse(meta) : DEFAULT_META);
    }

    getLocation(identifier){
        const {mapCenter} = this.get(identifier);
        return (mapCenter || LOCATION);
    }

    getLanguage(identifier){
        const {language} = this.get(identifier);
        return (language || EN);
    }

    getMenu(identifier){
        const {minimizedMenu} = this.get(identifier);
        return (!!minimizedMenu);
    }

    set(identifier, options = DEFAULT_META){
        const meta = JSON.stringify(options);
        localStorage.setItem(identifier, meta);
    }

    setToken(token){
        localStorage.setItem(AUTH_TOKEN, `${AUTH_PREFIX} ${token}`);
    }

    removeToken(){
        localStorage.removeItem(AUTH_TOKEN);
    }

    updateLanguage(identifier, language = EN){
        const oldMeta = localStorage.getItem(identifier);
        const {
            mapCenter,
            minimizedMenu,
        } = (oldMeta ? JSON.parse(oldMeta) : DEFAULT_META);

        const newMeta = JSON.stringify({
            language,
            mapCenter,
            minimizedMenu,
        });
        localStorage.setItem(identifier, newMeta);
    }

    updateMenu(identifier, minimizedMenu = false) {
        const oldMeta = localStorage.getItem(identifier);
        const {
            language,
            mapCenter,
        } = (oldMeta ? JSON.parse(oldMeta) : DEFAULT_META);

        const newMeta = JSON.stringify({
            language,
            mapCenter,
            minimizedMenu,
        });

        localStorage.setItem(identifier, newMeta);
    }

    updateLocation(identifier, mapCenter = LOCATION){
        const oldMeta = localStorage.getItem(identifier);
        const {
            language,
            minimizedMenu,
        } = (oldMeta ? JSON.parse(oldMeta) : DEFAULT_META);

        const newMeta = JSON.stringify({
            language,
            mapCenter,
            minimizedMenu,
        });

        localStorage.setItem(identifier, newMeta);  
    }
    
};

export default new UserMetaService();