import i18nModule from './i18nModule';

import transformParameters from './transformDeviceParameters';
import {
    DATA_RATE,
    WAKE_UP_TYPE,
    MAX_ACCELO_MEASURE,
    ORBITRACKER_PARAMS,
    MOV_DETECTION_FLAG,
    DEFAULT_PARAMETERS_SETTINGS,
} from '../constants/General';

const O_P = ORBITRACKER_PARAMS;

export default (params) => {
    const transformParams = transformParameters(params);
    const pending = 'pending';
    const result = {};

    if (params.movement_detection_flag === pending) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.GENERAL_SETTINGS.FIELDS.MOVEMENT_DETECTION')] = pending;
    } else if (params.movement_detection_flag) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.GENERAL_SETTINGS.FIELDS.MOVEMENT_DETECTION')] =
            transformParams[MOV_DETECTION_FLAG];
    };

    if (params.wake_up_type === pending) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.GENERAL_SETTINGS.FIELDS.WAKE_UP_MODE.TITLE')] = pending;
    } if (params.wake_up_type) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.GENERAL_SETTINGS.FIELDS.WAKE_UP_MODE.TITLE')] =
            transformParams[WAKE_UP_TYPE.NAME];
    };

    if (params.wake_up_period === pending) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.PERIODIC_SETTINGS.FIELDS.PERIOD')] = pending;
    } else if (params.wake_up_period) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.PERIODIC_SETTINGS.FIELDS.PERIOD')] =
            transformParams[O_P.PERIODIC_HOURS.NAME] + i18nModule.t('TIME.HOUR') + ' ' + transformParams[O_P.PERIODIC_MINUTES.NAME] + i18nModule.t('TIME.MINUTE');
    };

    if (params.calendar_start_hour === pending) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.CALENDAR_SETTINGS.FIELDS.START_HOUR')] = pending;
    } else if (params.calendar_start_hour) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.CALENDAR_SETTINGS.FIELDS.START_HOUR')] =
            transformParams[O_P.CALENDAR_START_HOURS.NAME] + i18nModule.t('TIME.HOUR') + ' ' + transformParams[O_P.CALENDAR_START_MINUTES.NAME] + i18nModule.t('TIME.MINUTE');
    };

    if (params.calendar_stop_hour === pending) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.CALENDAR_SETTINGS.FIELDS.STOP_HOUR')] = pending;
    } else if (params.calendar_stop_hour) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.CALENDAR_SETTINGS.FIELDS.STOP_HOUR')] =
            transformParams[O_P.CALENDAR_STOP_HOURS.NAME] + i18nModule.t('TIME.HOUR') + ' ' + transformParams[O_P.CALENDAR_STOP_MINUTES.NAME] + i18nModule.t('TIME.MINUTE');
    };

    if (params.calendar_nb_positions === pending) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.CALENDAR_SETTINGS.FIELDS.NUMBER_OF_POSITIONS')] = pending;
    } else if (params.calendar_nb_positions) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.CALENDAR_SETTINGS.FIELDS.NUMBER_OF_POSITIONS')] = 
            transformParams[O_P.CALENDAR_NUM_POSITION.NAME];
    };

    if (params.mov_immobile_to_start === pending) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.MOVEMENT_DETECTION.FIELDS.IMMOBILE_TIME_TO_START_DETECTION')] = pending;
    } else if (params.mov_immobile_to_start) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.MOVEMENT_DETECTION.FIELDS.IMMOBILE_TIME_TO_START_DETECTION')] = 
            transformParams[O_P.DETECTION_START_MINUTES.NAME] + i18nModule.t('TIME.MINUTE') + ' ' + transformParams[O_P.DETECTION_START_SECONDS.NAME] + i18nModule.t('TIME.SECOND');
    };

    if (params.mov_immobile_to_fix === pending) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.MOVEMENT_DETECTION.FIELDS.IMMOBILE_TIME_TO_TRIGGER_POSITION')] = pending;
    } else if (params.mov_immobile_to_fix) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.MOVEMENT_DETECTION.FIELDS.IMMOBILE_TIME_TO_TRIGGER_POSITION')] = 
            transformParams[O_P.DETECTION_TRIGGER_MINUTES.NAME] + i18nModule.t('TIME.MINUTE') + ' ' + transformParams[O_P.DETECTION_TRIGGER_SECONDS.NAME] + i18nModule.t('TIME.SECOND');
    };

    if (params.mov_time_between_fix === pending) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.MOVEMENT_DETECTION.FIELDS.MINIMUM_TIME_BETWEEN_TWO_MOVEMENT_DETECTION')] = pending;
    } else if (params.mov_time_between_fix) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.MOVEMENT_DETECTION.FIELDS.MINIMUM_TIME_BETWEEN_TWO_MOVEMENT_DETECTION')] = 
            transformParams[O_P.DETECTION_BETWEEN_HOURS.NAME] + i18nModule.t('TIME.HOUR') + ' ' + transformParams[O_P.DETECTION_BETWEEN_MINUTES.NAME] + i18nModule.t('TIME.MINUTE');
    };


    if (params.mov_data_rate === pending) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.ACCELEROMETER_SETTINGS.FIELDS.DATE_RATE')] = pending;
    } else if (params.mov_data_rate) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.ACCELEROMETER_SETTINGS.FIELDS.DATE_RATE')] = 
            transformParams[DATA_RATE.NAME] + i18nModule.t('ACCELEROMETER_PARAMS.HERTZ');
    };

    if (params.mov_max_measure === pending) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.ACCELEROMETER_SETTINGS.FIELDS.MAXIMUM_ACCELERATION_MEASURED')] = pending;
    } else if (params.mov_max_measure) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.ACCELEROMETER_SETTINGS.FIELDS.MAXIMUM_ACCELERATION_MEASURED')] = 
            transformParams[MAX_ACCELO_MEASURE.NAME] + i18nModule.t('ACCELEROMETER_PARAMS.GRAVITY');
    };

    if (params.mov_threshold === pending) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.ACCELEROMETER_SETTINGS.FIELDS.THRESHOLD_TO_TRIGGER_A_MEASURE')] = pending;
    } else if (params.mov_threshold) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.ACCELEROMETER_SETTINGS.FIELDS.THRESHOLD_TO_TRIGGER_A_MEASURE')] = 
            transformParams[O_P.ACCELERO_THERESHOLD.NAME] + i18nModule.t('ACCELEROMETER_PARAMS.MILLIGRAVITY');
    };


    if (params.gps_timeout === pending) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.GPS_SETTINGS.FIELDS.POSITION_ACQUISITION_TIMEOUT')] = pending;
    } else if (params.gps_timeout) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.GPS_SETTINGS.FIELDS.POSITION_ACQUISITION_TIMEOUT')] = 
            transformParams[O_P.GPS_TIMEOUT.NAME] + i18nModule.t('TIME.SECOND');
    };

    if (params.nb_miss === pending) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.GPS_SETTINGS.FIELDS.NUMBER_OF_MISSION_POSITIONS')] = pending;
    } else if (params.nb_miss) {
        result[i18nModule.t('ORBITRACKER.FORM.SECTIONS.GPS_SETTINGS.FIELDS.NUMBER_OF_MISSION_POSITIONS')] = 
            transformParams[O_P.GPS_MISSED.NAME];
    };

    return result;

};
