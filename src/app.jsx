/* GENERAL */
import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {Router, hashHistory} from 'react-router';
import {syncHistoryWithStore} from 'react-router-redux';

/* APP CONFIG */
import routes from './routes';
import configureStore from './configureStore';

/* INTERNATIONALIZATION */
import './utils/i18nModule';

/* STYLES */

/* MATERIAL UI */
import 'react-mdl/extra/material.js';
import 'react-mdl/extra/material.css';

/* REACT TABLE */
import 'react-table/react-table.css';

/* REACT TOASTR */
import 'react-redux-toastr/src/styles/index.scss';

/* DATE TIME PICKER */
import 'react-datetime/css/react-datetime.css';

const store = configureStore();
const history = syncHistoryWithStore(hashHistory, store);

render(
    <Provider store={store}>
        <Router history={history} routes={routes} />
    </Provider>, 
    document.getElementById('root')
);
