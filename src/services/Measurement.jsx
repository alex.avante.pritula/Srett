/* GENERAL */
import fetch from 'node-fetch';

/* UTILS */
import makeHeaders from '../utils/makeHeaders'; 
import reponseCallback from '../utils/reponseCallback';

/* CONSTANTS */
import {
    METHODS,
    API_PREFIX_URL,
} from '../constants/General';

const {WS} = API_PREFIX_URL;

const {
    GET,
    POST,
} = METHODS;

class MeasurementService {
    
    getLastPosition(identifier, fields){
        const fieldCodes = fields.map(field => `&fieldCodes=${field}`).join('');
        return fetch(`${WS}/measurement/entity/${identifier}?limit=1${fieldCodes}`, {
            method: GET,
            headers: makeHeaders(),
        }).then(reponseCallback);
    }

    getPositions(options){
        const {
            to,
            from,
            fields,
            identifier,
        } = options;

        const fieldCodes = fields ? fields.map(field => `&fieldCodes=${field}`).join('') : '';
        return fetch(`${WS}/measurement/entity/${identifier}?from=${from}&to=${to}${fieldCodes}`, {
            method: GET,
            headers: makeHeaders(),
        }).then(reponseCallback);  
    }
}

export default new MeasurementService();
