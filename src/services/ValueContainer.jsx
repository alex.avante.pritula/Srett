/* GENERAL */
import fetch from 'node-fetch';

/* UTILS */
import makeHeaders from '../utils/makeHeaders'; 
import reponseCallback from '../utils/reponseCallback';

/* CONSTANTS */
import {
    METHODS,
    DOMAIN_ID,
    API_PREFIX_URL,

    /* PAGINATION */
    ITEMS_PER_PAGE,
    ITEMS_PER_SEARCH,
    ITEMS_PER_SEARCH_AUTOCOMPLETE,
} from '../constants/General';

const {WS} = API_PREFIX_URL;
const {GET, POST} = METHODS;

class ValueContainerService {

    /* GENERAL SEARCH */
    generalSearch(options){
        const {
            size,
            field, 
            filter,
            entityTypeIdentifier, 
        } = options || {};

        const queries = ((filter && field) ? `&queries=${field}%3D@${filter}` : '');

        return fetch(`${WS}/value-container/search?size=${(size || ITEMS_PER_SEARCH)}&entityTypeIdentifier=${entityTypeIdentifier}${queries}`, {
            method: GET,
            headers: makeHeaders(),
        }).then(reponseCallback);
    }

    /* COMPLEX SEARCH */
    complexSearch(options){
        const {
            queries,
            filters,
            entityTypeIdentifier, 
        } = options || {};

        const shouldQueries = (queries ?
            Object.keys(queries)
                .map(field => `&shouldQueries=${field}=@${queries[field]}`)
                .join('') : ''
        );

        const shouldFilters = (filters ? 
            Object.keys(filters)
                .map(field => `&shouldFilters=${field}==${filters[field]}`)
                .join('') : ''
        );

        return fetch(`${WS}/value-container/complexSearch?size=${ITEMS_PER_SEARCH}&entityTypeIdentifier=${entityTypeIdentifier}${shouldQueries}${shouldFilters}`, {
            method: GET,
            headers: makeHeaders(),
        }).then(reponseCallback);
    }

    /* PROCESSES */
    getProcess(options){
        const {
            page,
            filter,
            sortField, 
            sortOrder,
        } = options || {};

        const pageFilter = (page !== undefined) ? `page=${page}` : '';
        const sortQuery = (sortField && sortOrder) ? `&sort=${sortField},${sortOrder}` : '';
        return fetch(`${WS}/value-container/orbitrack_iot@process/search?${pageFilter}&queries=device%3D@${filter}${sortQuery}`,{
            method: GET,
            headers: makeHeaders(),
        }).then(reponseCallback);
    }
    
    createProcess(body) {
        return fetch(`${WS}/value-container/orbitrack_iot@process/entity?domain-id=${DOMAIN_ID}`, {
            method: POST,
            headers: makeHeaders(),
            body: JSON.stringify(body),
        }).then(reponseCallback);
    }

    /* PARAMETERS */
    getParameters(device){
        return fetch(`${WS}/value-container/orbitrack_iot@orbitracker_parameter/search?queries=tracker_id%3D@${device}`, {
            method: GET,
            headers: makeHeaders(),
        }).then(reponseCallback);
    }

    /* DEVICES */
    getDevices(options){

        if(typeof options === 'string'){
            return fetch(`${WS}/value-container/srett_device@124/entity/${options}`, {
                method: GET,
                headers: makeHeaders(),
            }).then(reponseCallback);
        }

        const {
            page, 
            filter, 
            sortField, 
            sortOrder,
        } = options || {};
        
        const pageFilter = (page !== undefined) ? `&page=${page}` : '';
        const filterQuery = (filter ? `&shouldQueries=identifier=@${filter}`: '');
        const sortQuery = (sortField && sortOrder) ? `&sort=${sortField},${sortOrder}` : '';

        return fetch(`${WS}/value-container/complexSearch?&entityTypeIdentifier=srett_device@124${pageFilter}${sortQuery}${filterQuery}`, {
            method: GET,
            headers: makeHeaders(),
        }).then(reponseCallback);
    }

    updateDevice(identifier, fields = {}){
        return fetch(`${WS}/value-container/srett_device@124/entity/${identifier}`, {
            method: POST,
            headers: makeHeaders(),
            body: JSON.stringify(fields),
        }).then(reponseCallback);
    }

    /* AUTOCOMPLETE */
    getFreeDevices(filter){
        const filterQuery = ((filter) ? `&queries=identifier%3D@${filter}` : '');
        return fetch(`${WS}/value-container/search?size=${ITEMS_PER_SEARCH}&entityTypeIdentifier=srett_device@124&queries=association%3D~false${filterQuery}&sort=identifier,asc`, {
            method: GET,
            headers: makeHeaders(),
        }).then(reponseCallback);
    }

    /* EQUIPMENT */
    createEquipment(options){
        const {
            type,
            tracker,
            identifier,
        } = options || {};

        const body = {type};

        if(tracker)  body.tracker = tracker; 
        if(identifier) body.identifier = identifier; 

        return fetch(`${WS}/value-container/orbitrack_iot@equipment/entity?domain-id=${DOMAIN_ID}`, {
            method: POST,
            headers: makeHeaders(),
            body: JSON.stringify(body),
        }).then(reponseCallback);
    }

    associate(options){
        const {
            tracker,
            equipment,
        } = options || {};

        return fetch(`${WS}/value-container/orbitrack_iot@orbitracker_equipment/entity/${equipment}`, {
            method: POST,
            headers: makeHeaders(),
            body: JSON.stringify({tracker}),
        }).then(reponseCallback);
    }

    getEquipments(options){

        if(typeof options === 'string'){
            return fetch(`${WS}/value-container/orbitrack_iot@equipment/entity/${options}`, {
                method: GET,
                headers: makeHeaders(),
            }).then(reponseCallback);
        }

        const {
            page, 
            filter, 
            sortField, 
            sortOrder,
        } = options || {};

        const pageFilter = (page !== undefined) ? `&page=${page}` : '';
        const filterQuery = (filter ? `&shouldQueries=identifier=@${filter}&shouldQueries=type=@${filter}`: '');
        const sortQuery = (sortField && sortOrder) ? `&sort=${sortField},${sortOrder}` : '';

        return fetch(`${WS}/value-container/complexSearch?&entityTypeIdentifier=orbitrack_iot@equipment${pageFilter}${sortQuery}${filterQuery}`, {
            method: GET,
            headers: makeHeaders(),
        }).then(reponseCallback);
    }

    fullTextSearch(search) {
        return fetch(`${WS}/value-container/orbitrack_iot@equipment/fullTextSearch?&size=${ITEMS_PER_SEARCH}&searchPhrase&shouldFilters=reference==${search}&shouldFilters=type==${search}&shouldFilters=tracker_id==${search}`, {
            method: GET,
            headers: makeHeaders(),
        }).then(reponseCallback);
    }
}

export default new ValueContainerService();
