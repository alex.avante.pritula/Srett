/* GENERAL */
import fetch from 'node-fetch';

/* UTILS */
import makeHeaders from '../utils/makeHeaders'; 
import reponseCallback from '../utils/reponseCallback';
import UrlUncodeData from '../utils/makeUrlEncodedData';

/* CONSTANTS */
import {
    METHODS,
    AUTH_META,
    AUTH_TOKEN,
    AUTH_PREFIX,
    CONTENT_TYPE,
    API_PREFIX_URL,
} from '../constants/General';

const {GET, POST} = METHODS;
const {WS, OAUTH} = API_PREFIX_URL;

const {
    SCOPE,
    ENCODED,
    CLIENT_ID,
    GRANT_TYPE,
} = AUTH_META;

const {
    JSON_BODY,
    URL_ENCODED_FORM,
} = CONTENT_TYPE;

class UserService {

    signIn(username, password){
        const data = {
            username,
            password,
            scope: SCOPE,
            client_id: CLIENT_ID,
            grant_type: GRANT_TYPE,
        };

        return fetch(OAUTH, {
            method: POST,
            body: UrlUncodeData(data),
            headers: makeHeaders({
                Authorization: ENCODED,
                'Content-Type' : URL_ENCODED_FORM,
            }),
        }).then(reponseCallback);
    }

    getUserData(){
        return fetch(`${WS}/myAccount`, {
            method: GET,
            headers: makeHeaders(),
        }).then(reponseCallback);
    }

    updatePassword(password, newPassword){
        const data = {
            password,
            newPassword,
        };
        const token = localStorage.getItem(AUTH_TOKEN).substring(AUTH_PREFIX.length + 1);
        return fetch(`${WS}/myAccount/updatepassword?${AUTH_TOKEN}=${token}`, {
            body: JSON.stringify(data),
            method: POST,
            headers: makeHeaders(),
        }).then(reponseCallback);
    }

}

export default new UserService();
