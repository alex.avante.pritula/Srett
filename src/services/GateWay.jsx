/* GENERAL */
import fetch from 'node-fetch';

/* UTILS */
import makeHeaders from '../utils/makeHeaders'; 
import reponseCallback from '../utils/reponseCallback';

/* CONSTANTS */
import {
    METHODS,
    API_PREFIX_URL,
} from '../constants/General';

const {GATEWAY} = API_PREFIX_URL;
const {POST} = METHODS;

class GatewayFetchService {
    /* UPDATE DEVICE PARAMETERS: SAVE, REBOOT, SYNCHRONIZE */
    updateDeviceParameters(processId){
        return fetch(`${GATEWAY}/downlink/process/${processId}`, {
            method: POST,
            headers: makeHeaders(),
            body: JSON.stringify({}),
        }).then(reponseCallback);
    }
}

export default new GatewayFetchService();