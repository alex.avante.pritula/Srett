class GoogleMapService {
    getPlaces (input){
        if(!google || !google.maps) Promise.reject({});

        const service = new google.maps.places.AutocompleteService();
        return new Promise((resolve, reject) => {
            service.getPlacePredictions({input}, (predictions, status) => {
                if (
                    status != google.maps.places.PlacesServiceStatus.OK &&
                    status != google.maps.places.PlacesServiceStatus.ZERO_RESULTS
                ) reject({status});
                resolve(predictions || []);
            });
        });
    }

    getPlaceLocation(placeId){
        if(!google || !google.maps || !placeId) Promise.reject({});
        const geocoder = new google.maps.Geocoder();
        return new Promise((resolve, reject) => {
            geocoder.geocode({placeId}, (responses, status) =>{
                if (status != google.maps.places.PlacesServiceStatus.OK) reject({status});
                const {geometry} = responses[0];
                const {location} = geometry;
                resolve({
                    latitude: location.lat(),
                    longitude: location.lng(),
                });
            });
        });
    }
}

export default new GoogleMapService();