/* GENERAL */
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classnames from 'classnames';
import React, {Component} from 'react';
import {I18n, Trans} from 'react-i18next';
import {SubmissionError, reduxForm, Field, reset} from 'redux-form';

/* UI ELEMENTS */
import {Chip, IconButton, ChipContact} from 'react-mdl';
import AutoComplete from '../../../UI/AutoComplete/AutoComplete';

/* ICONS */
import GoSearch from 'react-icons/lib/go/search';
import FaSignOut from 'react-icons/lib/fa/sign-out';

/* ACTIONS */
import {signOutCall} from '../../../../actions/User';
import {
    resetSearchItemsCall,
    getSearchEquipmentItemsCompleteApiCall,
    getSearchOrbitrackerItemsCompleteApiCall,
    getSearchDetailedMapItemsCompleteApiCall,
} from '../../../../actions/Search';

import {changeMapCenterCall}  from '../../../../actions/GlobalMap';
import {getOrbitrackerItemsApiCall} from '../../../../actions/Orbitracker';
import {
    getClientEquipmentItemsApiCall,
    getClientEquipmentItemsCompleteApiCall,
} from '../../../../actions/Equipment';

/* CONSTANTS */
import {
    PAGES,
    REDUX_FORM,
    SEARCH_META,
    ON_PRESS_ENTER,
} from '../../../../constants/General';

const {SEARCH} = REDUX_FORM;

const {
    MAP,
    SETTINGS,
    EQUIPMENT,
    ORBITRACKER,
} = PAGES;

/* STYLES */
import './TopSideBar.scss';

class TopSideBarComponent extends Component {

    static propTypes = {
        /* REDUX FORM META */
        pristine: PropTypes.bool.isRequired,
        submitting: PropTypes.bool.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        resetSearchForm: PropTypes.func.isRequired, 

        /* REDUX STATE PROPS */
        title: PropTypes.string.isRequired,
        isFetching: PropTypes.bool.isRequired,
        identifier: PropTypes.string.isRequired,

        /* ACTIONS */
        signOutCall: PropTypes.func.isRequired,    
        resetSearchItemsCall: PropTypes.func.isRequired,  
        getClientEquipmentItemsCompleteApiCall: PropTypes.func.isRequired,
        getSearchEquipmentItemsCompleteApiCall: PropTypes.func.isRequired,  
        getSearchOrbitrackerItemsCompleteApiCall: PropTypes.func.isRequired,  
        getSearchDetailedMapItemsCompleteApiCall: PropTypes.func.isRequired,  
    };

    constructor(props) {
        super(props);
    }

    componentWillReceiveProps(nextState){
        const {
            title,
            resetSearchForm,
        } = this.props;

        if(title !== nextState.title) resetSearchForm();
    } 

    render() {
        const {
            items,
            title,
            identifier,

            pristine,
            submitting,
            handleSubmit,

            isFetching,

            signOutCall,
            resetSearchForm,
            changeMapCenterCall,
            getClientEquipmentItemsCompleteApiCall,
        } = this.props;  

        let disabled = (items.size < 1);

        const isMapPage = (PAGES[title] === MAP);
        const isEquipmentPage = (PAGES[title] === EQUIPMENT);

        if(isMapPage) disabled = (items.findIndex(({visible}) => title === visible) === -1);
        
        return (
            <I18n ns="translations">{
                (translate) => (
                    <section className={
                        classnames({
                            'top-sidebar-container': true,
                            'minimize': (PAGES[title] === SETTINGS),
                        })
                    }>
                        <p className="top-sidebar-container-title">
                            {translate(`MENU_ITEM.${title}`)}
                        </p>
                        {(title !== 'SETTINGS') && (
                            <form onSubmit={(!disabled && handleSubmit)} className="top-sidebar-container-search">
                                <IconButton 
                                    type="submit"
                                    name="run-search"
                                    disabled={disabled}
                                    component={GoSearch} 
                                    className="top-sidebar-container-search-button"
                                />  
                                <Field className="top-sidebar-container-search-input"
                                    type="text" 
                                    name="search" 
                                    items={items}
                                    component={AutoComplete} 
                                    freezeAfterSelect={(isMapPage || isEquipmentPage)}
                                    handleSubmit={(result) => {
                                        if(isMapPage) {
                                            resetSearchForm();
                                            changeMapCenterCall(identifier, result);
                                            return;
                                        }

                                        if(isEquipmentPage) {
                                            resetSearchForm();
                    
                                            getClientEquipmentItemsCompleteApiCall({
                                                page: 0,
                                                filter: result,
                                            });
                                            return;
                                        }

                                        if(disabled) return;
                                        setTimeout(handleSubmit);
                                    }}
                                    label={translate('SEARCH.TITLE')}
                                    dataIndex={'visible'}
                                    valueIndex={(PAGES[title] === ORBITRACKER ? 'visible': 'data')} 
                                />
                            </form>
                        )}
                        <Chip onClick={signOutCall} className="top-sidebar-container-logout">
                            <ChipContact>
                                <FaSignOut />
                            </ChipContact>
                            <span>{translate('AUTH.LOG_OUT')}</span>
                        </Chip>
                    </section>
                )
            }</I18n>
        );
    }
}

/* REDUX */
const mapStateToProps = ({User, Search}, {title}) => {
    const {
        items, 
        isFetching,
    } = Search;

    const {identifier} = User;

    return {
        items, 
        title,

        identifier,
        isFetching,
    };
};

const mapDispatchToProps = ({
    signOutCall,
    changeMapCenterCall,
    resetSearchItemsCall,

    getClientEquipmentItemsCompleteApiCall,
    getSearchEquipmentItemsCompleteApiCall,
    getSearchOrbitrackerItemsCompleteApiCall,
    getSearchDetailedMapItemsCompleteApiCall,

    resetSearchForm: () => (dispatch) => {
        dispatch(reset(SEARCH));
    }
});

/* REDUX FORM */
const searchReduxForm = {
    form: SEARCH,
    enableReinitialize: true,
    validate: ({search}, props) => {
        const {
            title,
            resetSearchItemsCall,
            getSearchEquipmentItemsCompleteApiCall,
            getSearchOrbitrackerItemsCompleteApiCall,
            getSearchDetailedMapItemsCompleteApiCall,
        } = props;

        if(!search) {
            resetSearchItemsCall();
            return {}; 
        }

        switch(PAGES[title]) {
            case EQUIPMENT: {
                getSearchEquipmentItemsCompleteApiCall(search);
                break;
            }
            case ORBITRACKER: {
                getSearchOrbitrackerItemsCompleteApiCall(search);
                break;
            }
            case MAP: {
                getSearchDetailedMapItemsCompleteApiCall(search);
                break;
            }
        }
        
        return {};
    },
    onSubmit: ({search = ''}, dispatch, {title}) => {
        const options = {
            page: 0,
            filter: search,
        };

        switch(PAGES[title]) {
            case EQUIPMENT: {
                getClientEquipmentItemsApiCall(options, dispatch);
                break;
            }
            case ORBITRACKER: {
                getOrbitrackerItemsApiCall(options, dispatch);
                break;
            }
        }
    },
};

const bindedReduxFormTopSideBarComponent = reduxForm(searchReduxForm)(TopSideBarComponent);
const connectedTopSideBarComponent = connect(mapStateToProps, mapDispatchToProps)(bindedReduxFormTopSideBarComponent);
export default connectedTopSideBarComponent;
