/* GENERAL */
import classnames from 'classnames';

/* REACT */
import {Link} from 'react-router';
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';

/* UI ELEMENTS */
import {IconButton, Button} from 'react-mdl';

/* ACTIONS */
import {
    toogleMinimizeMenuCall,
} from '../../../../actions/User';

/* ICONS */
import TiMap from 'react-icons/lib/ti/map';
import FaUser from 'react-icons/lib/fa/user';
import TiCompass from 'react-icons/lib/ti/compass';
import TiCloseButton from 'react-icons/lib/ti/times';
import MdSettings from 'react-icons/lib/md/settings';
import FaAlignJustify from 'react-icons/lib/fa/align-justify';
import MdFormatListBulleted from 'react-icons/lib/md/format-list-bulleted';

/* STYLES */
import './LeftSideBar.scss';

/* CONSTANTS */
import {PAGES} from '../../../../constants/General';

const {
    MAP,
    SUB,
    SETTINGS,
    EQUIPMENT,
    ORBITRACKER,
} = PAGES;

class LeftSideBarComponent extends Component {

    static propTypes = {
        /* REDUX STATE PROPS */
        minimizedMenu: PropTypes.bool, 
        
        domainId: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        firstName: PropTypes.string.isRequired,
        identifier: PropTypes.string.isRequired, 

        /* PROPS */
        activeItem: PropTypes.string.isRequired,

        /* ACTIONS */
        toogleMinimizeMenuCall: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        /* BINDED HANDLERS */
        this.handleToogleMinimizeMenu = this.handleToogleMinimizeMenu.bind(this);
    }

    handleToogleMinimizeMenu(){
        const {
            identifier,
            minimizedMenu,
            toogleMinimizeMenuCall,
        } = this.props;

        toogleMinimizeMenuCall(identifier, (!minimizedMenu));
    }

    render() {
        const {
            domainId,
            lastName,
            firstName,

            activeItem,

            minimizedMenu,
        } = this.props;

        const navButton = minimizedMenu ?
            <Button name="show-menu" 
                component={FaAlignJustify} 
                onClick={this.handleToogleMinimizeMenu}
                className="left-sidebar-container-header-button"
            />  
        :
            <IconButton name="hide-menu" 
                component={TiCloseButton} 
                onClick={this.handleToogleMinimizeMenu}
                className="left-sidebar-container-header-button"
            />  
        ;
        
        return (
            <I18n ns="translations">{
                (translate, {i18n}) => (
                    <section className={
                        classnames({
                            'full': (!minimizedMenu),
                            'minimized': minimizedMenu,
                            'left-sidebar-container': true, 
                        })
                    }>
                        <div className="left-sidebar-container-header" onClick={this.handleToogleMinimizeMenu}>
                            <FaUser className="left-sidebar-container-header-icon"/>
                            <div className="left-sidebar-container-header-description">
                                <p className="left-sidebar-container-header-description-title">
                                    {firstName}
                                </p>
                                <p className="left-sidebar-container-header-description-title">
                                    {lastName}
                                </p>
                                <span className="left-sidebar-container-header-description-domain">
                                    {domainId}
                                </span>
                            </div>
                            {navButton} 
                        </div>
                        <ul className="left-sidebar-container-menu">   
                            <li className={classnames({active: (activeItem === EQUIPMENT)})}>
                                <Link to={EQUIPMENT}>
                                    <MdFormatListBulleted />
                                    <span>
                                        {translate('MENU_ITEM.EQUIPMENT')}
                                    </span>
                                </Link>
                            </li>
                            <li className={
                                classnames({
                                    active: (activeItem === ORBITRACKER),
                                })
                            }>
                                <Link to={ORBITRACKER}>
                                    <TiCompass />
                                    <span>
                                        {translate('MENU_ITEM.ORBITRACKER')}
                                    </span>
                                </Link>
                            </li>
                            <li className={
                                classnames({
                                    active: (activeItem === MAP),
                                })
                            }>
                                <Link to={MAP}>
                                    <TiMap />
                                    <span>
                                        {translate('MENU_ITEM.MAP')}
                                    </span>
                                </Link>
                            </li>
                            <li className={
                                classnames({
                                    active: (activeItem === SETTINGS),
                                })
                            }>
                                <Link to={SETTINGS}>
                                    <MdSettings />
                                    <span>
                                        {translate('MENU_ITEM.SETTINGS')}
                                    </span>
                                </Link>
                            </li>
                            <div className="left-sidebar-container-menu-toogle" onClick={this.handleToogleMinimizeMenu} />
                        </ul>
                        <div className="left-sidebar-container-footer" onClick={this.handleToogleMinimizeMenu}>
                            <img src="../../../../../assets/images/title.png" />
                        </div>
                    </section>
                )
            }</I18n>
        );
    }
}

/* REDUX */
const mapStateToProps = (state) => { 
    const {User} = state;
    const {
        domainId,
        lastName,
        firstName,
        identifier,
        minimizedMenu,
    } = User;

    return {
        domainId,
        lastName,
        firstName,
        identifier,
        minimizedMenu,
    };
};

const mapDispatchToProps = ({
    toogleMinimizeMenuCall,
});

const connectedLeftSideBarComponent = connect(mapStateToProps, mapDispatchToProps)(LeftSideBarComponent);
export default connectedLeftSideBarComponent;
