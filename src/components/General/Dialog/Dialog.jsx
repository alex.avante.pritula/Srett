/* REACT */
import PropsTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';

/* UI ELEMENTS */
import {IconButton, Dialog, DialogTitle, DialogContent, DialogActions} from 'react-mdl';
import dialogPolyfill from 'dialog-polyfill/dialog-polyfill';

/* ICONS */
import MdClose from 'react-icons/lib/md/close';

/* ACTIONS */
import {dialogCloseCall} from '../../../actions/Dialog';

/* STYLES */
import './Dialog.scss';

class DialogWrapper extends Component {

    static propsTypes = {
        /* REDUX STATE PROPS */
        header: PropsTypes.string,
        isOpened: PropsTypes.bool.isRequired,

        /* ACTIONS */
        dialogCloseCall: PropsTypes.func.isRequired,
    };

    constructor(props){
        super(props);
    }

    componentDidMount() {
        const dialog = document.querySelector('dialog');
        dialogPolyfill.registerDialog(dialog);
        dialog.showModal();
    }

    render(){
        const {
            header,
            isOpened,
            children,
            dialogCloseCall,
        } = this.props;

        return (
            <Dialog ref={(dialog) => this.dialog = dialog}
                onCancel={dialogCloseCall}
                className="dialog-wrapper-container">
                <DialogTitle className="dialog-wrapper-container-title">
                    <span>{header}</span>    
                    <DialogActions>
                        <IconButton 
                            className="dialog-wrapper-container-close-button" 
                            name="close-button" 
                            component={MdClose} 
                            onClick={dialogCloseCall}
                        />
                    </DialogActions>
                </DialogTitle>
                <DialogContent className="dialog-wrapper-container-content">
                    {children}
                </DialogContent>
        </Dialog>
        );
    }
}

/* REDUX */
const mapStateToProps = ({Dialog}) => { 
    const {isOpened} = Dialog;
    return {isOpened};
};

const mapDispatchToProps = ({
    dialogCloseCall,
});

const connectedDialogWrapper = connect(mapStateToProps, mapDispatchToProps)(DialogWrapper);
export default connectedDialogWrapper;
