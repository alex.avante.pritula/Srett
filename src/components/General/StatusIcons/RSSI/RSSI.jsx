/* REAT */
import React from 'react';
import PropTypes from 'prop-types';

/* UI ELEMENTS */
import {Tooltip}  from 'react-mdl';
import Icon from 'react-icon-base';

/* UTILS */
import {moment} from '../../../../utils/i18nModule';

/* STYLES */
import './RSSI.scss';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';

/* CONSTANTS */
import {RSSI_META} from '../../../../constants/General';

const {
    COLOR,
    RANGE,
    ICON_SIZE,
    ITEMS_COUNT,
    LAST_MEASURE,
    ICON_ITEM_SIZE,
} = RSSI_META;   

const {
    ERROR,
    ACTIVE,
    DEFAULT,
} = COLOR;

const {MIN, MAX} = RANGE;
const STEP = Math.abs((MIN - MAX) / ITEMS_COUNT);

const rssiRectItem = (i, color) => (
    <rect 
        key={i}
        x={i * ICON_ITEM_SIZE} 
        style={{fill: color}}
        height={(i + 1) * (ICON_ITEM_SIZE - 1)} 
        y={ICON_SIZE - (i + 1) * (ICON_ITEM_SIZE - 1)}
        width={ITEMS_COUNT} 
    />
);

rssiRectItem.propTypes = {
    i: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
};


const RSSI = ({
    value, 
    disabled,
    onlyError,
    lastmeasure, 
}) => {
    const items = [];
    const isMoreTwoWeeks = (moment().add(-LAST_MEASURE, 'seconds') > lastmeasure);

    if(disabled || (onlyError ^ (!value))) {
        const color = (disabled || (value && lastmeasure && !isMoreTwoWeeks)) ? DEFAULT : ERROR;
        for (let i = 0; i < ITEMS_COUNT; i++) items.push(rssiRectItem(i, color));    
    } else {
        let countToFill = 0;
        
        if(value){
            const delta = Math.abs(parseInt(value - MAX));
            countToFill = ITEMS_COUNT - delta / STEP;
            const balance = delta % STEP;

            if(balance !== 0 && balance > STEP / 2) {
                countToFill--;
            }
        }

        for (let i = 0; i < ITEMS_COUNT; i++) {
            items.push(rssiRectItem(i,
                (i < countToFill) ? ACTIVE : DEFAULT,
            ));    
        }
    }

    const RSSIIcon = (
        <Icon className="rssi-container" viewBox={`0 0 ${ICON_SIZE} ${ICON_SIZE}`}>
            <g>{items}</g>
        </Icon>
    );

    if(!disabled && onlyError && isMoreTwoWeeks) {
        return (
            <I18n ns="translations">{
                (translate) => (
                    <Tooltip 
                        label={translate('ICON_ERROR.NO_RADIO_NETWORK')}
                        position="left"
                        className="custom-ui-tooltip">
                            {RSSIIcon}
                    </Tooltip>
                )
            }</I18n>    
        );
    }

    return RSSIIcon;
};         

RSSI.propTypes = {
    value: PropTypes.string,
    disabled: PropTypes.bool,
    onlyError: PropTypes.bool,
    lastmeasure: PropTypes.any, 
};

export default RSSI;
