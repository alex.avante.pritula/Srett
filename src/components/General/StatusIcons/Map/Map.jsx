/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* UI ELEMENTS */
import {Tooltip}  from 'react-mdl';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';

/* ICONS */
import TiMap from 'react-icons/lib/ti/map';

/* STYLES */
import './Map.scss';

/* CONSTANTS */
import {RSSI_META}from '../../../../constants/General';

const {COLOR} = RSSI_META;
const {ERROR, DEFAULT} = COLOR;

const MapIcon = ({
    disabled,
    latitude,
    longitude,
}) => (
    (disabled || (parseFloat(latitude) && parseFloat(longitude))) ?
        <TiMap style={{color: DEFAULT}} />:
        <I18n ns="translations">{
            (translate, {i18n}) => (
                <Tooltip 
                    label={translate('ICON_ERROR.NO_POSITION')}
                    position="left"
                    className="custom-ui-tooltip">     
                        <TiMap style={{color: ERROR}} />
                </Tooltip>   
            )
        }</I18n>        
);

MapIcon.propTypes = {
    disabled: PropTypes.bool,
    latitude: PropTypes.string,
    longitude: PropTypes.string,
};

export default MapIcon;
