/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* UI ELEMENTS */
import {Tooltip}  from 'react-mdl';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';

/* ICONS */
import FaBattery4 from 'react-icons/lib/fa/battery-4';
import FaBattery2 from 'react-icons/lib/fa/battery-2';
import FaBattery1 from 'react-icons/lib/fa/battery-1';
import FaBattery0 from 'react-icons/lib/fa/battery-0';

/* STYLES */
import './Battery.scss';

/* CONSTANTS */
import {
    RSSI_META,
    DEVICE_BATTERY_LIMIT,
} from '../../../../constants/General';

const {
    RED,
    GREEN,
    ORANGE,
} = DEVICE_BATTERY_LIMIT;

const {COLOR} = RSSI_META;
const {DEFAULT} = COLOR;

const Battery = ({
    value, 
    disabled,
    onlyError,
}) => {
    // SHOW DISABLED ICON
    if(disabled) {
        return <FaBattery0 style={{color: DEFAULT}} />;
    }

    // ONLY BATTERY ERROR STATUS
    if(onlyError){
        return (
           (!value || value <= RED.VALUE) ? 
           <I18n ns="translations">{
                (translate, {i18n}) => (
                    <Tooltip 
                        label={translate('ICON_ERROR.LOW_BATTERY')}
                        position="left"
                        className="custom-ui-tooltip">
                            <FaBattery0 style={{color: RED.COLOR}}/> 
                    </Tooltip> 
                )
            }</I18n> : <FaBattery4 style={{color: DEFAULT}}/> 
        );
    }

    // EMPTY BATTERY
    if(!value) {
        return <FaBattery0 style={{color: DEFAULT}}/>; // style={{color: RED.COLOR}}
    }

    // BATTERY POWER
    return (
        (value > ORANGE.VALUE) ? 
            <FaBattery4 style={{color: GREEN.COLOR}} /> : (
            (value > RED.VALUE) ? 
                <FaBattery2 style={{color: ORANGE.COLOR}}/> : 
                <FaBattery1 style={{color: RED.COLOR}} />
            )
    );
};

Battery.propTypes = {
    value: PropTypes.string, 
    disabled: PropTypes.bool,
    onlyError: PropTypes.bool,
};

export default Battery;