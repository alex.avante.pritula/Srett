/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* UI ELEMENTS */
import {Tooltip}  from 'react-mdl';

/* ICONS */
import MdFlashOn from 'react-icons/lib/md/flash-on';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';

/* UTILS */
import {moment} from '../../../../utils/i18nModule';

/* STYLES */
import './Measure.scss';

/* CONSTANTS */
import {RSSI_META}from '../../../../constants/General';

const {
    COLOR,
    LAST_MEASURE,
} = RSSI_META;
const {ERROR, DEFAULT} = COLOR;
 
const Measure = ({
    disabled,
    lastWakeUpMode,
}) =>(
    (disabled || lastWakeUpMode) ?
        <MdFlashOn style={{color: DEFAULT}} /> :
        <I18n ns="translations">{
            (translate) => (
                <Tooltip 
                    label={translate('ICON_ERROR.END_OF_MOVEMENT_DETECTION')}
                    position="left"
                    className="custom-ui-tooltip">     
                        <MdFlashOn style={{color: ERROR}} />
                </Tooltip>
            )
        }</I18n>    
);

Measure.propTypes = {
    disabled: PropTypes.bool,
    lastWakeUpMode: PropTypes.number,
};

export default Measure;
