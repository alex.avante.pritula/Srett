/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* UI ELEMENTS */
import {Spinner} from 'react-mdl';

/* STYLES */
import './LoadingContainer.scss';

export default () => (
    <div className="loading-container">
        <Spinner singleColor />
    </div>
);
