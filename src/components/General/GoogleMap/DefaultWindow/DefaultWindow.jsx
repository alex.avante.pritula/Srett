/* REACT */
import React, {Component} from 'react';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';

/* ICONS */
import MdError from 'react-icons/lib/md/error';

/* STYLES */
import './DefaultWindow.scss';

export default () => (
    <I18n ns="translations">{
        (translate, {i18n}) => (
            <section className="map-default-container">
                <MdError />
                <span>{translate('GOOGLE_MAP_ERROR')}</span>
            </section>
        )
    }</I18n>
);