/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REACT GOOGLE MAP */
import {
    Marker,
    GoogleMap,
    InfoWindow,
    OverlayView,

    withScriptjs,
    withGoogleMap,
} from 'react-google-maps';

import {MarkerClusterer} from 'react-google-maps/lib/components/addons/MarkerClusterer';
import {MarkerWithLabel} from 'react-google-maps/lib/components/addons/MarkerWithLabel';

/* STYLES */
import './GoogleMap.scss';

/* CONSTANTS */
import {
    ICON,
    ZOOM_RANGE,
    START_CONFIG,
    ANCHOR_OFFSET,
    GOOGLE_MAP_API,
} from '../../../constants/Map';

const {
    ZOOM,
    LOCATION,
} = START_CONFIG;

const {
    URL,
    KEY,
    VERSION,
    LIBRARIES,
} = GOOGLE_MAP_API;

const GOOGLE_MAP_LIBS = LIBRARIES.join(',');

export class MapContainer extends Component {

    static propTypes = {
        zoom: PropTypes.number,
        center: PropTypes.object,
        markers: PropTypes.array,

        zoomControl: PropTypes.bool,
        scrollwheel: PropTypes.bool,
        fullscreenControl: PropTypes.bool, 

        markerInfo: PropTypes.object,

        /* ACTIONS */
        onCenterChanged: PropTypes.func,
        onGoogleMapClick: PropTypes.func,
        onInfoWindowClose: PropTypes.func,
        onGoogleMapZoomChanged: PropTypes.func,
    };

    constructor(props) {
        super(props);

        /* BINDED HANDLERS */
        this.handleCenterChanged = this.handleCenterChanged.bind(this);
    }

    handleCenterChanged(){
        const {onCenterChanged} = this.props;
        if(!onCenterChanged) return;

        const center = this.googleMap.getCenter();
        const latitude = center.lat();
        const longitude = center.lng();

        onCenterChanged({latitude, longitude});
    }

    render() {
        const {
            zoom,
            center,
            markers,
            markerInfo,

            zoomControl,
            scrollwheel,
            fullscreenControl,

            onGoogleMapClick,
            onInfoWindowClose,
            onGoogleMapZoomChanged,
        } = this.props;

        const markerItems = [];
        const overlayItems = [];

        const iconMeta = Object.assign({anchor: new google.maps.Point(ANCHOR_OFFSET.x, ANCHOR_OFFSET.y)}, ICON);

        if(markers && markers.length > 0){
            markers.forEach((marker, index) => {

                const {
                    lat, 
                    lng, 
    
                    infoContent,
                    overlayContent,
    
                    fillColor,
                    strokeColor, 
    
                    onMarkerClick,
                    onMarkerMouseOut,
                    onMarkerMouseOver,
                } = marker;

                const itemIconMeta = Object.assign({}, iconMeta);
                
                if(fillColor) itemIconMeta.fillColor = fillColor;
                if(strokeColor) itemIconMeta.strokeColor = strokeColor;

                if(overlayContent) {
                    overlayItems.push(
                        <OverlayView
                            key={index}
                            position={{ 
                                lat: parseFloat(lat), 
                                lng: parseFloat(lng),
                            }}
                            mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}>
                            {overlayContent}
                        </OverlayView>
                    );
                }

                markerItems.push(
                    <Marker
                        key={index}
                        icon={itemIconMeta}
                        position={{
                            lat: parseFloat(lat), 
                            lng: parseFloat(lng),
                        }}
                        
                        onClick={onMarkerClick}
                        onMouseOut={onMarkerMouseOut}
                        onMouseOver={onMarkerMouseOver}
                    >
                    {infoContent && <InfoWindow onCloseClick={onInfoWindowClose}>{infoContent}</InfoWindow> }
                    </Marker>
                );
            });
        }

        const isClustered  = ((this.googleMap ? this.googleMap.getZoom() : (zoom || ZOOM)) < ZOOM_RANGE.MAX);

        return (
            <GoogleMap
                ref={(ref) => { this.googleMap = ref; }}
                onClick={onGoogleMapClick}
                onCenterChanged={this.handleCenterChanged}
                onZoomChanged={onGoogleMapZoomChanged}
                defaultZoom={zoom || ZOOM}
                defaultCenter={center}
                defaultOptions={{
                    zoomControl: (zoomControl !== undefined ? zoomControl :  true),
                    scrollwheel: (scrollwheel !== undefined ? scrollwheel :  true),
                    fullscreenControl: (fullscreenControl !== undefined ? fullscreenControl :  true),
                }}>{
                    isClustered ?
                        <MarkerClusterer
                            averageCenter
                            enableRetinaIcons>
                            {markerItems}
                        </MarkerClusterer>
                    :   markerItems
                }
                {overlayItems}
            </GoogleMap>
        );
    }
}

const WrappedMapContainer = withScriptjs(withGoogleMap((props) => <MapContainer {...props}/>));
export default (props) => (
    <WrappedMapContainer 
        {...props}
        mapElement={<div className="google-map-container" />}
        loadingElement={<div className="google-map-container" />}
        containerElement={<div className="google-map-container" />}
        googleMapURL={`${URL}?key=${KEY}&v=${VERSION}exp&libraries=${GOOGLE_MAP_LIBS}`}
    />
);
