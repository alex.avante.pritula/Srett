/* REACT */
import React, {Component} from 'react';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';

/* STYLES */
import './NotFound.scss';

export default () => (
    <I18n ns="translations">{
        (translate, {i18n}) => (
            <section className="error-container">
                <img src="../../../../assets/images/logo.png" />
                <span>{translate('TITLE')}</span>
                <h1>{translate('NOT_FOUND.STATUS')}</h1>
                <p>{translate('NOT_FOUND.DESCRIPTION')}</p>
            </section>
        )
    }</I18n>
);