/* GENERAL */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';
import {SubmissionError, reduxForm, Field, reset} from 'redux-form';

/* COMPONENTS */
import Dialog from '../General/Dialog/Dialog';
import TopSideBar from '../General/Navigation/TopSideBar/TopSideBar';
import LeftSideBar from '../General/Navigation/LeftSideBar/LeftSideBar';

/* DIALOG CONTAINERS */
/* EQUIPMENT */
import EquipmentDetailed from '../Equipment/EquipmentDetailed/EquipmentDetailed';
import EquipmentCreateForm from '../Equipment/EquipmentDialogForms/EquipmentCreateForm';
import OrbitrackerAssociatedForm from '../Equipment/EquipmentDialogForms/ConfigureAssociationForm';
import InfoWindowEquipment from '../GlobalMap/InfoWindow/InfoWindowDialogs/InfoWindowEquipment/InfoWindowEquipment';
/* ORBITRACKER */
import OrbitrackerConfigure from '../Orbitracker/OrbitrackerConfigure/OrbitrackerConfigure';
import EquipmentDetailedPositions from '../Orbitracker/OrbitrackerDetailedPositions/OrbitrackerDetailedPositions';
import InfoWindowOrbitracker from '../GlobalMap/InfoWindow/InfoWindowDialogs/InfoWindowOrbitracker/InfoWindowOrbitracker';

/* UI ELEMENTS */
import ReduxToastr, {toastr} from 'react-redux-toastr';

/* ACTIONS */
import {
    signOutCall,
    getUserDataCompleteApiCall,
} from '../../actions/User';

/* STYLES */
import './Srett.scss';

/* LOCALIZATION */
import i18nModule from '../../utils/i18nModule';

/* CONSTANTS */
import {
    DIALOGS,
    API_ERROR_STATUS,
    TOAST_MESSAGE_DURATION,
} from '../../constants/General';

const {
    FORBIDDEN,
    UNAUTHORIZED,
} = API_ERROR_STATUS;

const {
    DETAILED_MAP,
    ADD_NEW_EQUIPMENT,
    EQUIPMENT_DETAILS,
    DETAILED_POSITIONS,
    ORBITRACKER_DETAILS,
    CONFIGURE_ASSOCIATION,
    CONFIGURE_ORBITRACKER,
} = DIALOGS;

class SrettComponent extends Component {

    static propTypes = {
        /* REDUX STATE PROPS */
        error: PropTypes.object,
        action: PropTypes.string,

        meta: PropTypes.object.isRequired,

        /* ACTIONS */
        signOutCall: PropTypes.func.isRequired,
        getUserDataCompleteApiCall: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props);

        /* BINDED HANDLERS */
        this.getModalDialog = this.getModalDialog.bind(this);
    }

    componentWillMount(){
        this.props.getUserDataCompleteApiCall();
    }

    componentWillReceiveProps({action, error}){
        if(action && action !== this.props.action){

            const message = (typeof error === 'string') ? error:
                (error.message || (error.error_description || error.status));

            const title = error.title || '';

            toastr.error(
                title,
                message,
                {timeOut: TOAST_MESSAGE_DURATION},
            );

            if(typeof error === 'object' && (error.status === FORBIDDEN || error.status === UNAUTHORIZED)){
                this.props.signOutCall();
            }
        }
    }

    getModalDialog() {
        const {meta} = this.props;

        const {
            identifier,
            dialogName,
            customHeader,
        } = meta;

        switch (dialogName) {
            case CONFIGURE_ASSOCIATION:
                return (
                    <Dialog header={i18nModule.t('CLIENT_EQUIPMENT.DIALOGS.ASSOCIATED_ORBITRACKER')}>
                        <OrbitrackerAssociatedForm />
                    </Dialog>
                );
                break;
            case DETAILED_MAP:
                return (
                    <Dialog header={customHeader}>
                        <EquipmentDetailed />
                    </Dialog>
                );
                break;
            case ADD_NEW_EQUIPMENT:
                return (
                    <Dialog header={i18nModule.t('CLIENT_EQUIPMENT.DIALOGS.ADD_NEW_EQUIPMENT')}>
                        <EquipmentCreateForm />
                    </Dialog>
                );
                break;
            case CONFIGURE_ORBITRACKER:
                return (
                    <Dialog header={customHeader}>
                        <OrbitrackerConfigure />
                    </Dialog>
                );
                break;
            case DETAILED_POSITIONS:
                return (
                    <Dialog header={customHeader}>
                        <EquipmentDetailedPositions />
                    </Dialog>
                );
                break;
            case ORBITRACKER_DETAILS:
                return (
                    <Dialog  header={customHeader}>
                        <InfoWindowOrbitracker identifier={identifier} />
                    </Dialog>
                );
                break;
            case EQUIPMENT_DETAILS:
                return (
                    <Dialog header={customHeader}>
                        <InfoWindowEquipment identifier={identifier} />
                    </Dialog>
                );
                break;
        }
    }

    render() {
        const {children} = this.props;
        const {route} = children.props;

        const path = route ? route.path : '';
        const name = route ? route.name : '';

        return (
            <section className="strett-container">
                <LeftSideBar activeItem={path} />
                <ReduxToastr
                    preventDuplicates
                    newestOnTop={false}
                    position="top-right"
                    transitionIn="fadeIn"
                    transitionOut="fadeOut"
                    timeOut={TOAST_MESSAGE_DURATION}
                />
                {this.getModalDialog()}
                <section className="strett-container-main">
                    <TopSideBar title={name}/>
                    {children}
                </section>
            </section>
        );
    }
}

/* REDUX */
const mapStateToProps = ({Dialog, ErrorHandler}) => { 
    const {meta} = Dialog;
    const {error, action} = ErrorHandler;

    return {
        meta, 
        error,
        action,
    };
};

const mapDispatchToProps = ({
    signOutCall,
    getUserDataCompleteApiCall,
});

const connectedSrettComponent = connect(mapStateToProps, mapDispatchToProps)(SrettComponent);
export default connectedSrettComponent;
