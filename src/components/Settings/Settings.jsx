/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';
import {SubmissionError, reduxForm, Field, reset} from 'redux-form';

/* UI ELEMENTS */
import {Button, Spinner} from 'react-mdl';
/* CUSTOM */
import Switch from '../UI/Switch/Switch';
import TextField from '../UI/TextField/TextField';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';
import I18nModule from '../../utils/i18nModule';

/* ACTIONS */
import {
    changeLanguageCall,
    updateUserPasswordCompleteApiCall,
} from '../../actions/User';

/* STYLES */
import './Settings.scss';

/* CONSTANTS */
import {
    VALIDATION,
    REDUX_FORM,
    LOCALIZATION,
} from '../../constants/General'; 

const {SETTINGS} = REDUX_FORM;
const {PASSWORD} = VALIDATION;
const {EN, FR} = LOCALIZATION;

class UserSettingsComponent extends Component {

    static propTypes = {
        /* REDUX FORM META */
        pristine: PropTypes.bool.isRequired,
        submitting: PropTypes.bool.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        
        /* REDUX STATE PROPS */
        language: PropTypes.string.isRequired,
        identifier: PropTypes.string.isRequired, 

        /* ACTIONS */
        changeLanguageCall: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        /* BINDED HANDLERS */
        this.handleChangeLanguage = this.handleChangeLanguage.bind(this);
    }

    handleChangeLanguage(isFrench){
        const {
            identifier,
            changeLanguageCall,
        } = this.props;
        
        changeLanguageCall(identifier, (isFrench ? FR : EN));
    }

    render() {
        const {
            language,

            pristine,
            submitting,
            handleSubmit,
        } = this.props;   

        return (
            <I18n ns="translations">{
                (translate, {i18n}) => (
                    <section className="settings-container">
                        <form onSubmit={handleSubmit}>
                            <div className="settings-container-preference">
                                <p className="settings-container-preference-title">
                                    {translate('SETTINGS.CONNECTION.TITLE')}
                                </p>
                                <div className="settings-container-preference-connection">
                                    <div>
                                        <div className="settings-container-preference-item-container">
                                            <span className="settings-container-preference-label">
                                                {translate('SETTINGS.CONNECTION.EMAIL')}
                                            </span>
                                            <Field
                                                id="email"
                                                label=""
                                                type="text" 
                                                name="email" 
                                                disabled
                                                component={TextField}
                                            />
                                        </div>
                                        <div className="settings-container-preference-item-container">
                                            <span className="settings-container-preference-label">
                                                {translate('SETTINGS.CONNECTION.PASSWORD.CURRENT')}
                                            </span>
                                            <Field
                                                id="password"
                                                label=""
                                                type="password" 
                                                name="password" 
                                                component={TextField} 
                                            />
                                        </div> 
                                    </div>  
                                    <div>
                                        <div className="settings-container-preference-item-container">
                                            <span className="settings-container-preference-label">
                                                {translate('SETTINGS.CONNECTION.PASSWORD.NEW')}
                                            </span>
                                            <Field
                                                id="new_password"
                                                label=""
                                                type="password" 
                                                name="newPassword" 
                                                component={TextField} 
                                            />
                                        </div>
                                        <div className="settings-container-preference-item-container">
                                            <span className="settings-container-preference-label">
                                                {translate('SETTINGS.CONNECTION.PASSWORD.CONFIRM')}
                                            </span>
                                            <Field
                                                id="confirm_password"
                                                label=""
                                                type="password" 
                                                name="confirmPassword" 
                                                component={TextField} 
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <Button className="settings-container-submit" type="submit" 
                                primary raised disabled={submitting || pristine}>{
                                    submitting ? 
                                        <span> 
                                            <Spinner singleColor/> 
                                            {translate('BUTTON_REQUEST')}  
                                        </span>
                                    :  
                                        <span>
                                            {translate('SETTINGS.BUTTON')} 
                                        </span> 
                            }</Button>
                            <div className="settings-container-preference settings-container-preference-language">
                                <p className="settings-container-preference-title">
                                    {translate('SETTINGS.LANGUAGE.TITLE')}
                                </p>
                                <div className="settings-container-preference-item-container">
                                    <span className="settings-container-preference-label">
                                        {translate('SETTINGS.LANGUAGE.DISPLAY')}
                                    </span>
                                    <div>
                                        <span>
                                            {translate('SETTINGS.LANGUAGE.ITEMS.ENGLISH')}
                                        </span>
                                        <Switch checked={(language === FR)} onChange={this.handleChangeLanguage} />
                                        <span>
                                            {translate('SETTINGS.LANGUAGE.ITEMS.FRENCH')}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </section>
                )
            }</I18n>
        );
    }
}

/* REDUX */
const mapStateToProps = ({User}) => { 
    const {
        login,
        language,
        identifier,
    } = User;

    return {
        language,
        identifier,

        /* REDUX FORM INITIAL VALUES */
        initialValues: {
            email: login,
        },
    }; 
};
const mapDispatchToProps = ({
    changeLanguageCall,
});

/* REDUX FORM */
const validate = ({password, newPassword, confirmPassword}) => {
    const errors = {};

    if(password || newPassword || confirmPassword) {
        if(!password) {
            errors.password = I18nModule.t('REDUX_FORM_ERROR.PASSWORD.EMPTY');
        } else {
            if (!password.match(PASSWORD)) {
                errors.password = I18nModule.t('REDUX_FORM_ERROR.PASSWORD.VALIDATION');
            }
        } 

        if(!newPassword) {
            errors.newPassword = I18nModule.t('REDUX_FORM_ERROR.NEW_PASSWORD.EMPTY');
        } else {
            if (!newPassword.match(PASSWORD)) {
                errors.newPassword = I18nModule.t('REDUX_FORM_ERROR.PASSWORD.VALIDATION');
            }
        } 

        if(!confirmPassword) {
            errors.confirmPassword = I18nModule.t('REDUX_FORM_ERROR.CONFIRM_PASSWORD.EMPTY');
        } else {
            if (!confirmPassword.match(PASSWORD)) {
                errors.confirmPassword = I18nModule.t('REDUX_FORM_ERROR.PASSWORD.VALIDATION');
            }
        } 

        if(newPassword !== confirmPassword) {
            errors.confirmPassword = I18nModule.t('REDUX_FORM_ERROR.PASSWORD.NOT_COMPAIRE');
        }
    }

    return errors;
};

const settingsReduxForm = {
    form: SETTINGS,
    enableReinitialize: true,
    validate: (values, props) => {
        const {dirty} = props;
        if (!dirty) return {};
        return validate(values);
    },
    onSubmit: (values, dispatch) => {
        const errors = validate(values);
        if(Object.keys(errors).length > 0) {
            throw new SubmissionError(errors);
        }
        return updateUserPasswordCompleteApiCall(values, dispatch);
    },
    onSubmitSuccess: (result, dispatch) => {
        dispatch(reset(SETTINGS));
    },
};

const bindedReduxFormUserSettingsComponent = reduxForm(settingsReduxForm)(UserSettingsComponent);
const connectedUserSettingsComponent = connect(mapStateToProps, mapDispatchToProps)(bindedReduxFormUserSettingsComponent);
export default connectedUserSettingsComponent;
