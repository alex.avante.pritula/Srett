/* GENERAL */
import classnames from 'classnames';

/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* UTILS */
import {moment} from '../../../utils/i18nModule';

/* UI ELEMENTS */
import DateTime from 'react-datetime';

/* STYLES */
import './DateTimePicker.scss';

/* CONSTANTS */
import {
    NOW,
    DEFAULT_DATE_TIME_FORMAT,
} from '../../../constants/General';

const formats = DEFAULT_DATE_TIME_FORMAT.split(' ');

const CustomUIDateTimePicker = ({
    id,
    max,
    meta,
    input,
    locale,
    disabled,
    showTime,
    className,
    handleOnBlur,
    handleOnFocus,
}) => (
    <div className={`${className ? className :  ''} custom-ui-data-time-picker`}>
        <DateTime 
            open={false}
            locale={locale}
            closeOnTab={true}
            closeOnSelect={true}
            disableOnClickOutside={false}
            dateFormat={formats[0]}
            timeFormat={showTime === false ? false : formats[1]}
            inputProps={{
                id,
                readOnly: true,
                disabled: disabled,
                className: classnames({error: meta.invalid})
            }}
            value={input.value}
            onChange={(value) => {
                if(max){
                    if(max === NOW){
                        const today = moment();
                        if(today < value) value = moment(today);
                    } else {
                        if(max < value) value = moment(max);
                    }
                }

                input.onChange(value);
            }}
            onBlur={handleOnBlur}
            onFocus={handleOnFocus}
        />
        <span className="custom-ui-data-time-picker-error-message">
            {meta.error}
        </span>
    </div>
);

CustomUIDateTimePicker.propTypes = {
    disabled: PropTypes.bool, 

    handleOnBlur: PropTypes.func,
    handleOnFocus: PropTypes.func,

    input: PropTypes.shape({
        value: PropTypes.any,
        onChange: PropTypes.func, 
    }),

    meta: PropTypes.shape({
        invalid: PropTypes.bool, 
        error: PropTypes.string,
    }),

    max:  PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object,
    ]),

    locale: PropTypes.string.isRequired,
};

export default CustomUIDateTimePicker;
