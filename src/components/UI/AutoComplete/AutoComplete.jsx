/* GENERAL */
import Promise from 'bluebird';
import classnames from 'classnames';

/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';

/* UI ELEMENTS */
import {Textfield, List, ListItem} from 'react-mdl';

/* STYLES */
import './AutoComplete.scss';

export default class CustomUIAutoComplete extends Component {

    static propTypes = {
        /* PROPS */
        disabled: PropTypes.bool,
        freezeAfterSelect: PropTypes.bool,

        label: PropTypes.string.isRequired,
        input: PropTypes.object.isRequired,
        className: PropTypes.string.isRequired,
        dataIndex: PropTypes.string.isRequired,
        valueIndex: PropTypes.string.isRequired,

        /* ACTIONS */
        handleSubmit: PropTypes.func.isRequired,
    };

    constructor(props){
        super(props);

        /* STATE */
        this.state = {isOpened: false};

        /* BINDED HANDLERS */
        this.handleOnFocus = this.handleOnFocus.bind(this);
        this.handleOnChange = this.handleOnChange.bind(this);
        this.handleClickOutSide = this.handleClickOutSide.bind(this);
    }

    componentDidMount(){
        document.addEventListener('click', this.handleClickOutSide);
    }

    componentWillUnmount(){
        document.removeEventListener('click', this.handleClickOutSide);
    }

    handleClickOutSide(e){
        if (this.rootElement.contains(e.target)) return;
        this.setState({isOpened: false});
    }

    handleOnFocus(e){
       this.setState({isOpened: true});
    }

    handleOnChange(item){
        const {
            input, 
            handleSubmit,
            freezeAfterSelect,
        } = this.props;

        this.setState({isOpened: false});
        
        if(!freezeAfterSelect) input.onChange(item);
        if(handleSubmit) handleSubmit(item);
    }

    render(){
        const {
            meta,
            input,
            label,
            items,
            disabled,
            className,
            dataIndex,
            valueIndex,
        } = this.props;

        const {error} = meta;
        const {value} = input;
        const {isOpened} = this.state;

        const listItems = [];
        let noDataText = '';

        items.map((item, index) =>  {
            const text = item[dataIndex];
            if(!text) return;

            const indexOfMatched = text.toLowerCase().indexOf(value.toLowerCase());

            if(indexOfMatched == -1) return;
            
            const before = text.substring(0, indexOfMatched);
            const after = text.substring(indexOfMatched + value.length, text.length);

            listItems.push(
                <ListItem key={index} 
                    onClick={(disabled ? null : () => this.handleOnChange(item[valueIndex]))}
                    className="custom-ui-auto-complete-list-item">
                    <span><pre>{before}</pre></span>
                    <b><pre>{(indexOfMatched !== -1) ? value : ''}</pre></b>
                    <span><pre>{after}</pre></span>
                </ListItem>
            );
        });

        return (
            <I18n ns="translations">{
                (translate, {i18n}) => (
                    <div className={`${className ? className :  ''} custom-ui-auto-complete`}
                        ref={(ref) => { this.rootElement = ref; }}>
                        <Textfield 
                            {...input}
                            error={error}
                            label={label}
                            autoComplete="off"
                            //disabled={disabled}
                            readOnly={disabled}
                            onBlur={this.handleOnBlur}
                            onFocus={this.handleOnFocus}
                        />
                        <List className={
                            classnames({
                                opened: isOpened,
                                'custom-ui-auto-complete-list': true,
                            })
                        }>{
                            listItems.length > 0 ? listItems : 
                            <span className="custom-ui-auto-complete-no-data">
                                {translate(`SEARCH.${(input.value ? 'NO_DATA' : 'DEFAULT')}`)}
                            </span>
                        }</List>
                    </div>
                )
            }</I18n>
        );
    }
}
