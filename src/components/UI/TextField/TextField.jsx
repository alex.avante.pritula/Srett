/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* UI ELEMENTS */
import {Textfield} from 'react-mdl';

/* STYLES */
import './TextField.scss';

const CustomUITextField = ({
    input,
    label,
    className,
    meta: {touched, error},
    ...custom
}) => (
    <Textfield 
        {...input}
        {...custom}
        label={label}
        error={touched && error}
        className={`${className ? className :  ''} custom-ui-textfield`}
    />
);

CustomUITextField.propTypes = {
    className: PropTypes.string,
    
    meta: PropTypes.shape({
        error: PropTypes.string,
        touched: PropTypes.bool,
    }),

    input: PropTypes.object.isRequired,
    label: PropTypes.string.isRequired,
};

export default CustomUITextField;
