/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';

/* UI ELEMENTS */
import ReactTable from 'react-table';

/* CUSTOM */
import GridExpander from './GridExpander/GridExpander';
import GridAddButton from './GridAddButton/GridAddButton';
import GridPagination from './GridPagination/GridPagination';

/* CONSTANTS */
import {
    ITEMS_PER_PAGE,
    GRID_EXPANDER_SIZE,
} from '../../../constants/General';

export default class CustomUIGrid extends Component {

    static propTypes = {
        /* ACTIONS */
        handleAddEvent: PropTypes.func, 
        handlePageChange: PropTypes.func, 
        hasCustomExpander: PropTypes.bool, 
        handleSortedChange: PropTypes.func, 
        addTootltipLabel: PropTypes.string, 
        handleSubGridComponent: PropTypes.func, 
    
        /* PROPS */
        data: PropTypes.array.isRequired,
        page: PropTypes.number.isRequired,
        columns: PropTypes.array.isRequired,
        isFetching: PropTypes.bool.isRequired,
        totalPages: PropTypes.number.isRequired,
        defaultSorted: PropTypes.array.isRequired,
    };

    constructor(props){
        super(props);
        this.state = {expanded: {}};

        /* BINDED HANDLERS */
        this.PaginationComponent = this.PaginationComponent.bind(this);
    }

    componentWillReceiveProps(newprops) {
        if(newprops.isFetching !== this.props.isFetching){
            this.setState({ expanded: {} });
        }
    }

    PaginationComponent(){
        const {
            page,
            isFetching,
            totalPages,

            handlePageChange, 
        } = this.props;
        
        return (
            <GridPagination
                page={page}
                isFetching={isFetching}
                totalPages={totalPages}
                handlePageChange={handlePageChange}
            />
        );
    }

    render(){

        const {
            data,
            page,
            columns,
            isFetching,
            totalPages,
            defaultSorted,
            handleAddEvent, 
            addTootltipLabel,
            handlePageChange,
            hasCustomExpander,
            handleSortedChange,
            handleSubGridComponent,
        } = this.props;

        return (
            <I18n ns="translations">{
                (translate, {i18n}) => (
                    <ReactTable
                        manual
                        data={data}
                        page={page}
                        pages={totalPages} 
                        loading={isFetching}
                        
                        showPageSizeOptions={false}
                        defaultSorted={defaultSorted}
                        defaultPageSize={ITEMS_PER_PAGE}

                        onSortedChange={handleSortedChange}
                        SubComponent={handleSubGridComponent}
        
                        columns={hasCustomExpander ? columns : [{
                            columns: [{
                                expander: true,
                                width: GRID_EXPANDER_SIZE,
                                Expander: ({isExpanded}) => <GridExpander isExpanded={isExpanded}/>,
                                Header: handleAddEvent ? <GridAddButton addTootltipLabel={addTootltipLabel} handleAddEvent={handleAddEvent} /> : '',
                            }],
                        }].concat(columns)}
        
                        className="-striped -highlight grid-container"
                        freezeWhenExpanded={true}
                        noDataText={
                            <span className="grid-container-nodata-message">
                                {translate('GRID.NO_DATA_MESSAGE')}
                            </span>
                        }
                        
                        expanded={this.state.expanded}
                        onExpandedChange={
                            (newExpanded, index, p) => {
                            return this.state.expanded && +Object.keys(this.state.expanded)[0] === index[0]
                                ? this.setState({ expanded: {} })
                                : this.setState({ expanded: { [index]: {} } });
                            }
                        }

                        PaginationComponent={this.PaginationComponent}
                    />
                )
            }</I18n>  
        );
    }
}
