/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';

/* ICONS */
import FaAngleLeft from 'react-icons/lib/fa/angle-left';
import FaAngleRight from 'react-icons/lib/fa/angle-right';
import FaAngleDoubleLeft from 'react-icons/lib/fa/angle-double-left';
import FaAngleDoubleRight from 'react-icons/lib/fa/angle-double-right';

/* UI ELEMENTS */
import {Button, Textfield} from 'react-mdl';

/* STYLES */
import './GridPagination.scss';
export default class CustomUIGridPagination extends Component {

    static propTypes = {
        /* PROPS */
        page: PropTypes.number.isRequired,
        isFetching: PropTypes.bool.isRequired,
        totalPages: PropTypes.number.isRequired,

        /* ACTIONS */
        handlePageChange: PropTypes.func,
    };

    constructor(props){
        super(props);

        const {page} = props;
        this.state = {inputValue: 0};

        /* BINDED HANDLERS */
        this.handleChange = this.handleChange.bind(this);
        this.handleOnClickNext = this.handleOnClickNext.bind(this);
        this.handleOnClickLast = this.handleOnClickLast.bind(this);
        this.handleOnClickFirst = this.handleOnClickFirst.bind(this);
        this.handleOnPageChange = this.handleOnPageChange.bind(this);
        this.handleOnClickPrevious = this.handleOnClickPrevious.bind(this);
    }

    componentWillReceiveProps({page}){
        this.setState({inputValue: page + 1});
    }

    handleChange(e){
        this.setState({inputValue: e.target.value});
    }

    handleOnClickLast(){
        const {
            totalPages,
            handlePageChange,
        } = this.props;

        handlePageChange(totalPages - 1);
    }

    handleOnClickFirst(){
        this.props.handlePageChange(0);
    }

    handleOnClickNext(){
        const {
            page,
            handlePageChange,
        } = this.props;

        handlePageChange(page + 1);
    }

    handleOnClickPrevious(){
        const {
            page,
            handlePageChange,
        } = this.props;

        handlePageChange(page - 1);
    }

    handleOnPageChange(e){
        const value = parseFloat(e.target.value);
        const {
            page,
            totalPages,
            handlePageChange,
        } = this.props;

        if((value > totalPages) || (value === page + 1) || (value < 1)){
            return;
        }

        this.props.handlePageChange(value - 1);
    }

    render(){

        const {
            page,
            isFetching,
            totalPages,
        } = this.props;

        const {inputValue} = this.state;

        const isaAvigatableBefore = (!totalPages || isFetching || !page);
        const isNavigatableAfter = (!totalPages || isFetching || ((page + 1) === totalPages));

        return  (
            <I18n ns="translations">{
                (translate, {i18n}) => (
                    <div className="custom-ui-grid-pagination-container">
                        <div className="custom-ui-grid-pagination-container-before">
                            <Button 
                                disabled={isaAvigatableBefore} 
                                className="custom-ui-grid-pagination-container-before-first"
                                onClick={isaAvigatableBefore ? null : this.handleOnClickFirst}>
                                <span className="custom-ui-grid-pagination-container-full">
                                    {translate('PAGINATION.FIRST')}
                                </span>
                                <span className="custom-ui-grid-pagination-container-minimized">
                                    <FaAngleDoubleLeft />
                                </span>
                            </Button>
                            <Button 
                                disabled={isaAvigatableBefore} 
                                className="custom-ui-grid-pagination-container-before-previous"
                                onClick={isaAvigatableBefore ? null : this.handleOnClickPrevious}>
                                <span className="custom-ui-grid-pagination-container-full">
                                    {translate('PAGINATION.PREVIOUS')}
                                </span>
                                <span className="custom-ui-grid-pagination-container-minimized">
                                    <FaAngleLeft />
                                </span>
                            </Button>
                        </div>
                        <div className="custom-ui-grid-pagination-container-description">
                            <span>{translate('PAGINATION.PAGE')}</span>
                            <Textfield
                                min={totalPages ? 1 : 0}
                                label=""
                                type="number"
                                max={totalPages}
                                readOnly={isFetching || totalPages <= 1}
                                onChange={this.handleChange}
                                onBlur={this.handleOnPageChange}
                                value={isFetching ? '' : (totalPages ? inputValue : 0)}
                            />
                            <span>{translate('PAGINATION.PREPOSITION')}</span>
                            <span>{totalPages}</span>
                        </div>
                        <div className="custom-ui-grid-pagination-container-after">
                            <Button 
                                disabled={isNavigatableAfter} 
                                className="custom-ui-grid-pagination-container-after-next"
                                onClick={isNavigatableAfter ? null : this.handleOnClickNext}>
                                <span className="custom-ui-grid-pagination-container-full">
                                    {translate('PAGINATION.NEXT')}
                                </span>
                                <span className="custom-ui-grid-pagination-container-minimized">
                                    <FaAngleRight/>
                                </span>
                            </Button>
                            <Button 
                                disabled={isNavigatableAfter}
                                className="custom-ui-grid-pagination-container-after-last" 
                                onClick={isNavigatableAfter ? null : this.handleOnClickLast}>
                                <span className="custom-ui-grid-pagination-container-full">
                                    {translate('PAGINATION.LAST')}
                                </span>
                                <span className="custom-ui-grid-pagination-container-minimized">
                                    <FaAngleDoubleRight/>
                                </span>
                            </Button>
                        </div>
                    </div>
                )
            }</I18n>
        );
    }
}
