/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* UI ELEMENTS */
import {IconButton, Tooltip}  from 'react-mdl';

/* ICONS */
import MdAddCircle from 'react-icons/lib/md/add-circle';

/* STYLES */
import './GridAddButton.scss';

const CustomUIGridAddButton = ({
    handleAddEvent, 
    addTootltipLabel,
}) => (
    <Tooltip 
        position="right"
        label={<b>{addTootltipLabel || ''}</b>} 
        className="custom-ui-grid-add-button custom-ui-tooltip" >
        <IconButton 
            name="add-button" 
            component={MdAddCircle} 
            onClick={handleAddEvent}
        />
    </Tooltip>
);

CustomUIGridAddButton.propTypes = {
    handleAddEvent: PropTypes.func.isRequired,
    addTootltipLabel: PropTypes.string.isRequired,
};

export default CustomUIGridAddButton;