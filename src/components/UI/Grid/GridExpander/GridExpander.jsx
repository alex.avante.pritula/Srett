/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* ICONS */
import GoDiffAdded from 'react-icons/lib/go/diff-added';
import GoDiffRemoved from 'react-icons/lib/go/diff-removed';

/* STYLES */
import './GridExpander.scss';

const CustomUIGridExpander = ({isExpanded}) => (
    <div className="custom-ui-grid-expander">
        {isExpanded ? <GoDiffRemoved /> : <GoDiffAdded />}
    </div>
);

CustomUIGridExpander.propTypes = {
    isExpanded: PropTypes.any,
};

export default CustomUIGridExpander;
