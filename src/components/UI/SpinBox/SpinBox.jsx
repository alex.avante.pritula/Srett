/* REACT */
import React, {Component} from 'react';
import PropTypes from 'prop-types';

/* UI ELEMENTS */
import {Textfield} from 'react-mdl';

/* ICONS */
import FaAngleUp from 'react-icons/lib/fa/angle-up';
import FaAngleDown from 'react-icons/lib/fa/angle-down';

/* STYLES */
import './SpinBox.scss';

class CustomUISpinBox extends Component{
    static propTypes = {
        className: PropTypes.string,
    
        meta: PropTypes.shape({
            error: PropTypes.string,
        }),

        input: PropTypes.object.isRequired,
        label: PropTypes.string.isRequired,
        min: PropTypes.number,
        max: PropTypes.number,
        minLength: PropTypes.number,
    }

    componentWillMount() {
        this.props.input.onChange(this.spliceValue(+this.props.input.value));
    }

    handleChange = (e) => {
        const val = e.target.value;
        if (isNaN(+val)) return;
        this.props.input.onChange(val);
    };

    handleBlur = (e) => {
        this.props.input.onChange(this.spliceValue(+e.target.value));
    };

    makeDecrement = () => {
        const{input, min} = this.props;
        const val = +input.value; 
        if (val === min) return;
        input.onChange(this.spliceValue(val - 1));
    };

    makeIncrement = () => {
        const{input, max} = this.props;
        const val = +input.value; 
        if (val === max) return;
        input.onChange(this.spliceValue(val + 1));
    };

    spliceValue = (val) => {
        switch(this.props.minLength) {
            case 2:
            return ('0' + val).slice(-2);
            default:
            return val || '0';
        }
    };

    render() {
        const {
            min,
            max,
            meta: {error},
            input,
            label,
            className,
            ...custom} = this.props;

        return (
            <div className={`${className ? className :  ''} custom-ui-spinbox`}>
                <Textfield
                    {...input}
                    {...custom}
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    label={label}
                    error={error}
                    className="custom-ui-spinbox-textfield"
                />
                <div className="custom-ui-spinbox-range-navigation-container">
                    <button
                        type="button"
                        name="spin-box-up" 
                        onClick={this.makeIncrement}
                        className="custom-ui-spinbox-range-navigation-container-button"
                    ><FaAngleUp /></button>
    
                    <button
                        type="button"
                        name="spin-box-down" 
                        onClick={this.makeDecrement}
                        className="custom-ui-spinbox-range-navigation-container-button"
                    ><FaAngleDown /></button>
                </div>
            </div>);  
    }
};

export default CustomUISpinBox;
