/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* UI ELEMENTS */
import {Menu, MenuItem, Button} from 'react-mdl';

/* STYLES */
import './SelectField.scss';

class CustomUISelectField extends Component {

    static propTypes = {
        items: PropTypes.array.isRequired,
    }

    constructor(props) {
        super(props);
        this.state = {
            title: '',
        };
    }

    componentWillMount() {
        this.onSelectItem(this.props.input.value.toString());
    }

    componentWillReceiveProps({input}) {
        if (this.props.input.value !== input.value) {
            this.onSelectItem(input.value);
        }
    }

    onSelectItem = (value) => {
        this.props.items.forEach((item, index) => {
            if (item.value === value) {
                this.setState({
                    title: item.title,
                });
                this.props.input.onChange(value);
            }
        });
    }


    render() {
        const {
            id,
            items,
            className,
        } = this.props;
        const {title} = this.state;

        return (
            <div className={`${className ? className : ''} custom-ui-select-field`} >
                <Button type="button" id={id}
                    className="custom-ui-select-field-button" >
                    <span>{title}</span>
                </Button>
                <Menu target={id}>{
                    items.map((item, index) => {
                        return (
                            <MenuItem key={index} onClick={() => {
                                this.onSelectItem(item.value);
                            }}>
                                <span>{item.title}</span>
                            </MenuItem>
                        );
                    })
                }</Menu>
            </div>
        );
    }
}

export default CustomUISelectField;
