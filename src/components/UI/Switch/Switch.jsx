/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* UI ELEMENTS */
import {Switch} from 'react-mdl';

/* STYLES */
import './Switch.scss';

const CustomUISwitch = ({
    id,
    label,
    input,
    ripple,
    checked,
    onChange,
    className,
}) => {
    return (
        <Switch
            id={id}
            label={label}
            ripple={ripple}
            checked={input ? input.value : checked}
            onChange={(event) => {
                const value = event.target.checked;
                onChange ? onChange(value) : input.onChange(value);
            }}
            className={`${className ? className :  ''} custom-ui-switch`}
        />
    );
};

CustomUISwitch.propTypes = {
    id: PropTypes.string,
    checked: PropTypes.bool,
    label: PropTypes.string,
    onChange: PropTypes.func,
    className: PropTypes.string,
    
    input: PropTypes.shape({
        onCahnge: PropTypes.func,
    }),
};

export default CustomUISwitch;
