/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';
import {SubmissionError, reduxForm, Field, reset} from 'redux-form';

/* UI ELEMENTS */
import {Button, Spinner} from 'react-mdl';
/* CUSTOM */
import TextField from '../../components/UI/TextField/TextField';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';
import I18nModule from '../../utils/i18nModule';

/* ACTIONS */
import {signInCompleteApiCall} from '../../actions/User';

/* STYLES */
import './LogIn.scss';

/* CONSTANTS */
import {
    PAGES,
    VALIDATION,
    REDUX_FORM,
} from '../../constants/General'; 

const {START} = PAGES;
const {LOGIN} = REDUX_FORM;
const {EMAIL, PASSWORD} = VALIDATION;

class LogInComponent extends Component {

    static propTypes = {
        /* REDUX FORM META */
        pristine: PropTypes.bool.isRequired,
        submitting: PropTypes.bool.isRequired,
        handleSubmit: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);
    }

    render() {
        const {
            pristine,
            submitting,
            handleSubmit,
        } = this.props;   
        
        return (
            <I18n ns="translations">{
                (translate, {i18n}) => (
                    <section className="login-container">
                        <div className="login-container-header">
                            <img className="login-container-header-logo" src="../../../assets/images/logo.png"/>
                            <span className="login-container-header-title">
                                {translate('TITLE')}
                            </span>
                        </div>
                        <form className="login-container-form" onSubmit={handleSubmit}>
                            <Field  className="login-container-form-label"
                                type="text" 
                                name="email" 
                                floatingLabel
                                component={TextField} 
                                label={translate('AUTH.LABEL.EMAIL')} 
                            />
                            <Field className="login-container-form-label"
                                type="password" 
                                name="password" 
                                floatingLabel
                                component={TextField} 
                                label={translate('AUTH.LABEL.PASSWORD')}  
                            />
                            <div className="login-container-form-submit">
                                <Button type="submit" primary raised disabled={submitting || pristine}>{
                                    submitting ? 
                                        <span> 
                                            <Spinner singleColor/> 
                                            {translate('BUTTON_REQUEST')}  
                                        </span>
                                    :  
                                        <span>
                                            {translate('AUTH.LOG_IN')} 
                                        </span> 
                                }</Button>
                            </div>
                        </form>
                    </section>
                )
            }</I18n>
        );
    }
}

/* REDUX */
const mapStateToProps = () => { return {}; };
const mapDispatchToProps = ({});

/* REDUX FORM */
const validate = ({email, password}) => {
    const errors = {};

    if(!email) {
        errors.email = I18nModule.t('REDUX_FORM_ERROR.EMAIL.EMPTY');
    } else {
        if (!email.match(EMAIL)) {
            errors.email = I18nModule.t('REDUX_FORM_ERROR.EMAIL.VALIDATION');
        }
    }

    if(!password) {
        errors.password = I18nModule.t('REDUX_FORM_ERROR.PASSWORD.EMPTY');
    } else {
        if (!password.match(PASSWORD)) {
            errors.password = I18nModule.t('REDUX_FORM_ERROR.PASSWORD.VALIDATION');
        }
    }

    return errors;
};

const loginReduxForm = {
    form: LOGIN,
    validate: (values, props) => {
        const {dirty} = props;
        if (!dirty) return {};
        return validate(values);
    },
    onSubmit: (values, dispatch) => {
        const errors = validate(values);
        if(Object.keys(errors).length > 0) {
            throw new SubmissionError(errors);
        }
        return signInCompleteApiCall(values, dispatch);
    },
    onSubmitSuccess: (result, dispatch) => {
        dispatch(reset(LOGIN));
    },
};

const bindedReduxFormLogInComponent = reduxForm(loginReduxForm)(LogInComponent);
const connectedLogInComponent = connect(mapStateToProps, mapDispatchToProps)(bindedReduxFormLogInComponent);
export default connectedLogInComponent;
