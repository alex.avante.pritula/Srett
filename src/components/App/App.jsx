/* REACT */
import React, {Component} from 'react';
import 'dialog-polyfill/dialog-polyfill.css';

/* STYLES */
import './App.scss';

export default ({children}) => (
    <section className="app-container">
        {children}
    </section>
);
