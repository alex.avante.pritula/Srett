
/* GENERAL */
import classnames from 'classnames';

/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';
import {SubmissionError, reduxForm, Field, reset} from 'redux-form';

/* UI ELEMENTS */
import {IconButton} from 'react-mdl';
/* CUSTOM */
import Grid from '../../UI/Grid/Grid';
import DateTimePicker from '../../UI/DateTimePicker/DateTimePicker';

/* ICONS */
import FaMinus from 'react-icons/lib/fa/minus';
import TiIcon from 'react-icons/lib/ti/download';
/* CUSTOM */
import Map from '../../General/StatusIcons/Map/Map';
import Battery from '../../General/StatusIcons/Battery/Battery';
import Measure from '../../General/StatusIcons/Measure/Measure';

/* ACTIONS */
import {
    onDevicePositionsGridPageChangedCall,
    onDevicePositionsGridSortChangedCall,
    getDevicePositionsByTimeRangeApiCall,
    getDevicePositionsByTimeRangeCompleteApiCall,
} from '../../../actions/DevicePositions';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';
import i18nModule, {moment} from '../../../utils/i18nModule';

/* STYLES */
import './OrbitrackerDetailedPositions.scss';

/* CONSTANTS */
import {
    NOW,
    SORT_ORDER,
    REDUX_FORM,
    DEFAULT_SORTING,
    CSV_EXPORT_META,
    GRID_EXPANDER_SIZE,
    DEFAULT_DATE_TIME_FORMAT,
} from '../../../constants/General';

const {DEVICE_DETAILED_POSITIONS} =  REDUX_FORM;
const {
    PREFIX,
    NEW_ROW,    
    SEPARATOR,
} = CSV_EXPORT_META;

const {ASC, DESC} = SORT_ORDER;

class OrbitrackerDetailedPositionsComponent extends Component {

    static propTypes = {
        /* META */
        isFetching: PropTypes.bool.isRequired,

        /* REDUX FORM META */
        invalid:  PropTypes.bool.isRequired,
        pristine: PropTypes.bool.isRequired,
        submitting: PropTypes.bool.isRequired,
        handleSubmit: PropTypes.func.isRequired,

        /* REDUX STATE PROPS */
        to: PropTypes.object.isRequired, 
        from: PropTypes.object.isRequired,

        current: PropTypes.number.isRequired,
        totalPages: PropTypes.number.isRequired,

        language: PropTypes.string.isRequired,
        positions: PropTypes.object.isRequired,
        visibleItems: PropTypes.object.isRequired,

        identifier: PropTypes.string.isRequired,

        /* ACTIONS */
        onDevicePositionsGridPageChangedCall: PropTypes.func.isRequired,
        onDevicePositionsGridSortChangedCall: PropTypes.func.isRequired,
        getDevicePositionsByTimeRangeCompleteApiCall: PropTypes.func.isRequired,
    };

    constructor(props){
        super(props);

        /* BINDED HANDLERS */
        this.handlePageChange = this.handlePageChange.bind(this);
        this.handleSortedChange = this.handleSortedChange.bind(this);
    }

    componentWillMount(){
        const {
            to,
            from,
            identifier,
            getDevicePositionsByTimeRangeCompleteApiCall,
        } = this.props;

        getDevicePositionsByTimeRangeCompleteApiCall({
            to,
            from,
            identifier,
        });
    }

    handlePageChange(page){
        this.props.onDevicePositionsGridPageChangedCall(page);
    }

    handleSortedChange(sorted){
        const {id, desc} = sorted[0] || {};
        const sortOrder = desc ? DESC : ASC;
        const sortField = id || DEFAULT_SORTING.DETAILED_POSITIONS;
        this.props.onDevicePositionsGridSortChangedCall(sortField, sortOrder);
    }

    render() {

        const {
            to,

            language,
            positions,
            visibleItems,

            identifier,

            pristine,
            submitting,
            handleSubmit,

            current,
            totalPages,

            isFetching,
        } = this.props;  

        const EXPORT_DISABLE = (positions.size < 1 || submitting || isFetching);

        const EXPORT_TITLE  = [
                i18nModule.t('ORBITRACKER.GRID.DETAILED_POSITIONS.DATE'),
                i18nModule.t('ORBITRACKER.GRID.DETAILED_POSITIONS.LATITUDE'),
                i18nModule.t('ORBITRACKER.GRID.DETAILED_POSITIONS.LONGITUDE'),
                i18nModule.t('ORBITRACKER.GRID.DETAILED_POSITIONS.BATTERY'),
                i18nModule.t('ORBITRACKER.GRID.DETAILED_POSITIONS.TIME_TO_FIX'),
                i18nModule.t('ORBITRACKER.GRID.DETAILED_POSITIONS.REBOOTS'),
                i18nModule.t('ORBITRACKER.GRID.DETAILED_POSITIONS.STATUS'),
                i18nModule.t('ORBITRACKER.GRID.DETAILED_POSITIONS.FW_VERSION'),
            ].join(SEPARATOR);

        const EXPORT_FORMATTED_ITEMS = positions.map((item) => {
            const {
                status,
                battery,
                reboots,
                firmware,
                latitude,
                longitude,
                timetofix,
                lastmeasure,
            } = item;

            return [
                lastmeasure.format(DEFAULT_DATE_TIME_FORMAT),
                latitude,
                longitude,
                battery,
                timetofix,
                reboots,
                status,
                firmware,
            ].join(SEPARATOR);
        });

        const EXPORT = [EXPORT_TITLE, ...EXPORT_FORMATTED_ITEMS].join(NEW_ROW);

        return (
            <I18n ns="translations">{
                (translate) => (
                    <section className="orbitracker-modal-container">
                        <div className="orbitracker-modal-container-navigation">
                            <form className="orbitracker-modal-container-navigation-form">
                                <Field
                                    label=""
                                    name="from" 
                                    max={to}
                                    locale={language}
                                    component={DateTimePicker} 
                                    disabled={submitting || isFetching}
                                    className="orbitracker-modal-container-navigation-form-datetimepicker"
                                />
                                <FaMinus className="orbitracker-modal-container-navigation-form-line" />
                                <Field
                                    label=""
                                    name="to" 
                                    max={NOW}
                                    locale={language}
                                    component={DateTimePicker} 
                                    disabled={submitting || isFetching}
                                    className="orbitracker-modal-container-navigation-form-datetimepicker"
                                />
                            </form>
                            {EXPORT_DISABLE ? 
                                <IconButton 
                                    component={TiIcon} 
                                    disabled={EXPORT_DISABLE}
                                    name="orbitracker-modal-container-navigation-export" 
                                    className="orbitracker-modal-container-navigation-export"
                                /> :
                                <a target="_blank" download={`${identifier}.csv`} href={`${PREFIX},${EXPORT}`}>
                                    <IconButton 
                                        component={TiIcon}
                                        name="orbitracker-modal-container-navigation-export" 
                                        className="orbitracker-modal-container-navigation-export"
                                    />
                                </a>
                            }
                        </div>
                        <div className="orbitracker-detailed-positions-grid-wrapper">
                            <Grid 
                                data={
                                    visibleItems.toArray().map((item) => {
                                        const {
                                            battery,
                                            reboots,
                                            firmware,
                                            latitude,
                                            longitude,
                                            timetofix,
                                            lastmeasure,
                                            lastWakeUpMode,
                                        } = item;
                                        
                                        return {
                                            lastmeasure: (
                                                <span className="grid-container-row-item grid-container-row-item-title">
                                                    {lastmeasure ? lastmeasure.format(DEFAULT_DATE_TIME_FORMAT) : <b>-</b>}
                                                </span>
                                            ),

                                            timetofix: (
                                                <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-centered">
                                                    {timetofix || <b>-</b>}
                                                </span>
                                            ),

                                            status: (
                                                <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-centered grid-container-row-item-bold">
                                                    <Measure lastWakeUpMode={lastWakeUpMode} />
                                                    <Battery value={battery} onlyError={true}/>
                                                    <Map latitude={latitude} longitude={longitude} />
                                                </span>
                                            ),

                                            reboots: (
                                                <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-centered">
                                                    {parseInt(reboots) || <b>-</b>}
                                                </span>
                                            ),

                                            battery: (
                                                <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-centered">
                                                    {battery ? `${battery} %`: <b>-</b>}
                                                </span>
                                            ),

                                            firmware: (
                                                <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-centered">
                                                    {firmware || <b>-</b>}
                                                </span>
                                            ),

                                            latitude: (
                                                <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-centered grid-container-row-item-bold">
                                                    {(parseFloat(latitude) || <b>-</b>)}
                                                </span>
                                            ),

                                            longitude: (
                                                <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-centered grid-container-row-item-bold">
                                                    {(parseFloat(longitude) || <b>-</b>)}
                                                </span>
                                            ),
                                        };
                                    })
                                }
                                page={current}
                                defaultSorted={[{
                                    desc: true,
                                    id: DEFAULT_SORTING.DETAILED_POSITIONS,
                                }]}
                                hasCustomExpander={true}
                                columns={[{
                                        Header: (
                                            <span className="grid-container-column-title">
                                                {translate('ORBITRACKER.GRID.DETAILED_POSITIONS.DATE')}
                                            </span>
                                        ),
                                        accessor: 'lastmeasure',
                                        width: GRID_EXPANDER_SIZE * 4,
                                    },{
                                        Header:  (
                                            <span className="grid-container-column-title">
                                                {translate('ORBITRACKER.GRID.DETAILED_POSITIONS.LATITUDE')}
                                            </span>
                                        ),
                                        accessor: 'latitude',
                                    },{
                                        Header:  (
                                            <span className="grid-container-column-title">
                                                {translate('ORBITRACKER.GRID.DETAILED_POSITIONS.LONGITUDE')}
                                            </span>
                                        ),
                                        accessor: 'longitude',
                                    },{
                                        Header:  (
                                            <span className="grid-container-column-title">
                                                {translate('ORBITRACKER.GRID.DETAILED_POSITIONS.BATTERY')}
                                            </span>
                                        ),
                                        accessor: 'battery',
                                    },{
                                        Header:  (
                                            <span className="grid-container-column-title">
                                                {translate('ORBITRACKER.GRID.DETAILED_POSITIONS.TIME_TO_FIX')}
                                            </span>
                                        ),
                                        accessor: 'timetofix',
                                    },{
                                        Header:  (
                                            <span className="grid-container-column-title">
                                                {translate('ORBITRACKER.GRID.DETAILED_POSITIONS.REBOOTS')}
                                            </span>
                                        ),
                                        accessor: 'reboots',
                                    },{
                                        Header:  (
                                            <span className="grid-container-column-title">
                                                {translate('ORBITRACKER.GRID.DETAILED_POSITIONS.STATUS')}
                                            </span>
                                        ),
                                        sortable: false,
                                        accessor: 'status',
                                    },{
                                        Header:  (
                                            <span className="grid-container-column-title">
                                                {translate('ORBITRACKER.GRID.DETAILED_POSITIONS.FW_VERSION')}
                                            </span>
                                        ),
                                        accessor: 'firmware',
                                }]}
                                isFetching={isFetching}
                                totalPages={totalPages}
                                handlePageChange={this.handlePageChange}
                                handleSortedChange={this.handleSortedChange}
                            />
                        </div>  
                    </section>
                )
            }</I18n>
        );
    }
}

/* REDUX */
const mapStateToProps = (state) => {
    const {
        User, 
        Dialog, 
        DevicePositions,
    } = state;

    const {meta} = Dialog;
    const {language} = User;
    const {identifier} = meta;

    const {
        to,
        from, 
        positions,
        visibleItems,

        current,
        totalPages,
        isFetching,
    } = DevicePositions;

    return {
        to,
        from,
        language,
        positions,
        visibleItems,

        identifier,

        current,
        totalPages,
        isFetching,

        initialValues: {from, to},
    }; 
};

const mapDispatchToProps = ({
    onDevicePositionsGridPageChangedCall,
    onDevicePositionsGridSortChangedCall,
    getDevicePositionsByTimeRangeCompleteApiCall,
});

/* REDUX FORM */
const validate = ({to, from}) => {
    const errors = {};
    const today = moment();

    if(today < from) {
        errors.from  = i18nModule.t('REDUX_FORM_ERROR.DATE.NOW');
    }

    if(today < to) {
        errors.to = i18nModule.t('REDUX_FORM_ERROR.DATE.NOW');
    } 

    if(!errors.from && !errors.to && from > to) {
        errors.to = errors.from = i18nModule.t('REDUX_FORM_ERROR.DATE.RANGE');
    }

    return errors;
};

const OrbitrackerDetailedPositionsForm = {
    form: DEVICE_DETAILED_POSITIONS,
    enableReinitialize: true,
    validate: (values, props) => {
        const {dirty} = props;
        if (!dirty) return {};
        return validate(values);
    },
    onChange: (values, dispatch, props) => {
        const {from, to} = values;
        const {identifier} = props;

        return getDevicePositionsByTimeRangeApiCall({
            to,
            from,
            identifier,
        }, dispatch);
    },
};

const bindedReduxOrbitrackerDetailedPositionsComponent = reduxForm(OrbitrackerDetailedPositionsForm)(OrbitrackerDetailedPositionsComponent);
const connectedOrbitrackerDetailedPositionsComponent = connect(mapStateToProps, mapDispatchToProps)(bindedReduxOrbitrackerDetailedPositionsComponent);
export default connectedOrbitrackerDetailedPositionsComponent;
