/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';

/* ICONS */
import FaCog from 'react-icons/lib/fa/cog';
import TiCompass from 'react-icons/lib/ti/compass';
/* CUSTOM */
import Map from '../../General/StatusIcons/Map/Map';
import RSSI from '../../General/StatusIcons/RSSI/RSSI';
import Battery from '../../General/StatusIcons/Battery/Battery';
import Measure from '../../General/StatusIcons/Measure/Measure';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';

/* STYLES */
import './OrbitrackerGridItem.scss';

/* CONSTANTS */
import {RSSI_META}  from '../../../constants/General';
const {SUFFIX} = RSSI_META;

class OrbitrackerGridItemComponent extends Component {

    static propTypes = {
       rssi: PropTypes.string,
       battery: PropTypes.string,
       lastmeasure: PropTypes.any,
       firmware: PropTypes.string,
       latitude: PropTypes.string,
       longitude: PropTypes.string,
       timetofix: PropTypes.string,
       wakeUpMode: PropTypes.number,
       lastWakeUpMode: PropTypes.number,
    };

    constructor(props) {
        super(props);
    }

    render() {
        const {
            rssi,
            reboots,
            battery,
            firmware,
            latitude,
            longitude,
            timetofix,
            wakeUpMode,
            lastmeasure,
            lastWakeUpMode,
        } = this.props;

        return (
            <I18n ns="translations">{
                (translate, {i18n}) => (
                    <section className="orbitracker-container">
                        <div className="orbitracker-container-description">
                            <div className="orbitracker-container-description-item">
                                <span className="orbitracker-container-description-item-title">
                                    {translate('ORBITRACKER.ITEM.LATITUDE')}
                                </span>
                                <div className="orbitracker-container-description-item-content">
                                    <p className="orbitracker-container-description-item-content-label">
                                        {parseFloat(latitude) || <b>-</b>}
                                    </p>
                                </div>
                            </div>
                            <div className="orbitracker-container-description-item">
                                <span className="orbitracker-container-description-item-title">
                                    {translate('ORBITRACKER.ITEM.LONGITUDE')}
                                </span>
                                <div className="orbitracker-container-description-item-content">
                                    <p className="orbitracker-container-description-item-content-label">
                                        {parseFloat(longitude) || <b>-</b>}
                                    </p>
                                </div>
                            </div>
                            <div className="orbitracker-container-description-item">
                                <span className="orbitracker-container-description-item-title">
                                    {translate('ORBITRACKER.ITEM.REBOOT_COUNTER')}
                                </span>
                                <div className="orbitracker-container-description-item-content">
                                    <p className="orbitracker-container-description-item-content-label">
                                        {parseInt(reboots) || <b>-</b>}
                                    </p>
                                </div>
                            </div>
                            <div className="orbitracker-container-description-item">
                                <span className="orbitracker-container-description-item-title">
                                    {translate('ORBITRACKER.ITEM.WAKE_UP_MODE')}
                                </span>
                                <div className="orbitracker-container-description-item-content">
                                    <p className="orbitracker-container-description-item-content-label">
                                       {wakeUpMode !== undefined ? translate(`WAKE_UP_MODE.${wakeUpMode}`): '-'} 
                                    </p>
                                </div>
                            </div>
                            <div className="orbitracker-container-description-item">
                                <span className="orbitracker-container-description-item-title">
                                    {translate('ORBITRACKER.ITEM.RSSI')}
                                </span>
                                <div className="orbitracker-container-description-item-content">
                                    <RSSI value={rssi} />
                                    <p className="orbitracker-container-description-item-content-label">
                                        {rssi ? `${rssi} ${SUFFIX}`: <b>-</b>}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="orbitracker-container-description">
                            <div className="orbitracker-container-description-item">
                                <span className="orbitracker-container-description-item-title">
                                    {translate('ORBITRACKER.ITEM.BATTERY_LEVEL')}
                                </span>
                                <div className="orbitracker-container-description-item-content">
                                    <Battery value={battery}/>
                                    <p className="orbitracker-container-description-item-content-label">
                                        {battery ? `${battery} %`: <b>-</b>}
                                    </p>
                                </div>
                            </div>
                            <div className="orbitracker-container-description-item">
                                <span className="orbitracker-container-description-item-title">
                                    {translate('ORBITRACKER.ITEM.TIME_TO_FIX')}
                                </span>
                                <div className="orbitracker-container-description-item-content">
                                   <p className="orbitracker-container-description-item-content-label">
                                        {timetofix || <b>-</b>}
                                   </p>
                                </div>
                            </div>
                            <div className="orbitracker-container-description-item">
                                <span className="orbitracker-container-description-item-title">
                                    {translate('ORBITRACKER.ITEM.FIRMWARE_VERSION')}
                                </span>
                                <div className="orbitracker-container-description-item-content">
                                    <p className="orbitracker-container-description-item-content-label">
                                        {firmware || <b>-</b>}
                                    </p>
                                </div>
                            </div>
                            <div className="orbitracker-container-description-item">
                                <span className="orbitracker-container-description-item-title">
                                    {translate('ORBITRACKER.ITEM.LAST_WAKE_UP')}
                                </span>
                                <div className="orbitracker-container-description-item-content">
                                   <p className="orbitracker-container-description-item-content-label">
                                        {lastWakeUpMode !== undefined ? translate(`WAKE_UP_MODE.${lastWakeUpMode}`): '-'} 
                                   </p>
                                </div>
                            </div>
                            <div className="orbitracker-container-description-item">
                                <span className="orbitracker-container-description-item-title">
                                    {translate('ORBITRACKER.ITEM.STATUS')}
                                </span>
                                <div className="orbitracker-container-description-item-content">
                                    <Measure lastWakeUpMode={lastWakeUpMode} lastmeasure={lastmeasure} />
                                    <Battery value={battery} onlyError={true} />
                                    <RSSI value={rssi} onlyError={true} lastmeasure={lastmeasure}/>
                                    <Map latitude={latitude} longitude={longitude} />
                                </div>
                            </div>
                        </div>
                    </section>
                )
            }</I18n>
        );
    }
}

/* REDUX */
const mapStateToProps = ({Orbitracker}, {index, orbitracker}) => {
    const {orbitrackers} = Orbitracker;

    const {
        rssi,
        reboots,
        battery,
        firmware,
        latitude,
        longitude,
        timetofix,
        wakeUpMode,
        lastmeasure,
        lastWakeUpMode,
    } = orbitrackers.get(index) || orbitracker || {};

    return { 
        rssi,
        reboots,
        battery,
        firmware,
        latitude,
        longitude,
        timetofix,
        wakeUpMode,
        lastmeasure,
        lastWakeUpMode,
    };
};

const mapDispatchToProps = ({});

export default connect(mapStateToProps, mapDispatchToProps)(OrbitrackerGridItemComponent);
