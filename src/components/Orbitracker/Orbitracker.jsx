/* GENERAL */
import classnames from 'classnames';

/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';

/* UI ELEMENTS */
import {IconButton, Tooltip}  from 'react-mdl';
/* CUSTOM */
import Grid from '../UI/Grid/Grid';

/* COMPONENTS */
import OrbitrackerGridItem from './OrbitrackerGridItem/OrbitrackerGridItem';

/* ICONS */
import TiSpanner from 'react-icons/lib/ti/spanner';
import MdViewHeadline from 'react-icons/lib/md/view-headline';

/* ACTIONS */
import {
    resetOrbitrackersGridCall,
    getOrbitrackerItemsCompleteApiCall,
} from '../../actions/Orbitracker';

import {dialogOpenCall} from '../../actions/Dialog';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';
import i18nModule from '../../utils/i18nModule';

/* UTILS */
import equipmentTitle from '../../utils/equipmentTitle';
import orbitrackerTitle from '../../utils/orbitrackerTitle';

/* STYLES */
import './Orbitracker.scss';
import '../UI/Grid/Grid.scss';

/* CONSTANTS */
import {
    DIALOGS,
    SORT_ORDER,
    NULL_ENTITY,
    DEFAULT_SORTING,
    GRID_EXPANDER_SIZE,
    DEFAULT_DATE_TIME_FORMAT,
} from '../../constants/General';

const {ASC, DESC} = SORT_ORDER;
const {
    DETAILED_POSITIONS,
    CONFIGURE_ORBITRACKER,
} = DIALOGS;

class OrbitrackerComponent extends Component {
    
    static propTypes = {
        /* META */
        isOpened: PropTypes.bool.isRequired,
        isFetching: PropTypes.bool.isRequired,

        /* GRID META */
        filter: PropTypes.string.isRequired,
        current: PropTypes.number.isRequired,
        sortField: PropTypes.string.isRequired,
        sortOrder: PropTypes.string.isRequired,
        totalPages: PropTypes.number.isRequired, 

        /* GRID DATA */
        orbitrackers: PropTypes.object.isRequired,

        /* ACTIONS */
        resetOrbitrackersGridCall: PropTypes.func.isRequired,
        getOrbitrackerItemsCompleteApiCall: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        /* BINDED HANDLERS */
        this.handlePageChange = this.handlePageChange.bind(this);
        this.handleSortedChange = this.handleSortedChange.bind(this);
        this.handleSubGridComponent = this.handleSubGridComponent.bind(this);
        this.handleOpenDetailedPosition = this.handleOpenDetailedPosition.bind(this);
        this.handleConfigureOrbitracker = this.handleConfigureOrbitracker.bind(this);
    }

    componentWillMount(){
        this.props.getOrbitrackerItemsCompleteApiCall({
            page: 0, 
            filter: '',
            sortOrder: ASC,
            sortField: DEFAULT_SORTING.ORBITRACKER, 
        });
    }

    componentWillUnmount(){
       this.props.resetOrbitrackersGridCall();
    }

    handleOpenDetailedPosition(identifier){
        this.props.dialogOpenCall({
            identifier,
            dialogName: DETAILED_POSITIONS,
            customHeader: this.createCustomHeader(identifier),
        });
    }
    
    handleConfigureOrbitracker(identifier){
        const {CONFIGURE_ORBITRACKER} = DIALOGS;

        this.props.dialogOpenCall({
            dialogName: CONFIGURE_ORBITRACKER,
            identifier,
            customHeader: this.createCustomHeader(identifier),
        });
    }

    createCustomHeader(identifier) {
        const title = i18nModule.t('ORBITRACKER.DIALOGS.CONFIGURE_ORBITRACKER');
        return `${orbitrackerTitle(identifier)} ${title}`;
    }

    handlePageChange(page){
        const {
            filter,
            sortField,
            sortOrder,
            getOrbitrackerItemsCompleteApiCall,
        } = this.props;

        getOrbitrackerItemsCompleteApiCall({
            page, 
            filter,
            sortField, 
            sortOrder,
        });
    }

    handleSortedChange(sorted){
        const {
            filter,
            current,
            getOrbitrackerItemsCompleteApiCall,
        } = this.props;

        const {id, desc} = sorted[0] || {};
        const sortOrder = desc ? DESC : ASC;
        const sortField = id || DEFAULT_SORTING.ORBITRACKER;

        getOrbitrackerItemsCompleteApiCall({
            filter,
            sortField, 
            sortOrder,
            page: current,
        });
    }

    handleSubGridComponent({index}){
        return (<OrbitrackerGridItem index={index} />);
    }

    render() {
        const {
            isOpened,

            current,
            isFetching,
            totalPages,

            orbitrackers,
        } = this.props;

        return (
            <I18n ns="translations">{
                (translate) => (
                    <div className="orbitracker-grid-wrapper">
                        <Grid 
                            data={
                                orbitrackers.toArray().map((item) => {
                                    const {
                                        identifier,
                                        lastmeasure,
                                        equipmentRef,
                                    } = item;

                                    return {
                                        identifier: (
                                            <span className="grid-container-row-item grid-container-row-item-title">
                                                {orbitrackerTitle(identifier)}
                                            </span>
                                        ),
                        
                                        equipmentRef: (
                                            <span className="grid-container-row-item grid-container-row-item-title">
                                                {equipmentTitle(equipmentRef)}
                                            </span>
                                        ),
                        
                                        navigation: (
                                            <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-button-navigation grid-container-row-item-centered">
                                                <Tooltip 
                                                    label={translate('ORBITRACKER.TOOLTIP.DETAILED_POSITIONS')}
                                                    position="left"
                                                    className="custom-ui-tooltip">
                                                    <IconButton 
                                                        disabled={isOpened}
                                                        component={MdViewHeadline} 
                                                        name="open-detailed-positions-button" 
                                                        onClick={(isOpened ? null : () => this.handleOpenDetailedPosition(identifier))}
                                                    />
                                                </Tooltip>
                                                <Tooltip
                                                    label={translate('ORBITRACKER.TOOLTIP.CONFIGURE_ORBITRACKER')}
                                                    position="left"
                                                    className="custom-ui-tooltip">
                                                    <IconButton
                                                        disabled={isOpened} 
                                                        component={TiSpanner} 
                                                        name="configure-orbitracker-button" 
                                                        onClick={(isOpened ? null : () => this.handleConfigureOrbitracker(identifier))}
                                                    />
                                                </Tooltip>
                                            </span>
                                        ),
                        
                                        last_measure_date: (
                                            <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-centered grid-container-row-item-bold">
                                                {lastmeasure ? lastmeasure.format(DEFAULT_DATE_TIME_FORMAT) : <b>-</b>}
                                            </span> 
                                        ),
                                    };
                                })
                            }
                            page={current}
                            defaultSorted={[{
                                asc: true,
                                id: DEFAULT_SORTING.ORBITRACKER,
                            }]}
                            columns={[{
                                    Header: (
                                        <span className="grid-container-column-title">
                                            {translate('ORBITRACKER.GRID.MAIN.TRACKER_ID')}
                                        </span>
                                    ),
                                    accessor: 'identifier',
                                },{
                                    Header:  (
                                        <span className="grid-container-column-title">
                                            {translate('ORBITRACKER.GRID.MAIN.EQUIPMENT_REFERNCE')}
                                        </span>
                                    ),
                                    sortable: false,
                                    accessor: 'equipmentRef',
                                },{
                                    sortable: false,
                                    accessor: 'navigation',
                                    width: GRID_EXPANDER_SIZE * 2,
                                },{
                                    Header: (
                                        <span className="grid-container-column-title">
                                            {translate('ORBITRACKER.GRID.MAIN.LAST_MEASURE')}
                                        </span>
                                    ),
                                    accessor: 'last_measure_date',
                                    width: GRID_EXPANDER_SIZE * 4,
                            }]}
                            isFetching={isFetching}
                            totalPages={totalPages}
                            handlePageChange={this.handlePageChange}
                            handleSortedChange={this.handleSortedChange}
                            handleSubGridComponent={this.handleSubGridComponent}
                        />
                    </div>
                )
            }</I18n>    
        );
    }
}

/* REDUX */
const mapStateToProps = ({Orbitracker, Dialog}) => {
    const {  
        filter,
        current,
        sortField,
        sortOrder,
        
        isFetching,
        totalPages,

        orbitrackers,
    } = Orbitracker;

    const {isOpened} = Dialog;

    return {
        isOpened,

        filter,
        current,
        sortField,
        sortOrder,

        isFetching,
        totalPages,

        orbitrackers,
    };
};

const mapDispatchToProps = ({
    dialogOpenCall,
    resetOrbitrackersGridCall,
    getOrbitrackerItemsCompleteApiCall,
});

const connectedOrbitrackerComponent = connect(mapStateToProps, mapDispatchToProps)(OrbitrackerComponent);
export default connectedOrbitrackerComponent;
