/* GENERAL */
import classnames from 'classnames';

/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* LOCALIZATION */
import { I18n, Trans } from 'react-i18next';

/* UI ELEMENTS */
import {Tab, Tabs} from 'react-mdl';

/* COMPONENTS */
import OrbitrackerActivity from './OrbitrackerActivity/OrbitrackerActivity';
import OrbitrackerParameters from './OrbitrackerParameters/OrbitrackerParameters';

/* STYLES */
import './OrbitrackerConfigure.scss';

class OrbitrackerConfigure extends Component {

    static propTypes = {
       
    };

    constructor(props) {
        super(props);
        this.state = { activeTab: 0 };
    }

    get ActiveTab () {
        switch (this.state.activeTab) {
            case 0:
                return (
                    <OrbitrackerParameters />
                );
                break;
            case 1:
                return (
                    <OrbitrackerActivity />
                );
                break;
            default:
                return (
                    <OrbitrackerParameters />
                );
                break;
        }
    }


    render() {
        return (
            <I18n ns="translations">{
                (translate, { i18n }) => (
                    <section className="orbitracker-modal-container">
                        <div className="orbitracker-details-tabs">
                            <Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })} ripple>
                                <Tab>{translate('ORBITRACKER.TABS.PARAMETERS')}</Tab>
                                <Tab>{translate('ORBITRACKER.TABS.ACTIVITY')}</Tab>
                            </Tabs>
                            <div className="orbitracker-details-tab">
                                {this.ActiveTab}
                            </div>
                        </div>    
                    </section>
                )
            }</I18n>
        );
    }
}

export default OrbitrackerConfigure;