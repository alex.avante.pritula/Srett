/* GENERAL */
import classnames from 'classnames';

/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';

/* UI ELEMENTS */
import Grid from '../../../UI/Grid/Grid';

/* UTILS */
import transformDescriptions from '../../../../utils/deviceChangedSettingsDescription';

/* ACTIONS */
import {
    resetDeviceActivityGridCall,
    getDeviceActivityItemsCompleteApiCall,
} from '../../../../actions/DeviceActivity';

/* STYLES */
import './OrbitrackerActivity.scss';

/* CONSTANTS */
import {
    SORT_ORDER,
    PROCESS_TYPE,
    DEVICE_ACTIVITY_STATUS,
    DEFAULT_DATE_TIME_FORMAT,
} from '../../../../constants/General';

const {
    REBOOT,
    WRITE_PARAMETERS,
} = PROCESS_TYPE;
const {ASC, DESC} = SORT_ORDER;
const {FINISHED} = DEVICE_ACTIVITY_STATUS;

class OrbitrackerActivityComponent extends Component {

    static propTypes = {
        /* META */
        isFetching: PropTypes.bool.isRequired,

        /* GRID META */
        current: PropTypes.number.isRequired,
        sortField: PropTypes.string.isRequired,
        sortOrder: PropTypes.string.isRequired,
        totalPages: PropTypes.number.isRequired, 

        /* GRID DATA */
        processes: PropTypes.object.isRequired,

        /* REDUX STATE PROPS */
        identifier: PropTypes.string.isRequired,

        /* ACTIONS */
        resetDeviceActivityGridCall: PropTypes.func.isRequired,
        getDeviceActivityItemsCompleteApiCall: PropTypes.func.isRequired,
    };

    constructor(props){
        super(props);

        /* BINDED HANDLERS */
        this.handlePageChange = this.handlePageChange.bind(this);
        this.handleSortedChange = this.handleSortedChange.bind(this);
    }

    componentWillMount(){
        const {
            identifier,
            getDeviceActivityItemsCompleteApiCall,
        } = this.props;

        getDeviceActivityItemsCompleteApiCall({
            sortField: 'creation_date', 
            sortOrder: DESC,
            page: 0,
            filter: identifier,
        });
    }

    componentWillUnmount(){
        this.props.resetDeviceActivityGridCall();
    }

    handlePageChange(page){
        const {
            sortField,
            sortOrder,
            identifier,

            getDeviceActivityItemsCompleteApiCall,
        } = this.props;

        getDeviceActivityItemsCompleteApiCall({
            page, 
            sortField, 
            sortOrder,
            filter: identifier,
        });
    }

    handleSortedChange(sorted){
        const {
            current,
            identifier,

            getDeviceActivityItemsCompleteApiCall,
        } = this.props;

        const {id, desc} = sorted[0] || {};
        const sortOrder = desc ? DESC : ASC;
        const sortField = id || 'creation_date';

        getDeviceActivityItemsCompleteApiCall({
            sortField, 
            sortOrder,
            page: current,
            filter: identifier,
        });
    }

    render() {
        const {
            current,
            isFetching,
            totalPages,

            processes,
        } = this.props;

        return (
            <I18n ns="translations">{
                (translate) => (
                    <section className="orbitracker-activity-modal-container">
                        <div className="orbitracker-activity-grid-item">
                            <Grid
                                data={
                                    processes.toArray().map((item) => {
                                        const {
                                            status,
                                            payload,
                                            processType,
                                            creationDate,
                                            updationDate,
                                        } = item;

                                        const title = Object.keys(PROCESS_TYPE)[Object.values(PROCESS_TYPE).indexOf(processType)];
                                        let description = [];

                                        if(processType === WRITE_PARAMETERS){
                                            const parameters = transformDescriptions(payload);
                                            for(let i in parameters){
                                                description.push(<li key={i}>{i}: <code>{parameters[i]}</code>;</li>);
                                            }
                                        }

                                        return {
                                            creation_date: (
                                                <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-bold">
                                                    {creationDate ? creationDate.format(DEFAULT_DATE_TIME_FORMAT): <b>-</b>}
                                                </span>
                                            ),

                                            description: (
                                                <span className="grid-container-row-item grid-container-row-item-title">
                                                    <div>
                                                        <p className="grid-container-row-item-description-title">{translate(`ORBITRACKER.ACTIVITY.${title}`)}</p>
                                                        <ul className="grid-container-row-item-description-list">{description}</ul>
                                                    </div>
                                                </span>
                                            ),

                                            status: (
                                                <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-bold">
                                                    {status ? ((status === FINISHED) ? updationDate.format(DEFAULT_DATE_TIME_FORMAT) : status): <b>-</b>}
                                                </span>
                                            ),
                                        };
                                    })
                                }
                                page={current}
                                defaultSorted={[{
                                    id: 'creation_date',
                                    desc: true,
                                }]}
                                hasCustomExpander={true}
                                columns={[{
                                        Header: (
                                            <span className="grid-container-column-title">
                                                {translate('ORBITRACKER.GRID.ACTIVITY.DATE')}
                                            </span>
                                        ),
                                        accessor: 'creation_date',
                                    },{
                                        Header:  (
                                            <span className="grid-container-column-title">
                                                {translate('ORBITRACKER.GRID.ACTIVITY.DESCRIPTIONS')}
                                            </span>
                                        ),
                                        accessor: 'description',
                                        sortable: false,
                                    },{
                                        Header:  (
                                            <span className="grid-container-column-title">
                                                {translate('ORBITRACKER.GRID.ACTIVITY.STATUS')}
                                            </span>
                                        ),
                                        sortable: false,
                                        accessor: 'status',
                                }]}
                                isFetching={isFetching}
                                totalPages={totalPages}
                                handlePageChange={this.handlePageChange}
                                handleSortedChange={this.handleSortedChange}
                            />
                        </div>
                    </section>
                )
            }</I18n>
        );
    }
}

/* REDUX */
const mapStateToProps = ({DeviceActivity, Dialog}) => { 
    const {  
        current,
        sortField,
        sortOrder,
        
        isFetching,
        totalPages,

        processes,
    } = DeviceActivity;

    const {meta} = Dialog;
    const {identifier} = meta;

    return {
        current,
        sortField,
        sortOrder,

        isFetching,
        totalPages,

        processes,
        identifier,
    };
};

const mapDispatchToProps = ({
    resetDeviceActivityGridCall,
    getDeviceActivityItemsCompleteApiCall,
});

const connectedOrbitrackerActivityComponent = connect(mapStateToProps, mapDispatchToProps)(OrbitrackerActivityComponent);
export default connectedOrbitrackerActivityComponent;
