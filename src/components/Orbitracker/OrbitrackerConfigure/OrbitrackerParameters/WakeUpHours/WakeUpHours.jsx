/* GENERAL */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* UTILS */
import i18nModule from '../../../../../utils/i18nModule';
import wakeUpHoursCalculate from '../../../../../utils/wakeUpHoursCalculate';

/* STYLES */
import './WakeUpHours.scss';

class WakeUpHoursComponent extends Component {

    static propTypes = {
        calendarWakeUpValues: PropTypes.object.isRequired,
    };

    constructor(props){
        super(props);
        this.state = {
            items: wakeUpHoursCalculate(props.calendarWakeUpValues)
        };
    }

    componentWillReceiveProps({calendarWakeUpValues}) {
        this.setState({
            items: wakeUpHoursCalculate(calendarWakeUpValues),
        });
    }

    render() {
        const {items} = this.state;

        return (                           
            <div className="wake-up-hours-container">{
                items.map((item, index) => (
                    <div key={index} className="wake-up-hours-container-item">
                        <span>{`${('0' +item.output_date_hours).slice(-2)}${i18nModule.t('TIME.HOUR')}`}</span>
                        <span>{`${('0' +item.output_date_minutes).slice(-2)}${i18nModule.t('TIME.MINUTE')}`}</span>
                    </div>
                ))
            }</div>              
        );
    }
}

export default WakeUpHoursComponent;
