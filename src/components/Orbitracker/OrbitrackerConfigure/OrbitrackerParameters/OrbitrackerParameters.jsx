/* GENERAL */
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classnames from 'classnames';
import React, {Component} from 'react';
import {I18n, Trans} from 'react-i18next';
import {SubmissionError, reduxForm, Field, reset, change, formValueSelector} from 'redux-form';

/* UI ELEMENTS */
import {Button, Checkbox} from 'react-mdl';
/* CUSTOM */
import Switch from '../../../UI/Switch/Switch';
import SpinBox from '../../../UI/SpinBox/SpinBox';
import TextField from '../../../UI/TextField/TextField';
import SelectField from '../../../UI/SelectField/SelectField';
import DateTimePicker from '../../../UI/DateTimePicker/DateTimePicker';

/* COMPONENTS */
import WakeUpHours from './WakeUpHours/WakeUpHours';

/* UTILS */
import i18nModule from '../../../../utils/i18nModule';
import lifetimeCalculate from '../../../../utils/LifetimeEstimationCalculate';
import positionsInPeriodicModeCalculate from '../../../../utils/PositionsInPeriodicModeCalculate';
import periodOfDataResentCalculate from '../../../../utils/PeriodOfDataResentCalculate';
import orbitrackerTitle from '../../../../utils/orbitrackerTitle';
import transformParameters from '../../../../utils/transformDeviceParameters';

/* ACTIONS */
import {
    saveDeviceParametersApiCall,
    rebootDeviceParametersCompleteApiCall,
    synchronizeDeviceParametersCompleteApiCall,
    getDeviceParametersCompleteApiCall,
} from '../../../../actions/DeviceParameters';
import {dialogCloseCall} from '../../../../actions/Dialog';

/* CONSTANTS */
import {
    DATA_RATE,
    REDUX_FORM, 
    WAKE_UP_TYPE,
    MAX_ACCELO_MEASURE,
    ORBITRACKER_PARAMS,
    MOV_DETECTION_FLAG,
    LIFE_TIME_ESTIMATED,
    DEFAULT_PARAMETERS_SETTINGS,
} from '../../../../constants/General';

const {ORBITRACKER_PARAMETERS} = REDUX_FORM;

/* STYLES */
import './OrbitrackerParameters.scss';

class OrbitrackerParametersComponent extends Component {

    static propTypes = {
        language: PropTypes.string.isRequired, 

        /* REDUX FORM META */
        valid: PropTypes.bool.isRequired,
        submitting: PropTypes.bool.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        wakeUpMode: PropTypes.string,
        isFetching: PropTypes.bool.isRequired,
        wakeUpPeriod: PropTypes.object.isRequired,
        nbMissedPositions: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
        ]),
        calendarWakeUpValues: PropTypes.object.isRequired,

        /* ACTIONS */
        resetForm: PropTypes.func.isRequired,
        dialogCloseCall: PropTypes.func.isRequired,
        rebootDeviceParametersCompleteApiCall: PropTypes.func.isRequired,
        synchronizeDeviceParametersCompleteApiCall: PropTypes.func.isRequired,
        getDeviceParametersCompleteApiCall: PropTypes.func.isRequired,
    };

    constructor(props){
        super(props);
        this.state = {
            isShowAdvanced: false,
            periodOfDataresent: '',
        };

        /* BINDED HANDLERS */
        this.handleChangeAdvancedSettings = this.handleChangeAdvancedSettings.bind(this);
    }

    componentWillMount(){
        
        const {
            identifier,
            getDeviceParametersCompleteApiCall,
        } = this.props;

        getDeviceParametersCompleteApiCall(orbitrackerTitle(identifier));
    }

    componentWillReceiveProps({wakeUpMode, wakeUpPeriod, calendarWakeUpValues, nbMissedPositions}) {
        if (wakeUpMode !== this.props.wakeUpMode
            || wakeUpPeriod[ORBITRACKER_PARAMS.PERIODIC_HOURS.NAME] !== this.props.wakeUpPeriod[ORBITRACKER_PARAMS.PERIODIC_HOURS.NAME]
            || wakeUpPeriod[ORBITRACKER_PARAMS.PERIODIC_MINUTES.NAME] !== this.props.wakeUpPeriod[ORBITRACKER_PARAMS.PERIODIC_MINUTES.NAME]
            || calendarWakeUpValues[ORBITRACKER_PARAMS.CALENDAR_NUM_POSITION.NAME] !== this.props.calendarWakeUpValues[ORBITRACKER_PARAMS.CALENDAR_NUM_POSITION.NAME]
            || nbMissedPositions !== this.props.nbMissedPositions) {

            if (wakeUpMode ===  WAKE_UP_TYPE.OPTIONS[1].value) {
                /* CALENDAR */
                this.setState({
                    periodOfDataresent: periodOfDataResentCalculate(
                        +nbMissedPositions,
                        +calendarWakeUpValues[ORBITRACKER_PARAMS.CALENDAR_NUM_POSITION.NAME]
                    ),
                });
           
            } else if (wakeUpMode ===  WAKE_UP_TYPE.OPTIONS[2].value) {
                /* PERIODIC */
                this.setState({
                    periodOfDataresent: periodOfDataResentCalculate(
                        nbMissedPositions,
                        positionsInPeriodicModeCalculate(
                            +wakeUpPeriod[ORBITRACKER_PARAMS.PERIODIC_HOURS.NAME],
                            +wakeUpPeriod[ORBITRACKER_PARAMS.PERIODIC_MINUTES.NAME]
                        )
                    ),
                });          
            }
        }
    }

    reboot = () => {
        const {
            resetForm,
            identifier,
            dialogCloseCall,
            rebootDeviceParametersCompleteApiCall,
        } = this.props;

        rebootDeviceParametersCompleteApiCall(identifier)
        .then(() => {
            resetForm();
            dialogCloseCall();
        });
    }

    synchronize = () => {
        const {
            resetForm,
            identifier,
            dialogCloseCall,
            synchronizeDeviceParametersCompleteApiCall,
        } = this.props;
        const payload = Object.keys(DEFAULT_PARAMETERS_SETTINGS);

        synchronizeDeviceParametersCompleteApiCall(identifier, payload)
        .then(() => {
            resetForm();
            dialogCloseCall();
        });
    }

    handleChangeAdvancedSettings(e){
        this.setState({isShowAdvanced: e.target.checked});
    }
    
    render() {
        const {
            valid,
            pristine,
            language,
            submitting,
            handleSubmit,
            wakeUpMode,
            isFetching,
            initialValues,
            calendarWakeUpValues,
        } = this.props;
        
        const {
            isShowAdvanced,
            periodOfDataresent,
        } = this.state;

        return (
            <I18n ns="translations">{
                (translate, {i18n}) => (
                    <section className="orbitracker-params-container">
                        <form onSubmit={handleSubmit} className="orbitracker-params-container-form">
                            <div className="orbitracker-params-container-form-navigation">
                                <div className="orbitracker-params-container-form-navigation-buttons">
                                    <Button
                                        type="submit"
                                        disabled={submitting || isFetching || !valid}
                                        >
                                        {translate('ORBITRACKER.FORM.BUTTONS.SAVE')}
                                    </Button>
                                    <Button
                                        type="button"
                                        disabled={submitting || isFetching} 
                                        onClick={this.reboot}>
                                        {translate('ORBITRACKER.FORM.BUTTONS.REBOOT')}
                                    </Button>
                                    {
                                        isShowAdvanced && 
                                        <Button
                                            type="button"
                                            disabled={submitting || isFetching}
                                            onClick={this.synchronize}
                                        >
                                            {translate('ORBITRACKER.FORM.BUTTONS.SYNCHRONIZE')}
                                        </Button>
                                    }
                                </div>
                                <div className="orbitracker-params-container-form-navigation-show-more">
                                    <Checkbox 
                                        ripple
                                        disabled={submitting || isFetching} 
                                        checked={isShowAdvanced} 
                                        onChange={this.handleChangeAdvancedSettings}
                                        label={<span>{translate('ORBITRACKER.FORM.BUTTONS.ADVANCED')}</span>}
                                    />
                                </div>
                            </div>    

                            <div className="orbitracker-params-container-form-sections">
                                <div className="orbitracker-params-container-form-sections-item clearfix">
                                    
                                    {/* GENERAL SETTINGS START */}
                                    <div className="orbitracker-params-container-form-sections-item-column">
                                        <div className="orbitracker-params-container-form-sections-item-column-fieldset">
                                            <label 
                                                className="orbitracker-params-container-form-sections-item-column-fieldset-legend">
                                                {translate('ORBITRACKER.FORM.SECTIONS.GENERAL_SETTINGS.TITLE')}
                                            </label>

                                            <div className="orbitracker-params-container-form-sections-item-column-fieldset-content">
                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-movement orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                    <label 
                                                        htmlFor="movement-detection" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                            {translate('ORBITRACKER.FORM.SECTIONS.GENERAL_SETTINGS.FIELDS.MOVEMENT_DETECTION')}
                                                    </label>

                                                    <Field
                                                        label=""
                                                        component={Switch}
                                                        disabled={submitting || isFetching}
                                                        id="movement-detection"
                                                        name={MOV_DETECTION_FLAG}
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-switch"
                                                    />
                                                </div>

                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                    <label 
                                                        htmlFor="wake-up-type" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                        {translate('ORBITRACKER.FORM.SECTIONS.GENERAL_SETTINGS.FIELDS.WAKE_UP_MODE.TITLE')}
                                                    </label>

                                                    <Field
                                                        label=""
                                                        id="wake-up-type"
                                                        name={WAKE_UP_TYPE.NAME}
                                                        disabled={submitting || isFetching}
                                                        items={WAKE_UP_TYPE.OPTIONS.map(option => ({
                                                            title: translate(option.title),
                                                            value: option.value,
                                                        })
                                                    )}
                                                        component={SelectField} 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-selectfield"
                                                    />
                                                </div>

                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                    <label 
                                                        htmlFor="activation-date" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                        {translate('ORBITRACKER.FORM.SECTIONS.GENERAL_SETTINGS.FIELDS.ACTIVATION_DATE')}
                                                    </label>

                                                    <Field                   
                                                        label=""                                    
                                                        locale={language}
                                                        disabled={submitting || isFetching}
                                                        id="activation-date"
                                                        name="activationDate"
                                                        component={DateTimePicker} 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-datetimepicker"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                      </div>

                                      {/* GENERAL SETTINGS END */}
                                      {/* CALENDAR SETTINGS START */}

                                      <div className="orbitracker-params-container-form-sections-item-column">
                                        <fieldset disabled={wakeUpMode !== WAKE_UP_TYPE.OPTIONS[1].value} className="orbitracker-params-container-form-sections-item-column-fieldset">
                                            <label 
                                                className="orbitracker-params-container-form-sections-item-column-fieldset-legend">
                                                {translate('ORBITRACKER.FORM.SECTIONS.CALENDAR_SETTINGS.TITLE')}
                                            </label>

                                            <div className="orbitracker-params-container-form-sections-item-column-fieldset-content">
                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                    <label 
                                                        htmlFor="start-hour" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                        {translate('ORBITRACKER.FORM.SECTIONS.CALENDAR_SETTINGS.FIELDS.START_HOUR')}
                                                    </label>

                                                    <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">
                                                        <Field
                                                            label=""
                                                            id="start-hour"
                                                            min={ORBITRACKER_PARAMS.CALENDAR_START_HOURS.MIN}
                                                            max={ORBITRACKER_PARAMS.CALENDAR_START_HOURS.MAX}
                                                            name={ORBITRACKER_PARAMS.CALENDAR_START_HOURS.NAME}
                                                            minLength={2}
                                                            component={SpinBox}
                                                            disabled={submitting || isFetching}
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox"
                                                        />
                                                            
                                                        <label 
                                                            htmlFor="start-hour" 
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                            <b>{translate('TIME.HOUR')}</b>
                                                        </label>
                                                    </div>    

                                                    <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">
                                                        <Field
                                                            label=""
                                                            id="start-minute"
                                                            min={ORBITRACKER_PARAMS.CALENDAR_START_MINUTES.MIN}
                                                            max={ORBITRACKER_PARAMS.CALENDAR_START_MINUTES.MAX}
                                                            name={ORBITRACKER_PARAMS.CALENDAR_START_MINUTES.NAME}
                                                            minLength={2}
                                                            component={SpinBox}
                                                            disabled={submitting || isFetching}
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox"
                                                        />
                                                        
                                                        <label 
                                                            htmlFor="start-minute" 
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                            <b>{translate('TIME.MINUTE')}</b>
                                                        </label>
                                                    </div>    
                                                </div>

                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                    <label 
                                                        htmlFor="stop-hour" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                        {translate('ORBITRACKER.FORM.SECTIONS.CALENDAR_SETTINGS.FIELDS.STOP_HOUR')}
                                                    </label>

                                                    <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">
                                                        <Field
                                                            label=""
                                                            id="stop-hour"
                                                            min={ORBITRACKER_PARAMS.CALENDAR_STOP_HOURS.MIN}
                                                            max={ORBITRACKER_PARAMS.CALENDAR_STOP_HOURS.MAX}
                                                            name={ORBITRACKER_PARAMS.CALENDAR_STOP_HOURS.NAME}
                                                            minLength={2}
                                                            component={SpinBox} 
                                                            disabled={submitting || isFetching}
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox"
                                                        />
                                                        <label 
                                                            htmlFor="stop-hour" 
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                            <b>{translate('TIME.HOUR')}</b>
                                                        </label>
                                                    </div>

                                                    <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">   
                                                        <Field
                                                            label=""
                                                            id="stop-minute"
                                                            min={ORBITRACKER_PARAMS.CALENDAR_STOP_MINUTES.MIN}
                                                            max={ORBITRACKER_PARAMS.CALENDAR_STOP_MINUTES.MAX}
                                                            name={ORBITRACKER_PARAMS.CALENDAR_STOP_MINUTES.NAME}
                                                            minLength={2}
                                                            component={SpinBox} 
                                                            disabled={submitting || isFetching}
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox"
                                                        />
                                                        
                                                        <label 
                                                            htmlFor="stop-minute" 
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                            <b>{translate('TIME.MINUTE')}</b>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                    <label 
                                                        htmlFor="number-of-postions" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                        {translate('ORBITRACKER.FORM.SECTIONS.CALENDAR_SETTINGS.FIELDS.NUMBER_OF_POSITIONS')}
                                                    </label>

                                                    <Field
                                                        label=""
                                                        component={SpinBox} 
                                                        disabled={submitting || isFetching}
                                                        id="number-of-postions"
                                                        min={ORBITRACKER_PARAMS.CALENDAR_NUM_POSITION.MIN}
                                                        max={ORBITRACKER_PARAMS.CALENDAR_NUM_POSITION.MAX}
                                                        name={ORBITRACKER_PARAMS.CALENDAR_NUM_POSITION.NAME} 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox"
                                                    />
                                                </div>

                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                    <label 
                                                        htmlFor="triggered-measure-at" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                        <b>{translate('ORBITRACKER.FORM.SECTIONS.CALENDAR_SETTINGS.FIELDS.TRIGGERED_MEASURE_AT')}</b>
                                                    </label>
                                                </div>
                                                <div className="orbitracker-params-container-form-sections-wake-up-hours">
                                                    <WakeUpHours calendarWakeUpValues={calendarWakeUpValues} />
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>

                                    {/* CALENDAR SETTINGS END */}
                                    {/* PERIODIC SETTINGS START */}

                                    <div className="orbitracker-params-container-form-sections-item-column">
                                        <fieldset disabled={wakeUpMode !== WAKE_UP_TYPE.OPTIONS[2].value} className="orbitracker-params-container-form-sections-item-column-fieldset">
                                            <label 
                                                className="orbitracker-params-container-form-sections-item-column-fieldset-legend">
                                                {translate('ORBITRACKER.FORM.SECTIONS.PERIODIC_SETTINGS.TITLE')}
                                            </label>

                                            <div className="orbitracker-params-container-form-sections-item-column-fieldset-content">
                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                    <label 
                                                        htmlFor="period-hours" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                        {translate('ORBITRACKER.FORM.SECTIONS.PERIODIC_SETTINGS.FIELDS.PERIOD')}
                                                    </label>

                                                    <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">
                                                        <Field
                                                            label=""
                                                            min={ORBITRACKER_PARAMS.PERIODIC_HOURS.MIN}
                                                            max={ORBITRACKER_PARAMS.PERIODIC_HOURS.MAX}
                                                            id="period-hour"
                                                            name={ORBITRACKER_PARAMS.PERIODIC_HOURS.NAME}
                                                            minLength={2}
                                                            component={SpinBox}
                                                            disabled={submitting || isFetching}
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox"
                                                        />
                                                            
                                                        <label 
                                                            htmlFor="period-hours" 
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                            <b>{translate('TIME.HOUR')}</b>
                                                        </label>
                                                    </div>

                                                    <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">
                                                        <Field
                                                            label=""
                                                            min={ORBITRACKER_PARAMS.PERIODIC_MINUTES.MIN}
                                                            max={ORBITRACKER_PARAMS.PERIODIC_MINUTES.MAX}
                                                            id="period-minute"
                                                            name={ORBITRACKER_PARAMS.PERIODIC_MINUTES.NAME}
                                                            minLength={2}
                                                            component={SpinBox}
                                                            disabled={submitting || isFetching}
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox"
                                                        />
                                                        
                                                        <label 
                                                            htmlFor="period-minute" 
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                            <b>{translate('TIME.MINUTE')}</b>
                                                        </label>
                                                    </div>    
                                                </div>
                                            </div>    
                                        </fieldset>

                                        {/* PERIODIC SETTINGS END */}
                                        {/* ESTIMATE LIFETIME START */}

                                       <fieldset disabled={wakeUpMode === WAKE_UP_TYPE.OPTIONS[0].value} className="orbitracker-params-container-form-sections-item-column-fieldset">
                                             <label 
                                                className="orbitracker-params-container-form-sections-item-column-fieldset-legend">
                                                {translate('ORBITRACKER.FORM.SECTIONS.ESTIMATED_LIFETIME.TITLE')}
                                            </label>
                                            
                                            <div className="orbitracker-params-container-form-sections-item-column-fieldset-content">
                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                    <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">
                                                        <Field
                                                            label=""
                                                            component={TextField}
                                                            id="lifetime-current-year"
                                                            name={LIFE_TIME_ESTIMATED.YEAR}
                                                            onChange={event => event.preventDefault()}
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox"
                                                        />

                                                        <label 
                                                            htmlFor="lifetime-current-year" 
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                            <b>{translate('TIME.YEAR')}</b>
                                                        </label>
                                                    </div>
                                                    <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">
                                                        <Field
                                                            label=""
                                                            component={TextField}
                                                            id="lifetime-current-month"
                                                            name={LIFE_TIME_ESTIMATED.MONTH}
                                                            onChange={event => event.preventDefault()} 
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox"
                                                        />

                                                        <label 
                                                            htmlFor="lifetime-current-month" 
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                            <b>{translate('TIME.MONTH')}</b>
                                                        </label>
                                                    </div>
                                                    <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">
                                                        <Field
                                                            label=""
                                                            component={TextField}
                                                            id="lifetime-current-day"
                                                            name={LIFE_TIME_ESTIMATED.DAY}
                                                            onChange={event => event.preventDefault()}
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox"
                                                        />

                                                        <label 
                                                            htmlFor="lifetime-current-day" 
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                            <b>{translate('TIME.DAY')}</b>
                                                        </label>
                                                    </div>      
                                                </div>

                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                    <label 
                                                        htmlFor="last-measure-date" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                        {translate('ORBITRACKER.FORM.SECTIONS.ESTIMATED_LIFETIME.FIELDS.LAST_MEASURE_DATE')}
                                                    </label>

                                                    <Field
                                                        label=""
                                                        locale={language}
                                                        id="last-measure-date"
                                                        name={LIFE_TIME_ESTIMATED.MEASURE_DATE} 
                                                        component={DateTimePicker}
                                                        disabled={wakeUpMode === WAKE_UP_TYPE.OPTIONS[0].value}
                                                        showTime={false}
                                                        onChange={event => event.preventDefault()}
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-datetimepicker"
                                                    />
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>                                
                                </div> 

                                {/* ESTIMATE LIFETIME END */}
                                {/* MOVEMENT DETECTION SETTINGS START */}  

                                <div className="orbitracker-params-container-form-sections-item">
                                    <div className="orbitracker-params-container-form-sections-item-column-fieldset">
                                        <label className="orbitracker-params-container-form-sections-item-column-fieldset-legend">
                                            {translate('ORBITRACKER.FORM.SECTIONS.MOVEMENT_DETECTION.TITLE')}
                                        </label>

                                        <div className="orbitracker-params-container-form-sections-item-column-fieldset-content">
                                            <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                <label 
                                                    htmlFor="immobile-time-to-start-detection-minute" 
                                                    className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                    {translate('ORBITRACKER.FORM.SECTIONS.MOVEMENT_DETECTION.FIELDS.IMMOBILE_TIME_TO_START_DETECTION')}
                                                </label>

                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">   
                                                    <Field
                                                        label=""
                                                        component={SpinBox}
                                                        disabled={submitting || isFetching}
                                                        min={ORBITRACKER_PARAMS.DETECTION_START_MINUTES.MIN}
                                                        max={ORBITRACKER_PARAMS.DETECTION_START_MINUTES.MAX}
                                                        name={ORBITRACKER_PARAMS.DETECTION_START_MINUTES.NAME} 
                                                        id="immobile-time-to-start-detection-minute"
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox" 
                                                    />
                                                        
                                                    <label 
                                                        htmlFor="immobile-time-to-start-detection-minute" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                        <b>{translate('TIME.MINUTE')}</b>
                                                    </label>
                                                </div>

                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">   
                                                    <Field
                                                        label=""
                                                        component={SpinBox}
                                                        disabled={submitting || isFetching}
                                                        min={ORBITRACKER_PARAMS.DETECTION_START_SECONDS.MIN}
                                                        max={ORBITRACKER_PARAMS.DETECTION_START_SECONDS.MAX}
                                                        name={ORBITRACKER_PARAMS.DETECTION_START_SECONDS.NAME} 
                                                        id="immobile-time-to-start-detection-second"
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox" 
                                                    />
                                                    
                                                    <label 
                                                        htmlFor="immobile-time-to-start-detection-second" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                        <b>{translate('TIME.SECOND')}</b>
                                                    </label>
                                                </div>    
                                            </div>
                                            <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                <label 
                                                    htmlFor="immobile-time-to-trigger-position-hour" 
                                                    className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                    {translate('ORBITRACKER.FORM.SECTIONS.MOVEMENT_DETECTION.FIELDS.IMMOBILE_TIME_TO_TRIGGER_POSITION')}
                                                </label>

                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">
                                                    <Field
                                                        label=""
                                                        component={SpinBox}
                                                        disabled={submitting || isFetching}
                                                        min={ORBITRACKER_PARAMS.DETECTION_TRIGGER_MINUTES.MIN}
                                                        max={ORBITRACKER_PARAMS.DETECTION_TRIGGER_MINUTES.MAX}
                                                        name={ORBITRACKER_PARAMS.DETECTION_TRIGGER_MINUTES.NAME}
                                                        id="immobile-time-to-trigger-position-minute"
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox" 
                                                    />
                                                        
                                                    <label 
                                                        htmlFor="immobile-time-to-trigger-position-minute" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                        <b>{translate('TIME.MINUTE')}</b>
                                                    </label>
                                                </div>

                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">
                                                    <Field
                                                        label=""
                                                        component={SpinBox}
                                                        disabled={submitting || isFetching}
                                                        min={ORBITRACKER_PARAMS.DETECTION_TRIGGER_SECONDS.MIN}
                                                        max={ORBITRACKER_PARAMS.DETECTION_TRIGGER_SECONDS.MAX}
                                                        name={ORBITRACKER_PARAMS.DETECTION_TRIGGER_SECONDS.NAME} 
                                                        id="immobile-time-to-trigger-position-second"
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox" 
                                                    />
                                                    
                                                    <label 
                                                        htmlFor="immobile-time-to-trigger-position-second" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                        <b>{translate('TIME.SECOND')}</b>
                                                    </label>
                                                </div>  
                                            </div>
                                            <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                <label htmlFor="minimum-time-between-two-movement-detection-hour" 
                                                    className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                    {translate('ORBITRACKER.FORM.SECTIONS.MOVEMENT_DETECTION.FIELDS.MINIMUM_TIME_BETWEEN_TWO_MOVEMENT_DETECTION')}
                                                </label>

                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">
                                                    <Field
                                                        label=""
                                                        component={SpinBox} 
                                                        disabled={submitting || isFetching}
                                                        min={ORBITRACKER_PARAMS.DETECTION_BETWEEN_HOURS.MIN}
                                                        max={ORBITRACKER_PARAMS.DETECTION_BETWEEN_HOURS.MAX}
                                                        name={ORBITRACKER_PARAMS.DETECTION_BETWEEN_HOURS.NAME} 
                                                        id="minimum-time-between-two-movement-detection-hour"
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox" 
                                                    />
                                                        
                                                    <label 
                                                        htmlFor="minimum-time-between-two-movement-detection-hour" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                        <b>{translate('TIME.HOUR')}</b>
                                                    </label>
                                                </div>    

                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">
                                                    <Field
                                                        label=""
                                                        component={SpinBox} 
                                                        disabled={submitting || isFetching}
                                                        min={ORBITRACKER_PARAMS.DETECTION_BETWEEN_MINUTES.MIN}
                                                        max={ORBITRACKER_PARAMS.DETECTION_BETWEEN_MINUTES.MAX}
                                                        name={ORBITRACKER_PARAMS.DETECTION_BETWEEN_MINUTES.NAME} 
                                                        id="minimum-time-between-two-movement-detection-minute"
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox" 
                                                    />
                                                    
                                                    <label 
                                                        htmlFor="minimum-time-between-two-movement-detection-minute" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                        <b>{translate('TIME.MINUTE')}</b>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>    
                                    </div>
                                </div>

                                {/* MOVEMENT DETECTION SETTINGS END */}
                                {/* ACCELEROMETER SETTINGS START */}
                                <div hidden={!isShowAdvanced} className="orbitracker-params-container-form-sections-additional">         
                                    <div className="orbitracker-params-container-form-sections-item clearfix">
                                        <div className="orbitracker-params-container-form-sections-item-column-fieldset">
                                            <label className="orbitracker-params-container-form-sections-item-column-fieldset-legend">
                                                {translate('ORBITRACKER.FORM.SECTIONS.ACCELEROMETER_SETTINGS.TITLE')}
                                            </label>

                                            <div className="orbitracker-params-container-form-sections-item-column-fieldset-content">
                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                    <label 
                                                        htmlFor="date-rate" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                        {translate('ORBITRACKER.FORM.SECTIONS.ACCELEROMETER_SETTINGS.FIELDS.DATE_RATE')}
                                                    </label>

                                                    <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">

                                                        <Field
                                                            label=""
                                                            id="date-rate"
                                                            name={DATA_RATE.NAME}
                                                            items={DATA_RATE.OPTIONS}
                                                            disabled={submitting || isFetching}
                                                            component={SelectField}
                                                        />

                                                        <label 
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                            <b>{translate('ACCELEROMETER_PARAMS.HERTZ')}</b>
                                                        </label>
                                                    </div>
                                                </div> 

                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                    <label 
                                                        htmlFor="maximum-acceleration-measured" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                        {translate('ORBITRACKER.FORM.SECTIONS.ACCELEROMETER_SETTINGS.FIELDS.MAXIMUM_ACCELERATION_MEASURED') }
                                                    </label>

                                                    <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">

                                                        <Field
                                                            label=""
                                                            id="maximum-acceleration-measured"
                                                            name={MAX_ACCELO_MEASURE.NAME} 
                                                            items={MAX_ACCELO_MEASURE.OPTIONS}
                                                            disabled={submitting || isFetching}
                                                            component={SelectField} 
                                                        />

                                                        <label 
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                            <b>{translate('ACCELEROMETER_PARAMS.GRAVITY')}</b>
                                                        </label>

                                                    </div>
                                                </div>   

                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                    <label 
                                                        htmlFor="threshold-to-trigger-measure" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                        {translate('ORBITRACKER.FORM.SECTIONS.ACCELEROMETER_SETTINGS.FIELDS.THRESHOLD_TO_TRIGGER_A_MEASURE')}
                                                    </label>
                                                    <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">
                    
                                                        <Field
                                                            label=""
                                                            component={SpinBox} 
                                                            disabled={submitting || isFetching}
                                                            min={ORBITRACKER_PARAMS.ACCELERO_THERESHOLD.MIN}
                                                            max={ORBITRACKER_PARAMS.ACCELERO_THERESHOLD.MAX}
                                                            name={ORBITRACKER_PARAMS.ACCELERO_THERESHOLD.NAME}  
                                                            id="threshold-to-trigger-measure"
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox" 
                                                        />
                                                        
                                                        <label 
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                            <b>{translate('ACCELEROMETER_PARAMS.MILLIGRAVITY')}</b>
                                                        </label>
                                                    </div>
                                                    
                                                </div> 
                                            </div>
                                        </div>    
                                    </div>

                                    {/* ACCELEROMETER SETTINGS END */}
                                    {/* GPS SETTINGS START */}   

                                    <div className="orbitracker-params-container-form-sections-item">
                                        <div className="orbitracker-params-container-form-sections-item-column-fieldset">
                                            <label className="orbitracker-params-container-form-sections-item-column-fieldset-legend">  
                                                {translate('ORBITRACKER.FORM.SECTIONS.GPS_SETTINGS.TITLE')}
                                            </label>

                                            <div className="orbitracker-params-container-form-sections-item-column-fieldset-content">
                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                    <label 
                                                        htmlFor="position-acquisition-timeout" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                        {translate('ORBITRACKER.FORM.SECTIONS.GPS_SETTINGS.FIELDS.POSITION_ACQUISITION_TIMEOUT')}
                                                    </label>

                                                    <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">
                                                        <Field
                                                            label=""
                                                            component={SpinBox} 
                                                            disabled={submitting || isFetching}
                                                            min={ORBITRACKER_PARAMS.GPS_TIMEOUT.MIN}
                                                            max={ORBITRACKER_PARAMS.GPS_TIMEOUT.MAX}
                                                            name={ORBITRACKER_PARAMS.GPS_TIMEOUT.NAME}
                                                            id="position-acquisition-timeout"
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox" 
                                                        />

                                                        <label 
                                                            htmlFor="position-acquisition-timeout" 
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-label">
                                                            <b>{translate('TIME.SECOND')}</b>
                                                        </label>
                                                    </div>    
                                                </div>   

                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                    <label htmlFor="number-of-missing-positions" 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                        {translate('ORBITRACKER.FORM.SECTIONS.GPS_SETTINGS.FIELDS.NUMBER_OF_MISSION_POSITIONS')}
                                                    </label>

                                                    <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-group">

                                                        <Field
                                                            label=""
                                                            component={SpinBox} 
                                                            disabled={submitting || isFetching}
                                                            min={ORBITRACKER_PARAMS.GPS_MISSED.MIN}
                                                            max={ORBITRACKER_PARAMS.GPS_MISSED.MAX}
                                                            name={ORBITRACKER_PARAMS.GPS_MISSED.NAME}
                                                            id="number-of-missing-positions"
                                                            className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-spinbox" 
                                                        />
                                                    
                                                    </div>
                                                </div>   

                                                <div className="orbitracker-params-container-form-sections-item-column-fieldset-content-line">
                                                    <label 
                                                        className="orbitracker-params-container-form-sections-item-column-fieldset-content-line-title">
                                                        <b>{translate('ORBITRACKER.FORM.SECTIONS.GPS_SETTINGS.FIELDS.DESCRIPTION')}</b>
                                                        <b dangerouslySetInnerHTML={{ __html: periodOfDataresent }} />
                                                    </label>
                                                </div>  
                                            </div>    
                                        </div>    
                                    </div> 
                                </div>
                            </div>   
                        </form>  
                    </section>
                )
            }</I18n>
        );
    }
}

/* REDUX */
const mapStateToProps = (state) => {
    const {User, OrbitrackerParameters, Dialog} = state;
    const {language} = User;
    const {meta} = Dialog;
    const {parameters, isFetching} = OrbitrackerParameters;
    const {identifier} = meta;

    const wakeUpMode = formValueSelector(ORBITRACKER_PARAMETERS)(state, WAKE_UP_TYPE.NAME);

    const wakeUpPeriod = formValueSelector(ORBITRACKER_PARAMETERS)(state,
        ORBITRACKER_PARAMS.PERIODIC_HOURS.NAME,
        ORBITRACKER_PARAMS.PERIODIC_MINUTES.NAME,
    );

    const calendarWakeUpValues = formValueSelector(ORBITRACKER_PARAMETERS)(state,
        ORBITRACKER_PARAMS.CALENDAR_START_HOURS.NAME,
        ORBITRACKER_PARAMS.CALENDAR_START_MINUTES.NAME,
        ORBITRACKER_PARAMS.CALENDAR_STOP_HOURS.NAME,
        ORBITRACKER_PARAMS.CALENDAR_STOP_MINUTES.NAME,
        ORBITRACKER_PARAMS.CALENDAR_NUM_POSITION.NAME,
    );

    const nbMissedPositions =  formValueSelector(ORBITRACKER_PARAMETERS)(state, [ORBITRACKER_PARAMS.GPS_MISSED.NAME]);

    return {
        isFetching,
        identifier,
        language,

        wakeUpMode,
        wakeUpPeriod,
        nbMissedPositions,
        calendarWakeUpValues,
        initialValues: transformParameters(parameters),
    };
};

const mapDispatchToProps = ({
    dialogCloseCall,
    resetForm: () => (dispatch) => {
        dispatch(reset(ORBITRACKER_PARAMETERS));
    },
    rebootDeviceParametersCompleteApiCall,
    synchronizeDeviceParametersCompleteApiCall,
    getDeviceParametersCompleteApiCall,
});

/* REDUX FORM */
const validate = values => {
    const errors = {};
    for (const parameter in ORBITRACKER_PARAMS) {
        if (+values[ORBITRACKER_PARAMS[parameter].NAME] > ORBITRACKER_PARAMS[parameter].MAX) {
            errors[ORBITRACKER_PARAMS[parameter].NAME] = i18nModule.t('REDUX_FORM_ERROR.MAX_LIMIT') + ' ' + ORBITRACKER_PARAMS[parameter].MAX;
        } else if (+values[ORBITRACKER_PARAMS[parameter].NAME] < ORBITRACKER_PARAMS[parameter].MIN) {
            errors[ORBITRACKER_PARAMS[parameter].NAME] = i18nModule.t('REDUX_FORM_ERROR.MIN_LIMIT') + ' ' + ORBITRACKER_PARAMS[parameter].MIN;
        } 
    }
    /* PERIODIC */
    if (+values[ORBITRACKER_PARAMS.PERIODIC_HOURS.NAME] < 1 && +values[ORBITRACKER_PARAMS.PERIODIC_MINUTES.NAME] < 2) {
        errors[ORBITRACKER_PARAMS.PERIODIC_MINUTES.NAME] = i18nModule.t('REDUX_FORM_ERROR.MIN_LIMIT') + ' 2 ' + i18nModule.t('TIME.MINUTE');
    }
    if (+values[ORBITRACKER_PARAMS.PERIODIC_HOURS.NAME] === ORBITRACKER_PARAMS.PERIODIC_HOURS.MAX && +values[ORBITRACKER_PARAMS.PERIODIC_MINUTES.NAME] > 0) {
        errors[ORBITRACKER_PARAMS.PERIODIC_MINUTES.NAME] = i18nModule.t('REDUX_FORM_ERROR.MAX_LIMIT') +
        ' ' + ORBITRACKER_PARAMS.PERIODIC_HOURS.MAX + ' ' + i18nModule.t('TIME.HOUR');
    }

    /* MOVEMENT DETECTION */ 
    if (+values[ORBITRACKER_PARAMS.DETECTION_START_MINUTES.NAME] < 1 && +values[ORBITRACKER_PARAMS.DETECTION_START_SECONDS.NAME] < 30) {
        errors[ORBITRACKER_PARAMS.DETECTION_START_SECONDS.NAME] = i18nModule.t('REDUX_FORM_ERROR.MIN_LIMIT') + ' 30 ' + i18nModule.t('TIME.SECOND');
    }
    if (+values[ORBITRACKER_PARAMS.DETECTION_TRIGGER_MINUTES.NAME] < 1 && +values[ORBITRACKER_PARAMS.DETECTION_TRIGGER_SECONDS.NAME] < 30) {
        errors[ORBITRACKER_PARAMS.DETECTION_TRIGGER_SECONDS.NAME] = i18nModule.t('REDUX_FORM_ERROR.MIN_LIMIT') + ' 30 ' + i18nModule.t('TIME.SECOND');
    }
    if (+values[ORBITRACKER_PARAMS.DETECTION_BETWEEN_HOURS.NAME] < 1 && +values[ORBITRACKER_PARAMS.DETECTION_BETWEEN_MINUTES.NAME] < 5) {
        errors[ORBITRACKER_PARAMS.DETECTION_BETWEEN_MINUTES.NAME] = i18nModule.t('REDUX_FORM_ERROR.MIN_LIMIT') + ' 5 ' + i18nModule.t('TIME.MINUTE');
    }

    return errors;
};

const previousValues = {
    [ORBITRACKER_PARAMS.CALENDAR_NUM_POSITION.NAME]: '0',
    [ORBITRACKER_PARAMS.PERIODIC_HOURS.NAME]: '0',
    [ORBITRACKER_PARAMS.PERIODIC_MINUTES.NAME]: '0',
    [WAKE_UP_TYPE.NAME]: WAKE_UP_TYPE.OPTIONS[0].value,
};

const onChangeHandler = (values, dispatch) => {

    if (values[WAKE_UP_TYPE.NAME] !== previousValues[WAKE_UP_TYPE.NAME]
        || values[ORBITRACKER_PARAMS.CALENDAR_NUM_POSITION.NAME] != previousValues[ORBITRACKER_PARAMS.CALENDAR_NUM_POSITION.NAME]
        || values[ORBITRACKER_PARAMS.PERIODIC_HOURS.NAME] != previousValues[ORBITRACKER_PARAMS.PERIODIC_HOURS.NAME]
        || values[ORBITRACKER_PARAMS.PERIODIC_MINUTES.NAME] != previousValues[ORBITRACKER_PARAMS.PERIODIC_MINUTES.NAME]) {

        let lifetime = {};

        /* CALENDAR */
        if (values[WAKE_UP_TYPE.NAME] === WAKE_UP_TYPE.OPTIONS[1].value) {
            lifetime = lifetimeCalculate(
                +values[ORBITRACKER_PARAMS.CALENDAR_NUM_POSITION.NAME]
            );
        /* PERIODIC */
        } else if (values[WAKE_UP_TYPE.NAME] === WAKE_UP_TYPE.OPTIONS[2].value) {
            lifetime = lifetimeCalculate(
                positionsInPeriodicModeCalculate(
                    +values[ORBITRACKER_PARAMS.PERIODIC_HOURS.NAME],
                    +values[ORBITRACKER_PARAMS.PERIODIC_MINUTES.NAME]
                )
            );
        }

        dispatch(change(ORBITRACKER_PARAMETERS, [LIFE_TIME_ESTIMATED.YEAR], lifetime[LIFE_TIME_ESTIMATED.YEAR]));
        dispatch(change(ORBITRACKER_PARAMETERS, [LIFE_TIME_ESTIMATED.MONTH], lifetime[LIFE_TIME_ESTIMATED.MONTH]));
        dispatch(change(ORBITRACKER_PARAMETERS, LIFE_TIME_ESTIMATED.DAY, lifetime[LIFE_TIME_ESTIMATED.DAY]));
        dispatch(change(ORBITRACKER_PARAMETERS, [LIFE_TIME_ESTIMATED.MEASURE_DATE], lifetime[LIFE_TIME_ESTIMATED.MEASURE_DATE]));

        previousValues[ORBITRACKER_PARAMS.CALENDAR_NUM_POSITION.NAME] = values[ORBITRACKER_PARAMS.CALENDAR_NUM_POSITION.NAME];
        previousValues[WAKE_UP_TYPE.NAME] = values[WAKE_UP_TYPE.NAME];
        previousValues[ORBITRACKER_PARAMS.PERIODIC_HOURS.NAME] = values[ORBITRACKER_PARAMS.PERIODIC_HOURS.NAME];
        previousValues[ORBITRACKER_PARAMS.PERIODIC_MINUTES.NAME] =  values[ORBITRACKER_PARAMS.PERIODIC_MINUTES.NAME];
    }
};

const onSubmitChangedFields = (values, dispatch, props) => {
    const {initialValues} = props;
    const O_P = ORBITRACKER_PARAMS;
    const body = {};

    /* GENERAL */
    if (initialValues[MOV_DETECTION_FLAG] !== values[MOV_DETECTION_FLAG]) {
        body.movement_detection_flag = values[MOV_DETECTION_FLAG] + '';
    }
    if (initialValues[WAKE_UP_TYPE.NAME] !== values[WAKE_UP_TYPE.NAME]) {
        body.wake_up_type = values[WAKE_UP_TYPE.NAME];
    }
    /* CALENDAR */
    if (+initialValues[O_P.CALENDAR_START_HOURS.NAME] !== +values[O_P.CALENDAR_START_HOURS.NAME]) {
        body.calendar_start_hour = values[O_P.CALENDAR_START_HOURS.NAME] + '.0';
    }
    if (+initialValues[O_P.CALENDAR_START_MINUTES.NAME] !== +values[O_P.CALENDAR_START_MINUTES.NAME]) {
        body.calendar_start_minute = values[O_P.CALENDAR_START_MINUTES.NAME] + '.0';
    }
    if (+initialValues[O_P.CALENDAR_STOP_HOURS.NAME] !== +values[O_P.CALENDAR_STOP_HOURS.NAME]) {
        body.calendar_stop_hour = values[O_P.CALENDAR_STOP_HOURS.NAME] + '.0';
    }
    if (+initialValues[O_P.CALENDAR_STOP_MINUTES.NAME] !== +values[O_P.CALENDAR_STOP_MINUTES.NAME]) {
        body.calendar_stop_minute = values[O_P.CALENDAR_STOP_MINUTES.NAME] + '.0';
    }
    if (+initialValues[O_P.CALENDAR_NUM_POSITION.NAME] !== +values[O_P.CALENDAR_NUM_POSITION.NAME]) {
        body.calendar_nb_positions = values[O_P.CALENDAR_NUM_POSITION.NAME] + '.0';
    }

    /* PERIODIC */
    if ((+initialValues[O_P.PERIODIC_HOURS.NAME] !== +values[O_P.PERIODIC_HOURS.NAME])
        || (+initialValues[O_P.PERIODIC_MINUTES.NAME] !== +values[O_P.PERIODIC_MINUTES.NAME])) {
        body.wake_up_period = Math.round((values[O_P.PERIODIC_HOURS.NAME] * 3600) + (values[O_P.PERIODIC_MINUTES.NAME] * 60)) + '.0';
    } 
 
    /* MOVEMENT DETECTION */
    if ((+initialValues[O_P.DETECTION_START_MINUTES.NAME] !== +values[O_P.DETECTION_START_MINUTES.NAME])
        || (+initialValues[O_P.DETECTION_START_SECONDS.NAME] !== +values[O_P.DETECTION_START_SECONDS.NAME])) {
        body.mov_immobile_to_start = Math.round((values[O_P.DETECTION_START_MINUTES.NAME] * 60) + values[O_P.DETECTION_START_SECONDS.NAME]) + '.0';
    }

    if ((+initialValues[O_P.DETECTION_TRIGGER_MINUTES.NAME] !== +values[O_P.DETECTION_TRIGGER_MINUTES.NAME])
        || (+initialValues[O_P.DETECTION_TRIGGER_SECONDS.NAME] !== +values[O_P.DETECTION_TRIGGER_SECONDS.NAME])) {
        body.mov_immobile_to_fix = Math.round((values[O_P.DETECTION_TRIGGER_MINUTES.NAME] * 60) + values[O_P.DETECTION_TRIGGER_SECONDS.NAME]) + '.0';
    }

    if ((+initialValues[O_P.DETECTION_BETWEEN_HOURS.NAME] !== +values[O_P.DETECTION_BETWEEN_HOURS.NAME])
        || (+initialValues[O_P.DETECTION_BETWEEN_MINUTES.NAME] !== +values[O_P.DETECTION_BETWEEN_MINUTES.NAME])) {
        body.mov_immobile_to_fix = Math.round((values[O_P.DETECTION_BETWEEN_HOURS.NAME] * 3600) + (values[O_P.DETECTION_BETWEEN_MINUTES.NAME] * 60)) + '.0';
    }

    /* ACCELEROMETER */
    if (+initialValues[DATA_RATE.NAME] !== +values[DATA_RATE.NAME]) {
        body.mov_data_rate = values[DATA_RATE.NAME] + '.0';
    }
    if (+initialValues[MAX_ACCELO_MEASURE.NAME] !== +values[MAX_ACCELO_MEASURE.NAME]) {
        body.mov_max_measure = values[MAX_ACCELO_MEASURE.NAME] + '.0';
    }
    if (+initialValues[O_P.ACCELERO_THERESHOLD.NAME] !== +values[O_P.ACCELERO_THERESHOLD.NAME]) {
        body.mov_threshold = values[O_P.ACCELERO_THERESHOLD.NAME] + '.0';
    }

     /* GPS */
     if (+initialValues[O_P.GPS_TIMEOUT.NAME] !== +values[O_P.GPS_TIMEOUT.NAME]) {
        body.gps_timeout = values[O_P.GPS_TIMEOUT.NAME] + '.0';
    }
    if (+initialValues[O_P.GPS_MISSED.NAME] !== +values[O_P.GPS_MISSED.NAME]) {
        body.nb_miss = values[O_P.GPS_MISSED.NAME] + '.0';
    }

    return saveDeviceParametersApiCall(props.identifier, body, dispatch);
};

const orbitrackerParametersReduxForm = {
    form: ORBITRACKER_PARAMETERS,
    enableReinitialize: true,
    validate,
    onChange: onChangeHandler,
    onSubmit: onSubmitChangedFields,
    onSubmitSuccess: (result, dispatch, props) => {
        dispatch(reset(ORBITRACKER_PARAMETERS));
        props.dialogCloseCall();
    },
};

const bindedReduxFormOrbitrackerParametersComponent = reduxForm(orbitrackerParametersReduxForm)(OrbitrackerParametersComponent);
const connectedOrbitrackerParametersComponent = connect(mapStateToProps, mapDispatchToProps)(bindedReduxFormOrbitrackerParametersComponent);
export default connectedOrbitrackerParametersComponent;
