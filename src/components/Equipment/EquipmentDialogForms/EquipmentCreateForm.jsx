/* GENERAL */
import classnames from 'classnames';

/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';
import {SubmissionError, reduxForm, Field, reset} from 'redux-form';

/* UI ELEMENTS */
import {IconButton, FABButton, Tooltip, Spinner, Button} from 'react-mdl';
/* CUSTOM */
import TextField from '../../UI/TextField/TextField';
import AutoComplete from '../../UI/AutoComplete/AutoComplete';

/* ICONS */
import GoCheck from 'react-icons/lib/go/check';
import GoSearch from 'react-icons/lib/go/search';

/* STYLES */
import './EquipmentDialogForms.scss';

/* ACTIONS */
import {
    resetAutocompleteItemsCall,
    getAutocompleteItemsCompleteApiCall,
} from '../../../actions/Autocomplete';

import {
    getClientEquipmentItemsCompleteApiCall,
} from '../../../actions/Equipment';

import {
    dialogCloseCall,
} from '../../../actions/Dialog';

import {
    createNewClientEquipmentCompleteApiCall,
} from '../../../actions/Equipment';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';
import i18nModule from '../../../utils/i18nModule';

/* CONSTANTS */
import {
    VALIDATION,
    REDUX_FORM,
} from '../../../constants/General';

const {ADD_EQUIPMENT} = REDUX_FORM;
const {ALPHANUMERIC} = VALIDATION;

class EquipmentCreateFormComponent extends Component {

    static propTypes = {
        /* REDUX FORM META */
        valid: PropTypes.bool.isRequired,
        pristine: PropTypes.bool.isRequired,
        submitting: PropTypes.bool.isRequired,
        handleSubmit: PropTypes.func.isRequired,

        /* EQUIPMENT GRID PAGINATION & SORTING */
        filter: PropTypes.string.isRequired,
        current: PropTypes.number.isRequired,
        sortField: PropTypes.string.isRequired,
        sortOrder: PropTypes.string.isRequired,

        /* REDUX STATE PROPS */
        items: PropTypes.object.isRequired,

        /* ACTIONS */
        dialogCloseCall: PropTypes.func.isRequired,
        resetAutocompleteItemsCall: PropTypes.func.isRequired,
        getAutocompleteItemsCompleteApiCall: PropTypes.func.isRequired,
        getClientEquipmentItemsCompleteApiCall: PropTypes.func.isRequired, 
    };
    constructor(props){
        super(props);
    }

    componentWillMount(){
        this.props.getAutocompleteItemsCompleteApiCall();
    }

    render() {
        const {
            items,

            valid,
            pristine,
            submitting,
            handleSubmit,
        } = this.props;

        return (
            <I18n ns="translations">{
                (translate) => (
                    <section className="equipment-modal-container">
                        <form onSubmit={handleSubmit} className="equipment-modal-form">
                            <div className="equipment-modal-form-item">
                                <label htmlFor="name" className="equipment-modal-form-label">
                                    {translate('CLIENT_EQUIPMENT.FORM.EQUIPMENT_NAME.LABEL')}
                                </label>
                                <Field
                                    id="name"
                                    name="name"
                                    component={TextField}
                                    className="equipment-modal-form-field"
                                    label={translate('CLIENT_EQUIPMENT.FORM.EQUIPMENT_NAME.PLACEHOLDER')}
                                />
                            </div>
                            <div className="equipment-modal-form-item">
                                <label htmlFor="type" className="equipment-modal-form-label">
                                    {translate('CLIENT_EQUIPMENT.FORM.EQUIPMENT_TYPE.LABEL')}
                                </label>
                                <Field
                                    id="type"
                                    name="type"
                                    component={TextField}
                                    className="equipment-modal-form-field"
                                    label={translate('CLIENT_EQUIPMENT.FORM.EQUIPMENT_TYPE.PLACEHOLDER')}
                                />
                            </div>
                            <div className="equipment-modal-form-item">
                                <label htmlFor="textfield-Search" className="equipment-modal-form-label">
                                    {translate('CLIENT_EQUIPMENT.FORM.ORBITRACKER_ASSOCIATED.LABEL')}
                                </label>
                                <div className="equipment-modal-form-autocomplete">
                                    <IconButton
                                        name="run-search"
                                        component={GoSearch}
                                        className="equipment-modal-form-search-icon"
                                    />
                                    <Field
                                        type="text"
                                        name="orbitracker"
                                        id="search"
                                        items={items}
                                        component={AutoComplete}
                                        handleSubmit={handleSubmit}
                                        className="equipment-modal-autocomplete-field"
                                        label={translate('CLIENT_EQUIPMENT.FORM.ORBITRACKER_ASSOCIATED.PLACEHOLDER')}
                                        dataIndex={'visible'}
                                        valueIndex={'visible'}
                                    />
                                </div>
                                <div className="equipment-modal-form-submit-button">                                    
                                    <Button raised ripple type="submit" 
                                        className="equipment-modal-button" 
                                        disabled={!valid || pristine || submitting}>
                                        {submitting ? <Spinner singleColor/> : <GoCheck />}
                                    </Button>
                                </div>
                            </div>
                        </form>
                    </section>
                )
            }</I18n>
        );
    }
}

/* REDUX */
const mapStateToProps = ({Autocomplete, Equipment}) => {
    const {items} = Autocomplete;

    const {
        filter,
        current,
        sortField,
        sortOrder,
    } = Equipment;

    return {
        items,

        filter,
        current,
        sortField,
        sortOrder,

        initialValues: {
            name: '',
            type: '',
            orbitracker: '',
        },
    };
};

const mapDispatchToProps = ({
    dialogCloseCall,
    resetAutocompleteItemsCall,
    getAutocompleteItemsCompleteApiCall,
    getClientEquipmentItemsCompleteApiCall,
});

/* REDUX FORM */
const validate = ({name, type, orbitracker}) => {
    const errors = {};
    if (name && !name.match(ALPHANUMERIC)) {
        errors.name = i18nModule.t('REDUX_FORM_ERROR.ALPHANUMERIC');
    }

    if (!type) {
        errors.type = i18nModule.t('REDUX_FORM_ERROR.REQUIRED');
    } else {
        if (!type.match(ALPHANUMERIC)) {
            errors.type = i18nModule.t('REDUX_FORM_ERROR.ALPHANUMERIC');
        }
    }

    return errors;  
};

const addEquipmentReduxForm = {
    form: ADD_EQUIPMENT,
    enableReinitialize: true,
    onChange: (values, dispatch, props, oldValues) => {
        const {orbitracker} = values;

        if(oldValues.orbitracker != orbitracker){
            props.getAutocompleteItemsCompleteApiCall(orbitracker);
        }
    },
    validate: (values, props) => {
        const {dirty} = props;
        if (!dirty) return {};
        return validate(values);
    },
    onSubmit: (values, dispatch, {items}) => {
        let tracker;
        const {name, type, orbitracker} = values;

        if(orbitracker) { 
            const searchIndex = items.findIndex(({visible}) => visible === orbitracker);
            if(searchIndex === -1){
                return new SubmissionError({
                    orbitracker: i18nModule.t('REDUX_FORM_ERROR.INVALID_FIELD'),
                });
            }

            tracker = items.get(searchIndex).identifier;
        }

        return createNewClientEquipmentCompleteApiCall({
            type,
            tracker,
            identifier: name,
        }, dispatch);
    },
    onSubmitSuccess: (result, dispatch, props) => {
        dispatch(reset(ADD_EQUIPMENT));

        const {
            filter,
            current,
            sortField,
            sortOrder,

            dialogCloseCall,
            getClientEquipmentItemsCompleteApiCall,
        } = props;
        
        dialogCloseCall();
        getClientEquipmentItemsCompleteApiCall({
            filter,
            current,
            sortField,
            sortOrder,
        });
    },
};

const bindedReduxFormTopSideBarComponent = reduxForm(addEquipmentReduxForm)(EquipmentCreateFormComponent);
const connectedEquipmentCreateFormComponent = connect(mapStateToProps, mapDispatchToProps)(bindedReduxFormTopSideBarComponent);
export default connectedEquipmentCreateFormComponent;
