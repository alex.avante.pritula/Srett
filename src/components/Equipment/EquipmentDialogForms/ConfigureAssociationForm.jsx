/* GENERAL */
import classnames from 'classnames';

/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';
import {SubmissionError, reduxForm, formValueSelector, Field, reset} from 'redux-form';

/* UI ELEMENTS */
import {IconButton, Button, Spinner} from 'react-mdl';
/* CUSTOM */
import TextField from '../../UI/TextField/TextField';
import AutoComplete from '../../UI/AutoComplete/AutoComplete';

/* ICONS */
import GoDash from 'react-icons/lib/go/dash';
import GoCheck from 'react-icons/lib/go/check';
import GoSearch from 'react-icons/lib/go/search';

/* LOCALIZATION */
import {I18n} from 'react-i18next';

/* ACTIONS */
import { 
    resetAutocompleteItemsCall,
    getAutocompleteItemsCompleteApiCall,
} from '../../../actions/Autocomplete';

import {getClientEquipmentItemsCompleteApiCall} from '../../../actions/Equipment';

import {dialogCloseCall} from '../../../actions/Dialog';

import {
    associateClientEquipmentToOrbitrackerApiCall,
    associateClientEquipmentToOrbitrackerCompleteApiCall,
} from '../../../actions/Equipment';

/* LOCALIZATION */
import i18nModule from '../../../utils/i18nModule';

/* UTILS */
import orbitrackerTitle from '../../../utils/orbitrackerTitle';

/* STYLES */
import './EquipmentDialogForms.scss';

/* CONSTANTS */
import {
    REDUX_FORM,
    NULL_ENTITY,
} from '../../../constants/General';

const {CONFIGURE_ASSOCIATION} = REDUX_FORM;

class ConfiureAssociationComponent extends Component {

    static propTypes = {
        /* REDUX FROM META */
        valid: PropTypes.bool.isRequired,
        pristine: PropTypes.bool.isRequired,
        submitting: PropTypes.bool.isRequired,
        handleSubmit: PropTypes.func.isRequired,

        /* EQUIPMENT GRID PAGINATION & SORTING */
        filter: PropTypes.string.isRequired,
        current: PropTypes.number.isRequired,
        sortField: PropTypes.string.isRequired,
        sortOrder: PropTypes.string.isRequired,

        /* REDUX STATE PROPS */
        meta: PropTypes.object.isRequired,
        items: PropTypes.object.isRequired,

        /* ACTIONS */
        dialogCloseCall: PropTypes.func.isRequired,
        getClientEquipmentItemsCompleteApiCall: PropTypes.func.isRequired, 
        associateClientEquipmentToOrbitrackerCompleteApiCall: PropTypes.func.isRequired,
    };

    constructor(props){
        super(props);

        /* BINDED HANDLERS */
        this.removeAssociation = this.removeAssociation.bind(this);
    }

    removeAssociation(){
        const {
            identifier,
            submitSuccess,
            oldOrbitracker,
            associateClientEquipmentToOrbitrackerCompleteApiCall,
        } = this.props;

        return associateClientEquipmentToOrbitrackerCompleteApiCall({
            tracker: NULL_ENTITY,
            equipment: identifier,
            oldTracker: oldOrbitracker,
        }).then(() => {
            submitSuccess(this.props);
        });
    }

    render() {
        const {
            items,

            valid,
            pristine,
            submitting,
            handleSubmit,    

            isModalFetching,

            orbitrackerSyncValue,
            initialValues: {orbitracker},
        } = this.props;

        return (
            <I18n ns="translations">{
                (translate, { i18n }) => (
                    <section className="equipment-modal-container">
                        <form onSubmit={handleSubmit} className="equipment-modal-form">
                            <div className="equipment-modal-form-item">
                                <label htmlFor="name" className="equipment-modal-form-label">
                                    {translate('CLIENT_EQUIPMENT.FORM.EQUIPMENT_NAME.LABEL')}
                                </label>
                                <Field
                                    disabled
                                    name="name"
                                    component={TextField}
                                    className="equipment-modal-form-field"
                                    label={translate('CLIENT_EQUIPMENT.FORM.EQUIPMENT_NAME.PLACEHOLDER')}
                                />
                            </div>
                            <div className="equipment-modal-form-item">
                                <label htmlFor="textfield-Search" className="equipment-modal-form-label">
                                    {translate('CLIENT_EQUIPMENT.FORM.ORBITRACKER_ASSOCIATED.LABEL')}
                                </label>
                                <div className="equipment-modal-form-autocomplete">
                                    <IconButton
                                        name="run-search"
                                        component={GoSearch}
                                        className="equipment-modal-form-search-icon"
                                    />
                                    <Field
                                        type="text"
                                        name="orbitracker"
                                        items={items}
                                        component={AutoComplete}
                                        handleSubmit={handleSubmit}
                                        className="equipment-autocomplete-associated-field"
                                        label={translate('CLIENT_EQUIPMENT.FORM.ORBITRACKER_ASSOCIATED.NONE')}
                                        dataIndex="visible"
                                        valueIndex="visible"
                                    />
                                </div>
                                <div className="equipment-modal-form-submit-button">                                    
                                    <Button
                                        raised
                                        ripple
                                        type="submit"
                                        className="equipment-modal-button"
                                        disabled={isModalFetching || pristine || submitting || !orbitrackerSyncValue} >
                                        {(isModalFetching || submitting) ? <Spinner singleColor/> : <GoCheck />}
                                    </Button>
                                    <Button
                                        raised
                                        ripple
                                        type="button"
                                        onClick={this.removeAssociation}
                                        className="equipment-modal-button"
                                        disabled={isModalFetching || submitting || (orbitrackerSyncValue !== orbitracker)}>
                                        {(isModalFetching || submitting) ? <Spinner singleColor/> : <GoDash />}
                                    </Button>
                                </div>
                            </div>
                        </form>
                    </section>
                )
            }</I18n>
        );
    }
}

/* REDUX */
const mapStateToProps = (state) => {
    const {
        Dialog, 
        Autocomplete, 
        Equipment
    } = state;

    const {meta} = Dialog;
    const {items} = Autocomplete;
    const {
        filter,
        current,
        sortField,
        sortOrder,

        isModalFetching,
    } = Equipment;

    const {
        type, 
        identifier,
        orbitracker,
    } = meta;

    const display = (orbitracker ? orbitrackerTitle(orbitracker) : '');

    return {
        meta,
        items,
        identifier,

        filter,
        current,
        sortField,
        sortOrder,

        isModalFetching,
        oldOrbitracker: orbitracker,

        initialOrbitracker: display,
        orbitrackerSyncValue: formValueSelector(CONFIGURE_ASSOCIATION)(state, 'orbitracker'),

        initialValues: {
            name: type,
            orbitracker: display,
        },
    };
};

const submitSuccess = (dispatch, props) => {
    dispatch(reset(CONFIGURE_ASSOCIATION));

        const {
            filter,
            current,
            sortField,
            sortOrder,

            dialogCloseCall,
            getClientEquipmentItemsCompleteApiCall,
        } = props;

        dialogCloseCall();

        getClientEquipmentItemsCompleteApiCall({
            filter,
            current,
            sortField,
            sortOrder,
        });
};

const mapDispatchToProps = ({
    dialogCloseCall,
    resetAutocompleteItemsCall,
    getAutocompleteItemsCompleteApiCall,
    getClientEquipmentItemsCompleteApiCall,
    associateClientEquipmentToOrbitrackerCompleteApiCall,
    submitSuccess: (props) => (dispatch) => {
        submitSuccess(dispatch, props);
    },
});

/* REDUX FORM */
const ConfigureAssociationReduxForm = {    
    form: CONFIGURE_ASSOCIATION,
    enableReinitialize: true,
    validate: ({orbitracker}, props) => {
        const {
            items,
            resetAutocompleteItemsCall,
            getAutocompleteItemsCompleteApiCall,
        } = props;

        if(orbitracker) { getAutocompleteItemsCompleteApiCall(orbitracker); }
        else if(items.size > 0){ resetAutocompleteItemsCall(); }   

        return {};
    },
    onSubmit: ({orbitracker}, dispatch, props) => {
        let tracker = NULL_ENTITY;
        const {identifier, oldOrbitracker} = props;

        if(orbitracker) {
            const {items} = props;
            const searchIndex = items.findIndex(({visible}) => visible === orbitracker);

            if(searchIndex === -1){
                throw new SubmissionError({
                    orbitracker: i18nModule.t('REDUX_FORM_ERROR.INVALID_FIELD'),
                });
            }

            tracker = items.get(searchIndex).identifier;
        }

        return associateClientEquipmentToOrbitrackerApiCall({
            tracker,
            equipment: identifier,
            oldTracker: oldOrbitracker,
        }, dispatch);
    },
    onSubmitSuccess: (result, dispatch, props) => {
        submitSuccess(dispatch, props);
    },
};

const bindedReduxFormConfigureAssociationComponent = reduxForm(ConfigureAssociationReduxForm)(ConfiureAssociationComponent);
const connectedConfigureAssociationFormComponent = connect(mapStateToProps, mapDispatchToProps)(bindedReduxFormConfigureAssociationComponent);
export default connectedConfigureAssociationFormComponent;
