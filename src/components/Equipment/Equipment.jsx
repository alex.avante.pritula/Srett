/* GENERAL */
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {I18n, Trans} from 'react-i18next';

/* UI ELEMENTS */
import Grid from '../UI/Grid/Grid';

/* COMPONENTS */
import EquipmentGridItem from './EquipmentGridItem/EquipmentGridItem';

/* ACTIONS */
import {
    resetClientEquipmentsGridCall,
    getClientEquipmentItemsCompleteApiCall,
} from '../../actions/Equipment';

import {dialogOpenCall} from '../../actions/Dialog';

/* UTILS */
import equipmentTitle from '../../utils/equipmentTitle';

/* STYLES */
import './Equipment.scss';
import '../UI/Grid/Grid.scss';

/* CONSTANTS */
import {
    DIALOGS,
    SORT_ORDER,
    SEARCH_META,
    DEFAULT_SORTING,
    GRID_EXPANDER_SIZE,
    DEFAULT_DATE_TIME_FORMAT,
} from '../../constants/General';

const {ASC, DESC} = SORT_ORDER;
const {EQUIPMENT} = DEFAULT_SORTING;

class EquipmentComponent extends Component {
    
    static propTypes = {
        isGridFetching: PropTypes.bool.isRequired,

        filter: PropTypes.string.isRequired,
        current: PropTypes.number.isRequired,
        sortField: PropTypes.string.isRequired,
        sortOrder: PropTypes.string.isRequired,
        totalPages: PropTypes.number.isRequired, 

        equipments: PropTypes.object.isRequired,

        /* ACTIONS */
        dialogOpenCall: PropTypes.func.isRequired,
        resetClientEquipmentsGridCall: PropTypes.func.isRequired,
        getClientEquipmentItemsCompleteApiCall: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        /* BINDED HANDLERS */
        this.handlePageChange = this.handlePageChange.bind(this);
        this.handleSortedChange = this.handleSortedChange.bind(this);
        this.handleAddEquipment = this.handleAddEquipment.bind(this);
        this.handleSubGridComponent = this.handleSubGridComponent.bind(this);
    }

    componentWillMount(){
        this.props.getClientEquipmentItemsCompleteApiCall({
            page: 0, 
            filter: '',
            sortOrder: ASC,
            sortField: EQUIPMENT, 
        });
    }

    componentWillUnmount(){
        this.props.resetClientEquipmentsGridCall();
    }

    handlePageChange(page){
        const {
            filter,
            sortField,
            sortOrder,
            getClientEquipmentItemsCompleteApiCall,
        } = this.props;

        getClientEquipmentItemsCompleteApiCall({
            page, 
            filter,
            sortField, 
            sortOrder,
        });
    }

    handleSortedChange(sorted){
        const {
            filter,
            current,
            getClientEquipmentItemsCompleteApiCall,
        } = this.props;

        const {id, desc} = sorted[0] || {};
        const sortOrder = desc ? DESC : ASC;
        const sortField = id || EQUIPMENT;

        getClientEquipmentItemsCompleteApiCall({
            filter,
            sortField, 
            sortOrder,
            page: current, 
        });
    }

    handleSubGridComponent({index}){
        return (<EquipmentGridItem index={index}/>);
    }

    handleAddEquipment(){
        this.props.dialogOpenCall({
            dialogName: DIALOGS.ADD_NEW_EQUIPMENT,
        });
    }

    render() {
        const {
            current,
            totalPages,

            isGridFetching,

            equipments,
        } = this.props;

        return (
            <I18n ns="translations">{
                (translate, {i18n}) => (
                    <div className="equipment-grid-wrapper">
                        <Grid 
                            handleAddEvent={this.handleAddEquipment}
                            data={
                                equipments.toArray().map((item) => {
                                    const {
                                        type,
                                        tracker,
                                        identifier,
                                        lastmeasure,
                                        manufacturer,
                                    } = item;
                                    
                                    return {
                                        type: (
                                            <span className="grid-container-row-item grid-container-row-item-title">
                                                {type}
                                            </span>
                                        ),
                                        identifier: (
                                            <span className="grid-container-row-item grid-container-row-item-title">
                                                {equipmentTitle(identifier)}
                                            </span>
                                        ),
                                        pairing: (
                                            <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-centered grid-container-row-item-bold">
                                                {translate(`CONFIRM.${(tracker.identifier) ? 'YES': 'NO'}`)}
                                            </span>
                                        ),
                                        lastmeasure: (
                                            <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-centered grid-container-row-item-bold">
                                                {lastmeasure ? lastmeasure.format(DEFAULT_DATE_TIME_FORMAT) : <b>-</b>}
                                            </span> 
                                        ),
                                    };
                                })
                            }
                            defaultSorted={[{
                                asc: true,
                                id: EQUIPMENT,
                            }]}
                            addTootltipLabel={translate('CLIENT_EQUIPMENT.TOOLTIP.ADD')}
                            page={current}
                            columns={[{
                                    Header: (
                                        <span className="grid-container-column-title">
                                            {translate('CLIENT_EQUIPMENT.GRID.MAIN.TYPE')}
                                        </span>
                                    ),
                                    accessor: 'type',
                                },{
                                    Header:  (
                                        <span className="grid-container-column-title">
                                            {translate('CLIENT_EQUIPMENT.GRID.MAIN.REFERENCE')}
                                        </span>
                                    ),
                                    accessor: 'identifier',
                                },{
                                    Header:  (
                                        <span className="grid-container-column-title">
                                            {translate('CLIENT_EQUIPMENT.GRID.MAIN.PAIRING')}
                                        </span>
                                    ),
                                    sortable: false,
                                    accessor: 'pairing',
                                    width: GRID_EXPANDER_SIZE * 2,
                                },{
                                    Header: (
                                        <span className="grid-container-column-title">
                                            {translate('CLIENT_EQUIPMENT.GRID.MAIN.LAST_MEASURE')}
                                        </span>
                                    ),
                                    sortable: false,
                                    accessor: 'lastmeasure',
                                    width: GRID_EXPANDER_SIZE * 4,
                            }]}
                            totalPages={totalPages}
                            isFetching={isGridFetching}
                            handlePageChange={this.handlePageChange}
                            handleSortedChange={this.handleSortedChange}
                            handleSubGridComponent={this.handleSubGridComponent}
                        />
                    </div>
                )
            }</I18n>    
        );
    }
}

/* REDUX */
const mapStateToProps = ({Equipment}) => {
    const {  
        isGridFetching,

        filter,
        current,
        sortField,
        sortOrder,
        totalPages,

        equipments,
    } = Equipment;

    return {
        isGridFetching,

        filter,
        current,
        sortField,
        sortOrder,
        totalPages,

        equipments,
    };
};

const mapDispatchToProps = ({
    dialogOpenCall,
    resetClientEquipmentsGridCall,
    getClientEquipmentItemsCompleteApiCall,
});

const connectedEquipmentComponent = connect(mapStateToProps, mapDispatchToProps)(EquipmentComponent);
export default connectedEquipmentComponent;
