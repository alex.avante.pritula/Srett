/* GENERAL */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';

/* UI ELEMENTS */
import {IconButton, Tooltip} from 'react-mdl';

/* COMPONENTS */
import GoogleMap from '../../General/GoogleMap/GoogleMap';
import DefaultGoogleMapWindow from '../../General/GoogleMap/DefaultWindow/DefaultWindow';

/* ICONS */
import FaCog from 'react-icons/lib/fa/cog';
import TiCompass from 'react-icons/lib/ti/compass';
/* CUSTOM */
import Map from '../../General/StatusIcons/Map/Map';
import RSSI from '../../General/StatusIcons/RSSI/RSSI';
import Battery from '../../General/StatusIcons/Battery/Battery';
import Measure from '../../General/StatusIcons/Measure/Measure';

/* ACTIONS */
import {dialogOpenCall} from '../../../actions/Dialog';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';
import i18nModule from '../../../utils/i18nModule';

/* STYLES */
import './EquipmentGridItem.scss';

/* UTILS */
import equipmentTitle from '../../../utils/equipmentTitle';
import orbitrackerTitle from '../../../utils/orbitrackerTitle';

/* CONSTANTS */
import {
    DIALOGS,
    RSSI_META,
} from '../../../constants/General';
import {START_CONFIG} from '../../../constants/Map';

const {SUFFIX} = RSSI_META;
const {ZOOM} = START_CONFIG;

const {
    DETAILED_MAP,
    CONFIGURE_ORBITRACKER,
    CONFIGURE_ASSOCIATION,
} = DIALOGS;

class EquipmentGridItemComponent extends Component {

    static propTypes = {
        rssi: PropTypes.string,
        type: PropTypes.string,
        disabled: PropTypes.bool,
        battery: PropTypes.string,
        latitude: PropTypes.string,
        lastmeasure: PropTypes.any,
        longitude: PropTypes.string,
        reference: PropTypes.string,
        identifier: PropTypes.string,
        wakeUpMode: PropTypes.number,
        orbitracker: PropTypes.string,
        lastWakeUpMode: PropTypes.number,

        isOpened: PropTypes.bool.isRequired,
    };

    constructor(props) {
        super(props);

        /* BINDED HANDLERS */
        this.handleOpenGoogleMap = this.handleOpenGoogleMap.bind(this);
        this.handleConfigureAssociation = this.handleConfigureAssociation.bind(this);
        this.handleConfigureOrbitrackerDevice = this.handleConfigureOrbitrackerDevice.bind(this);
    }

    handleConfigureAssociation(){
        const {
            isOpened,
            
            type,
            identifier,
            orbitracker,
        } = this.props;

        if(isOpened) return;
        
        this.props.dialogOpenCall({
            type,
            identifier,
            orbitracker,
            dialogName: CONFIGURE_ASSOCIATION,
        });
    }

    handleOpenGoogleMap(){
        const {
            disabled,
            isOpened,
            identifier,
        } = this.props;

        if(disabled || isOpened) return;
        
        const title = i18nModule.t('CLIENT_EQUIPMENT.DIALOGS.DETAILED_MAP');

        this.props.dialogOpenCall({
            identifier,
            dialogName: DETAILED_MAP,
            customHeader: `${equipmentTitle(identifier)} - ${title}`,
        });
    }

    handleConfigureOrbitrackerDevice(){
        const {
            disabled,
            isOpened,
            orbitracker,
        } = this.props;

        if(disabled || isOpened) return;

        const title = i18nModule.t('ORBITRACKER.DIALOGS.CONFIGURE_ORBITRACKER');

        this.props.dialogOpenCall({
            orbitracker,
            dialogName: CONFIGURE_ORBITRACKER,
            identifier: orbitracker,
            customHeader: `${orbitrackerTitle(orbitracker)} - ${title}`,
        });
    }

    render() {
        const {
            rssi,
            battery,
            disabled,
            latitude,
            longitude,
            identifier,
            wakeUpMode,
            lastmeasure,
            lastWakeUpMode,
            
            equipment,
            orbitracker,

            isOpened,
        } = this.props;
        
        return (
            <I18n ns="translations">{
                (translate, {i18n}) => (
                    <section className="equipment-container">
                       <div className="equipment-container-description">
                            <div className="equipment-container-description-gps equipment-container-description-item">
                                <span className="equipment-container-description-item-title">
                                    {translate('CLIENT_EQUIPMENT.ITEM.ASSOCIATED_GPS')}
                                </span>
                                <div className="equipment-container-description-item-content">
                                    <p className="equipment-container-description-item-content-field">
                                        {orbitrackerTitle(orbitracker)}
                                    </p>
                                    <Tooltip
                                        position="left"
                                        label={translate('CLIENT_EQUIPMENT.TOOLTIP.CONFIGURE_ASSOCIATION')}
                                        className="custom-ui-tooltip">
                                        <IconButton
                                            component={FaCog} 
                                            disabled={isOpened}
                                            name="config-button"
                                            onClick={this.handleConfigureAssociation}
                                        />  
                                    </Tooltip>
                                    <Tooltip 
                                        position="left"
                                        label={translate('CLIENT_EQUIPMENT.TOOLTIP.CONFIGURE_ORBITRACKER')}
                                        className="custom-ui-tooltip">
                                        <IconButton 
                                            component={TiCompass} 
                                            name="assosiate-button" 
                                            disabled={isOpened || disabled}
                                            onClick={this.handleConfigureOrbitrackerDevice}
                                        /> 
                                    </Tooltip>
                                </div>
                            </div>
                            <div className="equipment-container-description-battery-level equipment-container-description-item">
                                <span className="equipment-container-description-item-title">
                                    {translate('CLIENT_EQUIPMENT.ITEM.BATTERY_LEVEL')}
                                </span>
                                <div className="equipment-container-description-item-content">
                                    <Battery value={battery} disabled={disabled} />
                                    <p className="equipment-container-description-item-content-field">
                                        {battery ? `${battery} %`: '-'}
                                    </p>
                                </div>
                            </div>
                            <div className="equipment-container-description-rssi equipment-container-description-item">
                                <span className="equipment-container-description-item-title">
                                    {translate('CLIENT_EQUIPMENT.ITEM.RSSI')}
                                </span>
                                <div className="equipment-container-description-item-content">
                                    <RSSI value={rssi} disabled={disabled}/>
                                    <p className="equipment-container-description-item-content-field">
                                        {rssi ? `${rssi} ${SUFFIX}`: '-'}
                                    </p>
                                </div>
                            </div>
                            <div className="equipment-container-description-wake-up equipment-container-description-item">
                                <span className="equipment-container-description-item-title">
                                    {translate('CLIENT_EQUIPMENT.ITEM.WAKE_UP_MODE')}
                                </span>
                                <div className="equipment-container-description-item-content">
                                    <p className="equipment-container-description-item-content-field">
                                        {wakeUpMode !== undefined ? translate(`WAKE_UP_MODE.${wakeUpMode}`) : '-'} 
                                    </p>
                                </div>
                            </div>
                            <div  className="equipment-container-description-item">
                                <span className="equipment-container-description-item-title">
                                    {translate('CLIENT_EQUIPMENT.ITEM.STATUS')}
                                </span>
                                <div className="equipment-container-description-item-content equipment-container-description-status">
                                    <Measure lastWakeUpMode={lastWakeUpMode} lastmeasure={lastmeasure} disabled={disabled}/>
                                    <Battery value={battery} onlyError={true} disabled={disabled}/>
                                    <RSSI value={rssi} lastmeasure={lastmeasure} onlyError={true} disabled={disabled}/>
                                    <Map latitude={latitude} longitude={longitude} disabled={disabled}/>
                                </div>
                            </div>        
                        </div>
                        {!equipment && 
                            (<div className="equipment-container-map">{
                                    (!disabled && parseFloat(latitude) && parseFloat(longitude)) ?  
                                        <GoogleMap
                                            scrollwheel={false}
                                            zoomControl={false}
                                            fullscreenControl={false}
                                            onGoogleMapClick = {this.handleOpenGoogleMap}
                                            zoom={ZOOM * 1.5}
                                            center={{
                                                lat: parseFloat(latitude), 
                                                lng: parseFloat(longitude),
                                            }}
                                            markers={[{
                                                lat: parseFloat(latitude), 
                                                lng: parseFloat(longitude),
                                                onMarkerClick: this.handleOpenGoogleMap,
                                            }]}
                                        />
                                    :
                                        <DefaultGoogleMapWindow />
                                }
                            </div>)
                        }
                    </section>
                )
            }</I18n>
        );
    }
}

/* REDUX */
const mapStateToProps = ({Equipment, Dialog}, {index, equipment}) => {
    const {equipments} = Equipment;

    const {
        type,
        tracker,
        reference,
        identifier,
        lastmeasure,
    } = equipments.get(index) || equipment || {};

    const {
        rssi,
        battery,
        latitude,
        longitude,
        wakeUpMode,
        lastWakeUpMode,
    } = tracker || {};

    const {isOpened} = Dialog;

    return {
        rssi,
        type,
        battery,
        latitude,
        longitude,
        reference,
        identifier,
        wakeUpMode,
        lastmeasure,
        lastWakeUpMode,
        disabled: (tracker ? !tracker.identifier : true),
        orbitracker: (tracker ? tracker.identifier : ''),

        isOpened,
    };
};

const mapDispatchToProps = ({
    dialogOpenCall,
});

const connectedEquipmentGridItemComponent = connect(mapStateToProps, mapDispatchToProps)(EquipmentGridItemComponent);
export default connectedEquipmentGridItemComponent;
