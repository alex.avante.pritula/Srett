/* GENERAL */
import classnames from 'classnames';

/* REACT */
import React, {Component} from 'react';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';

/* UI ELEMENTS */
import {Tab, Tabs} from 'react-mdl';

/* COMPONENTS */
import EquipmentDetailedMap from './EquipmentDetailedMap/EquipmentDetailedMap';
import EquipmentDetailedPositions from './EquipmentDetailedPositions/EquipmentDetailedPositions';

/* STYLES */
import './EquipmentDetailed.scss';

class EquipmentDetailedComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {activeTab: 0};

        /* BINDED HANDLERS */
        this.handleTabChange = this.handleTabChange.bind(this);
    }

    handleTabChange(activeTab){
       this.setState({activeTab});
    }

    get ActiveTab () {
        switch (this.state.activeTab) {
            case 0: {
                return <EquipmentDetailedMap />;
            }
            case 1: {
                return <EquipmentDetailedPositions />;
            }
            default: {
                return <EquipmentDetailedMap />;
            }
        }
    }

    render() {
        return (
            <I18n ns="translations">{
                (translate, { i18n }) => (
                    <section className="equipment-modal-container">
                        <div className="equipment-detailed-map-tabs">
                            <Tabs activeTab={this.state.activeTab} onChange={this.handleTabChange} ripple>
                                <Tab>{translate('CLIENT_EQUIPMENT.TABS.MAP')}</Tab>
                                <Tab>{translate('CLIENT_EQUIPMENT.TABS.DETAIL_POSITIONS')}</Tab>
                            </Tabs>
                            <section className="equipment-detailed-map-tab">
                                {this.ActiveTab}
                            </section>
                        </div>    
                    </section>
                )
            }</I18n>
        );
    }
}

export default EquipmentDetailedComponent;