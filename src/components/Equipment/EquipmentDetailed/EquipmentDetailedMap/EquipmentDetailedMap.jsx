/* GENERAL */
import classnames from 'classnames';

/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';
import {SubmissionError, reduxForm, Field, reset} from 'redux-form';

/* COMPONENTS */
import GoogleMap from '../../../General/GoogleMap/GoogleMap';
import OverlayWindow from '../../../GlobalMap/OverlayWindow/OverlayWindow';

/* UI ELEMENTS */
/* CUSTOM */
import DateTimePicker from '../../../UI/DateTimePicker/DateTimePicker';
import LoadingContainer from '../../../General/LoadingContainer/LoadingContainer';

/* ICONS */
import FaMinus from 'react-icons/lib/fa/minus';

/* ACTIONS */
import {
    getEquipmentPositionsByTimeRangeApiCall,
    getEquipmentPositionsByTimeRangeCompleteApiCall,
} from '../../../../actions/EquipmentPositions';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';
import i18nModule, {moment} from '../../../../utils/i18nModule';

/* UTILS */
import markerColor from '../../../../utils/markerColor';
import orbitrackerTitle from '../../../../utils/orbitrackerTitle';

/* STYLES */
import './EquipmentDetailedMap.scss';

/* CONSTANTS */
import {START_CONFIG} from '../../../../constants/Map';

import {
    NOW,
    REDUX_FORM,
    MAX_COLORED_MARKERS,
    DISABLED_MARKER_COLOR,
} from '../../../../constants/General';

const {ZOOM, LOCATION} = START_CONFIG;
const {EQUIPMENT_DETAILED_MAP} = REDUX_FORM;

class EquipmentDetailedMapComponent extends Component {
    
    static propTypes = {
        /* REDUX */
        language: PropTypes.string.isRequired,
        positions: PropTypes.object.isRequired,
        identifier: PropTypes.string.isRequired,

        isFetching: PropTypes.bool.isRequired, 

        /* REDUX FORM META */
        invalid:  PropTypes.bool.isRequired,
        pristine: PropTypes.bool.isRequired,
        submitting: PropTypes.bool.isRequired,
        handleSubmit: PropTypes.func.isRequired,

        /* ACTIONS */
        getEquipmentPositionsByTimeRangeCompleteApiCall: PropTypes.func.isRequired,
    };

    constructor(props){
        super(props);

        this.state = {
            hoverIndex: -1,
            disabledHover: false,
        };

        /* BINDED HANDLERS */
        this.handleZoomChange = this.handleZoomChange.bind(this);
        this.handleMarkerMouseOut = this.handleMarkerMouseOut.bind(this);
        this.handleMarkerMouseOver = this.handleMarkerMouseOver.bind(this);
        this.handleOnDateTimePickerBlur = this.handleOnDateTimePickerBlur.bind(this);
        this.handleOnDateTimePickerFocus = this.handleOnDateTimePickerFocus.bind(this);
    }

    componentWillMount(){
        const {
            to,
            from,
            identifier,
            getEquipmentPositionsByTimeRangeCompleteApiCall,
        } = this.props;

        getEquipmentPositionsByTimeRangeCompleteApiCall({
            to,
            from,
            identifier,
        });
    }

    componentWillReceiveProps({isFetching}) {
        if(this.props.isFetching !== isFetching) {
            this.setState({disabledHover: isFetching});
        }
    }

    handleOnDateTimePickerFocus(){
        this.setState({
            hoverIndex: -1,
            disabledHover: true,
        });
    }

    handleOnDateTimePickerBlur(){
        this.setState({disabledHover: false});
    }

    handleMarkerMouseOut(){
        this.setState({hoverIndex: -1});
    }

    handleMarkerMouseOver(hoverIndex){
        if(!this.state.disabledHover) {
            this.setState({hoverIndex});
        }
    }

    handleZoomChange(){
        this.setState({hoverIndex: -1});
    }

    componentWillUnmount(){
        //this.handleOnDateTimePickerBlur();
    }

    render() {
        const {
            to,
            
            invalid,
            pristine,
            submitting,
            handleSubmit,

            language,
            positions,

            isFetching,
        } = this.props;

        const {hoverIndex} = this.state;

        const markers = [];

        positions.forEach((position, index) => {
            const {
                latitude,
                longitude,
                identifier,
                lastmeasure,
            } = position;

            const {lat, lng} = ((markers.length < 1) ? {} : markers[markers.length - 1]);

            if(parseFloat(latitude) && parseFloat(longitude) && (lat !== latitude || lng !== longitude)){

                const overlayContent = ((index === hoverIndex) && (
                    <OverlayWindow 
                        date={lastmeasure}
                        title={orbitrackerTitle(identifier)}
                    />
                ));
    
                markers.push({
                    lat: parseFloat(latitude), 
                    lng: parseFloat(longitude),
    
                    fillColor: DISABLED_MARKER_COLOR,
                    strokeColor: DISABLED_MARKER_COLOR, 
    
                    overlayContent,
    
                    onMarkerMouseOut: this.handleMarkerMouseOut,
                    onMarkerMouseOver: () => { this.handleMarkerMouseOver(index); },
                });
            }
    
        });

        const coloredCount = Math.min(MAX_COLORED_MARKERS, markers.length);

        const defaultCenter = markers.length ? markers[0] : null;

        for(let i = 0; i < coloredCount; i++) {
            const markerItemColor = markerColor(coloredCount, i);
            markers[i].fillColor = markerItemColor;
            markers[i].strokeColor = markerItemColor;
        }
        
        return (
            <I18n ns="translations">{
                (translate) => (
                    <section className="equipment-modal-container">
                        {(submitting || isFetching) ? <LoadingContainer /> :
                        <GoogleMap
                            zoom={ZOOM * 2}
                            markers={markers}
                            center={defaultCenter}
                            onGoogleMapZoomChanged={this.handleZoomChange}
                        />}
                        <form className="equipment-modal-container-detailed-map-form">
                            <div className="equipment-modal-container-detailed-map-form-pickers">
                                <Field
                                    label=""
                                    max={to}
                                    name="from" 
                                    locale={language}
                                    component={DateTimePicker}
                                    disabled={submitting || isFetching} 
                                    handleOnBlur={this.handleOnDateTimePickerBlur}
                                    handleOnFocus={this.handleOnDateTimePickerFocus}
                                    className="equipment-modal-container-detailed-map-form-pickers-datetimepicker"
                                />
                                <FaMinus className="equipment-modal-container-detailed-map-form-pickers-line" />
                                <Field
                                    label=""
                                    max={NOW}
                                    name="to" 
                                    locale={language}
                                    component={DateTimePicker}
                                    disabled={submitting || isFetching} 
                                    handleOnBlur={this.handleOnDateTimePickerBlur}
                                    handleOnFocus={this.handleOnDateTimePickerFocus}
                                    className="equipment-modal-container-detailed-map-form-pickers-datetimepicker"
                                />
                            </div>
                        </form>
                    </section>
                )
            }</I18n>
        );
    }
}

/* REDUX */
const mapStateToProps = (state) => { 
    const {
        User, 
        Dialog, 
        EquipmentPositions,
    } = state;

    const {meta} = Dialog;
    const {language} = User;
    const {identifier} = meta;
    const {
        to,
        from, 
        positions,

        isFetching,
    } = EquipmentPositions;

    return {
        to,
        from, 
        language,
        positions,
        identifier,
        isFetching,
        initialValues: {from, to},
    }; 
};
const mapDispatchToProps = ({
    getEquipmentPositionsByTimeRangeCompleteApiCall,
});

/* REDUX FORM */
const validate = ({to, from}) => {
    const errors = {};
    const today = moment();

    if(today < from) {
        errors.from  = i18nModule.t('REDUX_FORM_ERROR.DATE.NOW');
    }

    if(today < to) {
        errors.to = i18nModule.t('REDUX_FORM_ERROR.DATE.NOW');
    } 

    if(!errors.from && !errors.to && from > to) {
        errors.to = errors.from = i18nModule.t('REDUX_FORM_ERROR.DATE.RANGE');
    }

    return errors;
};

const equipmentMapDetailedReduxForm = {
    form: EQUIPMENT_DETAILED_MAP,
    // enableReinitialize: true,
    validate: (values, props) => {
        const {dirty} = props;
        if (!dirty) return {};
        return validate(values);
    },
    onChange: (values, dispatch, props) => {
        const {from, to} = values;
        const {identifier} = props;
        
        return getEquipmentPositionsByTimeRangeApiCall({
            to,
            from,
            identifier,
        }, dispatch);
    },
};

const bindedReduxFormEquipmentDetailedMapComponent = reduxForm(equipmentMapDetailedReduxForm)(EquipmentDetailedMapComponent);
const connectedEquipmentDetailedMapComponent = connect(mapStateToProps, mapDispatchToProps)(bindedReduxFormEquipmentDetailedMapComponent);
export default connectedEquipmentDetailedMapComponent;
