/* GENERAL */
import classnames from 'classnames';

/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';
import {SubmissionError, reduxForm, Field, reset} from 'redux-form';

/* ICONS */
import FaMinus from 'react-icons/lib/fa/minus';
import TiIcon from 'react-icons/lib/ti/download';
/* CUSTOM */
import Map from '../../../General/StatusIcons/Map/Map';
import Battery from '../../../General/StatusIcons/Battery/Battery';
import Measure from '../../../General/StatusIcons/Measure/Measure';

/* ACTIONS */
import {
    onEquipmentPositionsGridPageChangedCall,
    onEquipmentPositionsGridSortChangedCall,
    getEquipmentPositionsByTimeRangeApiCall,
    getEquipmentPositionsByTimeRangeCompleteApiCall,
} from '../../../../actions/EquipmentPositions';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';
import i18nModule, {moment} from '../../../../utils/i18nModule';

/* UTILS */
import orbitrackerTitle from '../../../../utils/orbitrackerTitle';

/* UI ELEMENTS */
import {IconButton} from 'react-mdl';
/* CUSTOM */
import Grid from '../../../UI/Grid/Grid';
import DateTimePicker from '../../../UI/DateTimePicker/DateTimePicker';

/* STYLES */
import './EquipmentDetailedPositions.scss';

/* CONSTANTS */
import {
    NOW,
    REDUX_FORM,
    DEFAULT_SORTING,
    CSV_EXPORT_META,
    DEFAULT_DATE_TIME_FORMAT,
} from '../../../../constants/General';

const {EQUIPMENT_DETAILED_POSITIONS} = REDUX_FORM;

const {
    PREFIX,
    NEW_ROW,    
    SEPARATOR,
} = CSV_EXPORT_META;

class EquipmentDetailedPositionsComponent extends Component {

    static propTypes = {
        /* META */
        isFetching: PropTypes.bool.isRequired,

        /* REDUX FORM META */
        invalid:  PropTypes.bool.isRequired,
        pristine: PropTypes.bool.isRequired,
        submitting: PropTypes.bool.isRequired,
        handleSubmit: PropTypes.func.isRequired,

        /* REDUX STATE PROPS */
        to: PropTypes.object.isRequired, 
        from: PropTypes.object.isRequired,

        current: PropTypes.number.isRequired,
        totalPages: PropTypes.number.isRequired,

        language: PropTypes.string.isRequired,
        positions: PropTypes.object.isRequired,
        visibleItems: PropTypes.object.isRequired,

        identifier: PropTypes.string.isRequired,

        /* ACTIONS */
        onEquipmentPositionsGridPageChangedCall: PropTypes.func.isRequired,
        onEquipmentPositionsGridSortChangedCall: PropTypes.func.isRequired,
        getEquipmentPositionsByTimeRangeCompleteApiCall: PropTypes.func.isRequired,
    };

    constructor(props){
        super(props);

        /* BINDED HANDLERS */
        this.handlePageChange = this.handlePageChange.bind(this);
        this.handleSortedChange = this.handleSortedChange.bind(this);
    }

    componentWillMount(){
        const {
            to,
            from,
            identifier,
            getEquipmentPositionsByTimeRangeCompleteApiCall,
        } = this.props;

        getEquipmentPositionsByTimeRangeCompleteApiCall({
            to,
            from,
            identifier,
        });
    }

    handlePageChange(page){
        this.props.onEquipmentPositionsGridPageChangedCall(page);
    }

    handleSortedChange(sorted){
        const {id, desc} = sorted[0] || {};
        const sortOrder = desc ? DESC : ASC;
        const sortField = id || DEFAULT_SORTING.DETAILED_POSITIONS;
        this.props.onEquipmentPositionsGridSortChangedCall(sortField, sortOrder);
    }

    render() {
        const {
            to,

            language,
            positions,
            visibleItems,

            identifier,

            pristine,
            submitting,
            handleSubmit,

            current,
            totalPages,

            isFetching,
        } = this.props;  

        const EXPORT_DISABLE = (positions.size < 1 || submitting || isFetching);

        const EXPORT_TITLE  = [
                i18nModule.t('CLIENT_EQUIPMENT.GRID.DETAILED.DATE'),
                i18nModule.t('CLIENT_EQUIPMENT.GRID.DETAILED.ORBITRACKER'),
                i18nModule.t('CLIENT_EQUIPMENT.GRID.DETAILED.LATITUDE'),
                i18nModule.t('CLIENT_EQUIPMENT.GRID.DETAILED.LONGITUDE'),
                i18nModule.t('CLIENT_EQUIPMENT.GRID.DETAILED.STATUS'),
            ].join(SEPARATOR);

        const EXPORT_FORMATTED_ITEMS = positions.map((item) => {
            const {
                status,
                latitude,
                longitude,
                identifier,                
                lastmeasure,
            } = item;

            return [
                lastmeasure.format(DEFAULT_DATE_TIME_FORMAT),
                orbitrackerTitle(identifier),
                latitude,
                longitude,
                status,
            ].join(SEPARATOR);
        });

        const EXPORT = [EXPORT_TITLE, ...EXPORT_FORMATTED_ITEMS].join(NEW_ROW);

        return (
            <I18n ns="translations">{
                (translate) => (
                    <section className="equipment-modal-container">
                     <div className="equipment-modal-container-navigation">
                            <form className="equipment-modal-container-navigation-form">
                                <Field
                                    label=""
                                    max={to}
                                    name="from" 
                                    locale={language}
                                    component={DateTimePicker} 
                                    disabled={submitting || isFetching}
                                    className="equipment-modal-container-navigation-form-datetimepicker"
                                />
                                <FaMinus className="equipment-modal-container-navigation-form-line" />
                                <Field
                                    label=""
                                    name="to" 
                                    max={NOW}
                                    locale={language}
                                    component={DateTimePicker} 
                                    disabled={submitting || isFetching}
                                    className="equipment-modal-container-navigation-form-datetimepicker"
                                />
                            </form>
                            {EXPORT_DISABLE ? 
                                <IconButton 
                                    component={TiIcon} 
                                    disabled={EXPORT_DISABLE}
                                    name="orbitracker-modal-container-navigation-export" 
                                    className="orbitracker-modal-container-navigation-export"
                                /> :
                                <a target="_blank" download={`${identifier}.csv`} href={`${PREFIX},${EXPORT}`}>
                                    <IconButton 
                                        component={TiIcon}
                                        name="orbitracker-modal-container-navigation-export" 
                                        className="orbitracker-modal-container-navigation-export"
                                    />
                                </a>
                            }
                        </div>
                        <div className="detailed-position-grid-wrapper">  
                            <Grid 
                                data={
                                    visibleItems.toArray().map((item) => {
                                        const {
                                            battery,
                                            latitude,
                                            longitude,
                                            identifier,
                                            lastmeasure,
                                            lastWakeUpMode,
                                        } = item;
                                        
                                        return {
                                            lastmeasure: (
                                                <span className="grid-container-row-item grid-container-row-item-title">
                                                    {lastmeasure ? lastmeasure.format(DEFAULT_DATE_TIME_FORMAT) : <b>-</b>}
                                                </span>
                                            ),
                            
                                            identifier: (
                                                <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-centered">
                                                    {orbitrackerTitle(identifier)}
                                                </span>
                                            ),
                            
                                            status: (
                                                <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-centered grid-container-row-item-bold">
                                                    <Measure lastWakeUpMode={lastWakeUpMode} />
                                                    <Battery value={battery} onlyError={true}/>
                                                    <Map latitude={latitude} longitude={longitude} />
                                                </span>
                                            ),
                            
                                            latitude: ( 
                                                <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-centered grid-container-row-item-bold">
                                                    {(parseFloat(latitude) || <b>-</b>)}
                                                </span>
                                            ),
                            
                                            longitude: (
                                                <span className="grid-container-row-item grid-container-row-item-title grid-container-row-item-centered grid-container-row-item-bold">
                                                    {(parseFloat(longitude) || <b>-</b>)}
                                                </span>
                                            ),
                                        };
                                    })
                                }
                                page={current}
                                defaultSorted={[{
                                    desc: true,
                                    id: DEFAULT_SORTING.DETAILED_POSITIONS,
                                }]}
                                hasCustomExpander={true}
                                columns={[{
                                        Header: (
                                            <span className="grid-container-column-title">
                                                {translate('CLIENT_EQUIPMENT.GRID.DETAILED.DATE')}
                                            </span>
                                        ),
                                        accessor: 'lastmeasure',
                                    },{
                                        Header:  (
                                            <span className="grid-container-column-title">
                                                {translate('CLIENT_EQUIPMENT.GRID.DETAILED.ORBITRACKER')}
                                            </span>
                                        ),
                                        accessor: 'identifier',
                                    },{
                                        Header:  (
                                            <span className="grid-container-column-title">
                                                {translate('CLIENT_EQUIPMENT.GRID.DETAILED.LATITUDE')}
                                            </span>
                                        ),
                                        accessor: 'latitude',
                                    },{
                                        Header: (
                                            <span className="grid-container-column-title">
                                                {translate('CLIENT_EQUIPMENT.GRID.DETAILED.LONGITUDE')}
                                            </span>
                                        ),
                                        accessor: 'longitude',
                                    },{
                                        Header: (
                                            <span className="grid-container-column-title">
                                                {translate('CLIENT_EQUIPMENT.GRID.DETAILED.STATUS')}
                                            </span>
                                        ),
                                        accessor: 'status',
                                        sortable: false,
                                }]}
                                isFetching={false}
                                totalPages={totalPages}
                                handlePageChange={this.handlePageChange}
                                handleSortedChange={this.handleSortedChange}
                            />
                        </div> 
                    </section>
                )
            }</I18n>
        );
    }
}

/* REDUX */
const mapStateToProps = (state) => {
    const {
        User, 
        Dialog, 
        EquipmentPositions,
    } = state;

    const {meta} = Dialog;
    const {language} = User;
    const {identifier} = meta;

    const {
        to,
        from, 
        positions,
        visibleItems,

        current,
        totalPages,
        isFetching,
    } = EquipmentPositions;

    return {
        to,
        from,
        language,
        positions,
        visibleItems,

        identifier,

        current,
        totalPages,
        isFetching,

        initialValues: {from, to},
    }; 
};

const mapDispatchToProps = ({
    onEquipmentPositionsGridPageChangedCall,
    onEquipmentPositionsGridSortChangedCall,
    getEquipmentPositionsByTimeRangeCompleteApiCall,
});

/* REDUX FORM */
const validate = ({to, from}) => {
    const errors = {};
    const today = moment();

    if(today < from) {
        errors.from  = i18nModule.t('REDUX_FORM_ERROR.DATE.NOW');
    }

    if(today < to) {
        errors.to = i18nModule.t('REDUX_FORM_ERROR.DATE.NOW');
    } 

    if(!errors.from && !errors.to && from > to) {
        errors.to = errors.from = i18nModule.t('REDUX_FORM_ERROR.DATE.RANGE');
    }

    return errors;
};

const EquipmentDetailedPositionsForm = {
    form: EQUIPMENT_DETAILED_POSITIONS,
    // enableReinitialize: true,
    validate: (values, props) => {
        const {dirty} = props;
        if (!dirty) return {};
        return validate(values);
    },
    onChange: (values, dispatch, props) => {
        const {from, to} = values;
        const {identifier} = props;
        console.warn(from , to ); 
        return getEquipmentPositionsByTimeRangeApiCall({
            to,
            from,
            identifier,
        }, dispatch);
    },
};

const bindedReduxEquipmentDetailedPositionsComponent = reduxForm(EquipmentDetailedPositionsForm)(EquipmentDetailedPositionsComponent);
const connectedEquipmentDetailedPositionsComponent = connect(mapStateToProps, mapDispatchToProps)(bindedReduxEquipmentDetailedPositionsComponent);
export default connectedEquipmentDetailedPositionsComponent;
