/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';

/* LOCALIZATION */
import {I18n, Trans} from 'react-i18next';

/* COMPONENTS */
import GoogleMap from '../General/GoogleMap/GoogleMap';

import InfoWindow from './InfoWindow/InfoWindow';
import OverlayWindow from './OverlayWindow/OverlayWindow';

/* UTILS */
import equipmentTitle from '../../utils/equipmentTitle';

/* UI COMPONENTS */
import {IconButton} from 'react-mdl';
/* CUSTOM */
import TextField from '../UI/TextField/TextField';

/* ACTIONS */
import {
    resetGlobalMapCall,
    changeMapCenterCall,
    clientMapEquipmentItemsCompleteApiCall,
} from '../../actions/GlobalMap';

/* STYLES */
import './GlobalMap.scss';

/* CONSTANTS */
import {
    REDUX_FORM,
    DEFAULT_DATE_TIME_FORMAT,
} from '../../constants/General';

const {GOOGLE_MAP} = REDUX_FORM; 

class GlobalMapComponent extends Component {

    static propTypes = { 
        /* REDUX STATE PROPS */
        items: PropTypes.object.isRequired,
        center: PropTypes.object.isRequired, 
        identifier: PropTypes.string.isRequired,

        /* ACTIONS */
        resetGlobalMapCall: PropTypes.func.isRequired,
        clientMapEquipmentItemsCompleteApiCall: PropTypes.func.isRequired, 
    };

    constructor(props) {
        super(props);

        this.state = {
            hoverIndex: -1,
            activeIndex: -1,
        };

        /* BINDED HANDLERS */
        this.handleZoomChange = this.handleZoomChange.bind(this);
        this.handleMarkerClick = this.handleMarkerClick.bind(this);
        this.handlerCenterChanged = this.handlerCenterChanged.bind(this);
        this.handleMarkerMouseOut = this.handleMarkerMouseOut.bind(this);
        this.handleMarkerMouseOver = this.handleMarkerMouseOver.bind(this);
        this.handleInfoWindowClose = this.handleInfoWindowClose.bind(this);
    }

    componentWillMount(){
        const {
            identifier,
            changeMapCenterCall,
            clientMapEquipmentItemsCompleteApiCall,
        } = this.props;

        changeMapCenterCall(identifier);
        clientMapEquipmentItemsCompleteApiCall();
    }

    componentWillUnmount(){
        this.props.resetGlobalMapCall();
    }

    handleMarkerClick(activeIndex) {
        this.setState({activeIndex});
    }

    handleMarkerMouseOut(){
        this.setState({hoverIndex: -1});
    }

    handleMarkerMouseOver(hoverIndex){
        this.setState({hoverIndex});
    }

    handleInfoWindowClose(){
        this.setState({activeIndex: -1});
    }

    handlerCenterChanged({latitude, longitude}){
        const {
            identifier,
            changeMapCenterCall,
        } = this.props;

        changeMapCenterCall(identifier, {latitude, longitude});
    }

    handleZoomChange(){
        this.setState({
            activeIndex: -1,
            hoverIndex: -1,
        });
    }

    render() {
        const markers = [];
        let infoContent = (<div />);

        const {
            hoverIndex,
            activeIndex,
        } = this.state;

        const {
            items,
            center,
        } = this.props;

        items.forEach((equipment, index) => {
            const {
                type,
                tracker,
                identifier,
                lastmeasure,
            } = equipment;

            const {
                rssi,
                battery,
                latitude,
                longitude,
            } = tracker;

            if(parseFloat(longitude) && parseFloat(latitude)) {
                const infoContent = ((index === activeIndex) && (
                    <InfoWindow 
                        type={type}
                        rssi={rssi}
                        battery={battery}
                        equipmentRef={identifier}
                        latitude={parseFloat(latitude)}
                        longitude={parseFloat(longitude)}
                        orbitrackerRef={tracker.identifier}
                    />
                ));

                const overlayContent = ((index === hoverIndex) && (
                    <OverlayWindow 
                        title={equipmentTitle(identifier)}
                        date={lastmeasure}
                    />
                ));
                
                markers.push({
                    index,
                    lat: latitude, 
                    lng: longitude,

                    infoContent,
                    overlayContent,
                    
                    onMarkerMouseOut: this.handleMarkerMouseOut,
                    onMarkerClick: () => { this.handleMarkerClick(index); },
                    onMarkerMouseOver: () => { this.handleMarkerMouseOver(index); },
                });
            };
        }); 

        return (
            <I18n ns="translations">{
                (translate, {i18n}) => (
                    <section className="global-map-container">
                        <GoogleMap 
                            center={center}
                            markers={markers} 
                            onCenterChanged={this.handlerCenterChanged}
                            onGoogleMapClick={this.handleInfoWindowClose}
                            onInfoWindowClose={this.handleInfoWindowClose}
                            onGoogleMapZoomChanged={this.handleZoomChange}
                        />
                    </section>
                )
            }
            </I18n>
        );
    }
}

/* REDUX */
const mapStateToProps = ({GlobalMap, User}) => { 
    const {
        items,
        latitude,
        longitude,
    } = GlobalMap;

    const {identifier} = User;

    return {
        items,
        identifier,
        center: {
            lat: latitude,
            lng: longitude,
        },
    };
};

const mapDispatchToProps = ({
    resetGlobalMapCall,
    changeMapCenterCall,
    clientMapEquipmentItemsCompleteApiCall,
});

const connectedGlobalMapComponent = connect(mapStateToProps, mapDispatchToProps)(GlobalMapComponent);
export default connectedGlobalMapComponent;
