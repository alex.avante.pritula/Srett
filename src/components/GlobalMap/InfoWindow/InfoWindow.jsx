
/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';

/* UI ELEMENTS */
import {IconButton}  from 'react-mdl';

/* ICONS */
import MdViewHeadline from 'react-icons/lib/md/view-headline';
/* CUSTOM */
import RSSI from '../../General/StatusIcons/RSSI/RSSI';
import Battery from '../../General/StatusIcons/Battery/Battery';

/* LOCALIZATION */
import I18nModule from '../../../utils/i18nModule';

/* ACTIONS */
import {dialogOpenCall} from '../../../actions/Dialog';
import {changeMapCenterCall} from '../../../actions/GlobalMap';

/* UTILS */
import equipmentTitle from '../../../utils/equipmentTitle';
import orbitrackerTitle from '../../../utils/orbitrackerTitle';

/* STYLES */
import './InfoWindow.scss';

/* CONSTANTS */
import {
    DIALOGS,
    RSSI_META,
    SEARCH_META,
} from '../../../constants/General';

const {SUFFIX} = RSSI_META;
const {
    EQUIPMENT_DETAILS,
    ORBITRACKER_DETAILS,
} = DIALOGS;

class InfoWindowComponent extends Component {

    static propTypes = {
        user: PropTypes.string,

        type: PropTypes.string,
        rssi: PropTypes.string,
        battery: PropTypes.string, 

        latitude: PropTypes.number, 
        longitude: PropTypes.number, 
        
        equipmentRef: PropTypes.string, 
        orbitrackerRef: PropTypes.string,

        dialogOpenCall: PropTypes.func.isRequired, 
        changeMapCenterCall: PropTypes.func.isRequired, 
    };

    constructor(props){
        super(props);

        /* BINDED HANDLERS */
        this.handleRedirectToEquipment = this.handleRedirectToEquipment.bind(this);
        this.handleRedirectToOrbitracker = this.handleRedirectToOrbitracker.bind(this);
    }

    handleRedirectToOrbitracker() {
        const {
            user,
            
            latitude,
            longitude,
            orbitrackerRef,

            dialogOpenCall,
            changeMapCenterCall,
        } = this.props;

        dialogOpenCall({
            identifier: orbitrackerRef,
            dialogName: ORBITRACKER_DETAILS,
            customHeader: orbitrackerTitle(orbitrackerRef),
        });

        changeMapCenterCall(user, {latitude, longitude});
    }

    handleRedirectToEquipment() {
        const {
            user,

            latitude,
            longitude,
            equipmentRef,

            dialogOpenCall,
            changeMapCenterCall,
        } = this.props;

        dialogOpenCall({
            identifier: equipmentRef,
            dialogName: EQUIPMENT_DETAILS,
            customHeader: equipmentTitle(equipmentRef),  
        });

        changeMapCenterCall(user, {latitude, longitude});
    }

    render() {

        const {
            type,
            rssi,
            battery,
            equipmentRef,
            orbitrackerRef,
        } = this.props;

        return (
            <div className="info-window-container">
                <div className="info-window-container-item info-window-container-equipment">
                    <div className="info-window-container-item-status">
                        <span>{battery ? `${battery} %`: '-'}</span>
                        <Battery value={battery}/>
                    </div>
                    <div className="info-window-container-item-links">
                        <span>{I18nModule.t('MAP.INFO_WINDOW.EQUIPMENT')}</span>
                        <IconButton 
                            component={MdViewHeadline}
                            name="equipment-link-button"  
                            onClick={this.handleRedirectToEquipment}  
                        />
                    </div>
                    <div className="info-window-container-item-refs">
                        <span>{type}</span>
                    </div>
                </div>
                <div className="info-window-container-item info-window-container-orbitracker">
                    <div className="info-window-container-item-status">
                        <span>{rssi ? `${rssi} ${SUFFIX}`: '-'}</span>
                        <RSSI value={rssi} />
                    </div>
                    <div className="info-window-container-item-links">
                        <span>{I18nModule.t('MAP.INFO_WINDOW.ORBITRACKER')}</span>
                        <IconButton 
                            component={MdViewHeadline}
                            name="orbitracker-link-button"
                            onClick={this.handleRedirectToOrbitracker}  
                        />
                    </div>
                    <div className="info-window-container-item-refs">
                        <span>{orbitrackerRef.substring(SEARCH_META.ORBITRACKER.ENTITY_TYPE_IDENTIFIER.length + 1)}</span>
                    </div>
                </div>
            </div>
        );
    }
};

/* REDUX */
const mapStateToProps = ({User}) => { 
    const {identifier} = User;
    return {user: identifier};
};

const mapDispatchToProps = ({
    dialogOpenCall,
    changeMapCenterCall,
});

const connectedInfoWindowComponent = connect(mapStateToProps, mapDispatchToProps)(InfoWindowComponent);
export default connectedInfoWindowComponent;
