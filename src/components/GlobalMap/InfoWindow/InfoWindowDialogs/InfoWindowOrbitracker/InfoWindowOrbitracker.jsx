/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';

/* ACTIONS */
import {getOrbitrackerByIdentifierCompleteApiCall} from '../../../../../actions/Orbitracker';

/* COMPONENTS */
import LoadingContainer from '../../../../General/LoadingContainer/LoadingContainer';
import OrbitrackerGridItem from '../../../../Orbitracker/OrbitrackerGridItem/OrbitrackerGridItem';

/* STYLES */
import './InfoWindowOrbitracker.scss';

class InfoWindowOrbitrackerComponent extends Component {
    
    static propTypes = {
        isFetching: PropTypes.bool.isRequired,
        identifier: PropTypes.string.isRequired, 

        /* ACTIONS */
        getOrbitrackerByIdentifierCompleteApiCall: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        const {
            identifier,
            getOrbitrackerByIdentifierCompleteApiCall,
        } = this.props;

        getOrbitrackerByIdentifierCompleteApiCall(identifier);
    }

    render() {
        const {
            isFetching,
            orbitracker,
        } = this.props;

        return (
            <div className="orbitracker-details-wrapper">
                {isFetching ? <LoadingContainer /> : <OrbitrackerGridItem orbitracker={orbitracker} />}
            </div>
        );
    }
}

/* REDUX */
const mapStateToProps = ({OrbitrackerInfo}) => {
    const {orbitracker, isFetching} = OrbitrackerInfo;
    return {orbitracker, isFetching};
};

const mapDispatchToProps = ({
    getOrbitrackerByIdentifierCompleteApiCall,
});

const connectedInfoWindowOrbitrackerComponent = connect(mapStateToProps, mapDispatchToProps)(InfoWindowOrbitrackerComponent);
export default connectedInfoWindowOrbitrackerComponent;
