/* REACT */
import PropTypes from 'prop-types';
import React, {Component} from 'react';

/* REDUX */
import {connect} from 'react-redux';

/* ACTIONS */
import {getClientEquipmentByIdentifierCompleteApiCall} from '../../../../../actions/Equipment';

/* COMPONENTS */
import LoadingContainer from '../../../../General/LoadingContainer/LoadingContainer';
import EquipmentGridItem from '../../../../Equipment/EquipmentGridItem/EquipmentGridItem';

/* STYLES */
import './InfoWindowEquipment.scss';

class InfoWindowEquipmentComponent extends Component {
    
    static propTypes = {

        isFetching: PropTypes.bool.isRequired,
        identifier: PropTypes.string.isRequired,

        /* ACTIONS */
        getClientEquipmentByIdentifierCompleteApiCall: PropTypes.func.isRequired,
    };

    componentWillMount() {
        const {
            identifier,
            getClientEquipmentByIdentifierCompleteApiCall,
        } = this.props;

        getClientEquipmentByIdentifierCompleteApiCall(identifier);
    }

    render() {
        const {
            equipment,
            isFetching,
        } = this.props;

        return (
            <div className="equipment-details-wrapper">
                {isFetching ? <LoadingContainer /> : <EquipmentGridItem equipment={equipment} />}
            </div>
        );
    }
}

/* REDUX */
const mapStateToProps = ({EquipmentInfo}) => {
    const {equipment, isFetching} = EquipmentInfo;
    return {equipment, isFetching};
};

const mapDispatchToProps = ({
    getClientEquipmentByIdentifierCompleteApiCall,
});

const connectedInfoWindowEquipmentComponent = connect(mapStateToProps, mapDispatchToProps)(InfoWindowEquipmentComponent);
export default connectedInfoWindowEquipmentComponent;
