/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* STYLES */
import './OverlayWindow.scss';

/* CONSTANTS */
import {DEFAULT_DATE_TIME_FORMAT} from '../../../constants/General';

const OverlayWindow = ({date, title}) => (
    <div className="overlay-window-container">
        <p className="overlay-window-container-title">{title}</p>
        <span className="overlay-window-container-date">
            {(date ? date.format(DEFAULT_DATE_TIME_FORMAT): <b>-</b>)}
        </span>
    </div>
);

OverlayWindow.propTypes = {
    date: PropTypes.object,
    title: PropTypes.string,
};

export default OverlayWindow;
