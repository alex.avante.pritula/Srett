/* GENERAL */
export const NODE_ENV = {
    TEST: 'test',
    DEV: 'development',
    PROD: 'production',
};

export const METHODS = {
    GET: 'GET',
    PUT: 'PUT',
    POST: 'POST',
    PATCH: 'PATCH',
    DELETE: 'DELETE',
};

export const CONTENT_TYPE = {
    JSON_BODY: 'application/json',
    URL_ENCODED_FORM: 'application/x-www-form-urlencoded',
};

export const API_ERROR_STATUS = {
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    CONFLICT: 409,
};

export const CSV_EXPORT_META = {
    NEW_ROW: '\n',    
    SEPARATOR: ';',
    PREFIX: 'data:text/csv;charset=utf-8',
};

/* APP */
export const PAGES = {
    START: '/',  
    ANOTHER: '*',
    SUB: 'srett',

    MAP: 'map',
    LOGIN: 'login',
    ORBITRACKER: 'gps',
    SETTINGS: 'settings',
    EQUIPMENT: 'equipment',
};

export const LOCALIZATION = {
    EN: 'en',
    FR: 'fr',
};

export const NOW = 'now';

export const DOMAIN_ID = 'srett';
export const API_PREFIX_URL =  {
    WS: 'http://ws-sirius.orbitrack.net',
    OAUTH: 'http://oauth-sirius.orbitrack.net/oauth/token',
    GATEWAY: 'http://objenious.orbitrack.net',
};

export const VALIDATION = {
    HASH: /\#(.*?)\?/,
    EMAIL: /\S+@\S+\.\S+/,
    DECIMAL: "-?[0-9]*(\.[0-9]+)?",
    ALPHANUMERIC: /^[a-zA-Z0-9_]*$/,
    PASSWORD: /^((?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9]).{6,})\S$/,
};

export const DIALOGS = {
    DETAILED_MAP: 'detailed-map-dialog',
    ADD_NEW_EQUIPMENT: 'add-new-equipment-dialog',
    EQUIPMENT_DETAILS: 'equipment-details-dialog',
    DETAILED_POSITIONS: 'detailed-positions-dialog',
    ORBITRACKER_DETAILS: 'orbitracker-details-dialog',
    CONFIGURE_ORBITRACKER: 'configure-orbitracker-dialog',
    CONFIGURE_ASSOCIATION: 'change-associated-orbitracker-dialog',
};

export const REDUX_FORM = {
    LOGIN: 'login-form',
    SEARCH: 'search-form',
    SETTINGS: 'settings-form',
    GOOGLE_MAP: 'googlemap-form',
    ADD_EQUIPMENT: 'add-equipment-form',
    ORBITRACKER_PARAMETERS: 'orbitracker-params-form',
    CONFIGURE_ASSOCIATION: 'configure-association-form',
    EQUIPMENT_DETAILED_MAP: 'equipment-detailed-map-form',
    DEVICE_DETAILED_POSITIONS: 'device-detailed-positions-form',
    EQUIPMENT_DETAILED_POSITIONS: 'equipment-detailed-positions-form',
};

/* AUTH */
export const AUTH_PREFIX = 'Bearer';
export const AUTH_TOKEN = 'access_token';

export const AUTH_META = {
    SCOPE: 'ADMIN',
    GRANT_TYPE: 'password',
    CLIENT_ID: 'teleoxsrett',
    ENCODED: `Basic ${btoa('teleoxsrett:')}`,
};

export const SORT_ORDER = {
    ASC: 'asc',
    DESC: 'desc',
};

/* META INFO */
export const ITEMS_PER_PAGE = 20;
export const ITEMS_PER_SEARCH = 10;
export const ITEMS_MAX_COUNT_PER_API = 2000;

export const ON_PRESS_ENTER = 13;
export const GRID_EXPANDER_SIZE = 42;
export const TOAST_MESSAGE_DURATION = 2000;

export const DEFAULT_DAY_RANGE = 7;
export const DEFAULT_DATE_TIME_FORMAT = 'DD/MM/YYYY HH:mm:ss';

/* CUSTOM APP ENTITIES */
export const SEARCH_META = {
    ORBITRACKER: {
        FIELD: 'identifier',
        ENTITY_TYPE_IDENTIFIER: 'srett_device@124',
    },
    EQUIPMENT: {
        FIELD: 'type',
        ENTITY_TYPE_IDENTIFIER: 'orbitrack_iot@equipment',
    },
};

export const DEVICE_BATTERY_LIMIT = {
    RED: {
        VALUE: 10,
        COLOR: '#A52A2A',
    },
    ORANGE: {
        VALUE: 30,
        COLOR: '#F4A460',
    },
    GREEN: {
        VALUE: 100,
        COLOR: '#006400',
    },
};

export const RSSI_META = {
    SUFFIX: 'dBm',
    RANGE: {
        MIN: -90,
        MAX: -30,
    },
    COLOR: {
        ERROR: '#A52A2A',
        ACTIVE: '#000000',
        DEFAULT: '#d3d3d3',
    },
    ICON_SIZE: 40,
    ITEMS_COUNT: 5,
    ICON_ITEM_SIZE: 8,
    LAST_MEASURE: 1209600,
};

export const WAKE_UP_MODE = {
    MOVEMENT: 0,
    CALENDAR: 1,
    PERIODIC: 2,
};

export const DEVICE_ACTIVITY_STATUS = {
    PENDING: 'pending',
    ABORTING: 'aborting',
    FINISHED: 'finished',
};

export const NULL_ENTITY = '###NULL-ENTITY###';

export const DEFAULT_SORTING = {
    EQUIPMENT: 'type',
    ORBITRACKER: 'identifier',
    DETAILED_POSITIONS: 'lastmeasure',
};

export const PROCESS_TYPE = {
    REBOOT: 'reboot',
    READ_PARAMETERS: 'read_parameters',
    WRITE_PARAMETERS: 'write_parameters',
};

export const ORBITRACKER_PARAMS = {
    /* PERIODIC */
    PERIODIC_HOURS: {
        NAME: 'periodHour',
        MIN: 0,
        MAX: 48, // Calculate 172800s / 3600s = 48h
    },
    PERIODIC_MINUTES: {
        NAME: 'periodMinute',
        MIN: 0,
        MAX: 60,
    },
    /* CALENDAR */
    CALENDAR_START_HOURS: {
        NAME: 'calendar_start_hour',
        MIN: 0,
        MAX: 24,
    },
    CALENDAR_START_MINUTES: {
        NAME: 'calendar_start_minute',
        MIN: 0,
        MAX: 60,
    },
    CALENDAR_STOP_HOURS: {
        NAME: 'calendar_stop_hour',
        MIN: 0,
        MAX: 24,
    },
    CALENDAR_STOP_MINUTES: {
        NAME: 'calendar_stop_minute',
        MIN: 0,
        MAX: 60,
    },
    CALENDAR_NUM_POSITION: {
        NAME: 'calendar_nb_positions',
        MIN: 0,
        MAX: Infinity,
    },
    /* MOVEMENT DETECTION */
    DETECTION_START_MINUTES: {
        NAME: 'immobileTimeToStartDetectionMinutes',
        MIN: 0,
        MAX: 360, // Calculate = 21600s / 60s = 360m
    },
    DETECTION_START_SECONDS: {
        NAME: 'immobileTimeToStartDetectionSecond',
        MIN: 0,
        MAX: 60,
    },
    DETECTION_TRIGGER_MINUTES: {
        NAME: 'immobileTimeToTriggerPositionMinute',
        MIN: 0,
        MAX: 360, // Calculate = 21600s / 60s = 360m
    },
    DETECTION_TRIGGER_SECONDS: {
        NAME: 'immobileTimeToTriggerPositionSecond',
        MIN: 0,
        MAX: 60,
    },
    DETECTION_BETWEEN_HOURS: {
        NAME: 'minimumTimeBetweenTwoMovementDetectionHour',
        MIN: 0,
        MAX: 168, // Calculate = 604800s / 3600s = 168h
    },
    DETECTION_BETWEEN_MINUTES: {
        NAME: 'minimumTimeBetweenTwoMovementDetectionMinute',
        MIN: 0,
        MAX: 60,
    },
    /* ACCELEROMETER */
    ACCELERO_THERESHOLD: {
        NAME: 'mov_threshold',
        MIN: 0,
        MAX: Infinity,
    },
    /* GPS */
    GPS_TIMEOUT: {
        NAME: 'gps_timeout',
        MIN: 0,
        MAX: 65535,
    },
    GPS_MISSED: {
        NAME: 'nb_miss',
        MIN: 0,
        MAX: 65535,
    }
};

export const WAKE_UP_TYPE = {
    NAME: 'wake_up_type',
    OPTIONS: [
        {
            title: 'ORBITRACKER.FORM.SECTIONS.GENERAL_SETTINGS.FIELDS.WAKE_UP_MODE.OPTIONS.NONE',
            value: 'none',
        },
        {
            title: 'ORBITRACKER.FORM.SECTIONS.GENERAL_SETTINGS.FIELDS.WAKE_UP_MODE.OPTIONS.CALENDAR',
            value: 'calendar',
        },
        {
            title: 'ORBITRACKER.FORM.SECTIONS.GENERAL_SETTINGS.FIELDS.WAKE_UP_MODE.OPTIONS.PERIODIC',
            value: 'periodic',
        }   
    ]
};

export const DATA_RATE = {
    NAME: 'mov_data_rate',
    OPTIONS: [
        {
            title: 1,
            value: '1',
        },
        {
            title: 10,
            value: '10',
        },
        {
            title: 25,
            value: '25',
        },
        {
            title: 50,
            value: '50',
        },
        {
            title: 100,
            value: '100',
        },
        {
            title: 200,
            value: '200',
        },
        {
            title: 400,
            value: '400',
        },                   
    ]
};

export const MAX_ACCELO_MEASURE = {
    NAME: 'mov_max_measure',
    OPTIONS: [
        {
            title: 2,
            value: '2',
        },
        {
            title: 4,
            value: '4',
        },
        {
            title: 8,
            value: '8',
        },
        {
            title: 16,
            value: '16',
        },           
    ]
};

export const LIFE_TIME_ESTIMATED = {
    YEAR: 'lifetimeCurrentYear',
    MONTH: 'lifetimeCurrentMonth',
    DAY: 'lifetimeCurrentDay',
    MEASURE_DATE: 'lastMeasureDate',
};


export const MOV_DETECTION_FLAG = 'movement_detection_flag';

export const EQUIPMENT_DETAILS_ICON_META = {
    LIGHTNESS: 61,
    HUE_START: 200,
    HUE_VALUE: 160,
    SATURATION: 100,
};

export const DEFAULT_PARAMETERS_SETTINGS = {
    movement_detection_flag: 'false',
    wake_up_type: 'none',
    wake_up_period: '14400',
    calendar_start_hour: '6',
    calendar_start_minute: '0',
    calendar_stop_hour: '16',
    calendar_stop_minute: '0',
    calendar_nb_positions: '0',
    mov_immobile_to_start: '30',
    mov_immobile_to_fix: '300',
    mov_time_between_fix: '3600',
    mov_data_rate: '400',
    mov_max_measure: '16',
    mov_threshold: '0',
    gps_timeout: '6000',
    nb_miss: '0',
};

export const MAX_COLORED_MARKERS = 5;
export const DISABLED_MARKER_COLOR = '#4a4a4a';
