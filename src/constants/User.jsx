/* SING IN */
export const SIGN_IN_ERROR_API_CALL = 'SIGN_IN_ERROR_API_CALL';
export const SIGN_IN_REQUEST_API_CALL = 'SIGN_IN_REQUEST_API_CALL';
export const SIGN_IN_SUCCESS_API_CALL = 'SIGN_IN_SUCCESS_API_CALL';

/* GET USER INFO */
export const GET_USER_INFO_ERROR_API_CALL = 'GET_USER_INFO_ERROR_API_CALL';
export const GET_USER_INFO_REQUEST_API_CALL = 'GET_USER_INFO_REQUEST_API_CALL';
export const GET_USER_INFO_SUCCESS_API_CALL = 'GET_USER_INFO_SUCCESS_API_CALL';

/* UPDATE PASSWORD */
export const UPDATE_PASSWORD_ERROR_API_CALL = 'UPDATE_PASSWORD_ERROR_API_CALL';
export const UPDATE_PASSWORD_REQUEST_API_CALL = 'UPDATE_PASSWORD_REQUEST_API_CALL';
export const UPDATE_PASSWORD_SUCCESS_API_CALL = 'UPDATE_PASSWORD_SUCCESS_API_CALL';

/* SIGN OUT */
export const SIGN_OUT_CALL = 'SIGN_OUT_CALL';

/* CHANGE LANGUAGE */
export const CHANGE_LANGUAGE_CALL = 'CHANGE_LANGUAGE_CALL';

/* TOOGLE MINIMIZE MENU */
export const TOOGLE_MINIMIZE_MENU_CALL = 'TOOGLE_MINIMIZE_MENU_CALL';
