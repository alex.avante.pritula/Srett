/* GENERAL */
import {Record} from 'immutable';

/* ACTION TYPES */
import {
    /* SIGN IN */
    SIGN_IN_ERROR_API_CALL,
    SIGN_IN_REQUEST_API_CALL,
    SIGN_IN_SUCCESS_API_CALL,
    
    /* GET USER INFO */
    GET_USER_INFO_ERROR_API_CALL,
    GET_USER_INFO_REQUEST_API_CALL,
    GET_USER_INFO_SUCCESS_API_CALL,

    /* UPDATE PASSWORD */
    UPDATE_PASSWORD_ERROR_API_CALL,
    UPDATE_PASSWORD_REQUEST_API_CALL,
    UPDATE_PASSWORD_SUCCESS_API_CALL,
    
    /* SIGN OUT */
    SIGN_OUT_CALL,

    /* CHANGE LANGUAGE */
    CHANGE_LANGUAGE_CALL,

    /* TOOGLE MINIMIZE MENU */
    TOOGLE_MINIMIZE_MENU_CALL,
} from '../constants/User';

/* CONSTANTS */
import {
    AUTH_TOKEN,
    LOCALIZATION,
    API_ERROR_STATUS,
} from '../constants/General';

const {EN} = LOCALIZATION;
const {UNAUTHORIZED} = API_ERROR_STATUS;

/* IMMUTABLES */
/* INITIAL STATE */
const InitialState = Record({
    /* META */
    error: null,
    isFetching: false,
    loggedIn: (!!localStorage.getItem(AUTH_TOKEN)),

    /* USER INFO */
    login: '',
    domainId: '',
    lastName: '',
    firstName: '',
    identifier: '',

    language: EN,
    minimizedMenu: false,
});

const initialState = new InitialState();

export default (state = initialState, {type, payload, error}) => {

	switch (type) {

        case TOOGLE_MINIMIZE_MENU_CALL: {
            const {minimizedMenu} = payload;
            return state.withMutations((ctx) => {
                ctx
                    .set('minimizedMenu', minimizedMenu);
            });
        }
        
        /* SIGN IN */
        case SIGN_IN_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('loggedIn', false)
                    .set('isFetching', true)

                    .set('language', '')
                    .set('minimizedMenu', false)

                    .set('login', '')
                    .set('lastName', '')
                    .set('domainId', '')
                    .set('firstName', '')
                    .set('identifier', '');
            });
        }

        case SIGN_IN_SUCCESS_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('loggedIn', true)
                    .set('isFetching', false);
            });
        }

        case SIGN_IN_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isFetching', false);
            });
        }

        /* GET USER INFO */
        case GET_USER_INFO_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isFetching', true);
            });
        }

        case GET_USER_INFO_SUCCESS_API_CALL: {
            const {
                login,
                language,
                lastName,
                firstName,
                userDomain,
                identifier,
                minimizedMenu,
            } = payload;

            const {domainId} = userDomain;

            return state.withMutations((ctx) => {
                ctx
                    .set('isFetching', false)

                    .set('language', language)
                    .set('minimizedMenu', minimizedMenu)

                    .set('login', login)
                    .set('lastName', lastName)
                    .set('domainId', domainId)
                    .set('firstName', firstName)
                    .set('identifier', identifier);
            });
        }

        case GET_USER_INFO_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isFetching', false);

                if(error.status === UNAUTHORIZED) {
                    ctx.set('loggedIn', false);
                }
            });
        }

        /* SIGN OUT*/
        case SIGN_OUT_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('loggedIn', false)

                    .set('login', '')
                    .set('language', '')
                    .set('lastName', '')
                    .set('domainId', '')
                    .set('firstName', '')
                    .set('identifier', '');
            });
        }

        /* UPDATE PASSWORD */
        case UPDATE_PASSWORD_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isFetching', true);
            });
        }

        case UPDATE_PASSWORD_SUCCESS_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('isFetching', false);
            });
        }

        case UPDATE_PASSWORD_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isFetching', false);
            });
        }
        
        /* CHANGE LANGUAGE */
        case CHANGE_LANGUAGE_CALL: {
            const {language} = payload;
            
            return state.withMutations((ctx) => {
                ctx
                    .set('language', language);
            });
        }

		default: {
			return state;
		}
	}
};
