/* GENERAL */
import {combineReducers} from 'redux';

/* SPECIFIC REDUCERS */
import {routerReducer} from 'react-router-redux';
import {reducer as formReducer} from 'redux-form';
import {reducer as toastrReducer} from 'react-redux-toastr';

/* UTILS */
import ErrorHandler from './ErrorHandler';

/* CUSTOM REDUCERS */
import User from './User';
import Search from './Search';
import Dialog from './Dialog';
import GlobalMap from './GlobalMap';
import Equipment from './Equipment';
import Orbitracker from './Obritracker';
import Autocomplete from './Autocomplete';
import EquipmentInfo from './EquipmentInfo';
import DeviceActivity from './DeviceActivity';
import DevicePositions from './DevicePositions';
import OrbitrackerInfo from './ObritrackerInfo';
import EquipmentPositions from './EquipmentPositions';
import OrbitrackerParameters from './DeviceParameters';

export default combineReducers({
    ErrorHandler,

    form: formReducer,
    toastr: toastrReducer,
    routing: routerReducer,
    
    User,
    Search,
    Dialog,
    Equipment,
    GlobalMap,
    Orbitracker,
    Autocomplete,
    EquipmentInfo,
    DeviceActivity,
    DevicePositions,
    OrbitrackerInfo,
    EquipmentPositions,
    OrbitrackerParameters,
});
