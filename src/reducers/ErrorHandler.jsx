/* GENERAL */
import {Record} from 'immutable';
import {toastr} from 'react-redux-toastr';

/* CONSTANTS */
import {TOAST_MESSAGE_DURATION} from '../constants/General';
/* INITIAL STATE */
const InitialState = Record({
    error: null,
    action: null,
});

export const initialState = new InitialState();


export default (state = initialState, {type, error}) => {
    if(error){
        return state.withMutations((ctx) => {
            ctx   
                .set('error', error) 
                .set('action', type);            
        });
    }

    return state.withMutations((ctx) => {
        ctx   
            .set('error', null)
            .set('action', null);
    });

};
