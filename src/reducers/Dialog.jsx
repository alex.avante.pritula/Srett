/* GENERAL */
import {Record} from 'immutable';

/* ACTION TYPES */
import {
    /* DIALOG NAVIGATION */
    DIALOG_OPEN_CALL,
    DIALOG_CLOSE_CALL,
} from '../constants/Dialog';

/* INITIAL STATE */
const InitialState = Record({
    /* META */
    isOpened: false,

    /* DATA */
    meta: {},
});

const initialState = new InitialState();

export default (state = initialState, {type, payload}) => {

    switch(type){

        /* DIALOG NAVIGATION */
        case DIALOG_OPEN_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('isOpened', true)

                    .set('meta', payload);
            });
        }

        case DIALOG_CLOSE_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('isOpened', false)
                    
                    .set('meta', {});
            });
        }

        default: {
            return state;
        }
    }    
};
