/* GENERAL */
import {Record} from 'immutable';

/* ACTION TYPES */
import {
    /* GET ORBITRACKER BY IDENTIFIER */
    GET_ORBITRACKER_BY_IDENTIFIER_ERROR_API_CALL,
    GET_ORBITRACKER_BY_IDENTIFIER_REQUEST_API_CALL,
    GET_ORBITRACKER_BY_IDENTIFIER_SUCCESS_API_CALL,
} from '../constants/Orbitracker';

/* UTILS */
import deviceStatusMode from '../utils/deviceStatusMode';

/* IMMUTABLES */
import {Orbitracker} from '../immutables/Orbitracker';

/* INITIAL STATE */
const InitialState = Record({
    /* META */
    error: null,
    isFetching: false,

    /* DATA */
    orbitracker: new Orbitracker(),
});

const initialState = new InitialState();

export default (state = initialState, {type, payload, error}) => {

	switch (type) {
        case GET_ORBITRACKER_BY_IDENTIFIER_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isFetching', true)
                    
                    .set('orbitracker', new Orbitracker());
            });
        }

        case GET_ORBITRACKER_BY_IDENTIFIER_SUCCESS_API_CALL: {
            const {content} = payload;

            const {
                rssi,
                status,
                battery, 
                latitude, 
                longitude,
                domain_id, 
                timetofix, 
                equipment,
                identifier,
                lastmeasure,
                equipmentRef,
                reccordindex,
                reboot_counter,
                firmware_version,
            } = content;

            const {
                WAKE_UP_MODE,
                LAST_WAKE_UP_MODE,
            } = deviceStatusMode(status);
          
            const orbitracker = new Orbitracker({
                rssi,
                status,
                battery, 
                latitude, 
                longitude, 
                timetofix, 
                equipment,
                identifier,
                lastmeasure,
                reccordindex,
                equipmentRef,
                domainId: domain_id,
                reboots: reboot_counter,
                wakeUpMode: WAKE_UP_MODE,
                firmware: firmware_version,
                lastWakeUpMode: LAST_WAKE_UP_MODE,
            });
                      
            return state.withMutations((ctx) => {
                ctx
                    .set('isFetching', false)
                    .set('orbitracker', orbitracker);
            });
        }

        case GET_ORBITRACKER_BY_IDENTIFIER_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isFetching', false)

                    .set('orbitracker', new Orbitracker());
            });
        }

        default: {
			return state;
		} 
    }
};
