/* GENERAL */
import {Record, List} from 'immutable';

/* ACTION TYPES */
import {
    /* GET CLIENT EQUIPMENT ITEMS */
    GET_CLIENT_EQUIPMENT_ITEMS_ERROR_API_CALL,
    GET_CLIENT_EQUIPMENT_ITEMS_REQUEST_API_CALL,
    GET_CLIENT_EQUIPMENT_ITEMS_SUCCESS_API_CALL,

    /* RESET CLIENT EQUIPMENT DATA */
    RESET_CLIENT_EQUIPMENTS_GRID_CALL,

    /* CREATE NEW EQUIPMENT */
    CREATE_NEW_CLIENT_EQUIPMENT_ERROR_API_CALL,
    CREATE_NEW_CLIENT_EQUIPMENT_REQUEST_API_CALL,
    CREATE_NEW_CLIENT_EQUIPMENT_SUCCESS_API_CALL,
    
    /* ASSOCIATE CLIENT EQUIPMENT TO ORBITRACKER DEVICE */
    ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_ERROR_API_CALL,
    ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_REQUEST_API_CALL,
    ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_SUCCESS_API_CALL,
} from '../constants/Equipment';

/* UTILS */
import deviceStatusMode from '../utils/deviceStatusMode';

/* IMMUTABLES */
import {Equipment} from '../immutables/Equipment';
import {Orbitracker} from '../immutables/Orbitracker';

/* CONSTANTS */
import {
    SORT_ORDER,
    REDUX_FORM,
} from '../constants/General';

const {SEARCH} = REDUX_FORM;
const {ASC, DESC} = SORT_ORDER;

/* INITIAL STATE */
const InitialState = Record({
    /* META */
    error: null,
    isGridFetching: false,
    isModalFetching: false,

    /* PAGINATION & FILTERS */
    filter: '',
    current: 0,
    sortField: '', 
    sortOrder: '',
    totalPages: 0,

    /* DATA */
    equipments: List(),
});

const initialState = new InitialState();

export default (state = initialState, {type, payload, error}) => {

	switch (type) {

        /* GET CLIENT EQUIPMENT ITEMS */
        case GET_CLIENT_EQUIPMENT_ITEMS_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isGridFetching', true)
                    
                    .set('equipments', List());
            });
        }

        case GET_CLIENT_EQUIPMENT_ITEMS_SUCCESS_API_CALL: {

            const {
                filter,
                number,
                content,
                sortField,
                sortOrder,
                totalPages,
            } = payload;

            const equipments = content.map((item) => {
                const {
                    type,
                    tracker,
                    reference,
                    lastmeasure,
                } = item;

                const {
                    rssi,
                    label,
                    status,
                    battery,
                    latitude,
                    timetofix,
                    equipment,
                    longitude,
                    domain_id,
                    identifier,
                    reccordindex,
                } = tracker || {};

                const {
                    WAKE_UP_MODE,
                    LAST_WAKE_UP_MODE,
                } = (status ? deviceStatusMode(status) : {});
                
                return new Equipment({
                    type,
                    reference,
                    lastmeasure,

                    domainId: item.domain_id,
                    identifier: item.identifier,

                    tracker: new Orbitracker({
                        rssi,
                        label,
                        status,
                        battery,
                        latitude,
                        timetofix,
                        equipment,
                        longitude,
                        identifier,
                        reccordindex,
                        domainId: domain_id,
                        wakeUpMode: WAKE_UP_MODE,
                        lastWakeUpMode: LAST_WAKE_UP_MODE,
                    }),
                });
            });

            // equipments.sort(sortHandler(sortField, sortOrder));

            return state.withMutations((ctx) => {
                ctx
                    .set('isGridFetching', false)

                    .set('filter', filter)
                    .set('current', number)
                    .set('sortField', sortField) 
                    .set('sortOrder', sortOrder)
                    .set('totalPages', totalPages)

                    .set('equipments', List(equipments));
            });
        }

        case GET_CLIENT_EQUIPMENT_ITEMS_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isGridFetching', false)

                    .set('filter', '')
                    .set('current', 0)
                    .set('sortField', '') 
                    .set('sortOrder', '')
                    .set('totalPages', 0);
            });
        }

        /* RESET CLIENT EQUIPMENT DATA */
        case RESET_CLIENT_EQUIPMENTS_GRID_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('filter', '')
                    .set('current', 0)
                    .set('sortField', '') 
                    .set('sortOrder', '')
                    .set('totalPages', 0)
                    
                    .set('equipments', List());
            });
        }

        /* CREATE NEW EQUIPMENT */
        case CREATE_NEW_CLIENT_EQUIPMENT_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isModalFetching', true);
            });
        }
        
        case CREATE_NEW_CLIENT_EQUIPMENT_SUCCESS_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('isModalFetching', false);
            });
        }
    
        case CREATE_NEW_CLIENT_EQUIPMENT_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isModalFetching', false);
            });
        }

            /* ASSOCIATE CLIENT EQUIPMENT TO ORBITRACKER DEVICE */
        case ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isModalFetching', true);
            });
        }

        case ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_SUCCESS_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('isModalFetching', false);
            });
        }

        case ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isModalFetching', false);
            });
        }
    
        default: {
			return state;
		}
    }
};

/* CUSTOM SORT HANDLER */
function sortHandler(sortField, sortOrder){
    switch(sortField) {
        case 'identifier' : {
            if(sortOrder === ASC) {
                return (a, b) => {
                    const aIdentifier = a.get('identifier') || '';
                    const bIdentifier = b.get('identifier') || '';
                    return  aIdentifier.localeCompare(bIdentifier);
                };
            }    

            return (a, b) => {
                const aIdentifier = a.get('identifier') || '';
                const bIdentifier = b.get('identifier') || '';
                return -(aIdentifier.localeCompare(bIdentifier));
            };
        }

        case 'type': {
            if(sortOrder === ASC) {
                return (a, b) => {
                    const aType = a.get('type') || '';
                    const bType = b.get('type') || '';
                    return aType.localeCompare(bType);
                };
            } 

            return (a, b) => {
                const aType = a.get('type') || '';
                const bType = b.get('type') || '';
                return -(aType.localeCompare(bType));
            };
        }

        case 'pairing': {
            if(sortOrder === ASC) {
                return (a, b) => {
                    const aPairing = a.getIn(['tracker', 'identifier']) || '';
                    const bPairing = b.getIn(['tracker', 'identifier']) || '';
                    return aPairing.localeCompare(bPairing);
                };
            }   

            return (a, b) => {
                const aPairing = a.getIn(['tracker', 'identifier']) || '';
                const bPairing = b.getIn(['tracker', 'identifier']) || '';
                return -(aPairing.localeCompare(bPairing));
            };
        }

        case 'lastmeasure': {
            if(sortOrder === ASC) {
                return (a, b) => {
                    const aLastmeasure = a.get('lastmeasure') || null;
                    const bLastmeasure = b.get('lastmeasure') || null;
                    return (aLastmeasure > bLastmeasure ? 1 : -1);
                };
            }   

            return (a, b) => {
                const aLastmeasure = a.get('lastmeasure') || null;
                const bLastmeasure = b.get('lastmeasure') || null;
                return (aLastmeasure > bLastmeasure ? -1: 1);
            };
        }
    }
};
