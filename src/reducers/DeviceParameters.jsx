/* GENERAL */
import {Record} from 'immutable';

/* ACTION TYPES */
import {
    /* GET DEVICE PARAMETERS */
    GET_DEVICE_PARAMETERS_ERROR_API_CALL,
    GET_DEVICE_PARAMETERS_REQUEST_API_CALL,
    GET_DEVICE_PARAMETERS_SUCCESS_API_CALL,

    /* SAVE DEVICE PARAMETERS */
    SAVE_DEVICE_PARAMETERS_ERROR_API_CALL,
    SAVE_DEVICE_PARAMETERS_REQUEST_API_CALL,
    SAVE_DEVICE_PARAMETERS_SUCCESS_API_CALL,

    /* SYNCHRONIZE DEVICE PARAMETERS */
    SYNCHRONIZE_DEVICE_PARAMETERS_ERROR_API_CALL,
    SYNCHRONIZE_DEVICE_PARAMETERS_REQUEST_API_CALL,
    SYNCHRONIZE_DEVICE_PARAMETERS_SUCCESS_API_CALL,

    /* REBOOT DEVICE PARAMETERS */
    REBOOT_DEVICE_PARAMETERS_ERROR_API_CALL,
    REBOOT_DEVICE_PARAMETERS_SUCCESS_API_CALL,
    REBOOT_DEVICE_PARAMETERS_REQUEST_API_CALL,
} from '../constants/DeviceParameters';

/* IMMUTABLES */
import {Parameters} from '../immutables/Parameters';

/* CONSTANTS */
import {DEFAULT_PARAMETERS_SETTINGS} from '../constants/General';

/* INITIAL STATE */
const InitialState = Record({
    error: null,
    isFetching: false,
    parameters: new Parameters(),
});

const initialState = new InitialState();

export default (state = initialState, {type, payload, error}) => {

	switch (type) {

        /* GET DEVICE PARAMETERS */
        case GET_DEVICE_PARAMETERS_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isFetching', true);
            });
        }

        case GET_DEVICE_PARAMETERS_SUCCESS_API_CALL: {
            const {content} = payload;

            const {
                movement_detection_flag,
                wake_up_type,
                wake_up_period,
                calendar_start_hour,
                calendar_start_minute,
                calendar_stop_hour,
                calendar_stop_minute,
                calendar_nb_positions,
                mov_immobile_to_start,
                mov_immobile_to_fix,
                mov_time_between_fix,
                mov_data_rate,
                mov_max_measure,
                mov_threshold,
                gps_timeout,
                nb_miss,
            } = content;

            const parameters = new Parameters({
                movement_detection_flag: (movement_detection_flag || DEFAULT_PARAMETERS_SETTINGS.movement_detection_flag),
                wake_up_type: (wake_up_type || DEFAULT_PARAMETERS_SETTINGS.wake_up_type),
                wake_up_period: (wake_up_period || DEFAULT_PARAMETERS_SETTINGS.wake_up_period),
                calendar_start_hour: (calendar_start_hour || DEFAULT_PARAMETERS_SETTINGS.calendar_start_hour),
                calendar_start_minute: (calendar_start_minute || DEFAULT_PARAMETERS_SETTINGS.calendar_start_minute),
                calendar_stop_hour: (calendar_stop_hour || DEFAULT_PARAMETERS_SETTINGS.calendar_stop_hour),
                calendar_stop_minute: (calendar_stop_minute || DEFAULT_PARAMETERS_SETTINGS.calendar_stop_minute),
                calendar_nb_positions: (calendar_nb_positions || DEFAULT_PARAMETERS_SETTINGS.calendar_nb_positions),
                mov_immobile_to_start: (mov_immobile_to_start || DEFAULT_PARAMETERS_SETTINGS.mov_immobile_to_start),
                mov_immobile_to_fix: (mov_immobile_to_fix || DEFAULT_PARAMETERS_SETTINGS.mov_immobile_to_fix),
                mov_time_between_fix: (mov_time_between_fix || DEFAULT_PARAMETERS_SETTINGS.mov_time_between_fix),
                mov_data_rate: (mov_data_rate || DEFAULT_PARAMETERS_SETTINGS.mov_data_rate),
                mov_max_measure: (mov_max_measure || DEFAULT_PARAMETERS_SETTINGS.mov_max_measure),
                mov_threshold: (mov_threshold || DEFAULT_PARAMETERS_SETTINGS.mov_threshold),
                gps_timeout: (gps_timeout || DEFAULT_PARAMETERS_SETTINGS.gps_timeout),
                nb_miss: (nb_miss || DEFAULT_PARAMETERS_SETTINGS.nb_miss),
            });
                      
            return state.withMutations((ctx) => {
                ctx
                    .set('isFetching', false)
                    .set('parameters', parameters);
            });
        }

        case GET_DEVICE_PARAMETERS_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isFetching', false);
            });
        }

        /* SAVE DEVICE PARAMETERS */
        case SAVE_DEVICE_PARAMETERS_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isFetching', false);
            });
        }

        case SAVE_DEVICE_PARAMETERS_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isFetching', true);
            });
        }

        case SAVE_DEVICE_PARAMETERS_SUCCESS_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('isFetching', false);
            });
        }

        /* SYNCHRONIZE DEVICE PARAMETERS */
        case SYNCHRONIZE_DEVICE_PARAMETERS_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isFetching', false);
            });
        }

        case SYNCHRONIZE_DEVICE_PARAMETERS_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isFetching', true);
            });
        }

        case SYNCHRONIZE_DEVICE_PARAMETERS_SUCCESS_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('isFetching', false);
            });
        }

        /* REBOOT DEVICE PARAMETERS */
        case REBOOT_DEVICE_PARAMETERS_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isFetching', false);
            });
        }

        case REBOOT_DEVICE_PARAMETERS_SUCCESS_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('isFetching', false);
            });
        }

        case REBOOT_DEVICE_PARAMETERS_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isFetching', true);
            });
        }

        default: {
			return state;
		} 
    }
};
