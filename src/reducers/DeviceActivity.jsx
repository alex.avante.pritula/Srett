/* GENERAL */
import {Record, List} from 'immutable';

/* ACTION TYPES */
import {
    /* GET DEVICE ACTIVITY ITEMS */
    GET_DEVICE_ACTIVITY_ITEMS_ERROR_API_CALL,
    GET_DEVICE_ACTIVITY_ITEMS_REQUEST_API_CALL,
    GET_DEVICE_ACTIVITY_ITEMS_SUCCESS_API_CALL,

    /* RESET DEVICE ACTIVITY DATA */
    RESET_DEVICE_ACTIVITY_GRID_CALL,
} from '../constants/DeviceActivity';

/* UTILS */
import {moment} from '../utils/i18nModule';

/* IMMUTABLES */
import {Process, Payload} from '../immutables/Process';

/* CONSTANTS */
import {SORT_ORDER} from '../constants/General';

const {ASC, DESC} = SORT_ORDER;

/* INITIAL STATE */
const InitialState = Record({
    /* FETCH META */
    error: null,
    isFetching: false,

    /* PAGINATION & FILTERS */
    current: 0,
    sortField: '', 
    sortOrder: '',
    totalPages: 0,

    /* DATA */
    processes:  List(),
});

const initialState = new InitialState();

export default (state = initialState, {type, payload, error}) => {

    switch(type){

        /* GET DEVICE ACTIVITY ITEMS */
        case GET_DEVICE_ACTIVITY_ITEMS_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isFetching', true)
                    
                    .set('processes', List());
            });
        }

        case GET_DEVICE_ACTIVITY_ITEMS_SUCCESS_API_CALL: {

            const {
                number,
                content,
                sortField,
                sortOrder,
                totalPages,
            } = payload;

            const processes = content.map((item) => {

                const {
                    status,
                    device,
                    payload,
                    domain_id,
                    identifier,
                    update_date,
                    process_type,
                    creation_date,
                } = item;

                return new Process({
                    status,
                    device,
                    identifier,
                    domainId: domain_id,
                    processType: process_type,
                    upodationDate: moment(update_date),
                    creationDate: moment(creation_date),
                    payload: (payload ? JSON.parse(payload) :  {}),
                });
            });

            // processes.sort(sortHandler(sortField, sortOrder));

            return state.withMutations((ctx) => {
                ctx
                    .set('isFetching', false)

                    .set('current', number)
                    .set('sortField', sortField) 
                    .set('sortOrder', sortOrder)
                    .set('totalPages', totalPages)

                    .set('processes', List(processes));
            });
        }

        case GET_DEVICE_ACTIVITY_ITEMS_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isFetching', false)
                    
                    .set('current', 0)
                    .set('sortField', '') 
                    .set('sortOrder', '')
                    .set('totalPages', 0);
            });
        }

        /* RESET DEVICE ACTIVITY DATA */
        case RESET_DEVICE_ACTIVITY_GRID_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('current', 0)
                    .set('sortField', '') 
                    .set('sortOrder', '')
                    .set('totalPages', 0)
                    
                    .set('processes', List());
            });
        }        

        default: {
            return state;
        }
    }    
};

/* CUSTOM SORT HANDLER */
function sortHandler(sortField, sortOrder){
    switch(sortField) {
        case 'status': {
            if(sortOrder === ASC) {
                return (a, b) => {
                    const aPairing = a.get('result') || '';
                    const bPairing = b.get('result') || '';
                    return aPairing.localeCompare(bPairing);
                };
            }   

            return (a, b) => {
                const aPairing = a.get('result') || '';
                const bPairing = b.get('result') || '';
                return -aPairing.localeCompare(bPairing);
            };
        }

        case 'date': {
            if(sortOrder === ASC) {
                return (a, b) => {
                    const aCreationDate = a.get('creationDate') || null;
                    const bCreationDate = b.get('creationDate') || null;
                    return aCreationDate > bCreationDate ? 1: -1;
                };
            }   
            return (a, b) => {
                const aCreationDate = a.get('creationDate') || null;
                const bCreationDate = b.get('creationDate') || null;
                return aCreationDate > bCreationDate ? -1: 1;
            };
        }
    }
};
