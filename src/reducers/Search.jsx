/* GENERAL */
import {Record, List} from 'immutable';

/* ACTION TYPES */
import {
    /* GET SEARCH ITEMS */
    GET_SEARCH_ITEMS_ERROR_API_CALL,
    GET_SEARCH_ITEMS_REQUEST_API_CALL,
    GET_SEARCH_ITEMS_SUCCESS_API_CALL,

    /* RESET SEARCH_ITEMS */
    RESET_SEARCH_DATA_CALL,
} from '../constants/Search';

/* RESET DATA */
import {RESET_GLOBAL_MAP_CALL} from '../constants/Map';
import {RESET_ORBITRACKERS_GRID_CALL} from '../constants/Orbitracker';
import {RESET_CLIENT_EQUIPMENTS_GRID_CALL} from '../constants/Equipment';

/* INITIAL STATE */
const InitialState = Record({
    /* META */
    error: null,
    isFetching: false,

    /* DATA */
    items: List(),
});

export const initialState = new InitialState();

export default (state = initialState, {type, payload, error}) => {

    switch(type){
        /* RESET SEARCH_ITEMS */
        case RESET_SEARCH_DATA_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('items', List());
            });
        }

        /* GET SEARCH ITEMS ACTIONS */
        case GET_SEARCH_ITEMS_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isFetching', true);
            });
        }

        case GET_SEARCH_ITEMS_SUCCESS_API_CALL: {
            const isFetching = state.get('isFetching');
            
            if(isFetching){
                return state.withMutations((ctx) => {
                    ctx
                        .set('isFetching', false)
                        .set('items', List(payload));
                });
            }
        }

        case GET_SEARCH_ITEMS_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isFetching', false)
                    
                    .set('items', List());
            });
        }

        /* RESET DATA */
        case RESET_CLIENT_EQUIPMENTS_GRID_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('isFetching', false)
                    .set('items', List());
            });
        }

        case RESET_ORBITRACKERS_GRID_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('isFetching', false)
                    .set('items', List());
            });
        }

        case RESET_GLOBAL_MAP_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('isFetching', false)
                    .set('items', List());
            });
        }

        default: {
            return state;
        }
    }    
};
