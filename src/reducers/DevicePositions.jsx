/* GENERAL */
import {Record, List} from 'immutable';

/* ACTION TYPES */
import {
    /* GET DEVICE POSITIONS BY TIME RANGE */
    GET_DEVICE_POSITIONS_BY_TIME_RANGE_ERROR_API_CALL,
    GET_DEVICE_POSITIONS_BY_TIME_RANGE_SUCCESS_API_CALL,
    GET_DEVICE_POSITIONS_BY_TIME_RANGE_REQUEST_API_CALL,

    /* DEVICE POSITIONS GRID CLIENT PAGINATION */
    ON_DEVICE_POSITIONS_GRID_SORT_CHANGED_CALL,
    ON_DEVICE_POSITIONS_GRID_PAGE_CHANGED_CALL, 
} from '../constants/DevicePositions';

/* UTILS */
import {moment} from '../utils/i18nModule';
import deviceStatusMode from '../utils/deviceStatusMode';

/* IMMUTABLES */
import {Orbitracker} from '../immutables/Orbitracker';

/* CONSTANTS */
import {
    SORT_ORDER,
    REDUX_FORM,
    ITEMS_PER_PAGE,
    DEFAULT_SORTING,
    DEFAULT_DAY_RANGE,
} from '../constants/General';

const {SEARCH} = REDUX_FORM;
const {ASC, DESC} = SORT_ORDER;
const {DETAILED_POSITIONS} = DEFAULT_SORTING;

/* INITIAL STATE */
const InitialState = Record({
    /* META */
    error: null,
    isFetching: false,

    /* PAGINATION & FILTERS */
    current: 0,
    sortField: '', 
    sortOrder: '',
    totalPages: 0,

    to: moment(), 
    from: moment().add(-DEFAULT_DAY_RANGE, 'days'),
    
    /* DATA */
    positions: List(),
    visibleItems: List(),
});

const initialState = new InitialState();

export default (state = initialState, {type, payload, error}) => {

    switch(type){

        /* ON DEVICE POSITIONS GRID PAGE CHANGED */
        case ON_DEVICE_POSITIONS_GRID_PAGE_CHANGED_CALL: {
            const {page} = payload;
            const positions = state.get('positions');
            const sortOrder = state.get('sortOrder');
            const sortField = state.get('sortField');

            const visibleItems = positions
                .slice(page * ITEMS_PER_PAGE, (page + 1) * ITEMS_PER_PAGE)
                .sort(sortHandler(sortField, sortOrder));
            
            return state.withMutations((ctx) => {
                ctx   
                    .set('current', page)                
                    .set('visibleItems', visibleItems);
            });
        }

        /* ON DEVICE POSITIONS GRID SORT CHANGED */
        case ON_DEVICE_POSITIONS_GRID_SORT_CHANGED_CALL : {
            const {
                sortOrder,
                sortField,
            } = payload;

            const visibleItems = state
                .get('visibleItems')
                .sort(sortHandler(sortField, sortOrder));
            
            return state.withMutations((ctx) => {
                ctx   
                    .set('sortField', sortField)                
                    .set('sortOrder', sortOrder)
                    .set('visibleItems', visibleItems);
            });
        }

        /* GET DEVICE POSITIONS BY TIME RANGE */
        case GET_DEVICE_POSITIONS_BY_TIME_RANGE_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isFetching', true)
                    
                    .set('positions', List())
                    .set('visibleItems', List());
            });
        }

        case GET_DEVICE_POSITIONS_BY_TIME_RANGE_SUCCESS_API_CALL: {
            const {
                to,
                from,
                content,
            } = payload;

            const positions = content.map((item) => {
                const {
                    values,
                    timestamp,
                } = item;

                const {
                    status,
                    battery,
                    latitude,
                    longitude,
                    timetofix,
                    reboot_counter,
                    firmware_version,
                } = values;

                const {LAST_WAKE_UP_MODE} = deviceStatusMode(status);

                return new Orbitracker({
                    status,
                    battery,
                    latitude,
                    longitude,
                    timetofix,
                    reboots: reboot_counter,
                    firmware: firmware_version,
                    lastmeasure: moment(timestamp),
                    lastWakeUpMode: LAST_WAKE_UP_MODE,
                });
            }).sort(sortHandler(DETAILED_POSITIONS, DESC));

            const totalPages = Math.ceil(content.length / ITEMS_PER_PAGE);
            const visibleItems = positions.slice(0, ITEMS_PER_PAGE);

            return state.withMutations((ctx) => {
                ctx
                    .set('isFetching', false)
                    
                    .set('to', to)
                    .set('from', from)

                    .set('current', 0)
                    .set('sortOrder', DESC)
                    .set('sortField', DETAILED_POSITIONS)

                    .set('totalPages', totalPages)

                    .set('positions', List(positions))
                    .set('visibleItems', List(visibleItems));
            });
        }

        case GET_DEVICE_POSITIONS_BY_TIME_RANGE_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isFetching', false)

                    .set('current', 0)
                    .set('sortField', '') 
                    .set('sortOrder', '')
                    .set('totalPages', 0)
                    
                    .set('positions', List())
                    .set('visibleItems', List());
            });
        }

        default: {
            return state;
        }
    }    
};


/* CUSTOM SORT HANDLER */
function sortHandler(sortField, sortOrder){
    switch(sortField) {
        case 'lastmeasure' : {
            if(sortOrder === ASC) {
                return (a, b) => {
                    const aLastmeasure = a.get('lastmeasure') || null;
                    const bLastmeasure = b.get('lastmeasure') || null;
                    return (aLastmeasure > bLastmeasure ? 1: -1);
                };
            }   

            return (a, b) => {
                const aLastmeasure = a.get('lastmeasure') || null;
                const bLastmeasure = b.get('lastmeasure') || null;
                return (aLastmeasure > bLastmeasure ? -1: 1);
            };
        }

        case 'latitude': {
            if(sortOrder === ASC) {
                return (a, b) => {
                    return parseFloat(a.get('latitude')) - parseFloat(b.get('latitude'));
                };
            } 

            return (a, b) => {
                return parseFloat(b.get('latitude')) - parseFloat(a.get('latitude'));
            };
        }

        case 'longitude': {
            if(sortOrder === ASC) {
                return (a, b) => {
                    return parseFloat(a.get('longitude')) - parseFloat(b.get('longitude'));
                };
            }   

            return (a, b) => {
                return parseFloat(b.get('longitude')) - parseFloat(a.get('longitude'));
            };
        }

        case 'battery': {
            if(sortOrder === ASC) {
                return (a, b) => {
                    return parseFloat(a.get('battery')) - parseFloat(b.get('battery'));
                };
            }   

            return (a, b) => {
                return parseFloat(b.get('battery')) - parseFloat(a.get('battery'));
            };
        }

        case 'timetofix': {
            if(sortOrder === ASC) {
                return (a, b) => {
                    return parseFloat(a.get('timetofix')) - parseFloat(b.get('timetofix'));
                };
            }   

            return (a, b) => {
                return parseFloat(b.get('timetofix')) - parseFloat(a.get('timetofix'));
            };
        }

        case 'reboots': {
            if(sortOrder === ASC) {
                return (a, b) => {
                    return parseInt(a.get('reboots')) - parseInt(b.get('reboots'));
                };
            }   

            return (a, b) => {
                return parseInt(b.get('reboots')) - parseInt(a.get('reboots'));
            };
        }

        case 'firmware': {
            if(sortOrder === ASC) {
                return (a, b) => {
                    return parseFloat(a.get('firmware')) - parseFloat(b.get('firmware'));
                };
            }   

            return (a, b) => {
                return parseFloat(b.get('firmware')) - parseFloat(a.get('firmware'));
            };
        }
    }
};
