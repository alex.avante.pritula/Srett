/* GENERAL */
import {Record, List} from 'immutable';

/* ACTION TYPES */
import {
    /* GET CLIENT MAP ITEMS */
    GET_CLIENT_MAP_EQUIPMENT_ITEMS_ERROR_API_CALL,
    GET_CLIENT_MAP_EQUIPMENT_ITEMS_REQUEST_API_CALL,
    GET_CLIENT_MAP_EQUIPMENT_ITEMS_SUCCESS_API_CALL,

    /* CHANGE MAP CENTER AFTER SELECTING MARKER BY SEARCH */
    CHANGE_MAP_CENTER_CALL,
} from '../constants/Map';

/* UTILS */
import deviceStatusMode from '../utils/deviceStatusMode';

/* CONSTANTS */
import {START_CONFIG} from '../constants/Map';

const {LOCATION} = START_CONFIG;
const {lat, lng} = LOCATION;

/* IMMUTABLES */
import {Equipment} from '../immutables/Equipment';
import {Orbitracker} from '../immutables/Orbitracker';

/* INITIAL STATE */
export const InitialState = Record({
    /* META */
    error: null,
    isFetching: false,

    latitude: lat, 
    longitude: lng,

    /* DATA */
    items: List(),
});

export const initialState = new InitialState();

export default (state = initialState, {type, payload, error}) => {

    switch(type){

        /* GET CLIENT MAP ITEMS */
        case GET_CLIENT_MAP_EQUIPMENT_ITEMS_REQUEST_API_CALL : {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isFetching', true);
            });
        }

        case GET_CLIENT_MAP_EQUIPMENT_ITEMS_SUCCESS_API_CALL: {
            const {content} = payload;
            const items = content.map((item) => {
                const {
                    type,
                    tracker,
                    reference,
                    lastmeasure,
                } = item;

                const {
                    rssi,
                    label,
                    status,
                    battery,
                    latitude,
                    timetofix,
                    equipment,
                    longitude,
                    domain_id,
                    identifier,
                    reccordindex,
                } = tracker || {};

                const {
                    WAKE_UP_MODE,
                    LAST_WAKE_UP_MODE,
                } = (status ? deviceStatusMode(status) : {});

                return new Equipment({
                    type,
                    reference,
                    lastmeasure,

                    domainId: item.domain_id,
                    identifier: item.identifier,

                    tracker: new Orbitracker({
                        rssi,
                        status,
                        battery,
                        latitude,
                        timetofix,
                        equipment,
                        longitude,
                        identifier,
                        reccordindex,
                        domainId: domain_id,
                        wakeUpMode: WAKE_UP_MODE,
                        lastWakeUpMode: LAST_WAKE_UP_MODE,
                    }),
                });
            });

            return state.withMutations((ctx) => {
                ctx
                    .set('items', List(items))
                    
                    .set('isFetching', false);
            });
        }

        case GET_CLIENT_MAP_EQUIPMENT_ITEMS_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('items', List())

                    .set('error', error)
                    .set('isFetching', false);
            });
        }

        /* CHANGE MAP CENTER AFTER SELECTING MARKER BY SEARCH */
        case CHANGE_MAP_CENTER_CALL: {
            const {latitude, longitude} = payload;

            return state.withMutations((ctx) => {
                ctx
                    .set('latitude', latitude)
                    .set('longitude', longitude);
            });
        }

        default: {
            return state;
        }
    }    
};
