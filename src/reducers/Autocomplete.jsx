/* GENERAL */
import {Record, List} from 'immutable';

/* ACTION TYPES */
import {
    /* GET AUTOCOMPLETE ITEMS (ALL EXIST UNASSOCIATED ORBITRACKER DEVICES) */
    GET_AUTOCOMPLETE_ITEMS_ERROR_API_CALL,
    GET_AUTOCOMPLETE_ITEMS_REQUEST_API_CALL,
    GET_AUTOCOMPLETE_ITEMS_SUCCESS_API_CALL,

    /* RESET AUTOCOMPLETE DATA */
    RESET_AUTOCOMPLETE_DATA_CALL,
} from '../constants/Autocomplete';

/* INITIAL STATE */
const InitialState = Record({
    /* META */
    error: null,
    isFetching: false,

    /* DATA */
    items: List(),
});

const initialState = new InitialState();

export default (state = initialState, {type, payload, error}) => {

    switch(type){

        /* GET AUTOCOMPLETE ITEMS (ALL EXIST UNASSOCIATED ORBITRACKER DEVICES) */
        case GET_AUTOCOMPLETE_ITEMS_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isFetching', true);
            });
        }

        case GET_AUTOCOMPLETE_ITEMS_SUCCESS_API_CALL: {
            const {items} = payload;
            items.sort((a, b) => a['visible'].localeCompare(b['visible']));

            return state.withMutations((ctx) => {
                ctx
                    .set('isFetching', false)
                    .set('items', List(items));
            });
        }

        case GET_AUTOCOMPLETE_ITEMS_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isFetching', false)
                    
                    .set('items', List());
            });
        }

        /* RESET AUTOCOMPLETE DATA */
        case RESET_AUTOCOMPLETE_DATA_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('items', List());
            });
        }

        default: {
            return state;
        }
    }    
};
