/* GENERAL */
import {Record} from 'immutable';

/* ACTION TYPES */
import {
    /* GET CLIENT EQUIPMENT BY IDENTIFIER */
    GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_ERROR_API_CALL,
    GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_REQUEST_API_CALL,
    GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_SUCCESS_API_CALL,
} from '../constants/Equipment';

/* UTILS */
import deviceStatusMode from '../utils/deviceStatusMode';

/* IMMUTABLES */
import {Equipment} from '../immutables/Equipment';
import {Orbitracker} from '../immutables/Orbitracker';

/* INITIAL STATE */
const InitialState = Record({
    /* META */
    error: null,
    isFetching: false,

    /* DATA */
    equipment: new Equipment(),
});

export const initialState = new InitialState();

export default (state = initialState, {type, payload, error}) => {

	switch (type) {

        /* GET CLIENT EQUIPMENT BY IDENTIFIER */
        case GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isFetching', true)
                    
                    .set('equipment', new Equipment());
            });
        }

        case GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_SUCCESS_API_CALL: {

            const {
                content,
            } = payload;

            const {
                type,
                tracker,
                reference,
                lastmeasure,
                manufacturer,
            } = content;

            const {
                rssi,
                label,
                status,
                battery,
                latitude,
                timetofix,
                equipment,
                longitude,
                domain_id,
                identifier,
                reccordindex,
            } = tracker || {};

            const {
                WAKE_UP_MODE,
                LAST_WAKE_UP_MODE,
            } = (status ? deviceStatusMode(status) : {});
                
            const equipmentInfo =  new Equipment({
                type,
                reference,
                lastmeasure,

                domainId: content.domain_id,
                identifier: content.identifier,

                tracker: new Orbitracker({
                    rssi,
                    label,
                    status,
                    battery,
                    latitude,
                    timetofix,
                    equipment,
                    longitude,
                    identifier,
                    reccordindex,
                    domainId: domain_id,
                    wakeUpMode: WAKE_UP_MODE,
                    lastWakeUpMode: LAST_WAKE_UP_MODE,
                }),
            });

            return state.withMutations((ctx) => {
                ctx
                    .set('isFetching', false)

                    .set('equipment', equipmentInfo);
            });
        }

        case GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isFetching', false)

                    .set('equipment', new Equipment());
            });
        }

        default: {
          return state;
        } 
    };
};
