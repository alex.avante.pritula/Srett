/* GENERAL */
import {Record, List} from 'immutable';

/* ACTION TYPES */
import {
    /* GET ORBITRACKER ITEMS */
    GET_ORBITRACKER_ITEMS_ERROR_API_CALL,
    GET_ORBITRACKER_ITEMS_REQUEST_API_CALL,
    GET_ORBITRACKER_ITEMS_SUCCESS_API_CALL,

    /* RESET ORBITRACKER DATA */
    RESET_ORBITRACKERS_GRID_CALL,
} from '../constants/Orbitracker';

/* UTILS */
import deviceStatusMode from '../utils/deviceStatusMode';

/* IMMUTABLES */
import {Orbitracker} from '../immutables/Orbitracker';

/* CONSTANTS */
import {
    SORT_ORDER,
    REDUX_FORM,
} from '../constants/General';

const {SEARCH} = REDUX_FORM;
const {ASC, DESC} = SORT_ORDER;

/* INITIAL STATE */
const InitialState = Record({
    /* META */
    error: null,
    isFetching: false,

    /* PAGINATION & FILTERS */
    filter: '',
    current: 0,
    sortField: '', 
    sortOrder: '',
    totalPages: 0,
    
    /* DATA */
    orbitrackers: List(),
});

export const initialState = new InitialState();

export default (state = initialState, {type, payload, error}) => {

	switch (type) {

        /* GET ORBITRACKER ITEMS */
        case GET_ORBITRACKER_ITEMS_REQUEST_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', null)
                    .set('isFetching', true)
                    
                    .set('orbitrackers', List());
            });
        }

        case GET_ORBITRACKER_ITEMS_SUCCESS_API_CALL: {
            const {
                filter,
                number,
                content,
                sortField,
                sortOrder,
                totalPages,
            } = payload;

            const orbitrackers = content.map((item) => {
                const {
                    rssi ,
                    status,
                    battery, 
                    latitude, 
                    longitude,
                    domain_id, 
                    timetofix, 
                    equipment,
                    identifier,
                    lastmeasure,
                    equipmentRef,
                    reccordindex,
                    reboot_counter,
                    firmware_version,
                } = item;

                const {
                    WAKE_UP_MODE,
                    LAST_WAKE_UP_MODE,
                } = deviceStatusMode(status);

                return new Orbitracker({
                    rssi ,
                    status,
                    battery, 
                    latitude, 
                    longitude, 
                    timetofix, 
                    equipment,
                    identifier,
                    lastmeasure,
                    reccordindex,
                    equipmentRef,
                    domainId: domain_id,
                    reboots: reboot_counter,
                    wakeUpMode: WAKE_UP_MODE,
                    firmware: firmware_version,
                    lastWakeUpMode: LAST_WAKE_UP_MODE,
                });
            });
            
            // orbitrackers.sort(sortHandler(sortField, sortOrder));
           
            return state.withMutations((ctx) => {
                ctx
                    .set('isFetching', false)
                    
                    .set('filter', filter)
                    .set('current', number)
                    .set('sortField', sortField)
                    .set('sortOrder', sortOrder)
                    .set('totalPages', totalPages)

                    .set('orbitrackers', List(orbitrackers));
            });
        }

        case GET_ORBITRACKER_ITEMS_ERROR_API_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('error', error)
                    .set('isFetching', false)

                    .set('filter', '')
                    .set('current', 0)
                    .set('sortField', '') 
                    .set('sortOrder', '')
                    .set('totalPages', 0);
            });
        }

        /* RESET ORBITRACKER DATA */
        case RESET_ORBITRACKERS_GRID_CALL: {
            return state.withMutations((ctx) => {
                ctx
                    .set('filter', '')
                    .set('current', 0)
                    .set('sortField', '') 
                    .set('sortOrder', '')
                    .set('totalPages', 0)

                    .set('orbitrackers', List());
            });
        }

        default: {
			return state;
		} 
    }
};

/* CUSTOM SORT HANDLER */
export function sortHandler(sortField, sortOrder){
    switch(sortField) {
        case 'identifier' : {
            if(sortOrder === ASC) {
                return (a, b) => {
                    const aIdentifier = a.get('identifier') || '';
                    const bIdentifier = b.get('identifier') || '';
                    return aIdentifier.localeCompare(bIdentifier);
                };
            }    

            return (a, b) => {
                const aIdentifier = a.get('identifier') || '';
                const bIdentifier = b.get('identifier') || '';
                return -(aIdentifier.localeCompare(bIdentifier));
            };
        }

        case 'equipmentRef' : {
            if(sortOrder === ASC) {
                return (a, b) => {
                    const aRef = a.get('equipmentRef') || '';
                    const bRef = b.get('equipmentRef') || '';
                    return aRef.localeCompare(bRef);
                };
            }    

            return (a, b) => {
                const aRef = a.get('equipmentRef') || '';
                const bRef = b.get('equipmentRef') || '';
                return -(aRef.localeCompare(bRef));
            };
        }

        case 'lastmeasure': {
            if(sortOrder === ASC) {
                return (a, b) => {
                    const aLastmeasure = a.get('lastmeasure') || null;
                    const bLastmeasure = b.get('lastmeasure') || null;
                    return (aLastmeasure > bLastmeasure ? 1: -1);
                };
            }   

            return (a, b) => {
                const aLastmeasure = a.get('lastmeasure') || null;
                const bLastmeasure = b.get('lastmeasure') || null;
                return (aLastmeasure > bLastmeasure ? -1: 1);
            };
        }
    }
};
