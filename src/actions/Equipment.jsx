/* GENERAL */
import Promise from 'bluebird';
import {toastr} from 'react-redux-toastr';

/* ACTION TYPES */
import {
    /* GET CLIENT EQUIPMENTS ITEMS */
    GET_CLIENT_EQUIPMENT_ITEMS_ERROR_API_CALL,
    GET_CLIENT_EQUIPMENT_ITEMS_REQUEST_API_CALL,
    GET_CLIENT_EQUIPMENT_ITEMS_SUCCESS_API_CALL,

    /* RESET CLIENT EQUIPMENT DATA */
    RESET_CLIENT_EQUIPMENTS_GRID_CALL,

    /* CREATE NEW EQUIPMENT */
    CREATE_NEW_CLIENT_EQUIPMENT_ERROR_API_CALL,
    CREATE_NEW_CLIENT_EQUIPMENT_REQUEST_API_CALL,
    CREATE_NEW_CLIENT_EQUIPMENT_SUCCESS_API_CALL,

    /* ASSOCIATE CLIENT EQUIPMENT TO ORBITRACKER DEVICE */
    ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_ERROR_API_CALL,
    ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_REQUEST_API_CALL,
    ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_SUCCESS_API_CALL,

    /* GET CLIENT EQUIPMENT BY IDENTIFIER */
    GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_ERROR_API_CALL,
    GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_REQUEST_API_CALL,
    GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_SUCCESS_API_CALL,
} from '../constants/Equipment';

/* ACTION CREATORS */
import {getDeviceLastPositionErrorAction} from './DeviceLastPosition';

/* FETCH SERVICES */
import MeasurementFetchService from '../services/Measurement';
import ValueContainerFetchService from '../services/ValueContainer';

/* LOCALIZATION */
import I18nModule, {moment} from '../utils/i18nModule';

/* UTILS */
import equipmentTitle from '../utils/equipmentTitle';
import orbitrackerTitle from '../utils/orbitrackerTitle';

/* CONSTANTS */
import {
    NULL_ENTITY,
    TOAST_MESSAGE_DURATION,
} from '../constants/General';

/* ACTIONS */
/* RESET CLIENT EQUIPMENTS GRID */
export const resetClientEquipmentsGridAction = () =>{
    return {
        type: RESET_CLIENT_EQUIPMENTS_GRID_CALL,
    };
};

export const resetClientEquipmentsGridCall = () => (dispatch) => {
    dispatch(resetClientEquipmentsGridAction());
};

/* GET CLIENT EQUIPMENT ITEMS */
/* ACTION CREATORS */
export const getClientEquipmentItemsRequestAction = () => {
    return {
        type: GET_CLIENT_EQUIPMENT_ITEMS_REQUEST_API_CALL,
    };
};

export const getClientEquipmentItemsSuccessAction = (payload) => {
    return {
        payload,
        type: GET_CLIENT_EQUIPMENT_ITEMS_SUCCESS_API_CALL,
    };
};

export const getClientEquipmentItemsErrorAction = (error) => {
    return {
        error,
        type: GET_CLIENT_EQUIPMENT_ITEMS_ERROR_API_CALL,
    };
};

/* ADD LAST CLIENT EQUIPMENT POSITION & TIME AND FORMAT ITEMS */
function formatClientEquipmentItems(content, dispatch) {

    return Promise.mapSeries(content, (equipment) => {
        const {tracker} = equipment;

        if (tracker && (tracker.identifier && tracker.identifier !== NULL_ENTITY)) {
            equipment.lastmeasure = moment(tracker.last_measure_date);

            return MeasurementFetchService
                .getLastPosition(
                    tracker.identifier,
                    ['longitude', 'latitude'],
                ).then(({content}) => {
                    if(content.length < 1) return;

                    const {
                        values,
                        timestamp,
                    } = content[0];

                    const {
                        latitude,
                        longitude,
                    } = values;

                    if(parseFloat(latitude) && parseFloat(longitude)){
                        tracker.latitude = latitude;
                        tracker.longitude = longitude;
                        if(timestamp) equipment.lastmeasure = moment(timestamp);

                        return MeasurementFetchService
                            .getPositions({
                                to: timestamp,
                                from: timestamp,
                                identifier: tracker.identifier,
                                fields: ['status', 'battery', 'rssi', 'reccordindex', 'reboot_counter', 'firmware_version'],
                            })
                            .then(({content}) => {
                                if(content.length < 1) return;

                                const {
                                    values,
                                    timestamp,
                                } = content[0];

                                const {
                                    rssi,
                                    status,
                                    battery,
                                    reccordindex,
                                    reboot_counter,
                                    firmware_version,
                                } = values;

                                if(rssi) tracker.rssi = rssi;
                                if(status) tracker.status = status;
                                if(battery) tracker.battery = battery;
                                if(reccordindex) tracker.reccordindex = reccordindex;
                                if(reboot_counter) tracker.reboot_counter = reboot_counter;
                                if(firmware_version) tracker.firmware_version = firmware_version;
                            });
                    }

                })
                .catch((error) => {
                    dispatch(getDeviceLastPositionErrorAction(error));
                })
                .then(() => equipment);
        }
    
        return equipment;
    });
}

/* API CALL */
export const getClientEquipmentItemsApiCall = (values, dispatch) => {
    dispatch(getClientEquipmentItemsRequestAction());

    const {
        page, 
        filter,
        sortField, 
        sortOrder,
    } = values;

    const payload = {
        filter: filter || '',
        sortField: sortField || '',
        sortOrder: sortOrder || '',
    };
    
    return ValueContainerFetchService
        .getEquipments({
            page, 
            filter,
            sortOrder,
            sortField,
        })
        .then((data) => {
            const {
                page,
                content,
            } = data;
            
            const {
                number,
                totalPages,
            } = page;

            payload.number = number;
            payload.totalPages = totalPages;
            
            return formatClientEquipmentItems(content, dispatch);
        })
        .then((content) => {
            payload.content = content;
            dispatch(getClientEquipmentItemsSuccessAction(payload));
        })
        .catch((error) => {
            dispatch(getClientEquipmentItemsErrorAction(error));
        });
};

export const getClientEquipmentItemsCompleteApiCall = (values) => (dispatch) => {
    getClientEquipmentItemsApiCall(values, dispatch);
};

/* CREATE NEW CLIENT EQUIPMENT */
/* ACTION CREATORS */
export const createNewClientEquipmentRequestAction = () => {
    return {
        type: CREATE_NEW_CLIENT_EQUIPMENT_REQUEST_API_CALL,
    };
};

export const  createNewClientEquipmentSuccessAction = () => {
    return {
        type: CREATE_NEW_CLIENT_EQUIPMENT_SUCCESS_API_CALL,
    };
};

export const createNewClientEquipmentErrorAction = (error) => {
    return {
        error,
        type: CREATE_NEW_CLIENT_EQUIPMENT_ERROR_API_CALL,
    };
};

/* API CALL */
export const createNewClientEquipmentCompleteApiCall = (options, dispatch) => {
    dispatch(createNewClientEquipmentRequestAction());
    
    let title = '';
    let message = '';

    return ValueContainerFetchService
        .createEquipment(options)
        .then((result) => {
            
            const {
                type,
                tracker,
                identifier,
            } = result;

            const equipment = equipmentTitle(identifier);
            const orbitraker = orbitrackerTitle(tracker);

            title = type;
            message = I18nModule
                .t('CLIENT_EQUIPMENT.TOOLTIP.CREATE_NEW_EQUIPMENT')
                .replace('%equipment', equipment)
                .replace('%orbitracker', orbitraker);
            
            if(tracker){
                return ValueContainerFetchService
                    .updateDevice(tracker, {
                        association: true,
                        equipment: identifier,
                    });
            }
        })
        .then(() => {
            dispatch(createNewClientEquipmentSuccessAction());

            toastr.success(
                title, 
                message,
                {timeOut: TOAST_MESSAGE_DURATION},
            );
        })
        .catch((error) => {
            dispatch(createNewClientEquipmentErrorAction(error));
        });
};

/* ASSOCIATE CLIENT EQUIPMENT TO ORBITRACKER DEVICE  */
/* ACTION CREATORS */
export const associateClientEquipmentToOrbitrackerRequestAction = () =>{
    return {
        type: ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_REQUEST_API_CALL,
    };
};

export const associateClientEquipmentToOrbitrackerSuccessAction = () => {
    return {
        type: ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_SUCCESS_API_CALL,
    };
};

export const associateClientEquipmentToOrbitrackerErrorAction = (error) =>{
    return {
        error,
        type: ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_ERROR_API_CALL,
    };
};

/* API CALL */
export const associateClientEquipmentToOrbitrackerApiCall = (options, dispatch) => {
    dispatch(associateClientEquipmentToOrbitrackerRequestAction());

    let title = '';
    let message = '';

    const {
        tracker,
        equipment,
        oldTracker,
    } = options;

    return ValueContainerFetchService
        .associate({tracker, equipment})
        .then((result) => {
            const {
                type,
                tracker,
                identifier,
            } = result;

            const equipment = equipmentTitle(identifier);
            const orbitraker = orbitrackerTitle(tracker);

            title = type;
            message = I18nModule
                .t('CLIENT_EQUIPMENT.TOOLTIP.ASSOCIATED_ORBITRACKER')
                .replace('%equipment', equipment)
                .replace('%orbitracker', orbitraker);

            if(tracker){
                return ValueContainerFetchService
                    .updateDevice(tracker, {
                        association: true,
                        equipment: identifier,
                    });
            }
        })
        .then(() => {
            if(oldTracker){
                return ValueContainerFetchService
                    .updateDevice(oldTracker, {
                        equipment: null,
                        association: false,
                    });
            }
        })
        .then(() => {
            dispatch(associateClientEquipmentToOrbitrackerSuccessAction());

            toastr.success(
                title, 
                message,
                {timeOut: TOAST_MESSAGE_DURATION},
            );
        })
        .catch((error) => {
            dispatch(associateClientEquipmentToOrbitrackerErrorAction(error));
        });
};

/* GET CLIENT EQUIPMENT BY IDENTIFIER */
/* ACTION CREATORS */
export const  getClientEquipmentByIdentifierRequestAction = () =>{
    return {
        type: GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_REQUEST_API_CALL,
    };
};

export const  getClientEquipmentByIdentifierSuccessAction = (content) =>{
    return {
        payload: {content},
        type: GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_SUCCESS_API_CALL,
    };
};

export const getClientEquipmentByIdentifierErrorAction = (error) =>{
    return {
        error,
        type: GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_ERROR_API_CALL,
    };
};

/* API CALL */
export const getClientEquipmentByIdentifierCompleteApiCall = (identifier) => (dispatch) => {
    dispatch(getClientEquipmentByIdentifierRequestAction());
    
    return ValueContainerFetchService
        .getEquipments(identifier)
        .then((data) => {
            dispatch(getClientEquipmentByIdentifierSuccessAction(data));
        })
        .catch((error) => {
            dispatch(getClientEquipmentByIdentifierErrorAction(error));
        });
};

export const associateClientEquipmentToOrbitrackerCompleteApiCall = (options) => (dispatch) => {
    return associateClientEquipmentToOrbitrackerApiCall(options, dispatch);
};
