/* GENERAL */
import Promise from 'bluebird';

/* ACTION TYPES */
import {
    /* GET EQUIPMENT POSITIONS BY TIME RANGE */
    GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_ERROR_API_CALL,
    GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_REQUEST_API_CALL,
    GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_SUCCESS_API_CALL,

    /* EQUIPMENT POSITIONS GRID CLIENT PAGINATION */
    ON_EQUIPMENT_POSITIONS_GRID_SORT_CHANGED_CALL,
    ON_EQUIPMENT_POSITIONS_GRID_PAGE_CHANGED_CALL, 
} from '../constants/EquipmentPositions';

/* ACTION CREATORS */
import {getDeviceLastPositionErrorAction} from './DeviceLastPosition';

/* FETCH SERVICES */
import MeasurementFetchService from '../services/Measurement';
import ValueContainerFetchService from '../services/ValueContainer';

/* CONSTANTS */
import {NULL_ENTITY} from '../constants/General';

/* UTILS */
import {moment} from '../utils/i18nModule'; 

/* ACTIONS */
/* ON EQUIPMENT POSITIONS GRID PAGE CHANGED */
export const onEquipmentPositionsGridPageChangedAction = (page) =>{
    return {
        payload: {page},
        type: ON_EQUIPMENT_POSITIONS_GRID_PAGE_CHANGED_CALL,
    };
};

export const onEquipmentPositionsGridPageChangedCall = (page) => (dispatch) => {
    dispatch(onEquipmentPositionsGridPageChangedAction(page));
};

/* ON EQUIPMENT POSITIONS GRID SORT CHANGED */
export const onEquipmentPositionsGridSortChangedAction = (sortField, sortOrder) => {
    return {
        payload: {
            sortField, 
            sortOrder,
        },
        type: ON_EQUIPMENT_POSITIONS_GRID_SORT_CHANGED_CALL,
    };
};

export const onEquipmentPositionsGridSortChangedCall = (sortField, sortOrder) => (dispatch) => {
    dispatch(onEquipmentPositionsGridSortChangedAction(sortField, sortOrder));
};

/* GET DEVICE POSITIONS BY TIME RANGE */
/* ACTION CREATORS */
export const getEquipmentPositionsByTimeRangeRequestAction = () =>{
    return {
        type: GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_REQUEST_API_CALL,
    };
};

export const getEquipmentPositionsByTimeRangeSuccessAction = (payload) => {
    return {
        payload,
        type: GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_SUCCESS_API_CALL,
    };
};

export const getEquipmentPositionsByTimeRangeErrorAction = (error) => {
    return {
        error,
        type: GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_ERROR_API_CALL,
    };
};

/* ADD LAST CLIENT EQUIPMENT POSITION & TIME AND FORMAT ITEMS */
function formatEquipmentPositionsByTimeRange(content){
    const data = [];
    content.forEach((item) => {
        if(!item) return;
        const {tracker, positions} = item;
        data.push(...positions.map(({timestamp, values}) => Object.assign({tracker, timestamp}, values)));
    });

    return data;
}

function getDevicePositionsByTimeRange(content, options, dispatch) {
    const {from, to, onlyRange} = options;
    
    let tempFrom = onlyRange ? moment(from) : moment(to);
    let tempTo = moment(to);

    return Promise.mapSeries(content, ({timestamp, values}) => {
        const {tracker} = values;

        if(!onlyRange) {
            tempTo = tempFrom;
            tempFrom = moment(timestamp);
        }

        if(!tracker || tracker === NULL_ENTITY) return;

        const options = {
            to: tempTo,
            from: tempFrom,
            identifier: tracker,
            fields: ['status', 'battery', 'latitude', 'longitude'],
        };

        return MeasurementFetchService
            .getPositions(options)
            .then(({content}) => { return {positions: content, tracker}; })
            .catch((error) => {
                dispatch(getDeviceLastPositionErrorAction(error));
                return;
            });
    });
}

/* API CALL */
export const getEquipmentPositionsByTimeRangeApiCall = (options, dispatch) => {
    const {
        to,
        from,
        identifier,
    } = options;

    const payload = {to, from};
    dispatch(getEquipmentPositionsByTimeRangeRequestAction());
    return MeasurementFetchService
        .getPositions({
            to,
            from,
            identifier,
            fields: ['tracker'],
        })
        .then(({content}) => {
            if(content.length > 0) return {content};
            return MeasurementFetchService.getLastPosition(identifier, ['tracker']);
        })
        .then(({content}) => getDevicePositionsByTimeRange(content, {
            to, 
            from,
            onlyRange: true,
        }, dispatch))
        .then(formatEquipmentPositionsByTimeRange)
        .then((content) => {
            payload.content = content;
            dispatch(getEquipmentPositionsByTimeRangeSuccessAction(payload));
        })
        .catch((error) => {
            dispatch(getEquipmentPositionsByTimeRangeErrorAction(error)); 
        });
};
 
export const getEquipmentPositionsByTimeRangeCompleteApiCall = (options) => (dispatch) => {
    getEquipmentPositionsByTimeRangeApiCall(options, dispatch);
};
