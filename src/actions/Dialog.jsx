/* ACTION TYPES */
import {
    /* DIALOG NAVIGATION */
    DIALOG_OPEN_CALL,
    DIALOG_CLOSE_CALL,
} from '../constants/Dialog';

/* DIALOG OPEN */
export const dialogOpenAction = (payload) => {
    return {
        payload,
        type: DIALOG_OPEN_CALL,
    };
};

export const dialogOpenCall = (params) => (dispatch) => {
    dispatch(dialogOpenAction(params));
};

/* DIALOG CLOSE */
export const dialogCloseAction = () => {
    return {
        type: DIALOG_CLOSE_CALL,
    };
};

export const dialogCloseCall = () => (dispatch) => {
    dispatch(dialogCloseAction());
};
