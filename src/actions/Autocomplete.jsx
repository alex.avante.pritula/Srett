/* ACTION TYPES */
import {
    /* GET AUTOCOMPLETE ITEMS (ALL EXIST UNASSOCIATED DEVICES) */
    GET_AUTOCOMPLETE_ITEMS_ERROR_API_CALL,
    GET_AUTOCOMPLETE_ITEMS_REQUEST_API_CALL,
    GET_AUTOCOMPLETE_ITEMS_SUCCESS_API_CALL,

    /* RESET AUTOCOMPLETE DATA */
    RESET_AUTOCOMPLETE_DATA_CALL,
} from '../constants/Autocomplete';

/* FETCH SERVICES */
import ValueContainerFetchService from '../services/ValueContainer';

/* UTILS */
import orbitrackerTitle from '../utils/orbitrackerTitle';

/* ACTIONS */
/* RESET AUTOCOMPLETE ITEMS */
export const resetAutocompleteItemsAction = () => {
    return {
        type: RESET_AUTOCOMPLETE_DATA_CALL,
    };
};

export const resetAutocompleteItemsCall = () => (dispatch) => {
    dispatch(resetAutocompleteItemsAction());
};

/* GET AUTOCOMPLETE ITEMS (ALL EXIST UNASSOCIATED DEVICES) */
/* ACTION CREATORS */
export const getAutocompleteItemsRequestAction = () => {
    return {
        type: GET_AUTOCOMPLETE_ITEMS_REQUEST_API_CALL,
    };
};

export const getAutocompleteItemsErrorAction = (error) => {
    return {
        error,
        type: GET_AUTOCOMPLETE_ITEMS_ERROR_API_CALL,
    };
};

export const getAutocompleteItemsSuccessAction = (items) => {
    return {
        payload: {items},
        type: GET_AUTOCOMPLETE_ITEMS_SUCCESS_API_CALL,
    };
};

/* FORMAT AUTOCOMPLETE ITEMS */
function formatAutocompleteItems(content){
    return content.map((item) => {
        const {identifier} = item;
        return {
            identifier,
            visible: orbitrackerTitle(identifier),
        };
    });
}

/* API CALL */
export const getAutocompleteItemsCompleteApiCall = (filter) => (dispatch) => {
    dispatch(getAutocompleteItemsRequestAction());

    return ValueContainerFetchService
        .getFreeDevices(filter)
        .then(({content}) => formatAutocompleteItems(content))
        .then((content) => {
            dispatch(getAutocompleteItemsSuccessAction(content));
        })
        .catch((error) => {
            dispatch(getAutocompleteItemsErrorAction(error));
        });
};
