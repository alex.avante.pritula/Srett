/* ACTION TYPES */
import {
    /* GET DEVICE POSITIONS BY TIME RANGE */
    GET_DEVICE_POSITIONS_BY_TIME_RANGE_ERROR_API_CALL,
    GET_DEVICE_POSITIONS_BY_TIME_RANGE_REQUEST_API_CALL,
    GET_DEVICE_POSITIONS_BY_TIME_RANGE_SUCCESS_API_CALL,

    /* DEVICE POSITIONS GRID CLIENT PAGINATION */
    ON_DEVICE_POSITIONS_GRID_SORT_CHANGED_CALL,
    ON_DEVICE_POSITIONS_GRID_PAGE_CHANGED_CALL, 
} from '../constants/DevicePositions';

/* FETCH SERVICES */
import MeasurementFetchService from '../services/Measurement';

/* ACTIONS */
/* ON DEVICE POSITIONS GRID PAGE CHANGED */
export const onDevicePositionsGridPageChangedAction = (page) =>{
    return {
        payload: {page},
        type: ON_DEVICE_POSITIONS_GRID_PAGE_CHANGED_CALL,
    };
};

export const onDevicePositionsGridPageChangedCall = (page) => (dispatch) => {
    dispatch(onDevicePositionsGridPageChangedAction(page));
};

/* ON DEVICE POSITIONS GRID SORT CHANGED */
export const onDevicePositionsGridSortChangedAction = (sortField, sortOrder) =>{
    return {
        payload: {
            sortField, 
            sortOrder,
        },
        type: ON_DEVICE_POSITIONS_GRID_SORT_CHANGED_CALL,
    };
};

export const onDevicePositionsGridSortChangedCall = (sortField, sortOrder) => (dispatch) => {
    dispatch(onDevicePositionsGridSortChangedAction(sortField, sortOrder));
};

/* GET DEVICE POSITIONS BY TIME RANGE */
/* ACTION CREATORS */
export const getDevicePositionsByTimeRangeRequestAction = () => {
    return {
        type: GET_DEVICE_POSITIONS_BY_TIME_RANGE_REQUEST_API_CALL,
    };
};

export const getDevicePositionsByTimeRangeSuccessAction = (payload) => {
    return {
        payload,
        type: GET_DEVICE_POSITIONS_BY_TIME_RANGE_SUCCESS_API_CALL,
    };
};

export const getDevicePositionsByTimeRangeErrorAction = (error) => {
    return {
        error,
        type: GET_DEVICE_POSITIONS_BY_TIME_RANGE_ERROR_API_CALL,
    };
};

/* API CALL */
export const getDevicePositionsByTimeRangeApiCall = (options, dispatch) => {
    const {
        to,
        from,
        identifier,
    } = options;

    const payload = {to, from};
    dispatch(getDevicePositionsByTimeRangeRequestAction());

    return MeasurementFetchService
        .getPositions({
            to,
            from,
            identifier,
            fields: ['status', 'latitude', 'longitude', 'battery', 'firmware_version', 'timetofix', 'reboot_counter'],
        })
        .then(({content}) => {
            payload.content = content;
            dispatch(getDevicePositionsByTimeRangeSuccessAction(payload));
        })
        .catch((error) => {
            dispatch(getDevicePositionsByTimeRangeErrorAction(error)); 
        });
};
 
export const getDevicePositionsByTimeRangeCompleteApiCall = (options) => (dispatch) => {
    getDevicePositionsByTimeRangeApiCall(options, dispatch);
};
