/* ACTION TYPES */
import {
    /* DEVICE LAST MEASUREMENT POSITION ERROR */
    GET_DEVICE_LAST_POSITION_ERROR_API_CALL,
} from '../constants/DeviceLastPosition';

export const getDeviceLastPositionErrorAction = (error) => {
    return {
        error,
        type: GET_DEVICE_LAST_POSITION_ERROR_API_CALL,
    };
};
