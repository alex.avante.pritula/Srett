/* ACTION TYPES */
import {
    /* GET DEVICE ACTIVITY ITEMS */
    GET_DEVICE_ACTIVITY_ITEMS_ERROR_API_CALL,
    GET_DEVICE_ACTIVITY_ITEMS_REQUEST_API_CALL,
    GET_DEVICE_ACTIVITY_ITEMS_SUCCESS_API_CALL,

    /* RESET DEVICE ACTIVITY DATA */
    RESET_DEVICE_ACTIVITY_GRID_CALL,
} from '../constants/DeviceActivity';

/* FETCH SERVICES */
import ValueContainerFetchService from '../services/ValueContainer';

/* ACTIONS */
/* RESET DEVICE ACTIVITY DATA */
export const resetDeviceActivityGridAction = () => {
    return {
        type: RESET_DEVICE_ACTIVITY_GRID_CALL,
    };
};

export const resetDeviceActivityGridCall = () => (dispatch) => {
    dispatch(resetDeviceActivityGridAction());
};

/* GET DEVICE ACTIVITY ITEMS */
/* ACTION CREATORS */
export const getDeviceActivityItemsRequestAction = () => {
    return {
        type: GET_DEVICE_ACTIVITY_ITEMS_REQUEST_API_CALL,
    };
};

export const getDeviceActivityItemsSuccessAction = (payload) => {
    return {
        payload,
        type: GET_DEVICE_ACTIVITY_ITEMS_SUCCESS_API_CALL,
    };
};

export const getDeviceActivityItemsErrorAction = (error) => { 
    return {
        error,
        type: GET_DEVICE_ACTIVITY_ITEMS_ERROR_API_CALL,
    };
};

/* API CALL */
export const getDeviceActivityItemsCompleteApiCall = (values) => (dispatch) => {
    dispatch(getDeviceActivityItemsRequestAction());

    const {
        page, 
        filter,
        sortField, 
        sortOrder,
    } = values;

    const payload = {
        sortField: sortField || '',
        sortOrder: sortOrder || '',
    };

    return ValueContainerFetchService
        .getProcess({
            page, 
            filter,
            sortField, 
            sortOrder,
        })
        .then((data) => {
            const {
                page,
                content,
            } = data;
            
            const {
                number,
                totalPages,
            } = page;

            payload.number = number;
            payload.content = content;
            payload.totalPages = totalPages;
            dispatch(getDeviceActivityItemsSuccessAction(payload));
        })
        .catch((error) => {
            dispatch(getDeviceActivityItemsErrorAction(error));
        });
};
