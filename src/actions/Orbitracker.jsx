/* GENERAL */
import Promise from 'bluebird';

/* ACTION TYPES */
import {
    /* GET ORBITRACKER ITEMS */
    GET_ORBITRACKER_ITEMS_ERROR_API_CALL,
    GET_ORBITRACKER_ITEMS_REQUEST_API_CALL,
    GET_ORBITRACKER_ITEMS_SUCCESS_API_CALL,

    /* RESET ORBITRACKER DATA */
    RESET_ORBITRACKERS_GRID_CALL,

    /* GET ORBITRACKER BY IDENTIFIER */
    GET_ORBITRACKER_BY_IDENTIFIER_ERROR_API_CALL,
    GET_ORBITRACKER_BY_IDENTIFIER_REQUEST_API_CALL,
    GET_ORBITRACKER_BY_IDENTIFIER_SUCCESS_API_CALL,
} from '../constants/Orbitracker';

/* ACTION CREATORS */
import {getDeviceLastPositionErrorAction} from './DeviceLastPosition';

/* FETCH SERVICES */
import MeasurementFetchService from '../services/Measurement';
import ValueContainerFetchService from '../services/ValueContainer';

/* UTILS */
import {moment} from '../utils/i18nModule';

/* CONSTANTS */
import {NULL_ENTITY} from '../constants/General';

/* ACTIONS */
/* RESET ORBITRACKERS GRID */
export const resetOrbitrackersGridAction = () => {
    return {
        type: RESET_ORBITRACKERS_GRID_CALL,
    };
};

export const resetOrbitrackersGridCall = () => (dispatch) => {
    dispatch(resetOrbitrackersGridAction());
};

/* GET ORBITRACKER ITEMS */
/* ACTION CREATORS */
export const getOrbitrackerItemsRequestAction = () => {
    return {
        type: GET_ORBITRACKER_ITEMS_REQUEST_API_CALL,
    };
};

export const getOrbitrackerItemsSuccessAction = (payload) => {
    return {
        payload,
        type: GET_ORBITRACKER_ITEMS_SUCCESS_API_CALL,
    };
};

export const getOrbitrackerItemsErrorAction = (error) =>{
    return {
        error,
        type: GET_ORBITRACKER_ITEMS_ERROR_API_CALL,
    };
};

/* ADD LAST ORBITRACKER POSITION & TIME AND FORMAT ITEMS */
function formatOrbitrackerItems(content, dispatch){
    return Promise.mapSeries(content, (device) => {
        const {
            equipment,
            identifier,
            last_measure_date,
        } = device;

        if(equipment && equipment !== NULL_ENTITY && equipment.identifier) {
            device.equipmentRef = equipment.identifier;
        }

        device.lastmeasure = moment(last_measure_date);
        
        return MeasurementFetchService
            .getLastPosition(
                identifier,
                ['longitude', 'latitude', 'status', 'battery', 'rssi', 'reccordindex', 'reboot_counter', 'firmware_version'],
            )
            .then(({content}) => {
                return Promise.map(content, (indicator) => {
                    const {
                        values,
                        timestamp,
                    } = indicator;

                    const {
                        rssi,
                        status,
                        battery,
                        latitude,
                        longitude,
                        reccordindex,
                        reboot_counter,
                        firmware_version,
                    } = values;

                    if(rssi) device.rssi = rssi;
                    if(status) device.status = status;
                    if(battery) device.battery = battery;
                    if(reccordindex) device.reccordindex = reccordindex;
                    if(reboot_counter) device.reboot_counter = reboot_counter;
                    if(firmware_version) device.firmware_version = firmware_version;

                    if (parseFloat(longitude) && parseFloat(latitude)) {
                        device.latitude = latitude;
                        device.longitude = longitude;
                        if(timestamp) device.lastmeasure = moment(timestamp);
                    }
                });
            })
            .catch((error) => {
                dispatch(getDeviceLastPositionErrorAction(error));
            })
            .then(() => device);
    });
}

/* API CALL */
export const getOrbitrackerItemsApiCall = (options, dispatch) => {
    dispatch(getOrbitrackerItemsRequestAction());
    const {
        page, 
        filter,
        sortField, 
        sortOrder,
    } = options;

    const payload = {
        filter: filter || '',
        sortField: sortField || '',
        sortOrder: sortOrder || '',
    };

    ValueContainerFetchService
        .getDevices({
            page, 
            filter,
            sortOrder,
            sortField, 
        })
        .then((data) => {
            const {
                page,
                content,
            } = data;
            
            const {
                number,
                totalPages,
            } = page;

            payload.number = number;
            payload.totalPages = totalPages;
            return formatOrbitrackerItems(content, dispatch);
        })
        .then((content) =>{
            payload.content = content;
            dispatch(getOrbitrackerItemsSuccessAction(payload));
        })
        .catch((error) => {
            dispatch(getOrbitrackerItemsErrorAction(error));
        });
};

export const getOrbitrackerItemsCompleteApiCall = (options) => (dispatch) => {
    getOrbitrackerItemsApiCall(options, dispatch); 
};

/* GET ORBITRACKER BY IDENTIFIER */
/* ACTIONS */
export const getOrbitrackerByIdentifierRequestAction = () => { 
    return {
        type: GET_ORBITRACKER_BY_IDENTIFIER_REQUEST_API_CALL,
    };
};

export const getOrbitrackerByIdentifierSuccessAction = (content) => {
    return {
        payload: {content},
        type: GET_ORBITRACKER_BY_IDENTIFIER_SUCCESS_API_CALL,
    };
};

export const getOrbitrackerByIdentifierErrorAction = (error) => {
    return {
        error,
        type: GET_ORBITRACKER_BY_IDENTIFIER_ERROR_API_CALL,
    };
};

/* API CALL */
export const getOrbitrackerByIdentifierCompleteApiCall = (indentifier) => (dispatch) => {
    dispatch(getOrbitrackerByIdentifierRequestAction());

    ValueContainerFetchService
        .getDevices(indentifier)
        .then((content) => formatOrbitrackerItems([content]))
        .then((content) => {
            dispatch(getOrbitrackerByIdentifierSuccessAction(content[0]));
        })
        .catch((error) => {
            dispatch(getOrbitrackerByIdentifierErrorAction(error));
        });          
};
