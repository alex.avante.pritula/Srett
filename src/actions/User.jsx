/* GENERAL */
import {hashHistory} from 'react-router';
import {toastr} from 'react-redux-toastr';

/* REDUX */
import {SubmissionError} from 'redux-form';

/* ACTION TYPES */
import {
    /* SIGN IN */
    SIGN_IN_ERROR_API_CALL,
    SIGN_IN_REQUEST_API_CALL,
    SIGN_IN_SUCCESS_API_CALL,
    
    /* GET USER INFO */
    GET_USER_INFO_ERROR_API_CALL,
    GET_USER_INFO_REQUEST_API_CALL,
    GET_USER_INFO_SUCCESS_API_CALL,

    /* UPDATE PASSWORD */
    UPDATE_PASSWORD_ERROR_API_CALL,
    UPDATE_PASSWORD_REQUEST_API_CALL,
    UPDATE_PASSWORD_SUCCESS_API_CALL,

    /* SIGN OUT  */
    SIGN_OUT_CALL,

    /* CHANGE LANGUAGE */
    CHANGE_LANGUAGE_CALL,

    /* TOOGLE MINIMIZE MENU */
    TOOGLE_MINIMIZE_MENU_CALL,
} from '../constants/User';

/* LOCALIZATION */
import I18nModule from '../utils/i18nModule';

/* UTILS */
import UserMetaService from '../utils/userMetaService';

/* FETCH SERVICES */
import UserFetchService from '../services/User';

/* CONSTANTS */
import {
    PAGES,
    AUTH_TOKEN,
    AUTH_PREFIX,
    LOCALIZATION,
    API_ERROR_STATUS,
    TOAST_MESSAGE_DURATION,
} from '../constants/General';

const {EN} = LOCALIZATION;
const {START, LOGIN} = PAGES;

const {
    BAD_REQUEST,
    UNAUTHORIZED,
} = API_ERROR_STATUS;

/* ACTIONS */

/* TOOGLE MINIMIZE MENU */
export const toogleMinimizeMenuAction = (minimizedMenu) => {
    return {
        payload: {minimizedMenu},
        type: TOOGLE_MINIMIZE_MENU_CALL,
    };
};

export const toogleMinimizeMenuCall = (identifier, minimizedMenu) => (dispatch) => {
    UserMetaService.updateMenu(identifier, minimizedMenu);
    dispatch(toogleMinimizeMenuAction(minimizedMenu));
};

/* SIGN IN */
/* ACTION CREATORS */
export const signInRequestAction = () => {
    return {
        type: SIGN_IN_REQUEST_API_CALL,
    };
};

export const signInSuccessAction = () => {
    return {
        type: SIGN_IN_SUCCESS_API_CALL,
    };
};

export const signInErrorAction = (error) => {
    return {
        error,
        type: SIGN_IN_ERROR_API_CALL,
    };
};

/* API CALL */
export const signInCompleteApiCall = ({email, password} , dispatch) => {
    dispatch(signInRequestAction());
    return UserFetchService
        .signIn(email, password)
        .then((data) => {
            UserMetaService.setToken(data[AUTH_TOKEN]);
            dispatch(signInSuccessAction());
            hashHistory.push(START); 
            toastr.success(
                I18nModule.t('TITLE'), 
                I18nModule.t('WELCOME_MESSAGE'),
                {timeOut: TOAST_MESSAGE_DURATION}
            );
        }).catch((error) => {
            const {status} = error;
            dispatch(signInErrorAction(error));

            if(status === BAD_REQUEST || status === UNAUTHORIZED){
                throw new SubmissionError({ 
                    email: I18nModule.t('REDUX_FORM_ERROR.AUTH'),
                });
            }
        });
};

/* SIGN OUT  */
export const signOutAction = () => {
    return {
        type: SIGN_OUT_CALL,
    };
};

export const signOutCall = () => (dispatch) => {
    UserMetaService.removeToken();
    dispatch(signOutAction());
    I18nModule.changeLanguage(EN);
    hashHistory.push(LOGIN); 
};

/* GET USER INFO */
/* ACTION CREATORS */
export const getUserDataRequestAction = () => {
    return {
        type: GET_USER_INFO_REQUEST_API_CALL,
    };
};

export const getUserDataSuccessAction = (payload) => {
    return {
        payload,
        type: GET_USER_INFO_SUCCESS_API_CALL,
    };
};

export const getUserDataErrorAction = (error) => {
    return {
        error,
        type: GET_USER_INFO_ERROR_API_CALL,
    };
};

/* API CALL */
export const getUserDataCompleteApiCall = () => (dispatch) => {
    dispatch(getUserDataRequestAction());
    UserFetchService.getUserData()
        .then((user) => {
            const {identifier} = user;
            const meta = UserMetaService.get(identifier);
            const {language} = meta;
            I18nModule.changeLanguage(language);

            const payload = Object.assign({}, user, meta);
            dispatch(getUserDataSuccessAction(payload));

        }).catch((error) => {
            const {status} = error;
            if(status === UNAUTHORIZED) {
                localStorage.removeItem(AUTH_TOKEN);
                hashHistory.replace(LOGIN);
                
                toastr.error(
                    I18nModule.t('TITLE'), 
                    I18nModule.t('AUTH.UNAUTHORIZED'),
                    {timeOut: TOAST_MESSAGE_DURATION},
                );
            }
            dispatch(getUserDataErrorAction(error));
        });
};

/* UPDATE PASSWORD COMPLETE API CALL */
/* ACTION CREATORS */
export const  updateUserPasswordRequestAction = () => {
    return {
        type: UPDATE_PASSWORD_REQUEST_API_CALL,
    };
};

export const updateUserPasswordSuccessAction = () => {
    return {
        type: UPDATE_PASSWORD_SUCCESS_API_CALL,
    };
};

export const updateUserPasswordErrorAction = (error) =>{
    return {
        error,
        type: UPDATE_PASSWORD_ERROR_API_CALL,
    };
};

export const updateUserPasswordCompleteApiCall = ({password, newPassword} , dispatch)=> {
    dispatch(updateUserPasswordRequestAction());
    UserFetchService
        .updatePassword(password, newPassword)
        .then(() => {
            UserMetaService.removeToken();
            dispatch(signOutAction());
            I18nModule.changeLanguage(EN);
            hashHistory.push(LOGIN); 
            dispatch(updateUserPasswordSuccessAction());
        })
        .catch((error) => {
            dispatch(updateUserPasswordErrorAction(error));
        });
};

/* CHANGE LANGUAGE CALL */
export const changeLanguageAction = (language) => {
    return {
        payload: {language},
        type: CHANGE_LANGUAGE_CALL,
    };
};

export const changeLanguageCall = (identifier, language) => (dispatch) => {
    I18nModule.changeLanguage(language);
    UserMetaService.updateLanguage(identifier, language);
    dispatch(changeLanguageAction(language));
};
