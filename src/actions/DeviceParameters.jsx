/* GENERAL */
import {toastr} from 'react-redux-toastr';

/* ACTION TYPES */
import {
    /* GET DEVICE PARAMETERS */
    GET_DEVICE_PARAMETERS_ERROR_API_CALL,
    GET_DEVICE_PARAMETERS_REQUEST_API_CALL,
    GET_DEVICE_PARAMETERS_SUCCESS_API_CALL,

    /* SAVE DEVICE PARAMETERS */
    SAVE_DEVICE_PARAMETERS_ERROR_API_CALL,
    SAVE_DEVICE_PARAMETERS_REQUEST_API_CALL,
    SAVE_DEVICE_PARAMETERS_SUCCESS_API_CALL,

    /* SYNCHRONIZE DEVICE PARAMETERS */
    SYNCHRONIZE_DEVICE_PARAMETERS_ERROR_API_CALL,
    SYNCHRONIZE_DEVICE_PARAMETERS_REQUEST_API_CALL,
    SYNCHRONIZE_DEVICE_PARAMETERS_SUCCESS_API_CALL,

    /* REBOOT DEVICE PARAMETERS */
    REBOOT_DEVICE_PARAMETERS_ERROR_API_CALL,
    REBOOT_DEVICE_PARAMETERS_SUCCESS_API_CALL,
    REBOOT_DEVICE_PARAMETERS_REQUEST_API_CALL,
} from '../constants/DeviceParameters';

/* FETCH SERVICES */
import GatewayFetchService from '../services/GateWay';
import ValueContainerFetchService from '../services/ValueContainer';

/* UTILS */
import orbitrackerTitle from '../utils/orbitrackerTitle';

/* LOCALIZATION */
import I18nModule from '../utils/i18nModule';

/* CONSTANTS */
import {
    PROCESS_TYPE,
    TOAST_MESSAGE_DURATION,
    DEVICE_ACTIVITY_STATUS,
} from '../constants/General';

const {
    REBOOT,
    READ_PARAMETERS,
    WRITE_PARAMETERS,
} = PROCESS_TYPE;

const {PENDING} = DEVICE_ACTIVITY_STATUS;

/* GET DEVICE PARAMETERS */
/* ACTION CREATORS */
export const getDeviceParametersRequestAction = () => {
    return {
        type: GET_DEVICE_PARAMETERS_REQUEST_API_CALL,
    };
};

export const getDeviceParametersSuccessAction = (content) => {
    return {
        payload: {content},
        type: GET_DEVICE_PARAMETERS_SUCCESS_API_CALL,
    };
};

export const getDeviceParametersErrorAction = (error) => {
    return {
        error,
        type: GET_DEVICE_PARAMETERS_ERROR_API_CALL,
    };
};

/* API CALL */
export const getDeviceParametersCompleteApiCall = (identifier) => (dispatch) => {
    dispatch(getDeviceParametersRequestAction());
    
    return ValueContainerFetchService
        .getParameters(identifier)
        .then(({content}) => {
            dispatch(getDeviceParametersSuccessAction((content[0] || {})));
        })
        .catch((error) => {
            dispatch(getDeviceParametersErrorAction(error));
        });          
};


/* SAVE DEVICE PARAMETERS */
/* ACTION CREATORS */
export const saveDeviceParametersRequestAction = () => {
    return {
        type: SAVE_DEVICE_PARAMETERS_REQUEST_API_CALL,
    };
};

export const saveDeviceParametersSuccessAction = () => {
    return {
        type: SAVE_DEVICE_PARAMETERS_SUCCESS_API_CALL,
    };
};

export const saveDeviceParametersErrorAction = (error) => {
    return {
        error,
        type: SAVE_DEVICE_PARAMETERS_ERROR_API_CALL,
    };
};

/* API CALL */
export const saveDeviceParametersApiCall = (device, payload, dispatch) => {
    dispatch(saveDeviceParametersRequestAction());

    const data = {
        device,
        payload: JSON.stringify(payload),
        process_type: WRITE_PARAMETERS,
    };

    return ValueContainerFetchService
        .createProcess(data)
        .then(({identifier}) => GatewayFetchService.updateDeviceParameters(identifier))
        .then(() => {
            dispatch(saveDeviceParametersSuccessAction());
            toastr.success(
                orbitrackerTitle(device), 
                I18nModule.t(`ORBITRACKER.ACTIVITY.WRITE_PARAMETERS`),
                {timeOut: TOAST_MESSAGE_DURATION}
            );
        })
        .catch((error) => {
            dispatch(saveDeviceParametersErrorAction(error));
        });
};

export const saveDeviceParametersCompleteApiCall = (device, payload) => (dispatch) => {
    return saveDeviceParametersApiCall(device, payload, dispatch);
};

/* SYNCHRONIZE DEVICE PARAMETERS */
/* ACTION CREATORS */
export const synchronizeDeviceParametersRequestAction = () => {
    return {
        type: SYNCHRONIZE_DEVICE_PARAMETERS_REQUEST_API_CALL,
    };
};

export const synchronizeDeviceParametersSuccessAction = () => {
    return {
        type: SYNCHRONIZE_DEVICE_PARAMETERS_SUCCESS_API_CALL,
    };
};

export const synchronizeDeviceParametersErrorAction = (error) => {
    return {
        error,
        type: SYNCHRONIZE_DEVICE_PARAMETERS_ERROR_API_CALL,
    };
};

/* API CALL */
export const synchronizeDeviceParametersApiCall = (device, fields, dispatch) => {
    dispatch(synchronizeDeviceParametersRequestAction());

    const payload = {};
    fields.forEach((field) => { payload[field] = PENDING; });

    const data = {
        device,
        payload: JSON.stringify(payload),
        process_type: READ_PARAMETERS,
    };

    return ValueContainerFetchService
        .createProcess(data)
        .then(({identifier}) => GatewayFetchService.updateDeviceParameters(identifier))
        .then(() => {  
            dispatch(synchronizeDeviceParametersSuccessAction());
            toastr.success(
                orbitrackerTitle(device), 
                I18nModule.t(`ORBITRACKER.ACTIVITY.READ_PARAMETERS`),
                {timeOut: TOAST_MESSAGE_DURATION}
            );
        })
        .catch((error) => {
            dispatch(synchronizeDeviceParametersErrorAction(error));
        });

};

export const synchronizeDeviceParametersCompleteApiCall = (device, fields) => (dispatch) => {
    return synchronizeDeviceParametersApiCall(device, fields, dispatch);
};

/* REBOOT DEVICE PARAMETERS */
/* ACTION CREATORS */
export const rebootDeviceParametersRequestAction = () => {
    return {
        type: REBOOT_DEVICE_PARAMETERS_REQUEST_API_CALL,
    };
};

export const rebootDeviceParametersSuccessAction = () => {
    return {
        type: REBOOT_DEVICE_PARAMETERS_SUCCESS_API_CALL,
    };
};

export const rebootDeviceParametersErrorAction = (error) => {
    return {
        error,
        type: REBOOT_DEVICE_PARAMETERS_ERROR_API_CALL,
    };
};

/* API CALL */
export const rebootDeviceParametersApiCall = (device, dispatch) => {
    dispatch(rebootDeviceParametersRequestAction());

    const data = {
        device,
        process_type: REBOOT,
        payload: JSON.stringify({[REBOOT]: true}),
    };

    return ValueContainerFetchService
        .createProcess(data)
        .then(({identifier}) => GatewayFetchService.updateDeviceParameters(identifier))
        .then(() => {
            dispatch(rebootDeviceParametersSuccessAction());

            toastr.success(
                orbitrackerTitle(device), 
                I18nModule.t(`ORBITRACKER.ACTIVITY.REBOOT`),
                {timeOut: TOAST_MESSAGE_DURATION}
            );
        })
        .catch((error) => {
            dispatch(rebootDeviceParametersErrorAction(error));
        });

};

export const rebootDeviceParametersCompleteApiCall = (device) => (dispatch) => {
    return rebootDeviceParametersApiCall(device, dispatch);
};
