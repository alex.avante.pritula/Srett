/* GENERAL */
import Promise from 'bluebird';

/* ACTION TYPES */
import {
    /* GET SEARCH ITEMS */
    GET_SEARCH_ITEMS_ERROR_API_CALL,
    GET_SEARCH_ITEMS_REQUEST_API_CALL,
    GET_SEARCH_ITEMS_SUCCESS_API_CALL,

    /* RESET SEARCH_ITEMS */
    RESET_SEARCH_DATA_CALL,
} from '../constants/Search';

/* UTILS */
import equipmentTitle from '../utils/equipmentTitle';
import orbitrackerTitle from '../utils/orbitrackerTitle';

/* LOCALIZATION */
import i18nModule from './../utils/i18nModule';

/* ACTION CREATORS */
import {getDeviceLastPositionErrorAction} from './DeviceLastPosition';

/* CONSTANTS */
import {
    DOMAIN_ID,
    SEARCH_META,
} from '../constants/General';

const {
    EQUIPMENT,
    ORBITRACKER,
} = SEARCH_META;

/* FETCH SERVICES */
import GoogleMapFetchService from '../services/GoogleMap';
import ValueContainerFetchService from '../services/ValueContainer';

/* ACTIONS */
/* RESET SEARCH DATA */
export const resetSearchItemsAction = () => {
    return {
        type: RESET_SEARCH_DATA_CALL,
    };
};

export const resetSearchItemsCall = () => (dispatch) => {
    dispatch(resetSearchItemsAction());
};

/* GET SEARCH ITEMS */
/* ACTION CREATORS */
export const getSearchItemsRequestAction = () => {
    return {
        type: GET_SEARCH_ITEMS_REQUEST_API_CALL,
    };
};

export const getSearchItemsErrorAction = (error) => {
    return {
        error,
        type: GET_SEARCH_ITEMS_ERROR_API_CALL,
    };
};

export const getSearchItemsSuccessAction = (payload) => {
    return {
        payload,
        type: GET_SEARCH_ITEMS_SUCCESS_API_CALL,
    };
};

/* GET SEARCH ITEMS (EQUIPMENT) */
function formatSearchEquipmentItems(content){
    return content.map((item) => {
        const {type, identifier} = item;
        return {
            data: identifier,
            visible: `${type} / ${equipmentTitle(identifier)}`,
        };
    });
}

/* API CALL */
export const getSearchEquipmentItemsCompleteApiCall = (filter = '') => (dispatch) => {
    dispatch(getSearchItemsRequestAction());

    ValueContainerFetchService
        .complexSearch({
            queries: {
                type: filter,
                identifier: filter,
            },
            entityTypeIdentifier: EQUIPMENT.ENTITY_TYPE_IDENTIFIER,
        })
        .then(({content}) => formatSearchEquipmentItems(content))
        .then((content) => {
            dispatch(getSearchItemsSuccessAction(content));
        })
        .catch((error) => {
            dispatch(getSearchItemsErrorAction(error));
        });
};

/* GET SEARCH ITEMS (ORBITRACKER) */
export const formatSearchOrbitrackerItems = (content) => {
    return content.map((item) => {
        const {identifier} = item;
        return {visible: orbitrackerTitle(identifier)};
    });
};

export const getSearchOrbitrackerItemsCompleteApiCall = (filter = '') => (dispatch) => {
    dispatch(getSearchItemsRequestAction());

    ValueContainerFetchService
        .generalSearch({
            filter,
            field: ORBITRACKER.FIELD, 
            entityTypeIdentifier: ORBITRACKER.ENTITY_TYPE_IDENTIFIER, 
        })
        .then(({content}) => formatSearchOrbitrackerItems(content))
        .then((content) => {
            dispatch(getSearchItemsSuccessAction(content));
        })
        .catch((error) => {
            dispatch(getSearchItemsErrorAction(error));
        });
};

/* GET SEARCH ITEMS (DETAILED EQUIPMENT MAP) */

/* FORMAT SEARCH ITEMS (DETAILED EQUIPMENT MAP) */
function formatSearchDetailedMapItems(content){
    return content.map((item) => {
        const {
            type, 
            tracker,
            identifier,
        } = item;

        const {
            latitude,
            longitude,
        } = tracker;

        const equipment = equipmentTitle(identifier);
        const orbitracker = orbitrackerTitle(tracker.identifier);
        
        return {
            data: {
                latitude,
                longitude,
            },
            visible: `${type || '-'}/${equipment} ${orbitracker}`, 
        };
    });
}

/* GET LOCATION FORMAT SEARCH ITEMS (PLACES) */
function formatSearchDetailedMapPlaces(places, dispatch){
    return Promise.mapSeries(places, (place) => {
        const {
            place_id,
            description,
        } = place;
        
        return GoogleMapFetchService
            .getPlaceLocation(place_id)
            .then(({latitude, longitude}) => ({
                data: {
                    latitude,
                    longitude,
                },
                visible: description,
            }))
            .catch((error) => {
                error.title = i18nModule.t('GOOGLE_MAP');
                dispatch(getDeviceLastPositionErrorAction(error));
            });
    });
}

/* API CALL */
export const getSearchDetailedMapItemsCompleteApiCall = (filter = '') => (dispatch) => {
    dispatch(getSearchItemsRequestAction());

    ValueContainerFetchService
        .complexSearch({
            queries: {
                type: filter,
                identifier: filter,
                'tracker.identifier': filter,
            },
            filters: {
                'tracker.domain_id': DOMAIN_ID,
            },
            entityTypeIdentifier: EQUIPMENT.ENTITY_TYPE_IDENTIFIER,
        })
        .then(({content}) => {
            if(content.length > 0){
                return formatSearchDetailedMapItems(content);
            }

            return GoogleMapFetchService
                .getPlaces(filter)
                .then((places) => formatSearchDetailedMapPlaces(places, dispatch));
        })
        .then((content) => {
            dispatch(getSearchItemsSuccessAction(content));
        })
        .catch((error) => {
            dispatch(getSearchItemsErrorAction(error));
        });
};
