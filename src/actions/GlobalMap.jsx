/* GENERAL */
import Promise from 'bluebird';

/* ACTION TYPES */
import {
    /* GET CLIENT MAP EQUIPMENT ITEMS */
    GET_CLIENT_MAP_EQUIPMENT_ITEMS_ERROR_API_CALL,
    GET_CLIENT_MAP_EQUIPMENT_ITEMS_REQUEST_API_CALL,
    GET_CLIENT_MAP_EQUIPMENT_ITEMS_SUCCESS_API_CALL,

    /* RESET DATA ON THE GLOBAL MAP PAGE */
    RESET_GLOBAL_MAP_CALL,

    /* CHANGE MAP CENTER AFTER SELECTING MARKER BY SEARCH */
    CHANGE_MAP_CENTER_CALL,
} from '../constants/Map';

/* FETCH SERVICES */
import MeasurementFetchService from '../services/Measurement';
import ValueContainerFetchService from '../services/ValueContainer';

/* UTILS */
import {moment} from '../utils/i18nModule';
import UserMetaService from '../utils/userMetaService';

/* ACTION CREATORS */
import {getDeviceLastPositionErrorAction} from './DeviceLastPosition';

/* CONSTANTS */
import {
    DOMAIN_ID,
    SEARCH_META,
    ITEMS_MAX_COUNT_PER_API,
} from '../constants/General';

const {EQUIPMENT} = SEARCH_META;
const {ENTITY_TYPE_IDENTIFIER} = EQUIPMENT;

/* ACTIONS */
/* GET CLIENT MAP EQUIPMENT ITEMS */
/* ACTION CREATORS */
export const getClientMapEquipmentItemsRequestAction = () => {
    return {
        type: GET_CLIENT_MAP_EQUIPMENT_ITEMS_REQUEST_API_CALL,
    };
};

export const getClientMapEquipmentItemsSuccessAction = (payload) => {
    return {
        payload,
        type: GET_CLIENT_MAP_EQUIPMENT_ITEMS_SUCCESS_API_CALL,
    };
};

export const getClientMapEquipmentItemsErrorAction = (error) => {
    return {
        error,
        type: GET_CLIENT_MAP_EQUIPMENT_ITEMS_ERROR_API_CALL,
    };
};

/* ADD LAST CLIENT EQUIPMENT POSITION & TIME AND FORMAT ITEMS */
function formatClientMapEquipmentItems(content, dispatch) {
    return Promise.map(content, (equipment) => {
        const {tracker} = equipment;
        const {identifier} = tracker;
        equipment.lastmeasure = tracker.last_measure_date;

        return MeasurementFetchService
            .getLastPosition(
                identifier,
                ['longitude', 'latitude']
            ).then(({content}) => {

                if(content.length < 1) return;

                const {
                    values,
                    timestamp,
                } = content[0];

                const {
                    latitude,
                    longitude,
                } = values;

                const isValidPosition = (parseFloat(latitude) && parseFloat(longitude));
                
                if(isValidPosition){
                    tracker.latitude = latitude;
                    tracker.longitude = longitude;
                }

                if(parseFloat(tracker.latitude) && parseFloat(tracker.longitude) && timestamp) {
                    equipment.lastmeasure = moment(timestamp);
                }
            })
            .catch((error) => {
                dispatch(getDeviceLastPositionErrorAction(error));
            })
            .then(() => equipment);
    });
}

/* API CALL */
export const clientMapEquipmentItemsCompleteApiCall = () => (dispatch) => {
    dispatch(getClientMapEquipmentItemsRequestAction());

    return ValueContainerFetchService
        .generalSearch({
            filter: DOMAIN_ID,
            field: 'tracker.domain_id',
            size: ITEMS_MAX_COUNT_PER_API,
            entityTypeIdentifier: ENTITY_TYPE_IDENTIFIER,
        })
        .then(({content}) => formatClientMapEquipmentItems(content, dispatch))
        .then((content) => {
            dispatch(getClientMapEquipmentItemsSuccessAction({content}));
        })
        .catch((error) => {
            dispatch(getClientMapEquipmentItemsErrorAction(error));
        });
};

/* RESET GLOBAL MAP */
export const resetGlobalMapAction = () => {
    return {
        type: RESET_GLOBAL_MAP_CALL,
    };
};

export const resetGlobalMapCall = () => (dispatch) => {
    dispatch(resetGlobalMapAction());
};

/* CHANGE MAP CENTER AFTER SELECTING MARKER BY SEARCH */
export const changeMapCenterAction = (latitude, longitude) => {
    return {
        payload: {
            latitude,
            longitude,
        },
        type: CHANGE_MAP_CENTER_CALL,
    };
};

export const changeMapCenterCall = (identifier, location) => (dispatch) => {
    if(location){
        const {latitude, longitude} = location;
        UserMetaService.updateLocation(identifier, {
            lat: latitude,
            lng: longitude,
        });

        dispatch(changeMapCenterAction(parseFloat(latitude), parseFloat(longitude)));
        return;
    }

    const {lat, lng} = UserMetaService.getLocation(identifier);
    dispatch(changeMapCenterAction(parseFloat(lat), parseFloat(lng)));
};
