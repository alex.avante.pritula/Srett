/* GENERAL */
import {Record} from 'immutable';

/* CONSTANTS */
import {DOMAIN_ID} from '../constants/General';

export const Orbitracker = Record({
    rssi: '', 
    status: '',
    reboots: '',
    battery: '', 
    firmware: '',
    latitude: '', 
    longitude: '', 
    timetofix: '', 
    equipment: '',
    identifier: '',
    lastmeasure: '',
    equipmentRef: '',
    reccordindex: '',

    wakeUpMode: null,
    lastWakeUpMode: null,

    domainId: DOMAIN_ID,
});
