/* GENERAL */
import {Record} from 'immutable';

/* IMMUTABLES */
import {Orbitracker} from './Orbitracker';

/* CONSTANTS */
import {DOMAIN_ID} from '../constants/General';

export const Equipment = Record({
    type: '',
    status: '',
    reference: '',
    identifier: '',
    lastmeasure: '',

    domainId: DOMAIN_ID,
    tracker: new Orbitracker(),
});
