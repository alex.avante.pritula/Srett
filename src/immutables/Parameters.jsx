/* GENERAL */
import {Record} from 'immutable';

/* CONSTANTS */
import {DEFAULT_PARAMETERS_SETTINGS} from '../constants/General';

export const Parameters = Record(DEFAULT_PARAMETERS_SETTINGS);