
/* GENERAL */
import {Record} from 'immutable';

/* UTILS */
import {moment} from '../utils/i18nModule';

/* CONSTANTS */
import {DOMAIN_ID} from '../constants/General';

export const Process = Record({
    status: '',
    device: '',
    identifier: '',
    processType: '',

    payload: {},

    domain_id: DOMAIN_ID,
    creationDate: moment(),
    updationDate: moment(),
});
