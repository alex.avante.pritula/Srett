/* GENERAL */
import thunkMiddleware from 'redux-thunk';
import {routerMiddleware} from 'react-router-redux';
import {createStore, compose, applyMiddleware} from 'redux';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';

/* REDUCERS */
import rootReducer from './reducers';

/* CONSTANTS */
import {NODE_ENV} from './constants/General';

/* UTILS */
import logger from 'redux-logger';

const {PROD} = NODE_ENV;

export default function configureStore(initialState) {

    const middewares = [
        thunkMiddleware,
    ];

    if (process.env.NODE_ENV === PROD) {
        return createStore(rootReducer, initialState, compose(
                applyMiddleware(...middewares)
            )
        );
    }

    middewares.push(reduxImmutableStateInvariant(), logger);

    const store = createStore(rootReducer, initialState, compose(
            applyMiddleware(...middewares),
            window.devToolsExtension ? window.devToolsExtension() : f => f 
        )
    );

    if (module.hot) {
        module.hot.accept('../reducers', () => {
            const nextReducer = require('../reducers').default;
            store.replaceReducer(nextReducer);
        });
    }

    return store;
}
