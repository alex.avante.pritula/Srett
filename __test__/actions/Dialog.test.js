/* ACTIONS */
import {
    /* DIALOG NAVIGATION */
    dialogOpenAction,
    dialogCloseAction,
} from '../../src/actions/Dialog';

/* CONSTANTS */
import {
    TESTING,
    ACTION_CREATOR,
} from '../constants';

/* DIALOG NAVIGATION */
describe(`${ACTION_CREATOR} DIALOG NAVIGATION OPEN`, () => {
    it(TESTING, () => {
        const result = dialogOpenAction({customHeader: 'customHeader'});
        expect(result).toEqual({
            type: 'DIALOG_OPEN_CALL',
            payload: {customHeader: 'customHeader'},
        });
    });
});

describe(`${ACTION_CREATOR} DIALOG NAVIGATION CLOSE`, () => {
    it(TESTING, () => {
        const result = dialogCloseAction();
        expect(result).toEqual({type: 'DIALOG_CLOSE_CALL'});
    });
});
