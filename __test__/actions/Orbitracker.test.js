/* ACTIONS */
import {
    /* GET ORBITRACKER ITEMS */
    getOrbitrackerItemsErrorAction,
    getOrbitrackerItemsRequestAction,
    getOrbitrackerItemsSuccessAction,

    /* RESET ORBITRACKER DATA */
    resetOrbitrackersGridAction,

    /* GET ORBITRACKER BY IDENTIFIER */
    getOrbitrackerByIdentifierErrorAction,
    getOrbitrackerByIdentifierRequestAction,
    getOrbitrackerByIdentifierSuccessAction,
} from '../../src/actions/Orbitracker';

/* CONSTANTS */
import {
    TESTING,
    ACTION_CREATOR,
} from '../constants';

/* GET ORBITRACKER ITEMS */
describe(`${ACTION_CREATOR} GET ORBITRACKER ITEMS REQUEST`, () => {
    it(TESTING, () => {
        const result = getOrbitrackerItemsRequestAction();
        expect(result).toEqual({type: 'GET_ORBITRACKER_ITEMS_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} GET ORBITRACKER ITEMS ERROR`, () => {
    it(TESTING, () => {
        const result = getOrbitrackerItemsErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'GET_ORBITRACKER_ITEMS_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} GET ORBITRACKER ITEMS SUCCESS`, () => {
    it(TESTING, () => {
        const result = getOrbitrackerItemsSuccessAction({content: {}});
        expect(result).toEqual({
            payload: {content: {}},
            type: 'GET_ORBITRACKER_ITEMS_SUCCESS_API_CALL',
        });
    });
});

/* RESET ORBITRACKER DATA */
describe(`${ACTION_CREATOR} RESET ORBITRACKER DATA`, () => {
    it(TESTING, () => {
        const result = resetOrbitrackersGridAction();
        expect(result).toEqual({type: 'RESET_ORBITRACKERS_GRID_CALL'});
    });
});

/* GET ORBITRACKER BY IDENTIFIER */
describe(`${ACTION_CREATOR}  GET ORBITRACKER BY IDENTIFIER REQUEST`, () => {
    it(TESTING, () => {
        const result = getOrbitrackerByIdentifierRequestAction();
        expect(result).toEqual({type: 'GET_ORBITRACKER_BY_IDENTIFIER_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} GET ORBITRACKER BY IDENTIFIER ERROR`, () => {
    it(TESTING, () => {
        const result = getOrbitrackerByIdentifierErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'GET_ORBITRACKER_BY_IDENTIFIER_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} GET ORBITRACKER BY IDENTIFIER SUCCESS`, () => {
    it(TESTING, () => {
        const result = getOrbitrackerByIdentifierSuccessAction({});
        expect(result).toEqual({
            payload: {content: {}},
            type: 'GET_ORBITRACKER_BY_IDENTIFIER_SUCCESS_API_CALL',
        });
    });
});
