/* ACTIONS */
import {
    /* GET EQUIPMENT POSITIONS BY TIME RANGE */
    getEquipmentPositionsByTimeRangeErrorAction,
    getEquipmentPositionsByTimeRangeRequestAction,
    getEquipmentPositionsByTimeRangeSuccessAction,

    /* EQUIPMENT POSITIONS GRID CLIENT PAGINATION */
    onEquipmentPositionsGridPageChangedAction,
    onEquipmentPositionsGridSortChangedAction,
} from '../../src/actions/EquipmentPositions';

/* CONSTANTS */
import {
    TESTING,
    ACTION_CREATOR,
} from '../constants';

import {SORT_ORDER} from '../../src/constants/General';
const {ASC} = SORT_ORDER;

/* GET EQUIPMENT POSITIONS BY TIME RANGE  */
describe(`${ACTION_CREATOR} GET EQUIPMENT POSITIONS BY TIME RANGE REQUEST`, () => {
    it(TESTING, () => {
        const result = getEquipmentPositionsByTimeRangeRequestAction();
        expect(result).toEqual({type: 'GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} GET EQUIPMENT POSITIONS BY TIME RANGE ERROR`, () => {
    it(TESTING, () => {
        const result = getEquipmentPositionsByTimeRangeErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} GET EQUIPMENT POSITIONS BY TIME RANGE SUCCESS`, () => {
    it(TESTING, () => {
        const result = getEquipmentPositionsByTimeRangeSuccessAction({content: {}});
        expect(result).toEqual({
            payload: {content: {}},
            type: 'GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_SUCCESS_API_CALL',
        });
    });
});

/* EQUIPMENT POSITIONS GRID CLIENT PAGINATION */
describe(`${ACTION_CREATOR} EQUIPMENT POSITIONS GRID CLIENT PAGINATION PAGE CHANGED`, () => {
    it(TESTING, () => {
        const result = onEquipmentPositionsGridPageChangedAction(50);
        expect(result).toEqual({
            payload: {page: 50},
            type: 'ON_EQUIPMENT_POSITIONS_GRID_PAGE_CHANGED_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} EQUIPMENT POSITIONS GRID CLIENT PAGINATION SORT CHANGED`, () => {
    it(TESTING, () => {
        const result = onEquipmentPositionsGridSortChangedAction('testField', ASC);
        expect(result).toEqual({
            payload: {
                sortField: 'testField', 
                sortOrder: 'asc',
            },
            type: 'ON_EQUIPMENT_POSITIONS_GRID_SORT_CHANGED_CALL',
        });
    });
});