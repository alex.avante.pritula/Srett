/* ACTIONS */
import {
    /* GET SEARCH ITEMS */
    getSearchItemsErrorAction,
    getSearchItemsRequestAction,
    getSearchItemsSuccessAction,

    /* RESET SEARCH_ITEMS */
    resetSearchItemsAction,
} from '../../src/actions/Search';

/* CONSTANTS */
import {
    TESTING,
    ACTION_CREATOR,
} from '../constants';

/* RESET SEARCH_ITEMS */
describe(`${ACTION_CREATOR} GET ORBITRACKER ITEMS REQUEST`, () => {
    it(TESTING, () => {
        const result = resetSearchItemsAction();
        expect(result).toEqual({type: 'RESET_SEARCH_DATA_CALL'});
    });
});

/* GET SEARCH ITEMS */
describe(`${ACTION_CREATOR} GET SEARCH ITEMS REQUEST`, () => {
    it(TESTING, () => {
        const result = getSearchItemsRequestAction();
        expect(result).toEqual({type: 'GET_SEARCH_ITEMS_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} GET SEARCH ITEMS ERROR`, () => {
    it(TESTING, () => {
        const result = getSearchItemsErrorAction();
        expect(result).toEqual({type: 'GET_SEARCH_ITEMS_ERROR_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} GET SEARCH ITEMS SUCCESS`, () => {
    it(TESTING, () => {
        const result = getSearchItemsSuccessAction();
        expect(result).toEqual({type: 'GET_SEARCH_ITEMS_SUCCESS_API_CALL'});
    });
});
