/* ACTIONS */
import {
    /* RESET AUTOCOMPLETE DATA */
    resetAutocompleteItemsAction,

    /* GET AUTOCOMPLETE ITEMS (ALL EXIST UNASSOCIATED DEVICES) */
    getAutocompleteItemsErrorAction,
    getAutocompleteItemsRequestAction,
    getAutocompleteItemsSuccessAction,
} from '../../src/actions/Autocomplete';

/* CONSTANTS */
import {
    TESTING,
    ACTION_CREATOR,
} from '../constants';

/* RESET AUTOCOMPLETE DATA */
describe(`${ACTION_CREATOR} RESET AUTOCOMPLETE ITEMS`, () => {
    it(TESTING, () => {
        const result = resetAutocompleteItemsAction();
        expect(result).toEqual({type: 'RESET_AUTOCOMPLETE_DATA_CALL'});
    });
});

/* GET AUTOCOMPLETE ITEMS (ALL EXIST UNASSOCIATED DEVICES) */
describe(`${ACTION_CREATOR} GET AUTOCOMPLETE ITEMS REQUEST`, () => {
    it(TESTING, () => {
        const result = getAutocompleteItemsRequestAction();
        expect(result).toEqual({type: 'GET_AUTOCOMPLETE_ITEMS_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} GET AUTOCOMPLETE ITEMS ERROR`, () => {
    it(TESTING, () => {
        const result = getAutocompleteItemsErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'GET_AUTOCOMPLETE_ITEMS_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} GET AUTOCOMPLETE ITEMS SUCCESS`, () => {
    it(TESTING, () => {
        const result = getAutocompleteItemsSuccessAction([]);
        expect(result).toEqual({ 
            payload: {'items': []},
            type: 'GET_AUTOCOMPLETE_ITEMS_SUCCESS_API_CALL', 
        });
    });
});
