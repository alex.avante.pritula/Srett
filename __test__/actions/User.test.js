/* ACTIONS */
import {
    /* SIGN IN */
    signInErrorAction,
    signInRequestAction,
    signInSuccessAction,
    
    /* GET USER INFO */
    getUserDataErrorAction,
    getUserDataSuccessAction,
    getUserDataRequestAction,

    /* UPDATE PASSWORD */
    updateUserPasswordErrorAction,
    updateUserPasswordSuccessAction,
    updateUserPasswordRequestAction,

    /* SIGN OUT  */
    signOutAction,

    /* CHANGE LANGUAGE */
    changeLanguageAction,

    /* TOOGLE MINIMIZE MENU */
    toogleMinimizeMenuAction,
} from '../../src/actions/User';

/* CONSTANTS */
import {
    TESTING,
    ACTION_CREATOR,
} from '../constants';

import {LOCALIZATION} from '../../src/constants/General';
const {EN, FR} = LOCALIZATION;


/* SIGN IN */
describe(`${ACTION_CREATOR} SIGN IN REQUEST`, () => {
    it(TESTING, () => {
        const result = signInRequestAction();
        expect(result).toEqual({type: 'SIGN_IN_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} SIGN IN ERROR`, () => {
    it(TESTING, () => {
        const result = signInErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'SIGN_IN_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} SIGN IN SUCCESS`, () => {
    it(TESTING, () => {
        const result = signInSuccessAction();
        expect(result).toEqual({type: 'SIGN_IN_SUCCESS_API_CALL'});
    });
});

/* SIGN OUT  */
describe(`${ACTION_CREATOR} SIGN OUT`, () => {
    it(TESTING, () => {
        const result = signOutAction();
        expect(result).toEqual({type: 'SIGN_OUT_CALL'});
    });
});

/* UPDATE PASSWORD */
describe(`${ACTION_CREATOR} UPDATE PASSWORD REQUEST`, () => {
    it(TESTING, () => {
        const result = updateUserPasswordRequestAction();
        expect(result).toEqual({type: 'UPDATE_PASSWORD_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} UPDATE PASSWORD ERROR`, () => {
    it(TESTING, () => {
        const result = updateUserPasswordErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'UPDATE_PASSWORD_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} UPDATE PASSWORD SUCCESS`, () => {
    it(TESTING, () => {
        const result = updateUserPasswordSuccessAction();
        expect(result).toEqual({type: 'UPDATE_PASSWORD_SUCCESS_API_CALL'});
    });
});

/*  GET USER INFO */
describe(`${ACTION_CREATOR} GET USER INFO REQUEST`, () => {
    it(TESTING, () => {
        const result = getUserDataRequestAction();
        expect(result).toEqual({type: 'GET_USER_INFO_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} GET USER INFO ERROR`, () => {
    it(TESTING, () => {
        const result = getUserDataErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'GET_USER_INFO_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} GET USER INFO SUCCESS`, () => {
    it(TESTING, () => {
        const result = getUserDataSuccessAction({user: {}});
        expect(result).toEqual({
            payload: {user: {}},
            type: 'GET_USER_INFO_SUCCESS_API_CALL',
        });
    });
});

/* CHANGE LANGUAGE */
describe(`${ACTION_CREATOR} CHANGE LANGUAGE ENGLISH`, () => {
    it(TESTING, () => {
        const result = changeLanguageAction(EN);
        expect(result).toEqual({
            payload: {language: 'en'},
            type: 'CHANGE_LANGUAGE_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} CHANGE LANGUAGE FRENCH`, () => {
    it(TESTING, () => {
        const result = changeLanguageAction(FR);
        expect(result).toEqual({
            payload: {language: 'fr'},
            type: 'CHANGE_LANGUAGE_CALL',
        });
    });
});

/* TOOGLE MINIMIZE MENU */
describe(`${ACTION_CREATOR} TOOGLE MINIMIZE MENU`, () => {
    it(TESTING, () => {
        const result = toogleMinimizeMenuAction(true);
        expect(result).toEqual({
            payload: {minimizedMenu: true},
            type: 'TOOGLE_MINIMIZE_MENU_CALL',
        });
    });
});