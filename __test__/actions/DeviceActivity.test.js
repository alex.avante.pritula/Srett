/* ACTIONS */
import {
    /* GET DEVICE ACTIVITY ITEMS */
    getDeviceActivityItemsErrorAction,
    getDeviceActivityItemsRequestAction,
    getDeviceActivityItemsSuccessAction,

    /* RESET DEVICE ACTIVITY DATA */
    resetDeviceActivityGridAction,
} from '../../src/actions/DeviceActivity';

/* CONSTANTS */
import {
    TESTING,
    ACTION_CREATOR,
} from '../constants';

/* RESET DEVICE ACTIVITY DATA */
describe(`${ACTION_CREATOR} RESET DEVICE ACTIVITY DATA`, () => {
    it(TESTING, () => {
        const result = resetDeviceActivityGridAction();
        expect(result).toEqual({type: 'RESET_DEVICE_ACTIVITY_GRID_CALL'});
    });
});

/* GET DEVICE ACTIVITY ITEMS */
describe(`${ACTION_CREATOR} GET DEVICE ACTIVITY ITEMS REQUEST`, () => {
    it(TESTING, () => {
        const result = getDeviceActivityItemsRequestAction();
        expect(result).toEqual({type: 'GET_DEVICE_ACTIVITY_ITEMS_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} GET DEVICE ACTIVITY ITEMS ERROR`, () => {
    it(TESTING, () => {
        const result = getDeviceActivityItemsErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'GET_DEVICE_ACTIVITY_ITEMS_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} GET DEVICE ACTIVITY ITEMS SUCCESS`, () => {
    it(TESTING, () => {
        const result = getDeviceActivityItemsSuccessAction({content: []});
        expect(result).toEqual({
            payload: {content: []},
            type: 'GET_DEVICE_ACTIVITY_ITEMS_SUCCESS_API_CALL',
        });
    });
});