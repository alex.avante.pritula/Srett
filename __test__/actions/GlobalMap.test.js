/* ACTIONS */
import {
    /* GET CLIENT MAP EQUIPMENT ITEMS */
    getClientMapEquipmentItemsErrorAction,
    getClientMapEquipmentItemsSuccessAction,
    getClientMapEquipmentItemsRequestAction,

    /* RESET DATA ON THE GLOBAL MAP PAGE */
    resetGlobalMapAction,

    /* CHANGE MAP CENTER AFTER SELECTING MARKER BY SEARCH */
    changeMapCenterAction,
} from '../../src/actions/GlobalMap';

/* CONSTANTS */
import {
    TESTING,
    ACTION_CREATOR,
} from '../constants';

/* GET CLIENT MAP EQUIPMENT ITEMS */
describe(`${ACTION_CREATOR}  GET CLIENT MAP EQUIPMENT ITEMS REQUEST`, () => {
    it(TESTING, () => {
        const result = getClientMapEquipmentItemsRequestAction();
        expect(result).toEqual({type: 'GET_CLIENT_MAP_EQUIPMENT_ITEMS_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR}  GET CLIENT MAP EQUIPMENT ITEMS ERROR`, () => {
    it(TESTING, () => {
        const result = getClientMapEquipmentItemsErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'GET_CLIENT_MAP_EQUIPMENT_ITEMS_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR}  GET CLIENT MAP EQUIPMENT ITEMS SUCCESS`, () => {
    it(TESTING, () => {
        const result = getClientMapEquipmentItemsSuccessAction({content: {}});
        expect(result).toEqual({
            payload: {content: {}},
            type: 'GET_CLIENT_MAP_EQUIPMENT_ITEMS_SUCCESS_API_CALL',
        });
    });
});

/* RESET DATA ON THE GLOBAL MAP PAGE */
describe(`${ACTION_CREATOR} RESET DATA ON THE GLOBAL MAP PAGE`, () => {
    it(TESTING, () => {
        const result = resetGlobalMapAction();
        expect(result).toEqual({type: 'RESET_GLOBAL_MAP_CALL'});
    });
});

/* CHANGE MAP CENTER AFTER SELECTING MARKER BY SEARCH */
describe(`${ACTION_CREATOR} CHANGE MAP CENTER AFTER SELECTING MARKER BY SEARCH`, () => {
    it(TESTING, () => {
        const result = changeMapCenterAction(23, 54);
        expect(result).toEqual({
            payload: {
                latitude: 23,
                longitude: 54,
            },
            type: 'CHANGE_MAP_CENTER_CALL',
        });
    });
});