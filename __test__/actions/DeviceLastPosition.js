/* ACTIONS */
import {
    /* DEVICE LAST MEASUREMENT POSITION ERROR */
    getDeviceLastPositionErrorAction,
} from '../../src/actions/DeviceLastPosition';

/* CONSTANTS */
import {
    TESTING,
    ACTION_CREATOR,
} from '../constants';

/* DEVICE LAST MEASUREMENT POSITION ERROR */
describe(`${ACTION_CREATOR} DEVICE LAST MEASUREMENT POSITION ERROR`, () => {
    it(TESTING, () => {
        const result = getDeviceLastPositionErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'GET_DEVICE_LAST_POSITION_ERROR_API_CALL',
        });
    });
});
