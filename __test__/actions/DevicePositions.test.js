/* ACTIONS */
import {
    /* GET DEVICE POSITIONS BY TIME RANGE */
    getDevicePositionsByTimeRangeErrorAction,
    getDevicePositionsByTimeRangeSuccessAction,
    getDevicePositionsByTimeRangeRequestAction,

    /* DEVICE POSITIONS GRID CLIENT PAGINATION */
    onDevicePositionsGridPageChangedAction,
    onDevicePositionsGridSortChangedAction, 
} from '../../src/actions/DevicePositions';

/* CONSTANTS */
import {
    TESTING,
    ACTION_CREATOR,
} from '../constants';

import {SORT_ORDER} from '../../src/constants/General';
const {DESC} = SORT_ORDER;

/* GET DEVICE POSITIONS BY TIME RANGE */
describe(`${ACTION_CREATOR} GET DEVICE POSITIONS BY TIME RANGE REQUEST`, () => {
    it(TESTING, () => {
        const result = getDevicePositionsByTimeRangeRequestAction();
        expect(result).toEqual({type: 'GET_DEVICE_POSITIONS_BY_TIME_RANGE_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} GET DEVICE POSITIONS BY TIME RANGE ERROR`, () => {
    it(TESTING, () => {
        const result = getDevicePositionsByTimeRangeErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'GET_DEVICE_POSITIONS_BY_TIME_RANGE_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} GET DEVICE POSITIONS BY TIME RANGE SUCCESS`, () => {
    it(TESTING, () => {
        const result = getDevicePositionsByTimeRangeSuccessAction({content: []});
        expect(result).toEqual({
            payload: {content: []},
            type: 'GET_DEVICE_POSITIONS_BY_TIME_RANGE_SUCCESS_API_CALL',
        });
    });
});

/* DEVICE POSITIONS GRID CLIENT PAGINATION */
describe(`${ACTION_CREATOR} DEVICE POSITIONS GRID CLIENT PAGINATION PAGE CHANGED`, () => {
    it(TESTING, () => {
        const result = onDevicePositionsGridPageChangedAction(50);
        expect(result).toEqual({
            payload: {page: 50},
            type: 'ON_DEVICE_POSITIONS_GRID_PAGE_CHANGED_CALL'
        });
    });
});

describe(`${ACTION_CREATOR} DEVICE POSITIONS GRID CLIENT PAGINATION SORT CHANGED`, () => {
    it(TESTING, () => {
        const result = onDevicePositionsGridSortChangedAction('testField', DESC);
        expect(result).toEqual({
            payload: {
                sortField: 'testField', 
                sortOrder: 'desc',
            },
            type: 'ON_DEVICE_POSITIONS_GRID_SORT_CHANGED_CALL'
        });
    });
});
