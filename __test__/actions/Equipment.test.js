/* ACTIONS */
import {
    /* GET CLIENT EQUIPMENTS ITEMS */
    getClientEquipmentItemsErrorAction,
    getClientEquipmentItemsRequestAction,
    getClientEquipmentItemsSuccessAction,
    
    /* RESET CLIENT EQUIPMENT DATA */
    resetClientEquipmentsGridAction,

    /* CREATE NEW EQUIPMENT */
    createNewClientEquipmentErrorAction,
    createNewClientEquipmentRequestAction,
    createNewClientEquipmentSuccessAction,

    /* ASSOCIATE CLIENT EQUIPMENT TO ORBITRACKER DEVICE */
    associateClientEquipmentToOrbitrackerErrorAction,
    associateClientEquipmentToOrbitrackerRequestAction,
    associateClientEquipmentToOrbitrackerSuccessAction,

    /* GET CLIENT EQUIPMENT BY IDENTIFIER */
    getClientEquipmentByIdentifierErrorAction,
    getClientEquipmentByIdentifierRequestAction,
    getClientEquipmentByIdentifierSuccessAction,
} from '../../src/actions/Equipment';

/* CONSTANTS */
import {
    TESTING,
    ACTION_CREATOR,
} from '../constants';

/* GET CLIENT EQUIPMENTS ITEMS */
describe(`${ACTION_CREATOR} GET CLIENT EQUIPMENTS ITEMS REQUEST`, () => {
    it(TESTING, () => {
        const result = getClientEquipmentItemsRequestAction();
        expect(result).toEqual({type: 'GET_CLIENT_EQUIPMENT_ITEMS_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} GET CLIENT EQUIPMENTS ITEMS ERROR`, () => {
    it(TESTING, () => {
        const result = getClientEquipmentItemsErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'GET_CLIENT_EQUIPMENT_ITEMS_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} GET CLIENT EQUIPMENTS ITEMS SUCCESS`, () => {
    it(TESTING, () => {
        const result = getClientEquipmentItemsSuccessAction({content: []});
        expect(result).toEqual({
            payload: {content: []},
            type: 'GET_CLIENT_EQUIPMENT_ITEMS_SUCCESS_API_CALL',
        });
    });
});

/* RESET CLIENT EQUIPMENT DATA */
describe(`${ACTION_CREATOR} RESET CLIENT EQUIPMENTS GRID`, () => {
    it(TESTING, () => {
        const result = resetClientEquipmentsGridAction();
        expect(result).toEqual({type: 'RESET_CLIENT_EQUIPMENTS_GRID_CALL'});
    });
});

/* CREATE NEW EQUIPMENT */
describe(`${ACTION_CREATOR} CREATE NEW CLIENT EQUIPMENT REQUEST`, () => {
    it(TESTING, () => {
        const result = createNewClientEquipmentRequestAction();
        expect(result).toEqual({type: 'CREATE_NEW_CLIENT_EQUIPMENT_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} CREATE NEW CLIENT EQUIPMENT ERROR`, () => {
    it(TESTING, () => {
        const result = createNewClientEquipmentErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'CREATE_NEW_CLIENT_EQUIPMENT_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} CREATE NEW CLIENT EQUIPMENT SUCCESS`, () => {
    it(TESTING, () => {
        const result = createNewClientEquipmentSuccessAction();
        expect(result).toEqual({type: 'CREATE_NEW_CLIENT_EQUIPMENT_SUCCESS_API_CALL'});
    });
});


/* ASSOCIATE CLIENT EQUIPMENT TO ORBITRACKER DEVICE */
describe(`${ACTION_CREATOR} ASSOCIATE CLIENT EQUIPMENT TO ORBITRACKER DEVICE REQUEST`, () => {
    it(TESTING, () => {
        const result = associateClientEquipmentToOrbitrackerRequestAction();
        expect(result).toEqual({type: 'ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} ASSOCIATE CLIENT EQUIPMENT TO ORBITRACKER DEVICE ERROR`, () => {
    it(TESTING, () => {
        const result = associateClientEquipmentToOrbitrackerErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} ASSOCIATE CLIENT EQUIPMENT TO ORBITRACKER DEVICE SUCCESS`, () => {
    it(TESTING, () => {
        const result = associateClientEquipmentToOrbitrackerSuccessAction();
        expect(result).toEqual({type: 'ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_SUCCESS_API_CALL'});
    });
});

/* GET CLIENT EQUIPMENT BY IDENTIFIER */
describe(`${ACTION_CREATOR} GET CLIENT EQUIPMENT BY IDENTIFIER REQUEST`, () => {
    it(TESTING, () => {
        const result = getClientEquipmentByIdentifierRequestAction();
        expect(result).toEqual({type: 'GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} GET CLIENT EQUIPMENT BY IDENTIFIER ERROR`, () => {
    it(TESTING, () => {
        const result = getClientEquipmentByIdentifierErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} GET CLIENT EQUIPMENT BY IDENTIFIER SUCCESS`, () => {
    it(TESTING, () => {
        const result = getClientEquipmentByIdentifierSuccessAction({});
        expect(result).toEqual({
            payload: {content: {}},
            type: 'GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_SUCCESS_API_CALL'
        });
    });
});
