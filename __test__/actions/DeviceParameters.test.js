/* ACTIONS */
import {
    /* GET DEVICE PARAMETERS */
    getDeviceParametersErrorAction,
    getDeviceParametersSuccessAction,
    getDeviceParametersRequestAction,

    /* SAVE DEVICE PARAMETERS */
    saveDeviceParametersErrorAction,
    saveDeviceParametersRequestAction,
    saveDeviceParametersSuccessAction,

    /* SYNCHRONIZE DEVICE PARAMETERS */
    synchronizeDeviceParametersErrorAction,
    synchronizeDeviceParametersRequestAction,
    synchronizeDeviceParametersSuccessAction,

    /* REBOOT DEVICE PARAMETERS */
    rebootDeviceParametersErrorAction,
    rebootDeviceParametersRequestAction,
    rebootDeviceParametersSuccessAction,
} from '../../src/actions/DeviceParameters';

/* CONSTANTS */
import {
    TESTING,
    ACTION_CREATOR,
} from '../constants';

/* GET DEVICE PARAMETERS */
describe(`${ACTION_CREATOR} GET DEVICE PARAMETERS REQUEST`, () => {
    it(TESTING, () => {
        const result = getDeviceParametersRequestAction();
        expect(result).toEqual({type: 'GET_DEVICE_PARAMETERS_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} GET DEVICE PARAMETERS ERROR`, () => {
    it(TESTING, () => {
        const result = getDeviceParametersErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'GET_DEVICE_PARAMETERS_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} GET DEVICE PARAMETERS SUCCESS`, () => {
    it(TESTING, () => {
        const result = getDeviceParametersSuccessAction({});
        expect(result).toEqual({
            payload: {content: {}},
            type: 'GET_DEVICE_PARAMETERS_SUCCESS_API_CALL',
        });
    });
});

/* SAVE DEVICE PARAMETERS */
describe(`${ACTION_CREATOR} SAVE DEVICE PARAMETERS REQUEST`, () => {
    it(TESTING, () => {
        const result = saveDeviceParametersRequestAction();
        expect(result).toEqual({type: 'SAVE_DEVICE_PARAMETERS_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} SAVE DEVICE PARAMETERS ERROR`, () => {
    it(TESTING, () => {
        const result = saveDeviceParametersErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'SAVE_DEVICE_PARAMETERS_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} SAVE DEVICE PARAMETERS SUCCESS`, () => {
    it(TESTING, () => {
        const result = saveDeviceParametersSuccessAction();
        expect(result).toEqual({type: 'SAVE_DEVICE_PARAMETERS_SUCCESS_API_CALL'});
    });
});

/* SYNCHRONIZE DEVICE PARAMETERS */
describe(`${ACTION_CREATOR} SYNCHRONIZE DEVICE PARAMETERS REQUEST`, () => {
    it(TESTING, () => {
        const result = synchronizeDeviceParametersRequestAction();
        expect(result).toEqual({type: 'SYNCHRONIZE_DEVICE_PARAMETERS_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} SYNCHRONIZE DEVICE PARAMETERS ERROR`, () => {
    it(TESTING, () => {
        const result = synchronizeDeviceParametersErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'SYNCHRONIZE_DEVICE_PARAMETERS_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} SYNCHRONIZE DEVICE PARAMETERS SUCCESS`, () => {
    it(TESTING, () => {
        const result = synchronizeDeviceParametersSuccessAction();
        expect(result).toEqual({type: 'SYNCHRONIZE_DEVICE_PARAMETERS_SUCCESS_API_CALL'});
    });
});

/* REBOOT DEVICE PARAMETERS */
describe(`${ACTION_CREATOR} REBOOT DEVICE PARAMETERS REQUEST`, () => {
    it(TESTING, () => {
        const result = rebootDeviceParametersRequestAction();
        expect(result).toEqual({type: 'REBOOT_DEVICE_PARAMETERS_REQUEST_API_CALL'});
    });
});

describe(`${ACTION_CREATOR} REBOOT DEVICE PARAMETERS ERROR`, () => {
    it(TESTING, () => {
        const result = rebootDeviceParametersErrorAction(new Error());
        expect(result).toEqual({
            error: new Error(),
            type: 'REBOOT_DEVICE_PARAMETERS_ERROR_API_CALL',
        });
    });
});

describe(`${ACTION_CREATOR} REBOOT DEVICE PARAMETERS SUCCESS`, () => {
    it(TESTING, () => {
        const result = rebootDeviceParametersSuccessAction();
        expect(result).toEqual({type: 'REBOOT_DEVICE_PARAMETERS_SUCCESS_API_CALL'});
    });
});
