/*  SCHEMA */
import EquipmentsResponseSchema from '../response/Equipments.schema';
import AutoCompleteResponseSchema from '../response/Autocomplete.schema';

/* UTILS */
import Validate from '../../src/utils/AJVModule';
import beforAllCallBack from '../utils/beforeAllCallback';

/* FETCH SERVICES */
import ValueContainerFetchService from '../../src/services/ValueContainer';

/* CONSTANTS */
import {
    TESTING,
    FETCH_SERVICE,
    ACTION_CREATOR,
} from '../constants';

beforeAll(beforAllCallBack);

describe(`${FETCH_SERVICE} GET AUTOCOMPLETE ITEMS`, () => {
    it(TESTING, () => {
        return ValueContainerFetchService
            .getFreeDevices('')
            .then((result) => {
                expect(Validate(result, AutoCompleteResponseSchema)).toBe(true);
            })
            .catch((error) => {
                expect(error).toBeTruthy();
            });
    });
});


describe(`${FETCH_SERVICE} GET CLIENT EQUIPMENT ITEMS`, () => {
    it(`${TESTING}`, () => {
        return ValueContainerFetchService.getEquipments({page: 1})
            .then((result) => {
                expect(Validate(result, EquipmentsResponseSchema)).toBe(true);
            })
            .catch((error) => {
                expect(error).toBeTruthy();
            });
    });
});
