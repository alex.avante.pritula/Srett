
/* FETCH SERVICES */
import UserFetchService from '../../src/services/User';

class LocalStorageMock {
    constructor(){
        this.store = {};

        /* BINDED HANDLERS */
        this.clear = this.clear.bind(this);
        this.getItem = this.getItem.bind(this);
        this.setItem = this.setItem.bind(this);
        this.removeItem = this.removeItem.bind(this);
    }

    getItem(key) {
        return this.store[key];
    }

    setItem(key, value) {
        this.store[key] = value.toString();
    }

    clear() {
        this.store = {};
    }
    
    removeItem(key) {
        delete this.store[key];
    }
}

global.localStorage = new LocalStorageMock();
localStorage.setItem('access_token', 'Bearer 75cfcaba-8bb6-45bc-824a-d9f8ecdccec8');
export default localStorage;
