/* GENERAL */
import toBeType from 'jest-tobetype';

/* FETCH SERVICES */
import UserFetchService from '../../src/services/User';

/* UTILS */
import localStorage from './FakeLocalStorage';

/* CONSTANTS */
import {
    AUTH_TOKEN,
    AUTH_PREFIX,
} from '../../src/constants/General';

export default () => {
    expect.extend(toBeType);
    Object.defineProperty(window, 'localStorage', {value: localStorage});
    return UserFetchService
        .signIn(__USER__,  __PASSWORD__)
        .then((result) => {
            localStorage.setItem(AUTH_TOKEN, `${AUTH_PREFIX} ${result[AUTH_TOKEN]}`);
        });
};
