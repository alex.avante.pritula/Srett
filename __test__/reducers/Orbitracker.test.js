import * as matchers from 'jest-immutable-matchers';
import Immutable from 'immutable';

import obritrackerReducer, {
    initialState,
    sortHandler,
} from '../../src/reducers/Obritracker';

import {
    GET_ORBITRACKER_ITEMS_ERROR_API_CALL,
    GET_ORBITRACKER_ITEMS_REQUEST_API_CALL,
    GET_ORBITRACKER_ITEMS_SUCCESS_API_CALL,

    RESET_ORBITRACKERS_GRID_CALL,
} from '../../src/constants/Orbitracker';

/* UTILS */
import deviceStatusMode from '../../src/utils/deviceStatusMode';

/* CONSTANTS */
import {
    SORT_ORDER,
} from '../../src/constants/General';

const {DESC} = SORT_ORDER;

describe('Obritracker Reducer', () => {
    beforeEach(() => {
        jest.addMatchers(matchers);
    });

    it('has a default state', () => {
        expect(obritrackerReducer(initialState, {
            type: 'unexpected'
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                filter: '',
                current: 0,
                sortField: '', 
                sortOrder: '',
                totalPages: 0,                
                orbitrackers: Immutable.List(),
            })(),
        );
    });

    it('can handle GET_ORBITRACKER_ITEMS_REQUEST_API_CALL', () => {
        expect(obritrackerReducer(initialState, {
            type: GET_ORBITRACKER_ITEMS_REQUEST_API_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: true,
                filter: '',
                current: 0,
                sortField: '', 
                sortOrder: '',
                totalPages: 0,                
                orbitrackers: Immutable.List(),
            })(),
        );
    });

    it('can handle GET_ORBITRACKER_ITEMS_REQUEST_API_CALL', () => {
        expect(obritrackerReducer(initialState, {
            type: GET_ORBITRACKER_ITEMS_REQUEST_API_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: true,
                filter: '',
                current: 0,
                sortField: '', 
                sortOrder: '',
                totalPages: 0,                
                orbitrackers: Immutable.List(),
            })(),
        );
    });

    it('can handle GET_ORBITRACKER_ITEMS_SUCCESS_API_CALL', () => {

        const payload = {
            filter: '',
            number: 0,
            sortField: 'identifier',
            sortOrder: 'asc',
            totalPages: 1,
            content: [
                {
                    association: 'false',
                    battery: '0.0',
                    device_timestamp: '2017-07-18 09:57:57',
                    domain_id: 'srett',
                    equipment: '',
                    firmware_version: '32.0',
                    identifier: 'srett_device@124-8c192d7000051004',
                    latitude: '0.0',
                    longitude: '0.0',
                    reboot_counter: '-95.0',
                    reccordindex: '592.0',
                    rssi: '-53.0',
                    status: '44.0',
                    timetofix: '2.0',
                }
            ]
        };
        const {
            WAKE_UP_MODE,
            LAST_WAKE_UP_MODE,
        } = deviceStatusMode('44.0');

        expect(obritrackerReducer(initialState, {
            type: GET_ORBITRACKER_ITEMS_SUCCESS_API_CALL,
            payload,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                filter: '',
                current: 0,
                sortField: 'identifier', 
                sortOrder: 'asc',
                totalPages: 1,                
                orbitrackers: Immutable.List([
                    Immutable.Record({
                        rssi: '-53.0',
                        status: '44.0',
                        battery: '0.0',
                        latitude: '0.0',
                        longitude: '0.0',
                        timetofix: '2.0',
                        equipment: '',
                        identifier: 'srett_device@124-8c192d7000051004',
                        lastmeasure: undefined,
                        reccordindex: '592.0',
                        equipmentRef: undefined,
                        domainId: 'srett',
                        reboots: '-95.0',
                        wakeUpMode: WAKE_UP_MODE,
                        firmware: '32.0',
                        lastWakeUpMode: LAST_WAKE_UP_MODE,
                    })()
                ]),
            })(),
        );
    });

    it('can handle GET_ORBITRACKER_ITEMS_ERROR_API_CALL', () => {
        const error = new Error();
        expect(obritrackerReducer(initialState, {
            type: GET_ORBITRACKER_ITEMS_ERROR_API_CALL,
            error,
        })).toEqualImmutable(
            Immutable.Record({
                error,
                isFetching: false,
                filter: '',
                current: 0,
                sortField: '', 
                sortOrder: '',
                totalPages: 0,                
                orbitrackers: Immutable.List(),
            })(),
        );
    });

    it('can handle RESET_ORBITRACKERS_GRID_CALL', () => {
        expect(obritrackerReducer(initialState, {
            type: RESET_ORBITRACKERS_GRID_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                filter: '',
                current: 0,
                sortField: '', 
                sortOrder: '',
                totalPages: 0,                
                orbitrackers: Immutable.List(),
            })(),
        );
    });

});

describe('CUSTOM SORT HANDLER', () => {
    beforeEach(() => {
        jest.addMatchers(matchers);
    });
    const orbitrackers = Immutable.List([
        Immutable.Record({identifier: 'srett_device@124-8c192d7000051004'})(),
        Immutable.Record({identifier: 'srett_device@124-8c192d7000051005'})(),
        Immutable.Record({identifier: 'srett_device@124-8c192d7000051006'})(),
    ]);

    it('can sorting by identifier', () => {
        expect(orbitrackers.sort(sortHandler('identifier', DESC))).toEqualImmutable(
            Immutable.List([
                Immutable.Record({identifier: 'srett_device@124-8c192d7000051006'})(),
                Immutable.Record({identifier: 'srett_device@124-8c192d7000051005'})(),
                Immutable.Record({identifier: 'srett_device@124-8c192d7000051004'})(),
            ])
        );
    });

    const equipments = Immutable.List([
        Immutable.Record({equipmentRef: 'srett_device@124-8c192d7000051004'})(),
        Immutable.Record({equipmentRef: 'srett_device@124-8c192d7000051005'})(),
        Immutable.Record({equipmentRef: 'srett_device@124-8c192d7000051006'})(),
    ]);

    it('can sorting by equipmentRef', () => {
        expect(equipments.sort(sortHandler('equipmentRef', DESC))).toEqualImmutable(
            Immutable.List([
                Immutable.Record({equipmentRef: 'srett_device@124-8c192d7000051006'})(),
                Immutable.Record({equipmentRef: 'srett_device@124-8c192d7000051005'})(),
                Immutable.Record({equipmentRef: 'srett_device@124-8c192d7000051004'})(),
            ])
        );
    });
});
