import * as matchers from 'jest-immutable-matchers';
import Immutable from 'immutable';

import equipmentReducer, {initialState} from '../../src/reducers/Equipment';

/* ACTION TYPES */
import {
    /* GET CLIENT EQUIPMENT ITEMS */
    GET_CLIENT_EQUIPMENT_ITEMS_ERROR_API_CALL,
    GET_CLIENT_EQUIPMENT_ITEMS_REQUEST_API_CALL,
    GET_CLIENT_EQUIPMENT_ITEMS_SUCCESS_API_CALL,

    /* RESET CLIENT EQUIPMENT DATA */
    RESET_CLIENT_EQUIPMENTS_GRID_CALL,

    /* CREATE NEW EQUIPMENT */
    CREATE_NEW_CLIENT_EQUIPMENT_ERROR_API_CALL,
    CREATE_NEW_CLIENT_EQUIPMENT_REQUEST_API_CALL,
    CREATE_NEW_CLIENT_EQUIPMENT_SUCCESS_API_CALL,

    /* ASSOCIATE CLIENT EQUIPMENT TO ORBITRACKER DEVICE */
    ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_ERROR_API_CALL,
    ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_REQUEST_API_CALL,
    ASSOCIATE_CLIENT_EQUIPMENT_TO_ORBITRACKER_SUCCESS_API_CALL,
} from '../../src/constants/Equipment';

/* UTILS */
import deviceStatusMode from '../../src/utils/deviceStatusMode';

/* IMMUTABLES */
import {Equipment} from '../../src/immutables/Equipment';
import {Orbitracker} from '../../src/immutables/Orbitracker';

describe('Equipment Reducer', () => {
    beforeEach(() => {
        jest.addMatchers(matchers);
    });

    it('has a default state', () => {
        expect(equipmentReducer(initialState, {
            type: 'unexpected'
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isGridFetching: false,
                isModalFetching: false,

                filter: '',
                current: 0,
                sortField: '',
                sortOrder: '',
                totalPages: 0,

                equipments: Immutable.List(),
            })(),
        );
    });

    it('can handle GET_CLIENT_EQUIPMENT_ITEMS_REQUEST_API_CALL', () => {
        expect(equipmentReducer(initialState, {
            type: GET_CLIENT_EQUIPMENT_ITEMS_REQUEST_API_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isGridFetching: true,
                isModalFetching: false,

                filter: '',
                current: 0,
                sortField: '',
                sortOrder: '',
                totalPages: 0,

                equipments: Immutable.List(),
            })(),
        );
    });

    // it('can handle GET_CLIENT_EQUIPMENT_ITEMS_SUCCESS_API_CALL', () => {
    //     const payload = {
    //         filter: '',
    //         number: 0,
    //         sortField: 'type',
    //         sortOrder: 'asc',
    //         totalPages: 1,
    //         content: [
    //             {
    //                 domain_id: 'srett',
    //                 identifier: 'orbitrack_iot@equipment-478',
    //                 manufacturer: 'Ford',
    //                 reference: '4 7 8',
    //                 type: 'Container',
    //                 tracker: {
    //                     association: 'false',
    //                     battery:'88.0',
    //                     domain_id:'srett',
    //                     equipment: '###NULL-ENTITY###',
    //                     firmware_version: '19.0',
    //                     identifier: 'srett_device@124-8c192d7000051035',
    //                     last_measure_date: '2017-12-13 12:25:09',
    //                     latitude:'43.811672',
    //                     longitude:'5.0445538',
    //                     reboot_counter: '10.0',
    //                     reccordindex: '345.0',
    //                     rssi: '-57.0',
    //                     status: '45.0',
    //                     timetofix: '29.0',
    //                 }
    //             }
    //         ]
    //     };
    //     expect(equipmentReducer(initialState, {
    //         type: GET_CLIENT_EQUIPMENT_ITEMS_SUCCESS_API_CALL,
    //         payload,
    //     })).toEqualImmutable(
    //         Immutable.Record({
    //             error: null,
    //             isGridFetching: false,
    //             isModalFetching: false,

    //             filter: '',
    //             current: 0,
    //             sortField: 'type',
    //             sortOrder: 'asc',
    //             totalPages: 1,

    //             equipments: Immutable.List(),
    //         })(),
    //     );
    // });
});