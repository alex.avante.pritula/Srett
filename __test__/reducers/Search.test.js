import * as matchers from 'jest-immutable-matchers';
import Immutable from 'immutable';

import searchReducer, {initialState} from '../../src/reducers/Search';

/* ACTION TYPES */
import {
    /* GET SEARCH ITEMS */
    GET_SEARCH_ITEMS_ERROR_API_CALL,
    GET_SEARCH_ITEMS_REQUEST_API_CALL,
    GET_SEARCH_ITEMS_SUCCESS_API_CALL,

    /* RESET SEARCH_ITEMS */
    RESET_SEARCH_DATA_CALL,
} from '../../src/constants/Search';

/* RESET DATA */
import {RESET_GLOBAL_MAP_CALL} from '../../src/constants/Map';
import {RESET_ORBITRACKERS_GRID_CALL} from '../../src/constants/Orbitracker';
import {RESET_CLIENT_EQUIPMENTS_GRID_CALL} from '../../src/constants/Equipment';


describe('Search Reducer', () => {
    beforeEach(() => {
        jest.addMatchers(matchers);
    });

    it('has a default state', () => {
        expect(searchReducer(initialState, {
            type: 'unexpected'
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                items: Immutable.List(),
            })(),
        );
    });

    it('can handle RESET_SEARCH_DATA_CALL', () => {
        expect(searchReducer(initialState, {
            type: RESET_SEARCH_DATA_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                items: Immutable.List(),
            })(),
        );
    });

    it('can handle GET_SEARCH_ITEMS_REQUEST_API_CALL', () => {
        expect(searchReducer(initialState, {
            type: GET_SEARCH_ITEMS_REQUEST_API_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: true,
                items: Immutable.List(),
            })(),
        );
    });

    it('can handle GET_SEARCH_ITEMS_SUCCESS_API_CALL', () => {
        const payload = [
            {visible: "CodeCare_6032018"},
        ];
        expect(searchReducer(initialState, {
            type: GET_SEARCH_ITEMS_SUCCESS_API_CALL,
            payload,
        })).toEqualImmutable(
            Immutable.Record({
                error: undefined,
                isFetching: false,
                items: Immutable.List(),
            })(),
        );
    });

    it('can handle GET_SEARCH_ITEMS_ERROR_API_CALL', () => {
        const error = new Error();
        expect(searchReducer(initialState, {
            type: GET_SEARCH_ITEMS_ERROR_API_CALL,
            error,
        })).toEqualImmutable(
            Immutable.Record({
                error: error,
                isFetching: false,
                items: Immutable.List(),
            })(),
        );
    });

    it('can handle RESET_CLIENT_EQUIPMENTS_GRID_CALL', () => {
        expect(searchReducer(initialState, {
            type: RESET_CLIENT_EQUIPMENTS_GRID_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                items: Immutable.List(),
            })(),
        );
    });

    it('can handle RESET_ORBITRACKERS_GRID_CALL', () => {
        expect(searchReducer(initialState, {
            type: RESET_ORBITRACKERS_GRID_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                items: Immutable.List(),
            })(),
        );
    });

    it('can handle RESET_GLOBAL_MAP_CALL', () => {
        expect(searchReducer(initialState, {
            type: RESET_GLOBAL_MAP_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                items: Immutable.List(),
            })(),
        );
    });
});