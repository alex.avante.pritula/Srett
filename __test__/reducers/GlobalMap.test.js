import * as matchers from 'jest-immutable-matchers';
import Immutable from 'immutable';

import globalMapReducer, {
    initialState,
} from '../../src/reducers/GlobalMap';

import {
    GET_CLIENT_MAP_EQUIPMENT_ITEMS_ERROR_API_CALL,
    GET_CLIENT_MAP_EQUIPMENT_ITEMS_REQUEST_API_CALL,
    GET_CLIENT_MAP_EQUIPMENT_ITEMS_SUCCESS_API_CALL,

    CHANGE_MAP_CENTER_CALL,
} from '../../src/constants/Map';

/* UTILS */
import deviceStatusMode from '../../src/utils/deviceStatusMode';

/* CONSTANTS */
import {START_CONFIG} from '../../src/constants/Map';

const {LOCATION} = START_CONFIG;
const {lat, lng} = LOCATION;


describe('GlobalMap Reducer', () => {

    beforeEach(() => {
        jest.addMatchers(matchers);
    });

    it('has a default state', () => {
        expect(globalMapReducer(initialState, {
            type: 'unexpected'
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                latitude: lat, 
                longitude: lng,
                items: Immutable.List(),
            })(),
        );
    });

    it('can handle GET_CLIENT_MAP_EQUIPMENT_ITEMS_REQUEST_API_CALL', () => {
        expect(globalMapReducer(initialState, {
            type: GET_CLIENT_MAP_EQUIPMENT_ITEMS_REQUEST_API_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: true,
                latitude: lat, 
                longitude: lng,
                items: Immutable.List(),
            })(),
        );
    });

    it('can handle GET_CLIENT_MAP_EQUIPMENT_ITEMS_SUCCESS_API_CALL', () => {
        const payload = {
            content: [
                {
                    type: 'Container',
                    reference: '4 7 8',
                    lastmeasure: '',
                    tracker: {
                        rssi: '-57.0',
                        label: '',
                        status: '45.0',
                        battery: '88.0',
                        latitude: '43.811672',
                        timetofix: '29.0',
                        equipment: '###NULL-ENTITY###',
                        longitude: '5.0445538',
                        domain_id: 'srett',
                        identifier: 'srett_device@124-8c192d7000051035',
                        reccordindex: '345.0',
                    }
                }
            ],
        };

        const {
            WAKE_UP_MODE,
            LAST_WAKE_UP_MODE,
        } = deviceStatusMode('45.0');

        expect(globalMapReducer(initialState, {
            type: GET_CLIENT_MAP_EQUIPMENT_ITEMS_SUCCESS_API_CALL,
            payload,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                latitude: lat, 
                longitude: lng,
                items: Immutable.List([
                    Immutable.Record({
                        type: 'Container',
                        status: '',
                        reference: '4 7 8',
                        lastmeasure: '',
                        identifier: undefined,
                        domainId: undefined,
    
                        tracker: Immutable.Record({
                            rssi: '-57.0',
                            status: '45.0',
                            reboots: '',
                            battery: '88.0',
                            firmware: '',
                            latitude: '43.811672',
                            longitude: '5.0445538',
                            timetofix: '29.0',
                            equipment: '###NULL-ENTITY###',
                            identifier: 'srett_device@124-8c192d7000051035',
                            lastmeasure: '',
                            equipmentRef: '',
                            reccordindex: '345.0',
                            wakeUpMode: WAKE_UP_MODE,
                            lastWakeUpMode: LAST_WAKE_UP_MODE,
                            domainId: 'srett',
                        })(),
                    })()
                ]),
            })(),
        );
    });

    it('can handle GET_CLIENT_MAP_EQUIPMENT_ITEMS_ERROR_API_CALL', () => {
        const error = new Error();
        expect(globalMapReducer(initialState, {
            type: GET_CLIENT_MAP_EQUIPMENT_ITEMS_ERROR_API_CALL,
            error,
        })).toEqualImmutable(
            Immutable.Record({
                error,
                isFetching: false,
                latitude: lat, 
                longitude: lng,
                items: Immutable.List(),
            })(),
        );
    });

    it('can handle CHANGE_MAP_CENTER_CALL', () => {
        const payload = {
            latitude: '0.01',
            longitude: '0.02'
        };
        expect(globalMapReducer(initialState, {
            type: CHANGE_MAP_CENTER_CALL,
            payload,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                latitude: '0.01', 
                longitude: '0.02',
                items: Immutable.List(),
            })(),
        );
    });
});