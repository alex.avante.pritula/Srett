import * as matchers from 'jest-immutable-matchers';
import Immutable from 'immutable';

import equipmentPositionsReducer, {initialState} from '../../src/reducers/EquipmentPositions';

/* ACTION TYPES */
import {
    /* GET EQUIPMENT POSITIONS BY TIME RANGE */
    GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_ERROR_API_CALL,
    GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_SUCCESS_API_CALL,
    GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_REQUEST_API_CALL,

    /* EQUIPMENT POSITIONS GRID CLIENT PAGINATION */
    ON_EQUIPMENT_POSITIONS_GRID_SORT_CHANGED_CALL,
    ON_EQUIPMENT_POSITIONS_GRID_PAGE_CHANGED_CALL, 
} from '../../src/constants/EquipmentPositions';

/* UTILS */
import {moment} from '../../src/utils/i18nModule';

import {
    DEFAULT_DAY_RANGE,
} from '../../src/constants/General';

describe('EquipmentPositions Reducer', () => {
    beforeEach(() => {
        jest.addMatchers(matchers);
    });

    it('has a default state', () => {
        expect(equipmentPositionsReducer(initialState, {
            type: 'unexpected'
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: true,

                current: 0,
                sortField: '', 
                sortOrder: '',
                totalPages: 0,

                to: initialState.to, 
                from: initialState.from,
                
                positions: Immutable.List(),
                visibleItems: Immutable.List(),
            })(),
        );
    });

    it('can handle ON_EQUIPMENT_POSITIONS_GRID_PAGE_CHANGED_CALL', () => {
        const payload = {
            page: 2,
        };
        const {page} = payload;
        expect(equipmentPositionsReducer(initialState, {
            type: ON_EQUIPMENT_POSITIONS_GRID_PAGE_CHANGED_CALL,
            payload,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: true,
                current: page,
                sortField: '', 
                sortOrder: '',
                totalPages: 0,
                to: initialState.to, 
                from: initialState.from,                
                positions: Immutable.List(),
                visibleItems: Immutable.List(),
            })(),
        );
    });

    it('can handle ON_EQUIPMENT_POSITIONS_GRID_SORT_CHANGED_CALL', () => {
        const payload = {
            sortOrder: 'ASC',
            sortField: 'identifier'
        };
        const {sortOrder, sortField} = payload;
        expect(equipmentPositionsReducer(initialState, {
            type: ON_EQUIPMENT_POSITIONS_GRID_SORT_CHANGED_CALL,
            payload,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: true,
                current: 0,
                sortField: sortField, 
                sortOrder: sortOrder,
                totalPages: 0,
                to: initialState.to, 
                from: initialState.from,                
                positions: Immutable.List(),
                visibleItems: Immutable.List(),
            })(),
        );
    });

    it('can handle GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_REQUEST_API_CALL', () => {
        expect(equipmentPositionsReducer(initialState, {
            type: GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_REQUEST_API_CALL,
        })).toEqualImmutable(
            initialState,
        );
    });

    it('can handle GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_SUCCESS_API_CALL', () => {
        const payload = {
            to: moment(), 
            from: moment().add(-DEFAULT_DAY_RANGE, 'days'),
            content: [],
        };
        const {to, from} = payload;
        expect(equipmentPositionsReducer(initialState, {
            type: GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_SUCCESS_API_CALL,
            payload,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                current: 0,
                sortField: '', 
                sortOrder: '',
                totalPages: 0,
                to: to, 
                from: from,                
                positions: Immutable.List(),
                visibleItems: Immutable.List(),
            })(),
        );
    });

    it('can handle GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_ERROR_API_CALL', () => {
        const error = new Error();
        expect(equipmentPositionsReducer(initialState, {
            type: GET_EQUIPMENT_POSITIONS_BY_TIME_RANGE_ERROR_API_CALL,
            error,
        })).toEqualImmutable(
            Immutable.Record({
                error,
                isFetching: false,
                current: 0,
                sortField: '', 
                sortOrder: '',
                totalPages: 0,
                to: initialState.to, 
                from: initialState.from,                   
                positions: Immutable.List(),
                visibleItems: Immutable.List(),
            })(),
        );
    });
});
