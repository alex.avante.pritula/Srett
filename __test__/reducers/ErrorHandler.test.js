import * as matchers from 'jest-immutable-matchers';
import Immutable from 'immutable';

import errorHandlerReducer, {
    initialState,
} from '../../src/reducers/ErrorHandler';

describe('ErrorHandler Reducer', () => {

    beforeEach(() => {
        jest.addMatchers(matchers);
    });

    it('has a default state', () => {
        expect(errorHandlerReducer(initialState, {
            type: 'unexpected'
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                action: null,
            })(),
        );
    });

    it('can handle error', () => {
        const error = new Error();
        expect(errorHandlerReducer(initialState, {
            type: 'ANY',
            error,
        })).toEqualImmutable(
            Immutable.Record({
                error,
                action: 'ANY',
            })(),
        );
    });

    it('can return initialState', () => {
        expect(errorHandlerReducer(initialState, {
            type: 'ANY',
        })).toEqualImmutable(
            initialState,
        );
    });
});
