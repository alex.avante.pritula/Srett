import * as matchers from 'jest-immutable-matchers';
import Immutable from 'immutable';

import obritrackerInfoReducer from '../../src/reducers/ObritrackerInfo';

import {
    GET_ORBITRACKER_BY_IDENTIFIER_ERROR_API_CALL,
    GET_ORBITRACKER_BY_IDENTIFIER_REQUEST_API_CALL,
    GET_ORBITRACKER_BY_IDENTIFIER_SUCCESS_API_CALL,
} from '../../src/constants/Orbitracker';

/* UTILS */
import deviceStatusMode from '../../src/utils/deviceStatusMode';

/* IMMUTABLES */
import { Orbitracker } from '../../src/immutables/Orbitracker';

describe('ObritrackerInfo Reducer', () => {
    beforeEach(() => {
        jest.addMatchers(matchers);
    });

    it('has a default state', () => {
        expect(obritrackerInfoReducer(undefined, {
            type: 'unexpected'
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                orbitracker: new Orbitracker(),
            })(),
        );
    });

    it('can handle GET_ORBITRACKER_BY_IDENTIFIER_REQUEST_API_CALL', () => {
        expect(obritrackerInfoReducer(undefined, {
            type: GET_ORBITRACKER_BY_IDENTIFIER_REQUEST_API_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: true,
                orbitracker: new Orbitracker(),
            })(),
        );
    });

    it('can handle GET_ORBITRACKER_BY_IDENTIFIER_SUCCESS_API_CALL', () => {

        const payload = {
            content: {
                rssi: 'rssi',
                status: 'status',
                battery: 'battery', 
                latitude: 'latitude', 
                longitude: 'longitude', 
                timetofix: 'timetofix', 
                equipment: 'equipment',
                identifier: 'identifier',
                lastmeasure: 'lastmeasure',
                reccordindex: 'reccordindex',
                equipmentRef: 'equipmentRef',
                domain_id: 'domain_id',
                reboot_counter: 'reboot_counter',
                firmware_version: 'firmware_version',
            }
        };

        const {
            rssi,
            status,
            battery, 
            latitude, 
            longitude, 
            timetofix, 
            equipment,
            identifier,
            lastmeasure,
            reccordindex,
            equipmentRef,
            domain_id,
            reboot_counter,
            firmware_version,
        } = payload.content;

        const {
            WAKE_UP_MODE,
            LAST_WAKE_UP_MODE,
        } = deviceStatusMode(status);

        const orbitracker = new Orbitracker({
            rssi,
            status,
            battery, 
            latitude, 
            longitude, 
            timetofix, 
            equipment,
            identifier,
            lastmeasure,
            reccordindex,
            equipmentRef,
            domainId: domain_id,
            reboots: reboot_counter,
            wakeUpMode: WAKE_UP_MODE,
            firmware: firmware_version,
            lastWakeUpMode: LAST_WAKE_UP_MODE,
        });

        expect(obritrackerInfoReducer(undefined, {
            type: GET_ORBITRACKER_BY_IDENTIFIER_SUCCESS_API_CALL,
            payload,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                orbitracker: orbitracker,
            })(),
        );
    });

    it('can handle GET_ORBITRACKER_BY_IDENTIFIER_ERROR_API_CALL', () => {
        const error = new Error();
        expect(obritrackerInfoReducer(undefined, {
            type: GET_ORBITRACKER_BY_IDENTIFIER_ERROR_API_CALL,
            error,
        })).toEqualImmutable(
            Immutable.Record({
                error,
                isFetching: false,
                orbitracker: new Orbitracker(),
            })(),
        );
    });

});
