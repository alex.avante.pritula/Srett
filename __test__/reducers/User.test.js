import * as matchers from 'jest-immutable-matchers';
import Immutable from 'immutable';
import localStorage from '../utils/FakeLocalStorage';

import userReducer from '../../src/reducers/User';

import {
    /* SIGN IN */
    SIGN_IN_ERROR_API_CALL,
    SIGN_IN_REQUEST_API_CALL,
    SIGN_IN_SUCCESS_API_CALL,
    
    /* GET USER INFO */
    GET_USER_INFO_ERROR_API_CALL,
    GET_USER_INFO_REQUEST_API_CALL,
    GET_USER_INFO_SUCCESS_API_CALL,

    /* UPDATE PASSWORD */
    UPDATE_PASSWORD_ERROR_API_CALL,
    UPDATE_PASSWORD_REQUEST_API_CALL,
    UPDATE_PASSWORD_SUCCESS_API_CALL,
    
    /* SIGN OUT */
    SIGN_OUT_CALL,

    /* CHANGE LANGUAGE */
    CHANGE_LANGUAGE_CALL,

    /* TOOGLE MINIMIZE MENU */
    TOOGLE_MINIMIZE_MENU_CALL,
} from '../../src/constants/User';

import {
    AUTH_TOKEN,
    LOCALIZATION,
} from '../../src/constants/General';

const {EN} = LOCALIZATION;

describe('User Reducer', () => {
    beforeEach(() => {
        jest.addMatchers(matchers);
      });
    it('has a default state', () => {
        expect(userReducer(undefined, {
            type: 'unexpected'
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                loggedIn: (!!localStorage.getItem(AUTH_TOKEN)),

                login: '',
                domainId: '',
                lastName: '',
                firstName: '',
                identifier: '',
            
                language: EN,
                minimizedMenu: false,
            })(),
        );
    });

    it('can handle TOOGLE_MINIMIZE_MENU_CALL', () => {
        const payload = {
            minimizedMenu: true,
        };
        const {minimizedMenu} = payload;
        expect(userReducer(undefined, {
            type: TOOGLE_MINIMIZE_MENU_CALL,
            payload,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                loggedIn: true,

                login: '',
                domainId: '',
                lastName: '',
                firstName: '',
                identifier: '',
            
                language: EN,
                minimizedMenu,
            })(),
        );
    });

    it('can handle SIGN_IN_REQUEST_API_CALL', () => {
        expect(userReducer(undefined, {
            type: SIGN_IN_REQUEST_API_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: true,
                loggedIn: false,

                login: '',
                domainId: '',
                lastName: '',
                firstName: '',
                identifier: '',
            
                language: '',
                minimizedMenu: false,
            })(),
        );
    });

    it('can handle SIGN_IN_SUCCESS_API_CALL', () => {
        expect(userReducer(undefined, {
            type: SIGN_IN_SUCCESS_API_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                loggedIn: true,

                login: '',
                domainId: '',
                lastName: '',
                firstName: '',
                identifier: '',
            
                language: EN,
                minimizedMenu: false,
            })(),
        );
    });

    it('can handle SIGN_IN_ERROR_API_CALL', () => {
        const error = new Error();
        expect(userReducer(undefined, {
            type: SIGN_IN_ERROR_API_CALL,
            error,
        })).toEqualImmutable(
            Immutable.Record({
                error,
                isFetching: false,
                loggedIn: true,

                login: '',
                domainId: '',
                lastName: '',
                firstName: '',
                identifier: '',
            
                language: EN,
                minimizedMenu: false,
            })(),
        );
    });

    it('can handle GET_USER_INFO_REQUEST_API_CALL', () => {
        expect(userReducer(undefined, {
            type: GET_USER_INFO_REQUEST_API_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: true,
                loggedIn: true,

                login: '',
                domainId: '',
                lastName: '',
                firstName: '',
                identifier: '',
            
                language: EN,
                minimizedMenu: false,
            })(),
        );
    });

    it('can handle GET_USER_INFO_SUCCESS_API_CALL', () => {
        const payload = {
            login: 'login',
            language: 'en',
            lastName: 'lastName',
            lastName: 'lastName',
            userDomain: 'userDomain',
            identifier: 'identifier',
            minimizedMenu: true,
        };

        const {
            login,
            language,
            lastName,
            firstName,
            userDomain,
            identifier,
            minimizedMenu,
        } = payload;

        expect(userReducer(undefined, {
            type: GET_USER_INFO_SUCCESS_API_CALL,
            payload,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                loggedIn: true,

                login,
                domainId: userDomain.domainId,
                lastName,
                firstName,
                identifier,
            
                language,
                minimizedMenu,
            })(),
        );
    });

    it('can handle GET_USER_INFO_ERROR_API_CALL', () => {
        const error = new Error();
        expect(userReducer(undefined, {
            type: GET_USER_INFO_ERROR_API_CALL,
            error,
        })).toEqualImmutable(
            Immutable.Record({
                error,
                isFetching: false,
                loggedIn: true,

                login: '',
                domainId: '',
                lastName: '',
                firstName: '',
                identifier: '',
            
                language: EN,
                minimizedMenu: false,
            })(),
        );
    });

    it('can handle SIGN_OUT_CALL', () => {
        expect(userReducer(undefined, {
            type: SIGN_OUT_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                loggedIn: false,

                login: '',
                domainId: '',
                lastName: '',
                firstName: '',
                identifier: '',
            
                language: '',
                minimizedMenu: false,
            })(),
        );
    });

    it('can handle UPDATE_PASSWORD_REQUEST_API_CALL', () => {
        expect(userReducer(undefined, {
            type: UPDATE_PASSWORD_REQUEST_API_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: true,
                loggedIn: true,

                login: '',
                domainId: '',
                lastName: '',
                firstName: '',
                identifier: '',
            
                language: EN,
                minimizedMenu: false,
            })(),
        );
    });

    it('can handle UPDATE_PASSWORD_SUCCESS_API_CALL', () => {
        expect(userReducer(undefined, {
            type: UPDATE_PASSWORD_SUCCESS_API_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                loggedIn: true,

                login: '',
                domainId: '',
                lastName: '',
                firstName: '',
                identifier: '',
            
                language: EN,
                minimizedMenu: false,
            })(),
        );
    });

    it('can handle UPDATE_PASSWORD_ERROR_API_CALL', () => {
        const error = new Error();
        expect(userReducer(undefined, {
            type: UPDATE_PASSWORD_ERROR_API_CALL,
            error,
        })).toEqualImmutable(
            Immutable.Record({
                error,
                isFetching: false,
                loggedIn: true,

                login: '',
                domainId: '',
                lastName: '',
                firstName: '',
                identifier: '',
            
                language: EN,
                minimizedMenu: false,
            })(),
        );
    });

    it('can handle CHANGE_LANGUAGE_CALL', () => {
        const payload = {
            language: EN,
        };
        const {language} = payload;
        expect(userReducer(undefined, {
            type: CHANGE_LANGUAGE_CALL,
            payload,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                loggedIn: true,

                login: '',
                domainId: '',
                lastName: '',
                firstName: '',
                identifier: '',
            
                language,
                minimizedMenu: false,
            })(),
        );
    });
});