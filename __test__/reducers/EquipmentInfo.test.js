import * as matchers from 'jest-immutable-matchers';
import Immutable from 'immutable';

import equipmentInfoReducer, {initialState} from '../../src/reducers/EquipmentInfo';

/* ACTION TYPES */
import {
    GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_ERROR_API_CALL,
    GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_REQUEST_API_CALL,
    GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_SUCCESS_API_CALL,
} from '../../src/constants/Equipment';

/* UTILS */
import deviceStatusMode from '../../src/utils/deviceStatusMode';

/* IMMUTABLES */
import {Equipment} from '../../src/immutables/Equipment';
import {Orbitracker} from '../../src/immutables/Orbitracker';

describe('EquipmentInfo Reducer', () => {
    beforeEach(() => {
        jest.addMatchers(matchers);
    });

    it('has a default state', () => {
        expect(equipmentInfoReducer(initialState, {
            type: 'unexpected'
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                equipment: new Equipment(),
            })(),
        );
    });

    it('can handle GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_REQUEST_API_CALL', () => {
        expect(equipmentInfoReducer(initialState, {
            type: GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_REQUEST_API_CALL,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: true,
                equipment: new Equipment(),
            })(),
        );
    });

    it('can handle GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_SUCCESS_API_CALL', () => {
        const payload = {
            content: {
                domain_id: 'srett',
                identifier: 'orbitrack_iot@equipment-sac_fabrice',
                tracker: {
                    identifier: 'srett_device@124-8c192d7000000007',
                    reboot_counter: '21.0',
                    latitude: '48.83419',
                    association: 'true',
                    equipment: 'orbitrack_iot@equipment-sac_fabrice',
                    firmware_version: '22.0',
                    identifier: 'srett_device@124-8c192d7000000007',
                    last_measure_date: '2018-03-26 17:02:44',
                    latitude: '48.83419',
                    longitude: '2.2570565',
                    reboot_counter: '21.0',
                    reccordindex: '957.0',
                    status: '13.0',
                    timetofix: '6.0',
                },
                type: 'sac',
            }
        };
        const {
            content,
        } = payload;
        const {
            WAKE_UP_MODE,
            LAST_WAKE_UP_MODE,
        } = (content.tracker.status ? deviceStatusMode(content.tracker.status) : {});
        expect(equipmentInfoReducer(initialState, {
            type: GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_SUCCESS_API_CALL,
            payload,
        })).toEqualImmutable(
            Immutable.Record({
                error: null,
                isFetching: false,
                equipment: new Equipment({
                    type: 'sac',
                    reference: undefined,
                    lastmeasure: undefined,

                    domainId: content.domain_id,
                    identifier: content.identifier,

                    tracker: new Orbitracker({
                        rssi: undefined,
                        label: undefined,
                        status: '13.0',
                        battery: undefined,
                        latitude: '48.83419',
                        timetofix: '6.0',
                        equipment: 'orbitrack_iot@equipment-sac_fabrice',
                        longitude: '2.2570565',
                        identifier: 'srett_device@124-8c192d7000000007',
                        reccordindex: '957.0',
                        domainId: undefined,
                        wakeUpMode: WAKE_UP_MODE,
                        lastWakeUpMode: LAST_WAKE_UP_MODE,
                    }),
                }),
            })(),
        );
    });

    it('can handle GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_ERROR_API_CALL', () => {
        const error = new Error();
        expect(equipmentInfoReducer(initialState, {
            type: GET_CLIENT_EQUIPMENT_BY_IDENTIFIER_ERROR_API_CALL,
            error,
        })).toEqualImmutable(
            Immutable.Record({
                error,
                isFetching: false,
                equipment: new Equipment(),
            })(),
        );
    });
});