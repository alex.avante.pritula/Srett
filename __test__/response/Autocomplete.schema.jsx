export default {
    type: 'object',
    required: ['page', 'content'],
    properties: {
        links: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    // NOT NEEDED
                },
            },
        },
        page: {
            type: 'object',
            required: ['size', 'number', 'totalPages', 'totalElements'],
            properties: {
                size: {
                    type: 'number',
                },
                number: {
                    type: 'number',
                },
                totalPages: {
                    type: 'number',
                },
                totalElements: {
                    type: 'number',
                }
            },
        },
        content: {
            type: 'array',
            items: {
                type: 'object',
                required: ['identifier'],
                properties: {
                    identifier: {
                        type: 'string',
                        pattern: 'srett_device@124-[a-zA-Z0-9_]*',
                    },
                    rssi: {
                        type: 'string',
                    },
                    reboot_counter: {
                        type: 'string',
                    }, 
                    latitude: {
                        type: 'string',
                    },
                    _type: {
                        type: 'string',
                        pattern: 'srett_device@124',
                    },
                    association: {
                        type: 'string',
                    },
                    equipment: {
                        type: 'object',
                    }, 
                    firmware_version: {
                        type: 'string',
                    },
                    battery: {
                        type: 'string',
                    },
                    _score: {
                        type: 'number',
                    },
                    domain_id: {
                        type: 'string',
                    },
                    reccordindex: {
                        type: 'string',
                    },
                    timetofix: {
                        type: 'string',
                    },
                    longitude: {
                        type: 'string',
                    },
                    status: {
                        type: 'string',
                    },
                },
            },
        },
    },
};
