export default {
    type: 'object',
    required: ['page', 'content'],
    properties: {
        links: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    // NOT NEEDED
                },
            },
        },
        page: {
            type: 'object',
            required: ['size', 'number', 'totalPages', 'totalElements'],
            properties: {
                size: {
                    type: 'number',
                },
                number: {
                    type: 'number',
                },
                totalPages: {
                    type: 'number',
                },
                totalElements: {
                    type: 'number',
                },
            },
        },
        content: {
            type: 'array',
            items: {
                type: 'object',
                required: ['identifier'],
                switch: [
                    { 
                        if: { 
                            properties: { 
                                tracker: {
                                    type: 'null',
                                },
                            },
                        }, 
                        then: true, 
                    },
                    { 
                        if: { 
                            properties: { 
                                tracker: {
                                    type: 'object',
                                    required: ['identifier'],
                                    properties: {
                                        identifier: {
                                            type: 'string',
                                            pattern: 'orbitrack_iot@equipment-[a-zA-Z0-9_]*',
                                        },
                                        rssi: {
                                            type: 'string',
                                        },
                                        reboot_counter: {
                                            type: 'string',
                                        },
                                        latitude: {
                                            type: 'string',
                                        },
                                        association: {
                                            type: 'string',
                                        },
                                        firmware_version: {
                                            type: 'string',
                                        },
                                        battery: {
                                            type: 'string',
                                        },
                                        domain_id: {
                                            type: 'string',
                                        },
                                        device_timestamp: {
                                            type: 'string',
                                            format: 'date-time',
                                        },
                                        reccordindex: {
                                            type: 'string',
                                        },
                                        timetofix: {
                                            type: 'string',
                                        },
                                        longitude: {
                                            type: 'string',
                                        },
                                        status: {
                                            type: 'string',
                                        },
                                    },
                                },
                            },
                        }, 
                        then: true ,
                    },
                    { then: false },
                ],

                properties: {
                    identifier: {
                        type: 'string',
                        pattern: 'srett_device@124-[a-zA-Z0-9_]*',
                    },
                    domain_id: {
                        type: 'string',
                    },
                    type: {
                        type: 'string',
                    },
                    _type: {
                        type: 'string',
                    },
                    _score: {
                        type: 'number',
                    },
                    manufacturer: {
                        type: 'string',
                    },
                },
            },
        },
    },
};