export default {
    type: 'object',
    required: ['content'],
    properties: {
        links: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    // NOT NEEDED
                },
            },
        },
        content: {
            type: 'array',
            items: {
                type: 'object',
                required: ['timestamp', 'values'],
                properties: {
                    timestamp: {
                        type: 'string',
                    },
                    values: {
                        type: 'object',
                        required: ['battery', 'status', 'latitude', 'longitude'],
                        properties: {
                            status: {
                                type: 'string',
                            },
                            latitude: {
                                type: 'string',
                            },
                            longitude: {
                                type: 'string',
                            },
                            battery: {
                                type: 'string',
                            },
                        },
                    },
                },
            },
        },
    },
};