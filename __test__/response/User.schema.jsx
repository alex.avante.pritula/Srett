export default {
    type: 'object',
    required: [
        'active', 
        'login', 
        'lastName',
        'firstName',  
        'identifier', 
        'userDomain',
        'creationTimestamp', 
    ],
    properties: {
        active: {
            type: 'boolean',
        },
        creationTimestamp: {
            type: 'number',
        },
        firstName: {
            type: 'string',
        },
        lastName: {
            type: 'string',
        },
        login: {
            type: 'string',
        },
        identifier: {
            type: 'string',
        },
        userDomain: {
            type: 'object',
            properties: {
                admin: {
                    type: 'boolean',
                },
                domainId: {
                    type: 'string',
                },
                isDefault: {
                    type: 'boolean'
                },
                userId: {
                    type: 'string',
                },
            },
        },
        planets: {
            type: 'array',
            items: {
                type: 'object',
                properties: {

                },
            },
        }
    },
};
