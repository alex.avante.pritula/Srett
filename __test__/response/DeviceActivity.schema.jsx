export default {
    type: 'object',
    required: ['page', 'content'],
    properties: {
        links: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    // NOT NEEDED
                },
            },
        },
        page: {
            type: 'object',
            required: ['size', 'number', 'totalPages', 'totalElements'],
            properties: {
                size: {
                    type: 'number',
                },
                number: {
                    type: 'number',
                },
                totalPages: {
                    type: 'number',
                },
                totalElements: {
                    type: 'number',
                }
            },
        },
        content: {
            type: 'array',
            items: {
                type: 'object',
                required: ['identifier', 'device', 'creation_date', 'process_type'],
                properties: {
                    creation_date: {
                        type: 'string',
                    },
                    update_date: {
                        type: 'string',
                    },
                    device: {
                        type: 'string',
                    },
                    domain_id: {
                        type: 'string',
                    },
                    identifier: {
                        type: 'string',
                    },
                    process_type: {
                        type: 'string',
                    },
                    payload: {
                        type: 'string',
                    },
                    _type: {
                        type: 'string',
                    }
                },
            },
        },
    },
};


/* 
creation_date
"2018-03-26 16:52:41"
device
:
"srett_device@124-8c192d7000000007"
domain_id
:
"srett"
identifier
:
"orbitrack_iot@process-m23tssrd75"
payload
:
"{"movement_detection_flag":"pending","wake_up_type":"periodic","wake_up_period":"300","calendar_start_hour":"pending","calendar_start_minute":"pending","calendar_stop_hour":"16","calendar_stop_minute":"pending","calendar_nb_positions":"pending","mov_immobile_to_start":"pending","mov_immobile_to_fix":"pending","mov_time_between_fix":"pending","mov_data_rate":"pending","mov_max_measure":"pending","mov_threshold":"pending","gps_timeout":"pending","nb_miss":"pending"}"
process_type
:
"read_parameters"
status
:
"finished"
update_date
:
"2018-03-26 17:00:15"
_score
:
"NaN"
_type
:
"orbitrack_iot@process"

*/